<?php
get_header();
if (have_posts()) {
    the_post();
    echo do_shortcode('[magazine_page]');
}
get_footer();
