module.exports = {
  xs: '480px',
  sm: '768px',
  md: '980px',
  lg: '1100px',
  xl: '1400px'
};
