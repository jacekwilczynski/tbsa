const { Number: SassNumber } = require('node-sass').types;

module.exports = {
  minHeight: {
    default: new SassNumber(60, 'px')
  }
};
