const sass = require('./sassDimension');

const height = {
  full: sass(150, 'px'),
  sm: sass(80, 'px') // Not used by Sass files, only derived from Divi
};

const logo = {
  height: {
    full: sass(67, 'px'),
    sm: sass(50, 'px')
  },
  padding: {
    vertical: {
      full: () => height.full.subtract(logo.height.full).divide(sass(2)),
      sm: () => height.sm.subtract(logo.height.sm).divide(sass(2))
    }
  }
};

module.exports = {
  breakpoint: sass(1200, 'px'),
  height,
  logo
};
