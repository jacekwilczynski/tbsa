const { Number: SassNumber } = require('node-sass').types;

module.exports = {
  family: {
    primary: 'Apolonia, sans-serif',
    headings: 'Apolonia, serif'
  },
  size: {
    regular: {
      sm: new SassNumber(14, 'px'),
      base: new SassNumber(16, 'px'),
      lg: new SassNumber(20, 'px')
    },
    heading: {
      base: new SassNumber(36, 'px'),
      md: new SassNumber(30, 'px'),
      sm: new SassNumber(24, 'px'),
      xs: new SassNumber(18, 'px')
    }
  },
  lineHeight: {
    base: new SassNumber(1.5),
    basePx: theme =>
      new SassNumber(
        theme.font.size.regular.base.getValue() *
          theme.font.lineHeight.base.getValue(),
        'px'
      )
  }
};
