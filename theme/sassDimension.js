const { SassDimension } = require('node-sass-utils')(require('node-sass'));

module.exports = (...args) => new SassDimension(...args);
