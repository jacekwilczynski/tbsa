const sass = require('./sassDimension');

module.exports = {
  maxWidth: sass(1060, 'px'),
  width: sass(80, '%')
};
