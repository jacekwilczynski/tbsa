const { Color } = require('node-sass').types;

module.exports = {
  primary: {
    full: new Color(0xffd02a32),
    strong: new Color(0xffcf2931),
    dark: new Color(0xffc20b13),
    darker: new Color(0xffc00a12),
    dim: new Color(0xffa4262c)
  },
  dark: {
    full: new Color(0xff1d1e22),
    dim: new Color(0xff9e9e9e)
  },
  light: {
    full: new Color(0xffffffff)
  },
  gray: {
    superLight: new Color(0xfff6f6f6),
    veryLight: new Color(0xfff4f4f4),
    light: new Color(0xff9e9e9e)
  }
};
