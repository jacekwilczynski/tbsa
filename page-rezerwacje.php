<?php

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style(
        'bookingsPage-style',
        get_theme_file_uri('/build/bookingsPage-style.css'),
        null,
        WP_DEBUG ? microtime() : '1.0'
    );
});

add_action('body_class', function (array $classes) {
    $classes[] = 'bookings-page';

    return $classes;
});

get_header();
do_action('bookings_page');
get_footer();
