const path = require('path');
const theme = require('./theme');
const { castToSass } = require('node-sass-utils')(require('node-sass'));
const sass = require('./theme/sassDimension');

module.exports = require('./getWebpackConfig')({
  scripts: {
    'main-script': './js/main.js',
    'admin-script': './js/admin.js',
    'myAccount-script': './js/my-account.js',
    'slick': './js/sliders.js'
  },
  styles: {
    'main-style': './sass/main.scss',
    'admin-style': './sass/admin.scss',
    'eventSearch-style': './sass/event-search.scss',
    'myAccount-style': './sass/my-account.scss',
    'bookingsPage-style': './sass/bookings_deprecated.scss'
  },
  copy: [
    './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
    './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
    './node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
    './node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css'
  ],
  alias: {
    'sass-utils': path.resolve(__dirname, 'sass/~sass-utils'),
    'theme': path.resolve(__dirname, 'sass/~theme'),
    'assets': path.resolve(__dirname, 'assets'),
    '~js-utils': path.resolve(__dirname, 'js/~js-utils'),
    '~styles-loaded-from-js': path.resolve(__dirname, 'sass/~styles-loaded-from-js'),
    '~templates': path.resolve(__dirname, 'templates')
  },
  externals: {
    jquery: 'jQuery'
  },
  sassFunctions: {
    'rem($px)': function(px) {
      return sass(Number.parseFloat(px.getValue()) / 16, 'rem');
    },
    'vw($operator, $breakpoint)': function(operator, breakpoint) {
      const width = theme.breakpoint[breakpoint.getValue()];
      const prefix = operator.getValue() === 'max' ? 'not all and ' : '';
      return castToSass(`${prefix}(min-width: ${width})`);
    },
    'theme($keys)': function(keys) {
      let value = keys
        .getValue()
        .split('.')
        .reduce((res, key) => res[key], theme);
      if (typeof value === 'function') {
        value = value(theme);
      }
      return castToSass(value);
    }
  }
});
