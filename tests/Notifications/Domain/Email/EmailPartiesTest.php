<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

use PHPUnit\Framework\TestCase;

class EmailPartiesTest extends TestCase
{
    public function testMap(): void
    {
        $recipient = $this->prophesize(EmailAddress::class);
        $sender = $this->prophesize(EmailAddress::class);
        $replyTo = $this->prophesize(EmailAddress::class);
        $partiesBefore = new EmailParties(
            $recipient->reveal(),
            $sender->reveal(),
            $replyTo->reveal()
        );

        $recipient->__toString()->willReturn('recipient');
        $sender->__toString()->willReturn('sender');
        $replyTo->__toString()->willReturn('replyTo');

        $callback = new class implements StringFunction
        {
            public function execute(string $input): string
            {
                return strtoupper($input);
            }
        };

        $partiesAfter = $partiesBefore->map($callback);

        self::assertEquals('RECIPIENT', $partiesAfter->getRecipient());
        self::assertEquals('SENDER', $partiesAfter->getSender());
        self::assertEquals('REPLYTO', $partiesAfter->getReplyTo());
    }
}
