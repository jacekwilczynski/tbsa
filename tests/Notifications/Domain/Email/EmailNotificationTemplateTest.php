<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

use PHPUnit\Framework\TestCase;

class EmailNotificationTemplateTest extends TestCase
{
    public function testFill(): void
    {
        $template = new EmailNotificationTemplate(
            new EmailParties(
                EmailAddress::fromString('{recipient}'),
                EmailAddress::fromString('{sender}'),
                EmailAddress::fromString('{reply_to}')
            ),
            '{subject}',
            '{content}'
        );

        /** @var EmailNotification $notification */
        $notification = $template->fill([
            'recipient' => 'RECIPIENT',
            'sender'    => 'SENDER',
            'reply_to'  => 'REPLY_TO',
            'subject'   => 'SUBJECT',
            'content'   => 'CONTENT',
        ]);

        self::assertInstanceOf(EmailNotification::class, $notification);
        self::assertEquals('RECIPIENT', (string) $notification->getParties()->getRecipient());
        self::assertEquals('SENDER', (string) $notification->getParties()->getSender());
        self::assertEquals('REPLY_TO', (string) $notification->getParties()->getReplyTo());
        self::assertEquals('SUBJECT', $notification->getSubject());
        self::assertEquals('CONTENT', $notification->getContent());
    }
}
