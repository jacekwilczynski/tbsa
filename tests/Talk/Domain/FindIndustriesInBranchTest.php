<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use MobilitySoft\TBSA\Talk\Domain\User\Users;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class FindIndustriesInBranchTest extends TestCase
{
    /**
     * @var UserRepository|ObjectProphecy
     */
    private $userRepository;

    /**
     * @var FindIndustriesInBranch
     */
    private $findIndustriesInBranch;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->prophesize(UserRepository::class);
        $this->findIndustriesInBranch = new FindIndustriesInBranch($this->userRepository->reveal());
    }

    public function testReturnsEmptyIfBranchHasNoMembers(): void
    {
        $branchId = 234;
        $this->userRepository
            ->getBranchMembers($branchId)
            ->willReturn(new Users());

        self::assertEquals(
            new Industries(),
            $this->findIndustriesInBranch->execute($branchId)
        );
    }

    public function testReturnsEmptyIfMemberHasNoEntrepreneurProfile(): void
    {
        $branchId = 59713;
        $user = $this->prophesize(User::class);
        $this->userRepository->getBranchMembers($branchId)->willReturn(new Users($user->reveal()));
        $user->getEntrepreneur()->willReturn(null);

        self::assertEquals(
            new Industries(),
            $this->findIndustriesInBranch->execute($branchId)
        );
    }

    public function testMergesIndustriesFromMultipleMembers(): void
    {
        $branchId = 1;
        $people = [
            'Albert' => [
                'user'         => $this->prophesize(User::class),
                'entrepreneur' => $this->prophesize(Entrepreneur::class),
                'industries'   => ['Physics', 'Celebrity lifestyle'],
            ],
            'Maria'  => [
                'user'         => $this->prophesize(User::class),
                'entrepreneur' => $this->prophesize(Entrepreneur::class),
                'industries'   => ['Chemistry'],
            ],
            'Elon'   => [
                'user'         => $this->prophesize(User::class),
                'entrepreneur' => $this->prophesize(Entrepreneur::class),
                'industries'   => ['Engineering', 'Celebrity lifestyle'],
            ],
        ];
        $users = new Users(...array_values(array_map(function (array $person): User {
            return $person['user']->reveal();
        }, $people)));

        foreach ($people as $person) {
            $person['entrepreneur']->getIndustries()->willReturn(new Industries(...$person['industries']));
            $person['user']->getEntrepreneur()->willReturn($person['entrepreneur']->reveal());
        }
        $this->userRepository->getBranchMembers($branchId)->willReturn($users);

        self::assertEquals(
            new Industries('Physics', 'Celebrity lifestyle', 'Chemistry', 'Engineering'),
            $this->findIndustriesInBranch->execute($branchId)
        );
    }
}
