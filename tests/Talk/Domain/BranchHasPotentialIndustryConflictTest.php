<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branch;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branches;
use MobilitySoft\TBSA\Talk\Domain\Talk\Industry;
use MobilitySoft\TBSA\Talk\Domain\Talk\Talk;
use MobilitySoft\TBSA\Talk\Domain\Talk\TalkRepository;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class BranchHasPotentialIndustryConflictTest extends TestCase
{
    private const BRANCH_ID = 54;
    private const TALK_ID = 432;
    private const USER_ID = 4;

    /**
     * @var FindIndustriesInBranch|ObjectProphecy
     */
    private $findIndustriesInBranch;

    /**
     * @var TalkRepository|ObjectProphecy
     */
    private $talkRepository;

    /**
     * @var UserRepository|ObjectProphecy
     */
    private $userRepository;

    /**
     * @var BranchHasPotentialIndustryConflict
     */
    private $subject;

    /**
     * @var Talk|ObjectProphecy
     */
    private $talk;

    /**
     * @var User|ObjectProphecy
     */
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->findIndustriesInBranch = $this->prophesize(FindIndustriesInBranch::class);
        $this->talkRepository = $this->prophesize(TalkRepository::class);
        $this->userRepository = $this->prophesize(UserRepository::class);
        $this->subject = new BranchHasPotentialIndustryConflict(
            $this->findIndustriesInBranch->reveal(),
            $this->talkRepository->reveal(),
            $this->userRepository->reveal()
        );
        $this->talk = $this->prophesize(Talk::class);
        $this->user = $this->prophesize(User::class);

        $this->talkRepository
            ->getById(self::TALK_ID)
            ->willReturn($this->talk->reveal());
        $this->userRepository
            ->getById(self::USER_ID)
            ->willReturn($this->user);
        $this->user
            ->getMemberBranches()
            ->willReturn(new Branches());
    }

    public function testNoConflictIfNoIndustriesInBranch(): void
    {
        $this->talk
            ->getIndustry()
            ->willReturn(new Industry(2, 'Something'));
        $this->findIndustriesInBranch
            ->execute(self::BRANCH_ID)
            ->willReturn(new Industries());

        self::assertFalse(
            $this->executeSubject(),
            'Detected industry conflict where there was none.'
        );
    }

    public function testNoConflictIfNoIndustryInTalk(): void
    {
        $this->talk
            ->getIndustry()
            ->willReturn(null);
        $this->findIndustriesInBranch
            ->execute(self::BRANCH_ID)
            ->willReturn(new Industries('a', 'b'));

        self::assertFalse(
            $this->executeSubject(),
            'Detected industry conflict where there was none.'
        );
    }

    public function testNoConflictIfDifferentIndustries(): void
    {
        $this->talk
            ->getIndustry()
            ->willReturn(new Industry(1, 'Real estate'));
        $this->findIndustriesInBranch
            ->execute(self::BRANCH_ID)
            ->willReturn(new Industries('Accounting', 'Security'));

        self::assertFalse(
            $this->executeSubject(),
            'Detected industry conflict where there was none.'
        );
    }

    public function testConflictIfIndustryMatches(): void
    {
        $matchingIndustry = 'Carpentry';
        $branchIndustries = new Industries(
            'Medicine',
            'Computer science',
            $matchingIndustry,
            'Fashion'
        );

        $this->talk
            ->getIndustry()
            ->willReturn(new Industry(1, $matchingIndustry));
        $this->findIndustriesInBranch
            ->execute(self::BRANCH_ID)
            ->willReturn($branchIndustries);

        self::assertTrue(
            $this->executeSubject(),
            "Didn't detect an industry conflict."
        );
    }

    public function testNoConflictIfSubmittingToOnesOwnBranch(): void
    {
        $branch = $this->prophesize(Branch::class);
        $matchingIndustry = 'Carpentry';
        $branchIndustries = new Industries(
            'Medicine',
            'Computer science',
            $matchingIndustry,
            'Fashion'
        );

        $branch
            ->getId()
            ->willReturn(self::BRANCH_ID);
        $this->user
            ->getMemberBranches()
            ->willReturn(new Branches($branch->reveal()));
        $this->talk
            ->getIndustry()
            ->willReturn(new Industry(1, $matchingIndustry));
        $this->findIndustriesInBranch
            ->execute(self::BRANCH_ID)
            ->willReturn($branchIndustries);

        self::assertFalse(
            $this->executeSubject(),
            'Detected industry conflict where there was none ' .
            'because the submitter is a member of the branch.'
        );
    }

    private function executeSubject(): bool
    {
        return $this->subject->execute(
            self::TALK_ID,
            self::BRANCH_ID,
            self::USER_ID
        );
    }
}
