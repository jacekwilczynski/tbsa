<?php

require_once __DIR__ . '/../vendor/autoload.php';

if (!class_exists('WC_DateTime')) {
    class WC_DateTime extends DateTime
    {
    }
}

if (!class_exists('WC_Order')) {
    interface WC_Order {
        public function get_id(): int;
        public function get_billing_first_name(): string;
        public function get_billing_last_name(): string;
        public function get_billing_email(): string;
        public function get_billing_phone(): string;
        public function get_date_created(): WC_DateTime;
    }
}

if (!class_exists('WC_Order_Item_Product')) {
    interface WC_Order_Item_Product
    {
        public function get_meta(string $key): string;
    }
}
