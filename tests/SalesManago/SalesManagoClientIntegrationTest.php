<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago;

use GuzzleHttp\Client;
use MobilitySoft\TBSA\SalesManago\Client\Contact;
use MobilitySoft\TBSA\SalesManago\Client\SalesManagoClient;
use MobilitySoft\TBSA\SalesManago\IdentificationDataProvider\SalesManagoIdentificationDataProvider;
use PHPUnit\Framework\TestCase;

class SalesManagoClientIntegrationTest extends TestCase
{
    /**
     * @var SalesManagoClient
     */
    private $client;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        require_once __DIR__ . '/../../../../../../env.test.php';
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = new SalesManagoClient(
            new Client(),
            (new SalesManagoIdentificationDataProvider())->getData()
        );
    }

    public function testCanUpsertContact(): void
    {
        $this->expectNotToPerformAssertions();
        $this->client->upsertContact(
            (new Contact('test@vkj9385w7hf8ah2w3d8932r35y34ydfk6843w.com'))
                ->setPhone('123456789')
                ->setName('Test Integracji')
                ->addTags('TB_TEST')
                ->setProperty('ostatnio_uruchomiono', date('Y-m-d H:i:s'))
        );
    }
}
