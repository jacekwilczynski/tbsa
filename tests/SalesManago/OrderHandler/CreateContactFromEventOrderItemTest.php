<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\OrderHandler;

use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\SalesManago\Client\Contact;
use MobilitySoft\TBSA\SalesManago\TagFormatter;
use MobilitySoft\TBSA\ValueObject\BranchLocation;
use MobilitySoft\TBSA\ValueObject\Timespan;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

class CreateContactFromEventOrderItemTest extends TestCase
{
    private const EVENT_ID_ORDER_META_KEY = '_tbsa_eventId';
    private const EVENT_ID = 5384;
    private const EMAIL = 'john.smith@abc.com';
    private const FIRST_NAME = 'John';
    private const LAST_NAME = 'Smith';
    private const FULL_NAME = 'John Smith';
    private const PHONE = '+48123456789';
    private const ORDER_ID = 76333;
    private const ORDER_DATE = '2019-12-31';
    private const EVENT_DATE = '2020-01-19';
    private const CITY = 'Poznań';
    private const VOIVODESHIP = 'wielkopolskie';
    private const BASE_TAG = 'TB_GOSCIE';
    private const PARTIAL_CITY_TAG = 'POZNAN';
    private const COMPLETE_CITY_TAG = 'TB_GOSCIE_POZNAN';
    private const CITY_KEY = 'miasto';
    private const ORDER_DATE_KEY = 'data_zamowienia';
    private const EVENT_DATE_KEY = 'data_wydarzenia';
    private const INDEX = 5;

    /**
     * @var EventRepositoryInterface|ObjectProphecy
     */
    private $eventRepositoryProphecy;

    /**
     * @var CreateContactFromEventOrder
     */
    private $createContact;

    /**
     * @var \WC_Order
     */
    private $order;

    /**
     * @var \WC_Order_Item_Product
     */
    private $item;

    protected function setUp(): void
    {
        parent::setUp();
        $this->eventRepositoryProphecy = $this->prophesize(EventRepositoryInterface::class);
        $this->createContact = new CreateContactFromEventOrder(
            $this->eventRepositoryProphecy->reveal(),
            $this->createTagFormatter()
        );
        $this->order = $this->createOrder();
        $this->item = $this->createItem();
    }

    public function testCreatesContact(): void
    {
        $location = new BranchLocation(self::CITY, self::VOIVODESHIP);
        $branch = $this->createBranch($location);
        $this->eventRepositoryProphecy
            ->getById(self::EVENT_ID)
            ->willReturn($this->createEvent($branch));

        $generatedContact = $this->createContact->execute($this->order, $this->item, self::INDEX);
        self::assertEquals($this->fullContact(), $generatedContact);
    }

    public function testFailsIfEventDoesNotExist(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Event ' . self::EVENT_ID . ' not found.');
        $this->createContact->execute($this->order, $this->item, self::INDEX);
    }

    public function testDoesNotCrashIfEventHasNoBranch(): void
    {
        $this->eventRepositoryProphecy
            ->getById(self::EVENT_ID)
            ->willReturn($this->createEvent(null));
        $generatedContact = $this->createContact->execute($this->order, $this->item, self::INDEX);
        self::assertEquals($this->contactWithoutCity(), $generatedContact);
    }

    public function testDoesNotCrashIfBranchHasNoLocation(): void
    {
        $branch = $this->createBranch(null);
        $event = $this->createEvent($branch);
        $this->eventRepositoryProphecy
            ->getById(self::EVENT_ID)
            ->willReturn($event);
        $generatedContact = $this->createContact->execute($this->order, $this->item, self::INDEX);
        self::assertEquals($this->contactWithoutCity(), $generatedContact);
    }

    private function createOrder(): \WC_Order
    {
        $orderProphecy = $this->prophesize(\WC_Order::class);
        $orderProphecy->get_id()->willReturn(self::ORDER_ID);
        $orderProphecy->get_billing_email()->willReturn(self::EMAIL);
        $orderProphecy->get_billing_first_name()->willReturn(self::FIRST_NAME);
        $orderProphecy->get_billing_last_name()->willReturn(self::LAST_NAME);
        $orderProphecy->get_billing_phone()->willReturn(self::PHONE);
        $orderProphecy->get_date_created()->willReturn(new \WC_DateTime(self::ORDER_DATE));
        return $orderProphecy->reveal();
    }

    private function createItem(): \WC_Order_Item_Product
    {
        $itemProphecy = $this->prophesize(\WC_Order_Item_Product::class);
        $itemProphecy
            ->get_meta(self::EVENT_ID_ORDER_META_KEY)
            ->willReturn((string) self::EVENT_ID);
        return $itemProphecy->reveal();
    }

    private function createEvent(?BranchInterface $branch): EventInterface
    {
        $eventProphecy = $this->prophesize(EventInterface::class);
        $timespan = new Timespan(false, new \DateTimeImmutable(self::EVENT_DATE));
        $eventProphecy->getTimespan()->willReturn($timespan);
        $eventProphecy->getBranch()->willReturn($branch);
        return $eventProphecy->reveal();
    }

    private function createBranch(?BranchLocation $location): BranchInterface
    {
        $branchProphecy = $this->prophesize(BranchInterface::class);
        $branchProphecy->getLocation()->willReturn($location);
        return $branchProphecy->reveal();
    }

    private function createTagFormatter(): TagFormatter
    {
        $tagFormatterProphecy = $this->prophesize(TagFormatter::class);
        $tagFormatterProphecy->format(self::CITY)->willReturn(self::PARTIAL_CITY_TAG);
        return $tagFormatterProphecy->reveal();
    }

    private function fullContact(): Contact
    {
        return (new Contact(self::EMAIL))
            ->setPhone(self::PHONE)
            ->setName(self::FULL_NAME)
            ->addTags(self::BASE_TAG, self::COMPLETE_CITY_TAG)
            ->setProperty(self::CITY_KEY, self::CITY, self::INDEX)
            ->setProperty(self::ORDER_DATE_KEY, self::ORDER_DATE, self::INDEX)
            ->setProperty(self::EVENT_DATE_KEY, self::EVENT_DATE, self::INDEX);
    }

    private function contactWithoutCity(): Contact
    {
        return (new Contact(self::EMAIL))
            ->setPhone(self::PHONE)
            ->setName(self::FULL_NAME)
            ->addTags(self::BASE_TAG)
            ->setProperty(self::ORDER_DATE_KEY, self::ORDER_DATE, self::INDEX)
            ->setProperty(self::EVENT_DATE_KEY, self::EVENT_DATE, self::INDEX);
    }
}
