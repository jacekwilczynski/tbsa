<?php

namespace MobilitySoft\TBSA\Utils;

use PHPUnit\Framework\TestCase;

class ReplacePlaceholdersTest extends TestCase
{
    /**
     * @var ReplacePlaceholders
     */
    private $instance;

    protected function setUp(): void
    {
        parent::setUp();
        $this->instance = new ReplacePlaceholders();
    }

    public function testDoesNotChangeContentWithoutPlaceholders(): void
    {
        self::assertEquals(
            'ab cd 345',
            $this->instance->execute(
                'ab cd 345',
                ['key' => 'value']
            )
        );
    }

    public function testReplacesSinglePlaceholder(): void
    {
        $template = 'Hello, {name}!';

        self::assertEquals(
            'Hello, John!',
            $this->instance->execute($template, ['name' => 'John'])
        );

        self::assertEquals(
            'Hello, Ann!',
            $this->instance->execute($template, ['name' => 'Ann'])
        );
    }

    public function testReplacesTwoPlacesholders(): void
    {
        $template = 'Hi, my name is {name}, and I am {age} years old.';

        self::assertEquals(
            'Hi, my name is Michael, and I am 34 years old.',
            $this->instance->execute($template, [
                'name' => 'Michael',
                'age'  => 34,
            ])
        );
    }

    public function testDoesNotAffectUndefinedPlaceholders(): void
    {
        $template = 'There is no value for {abc} in the supplied data.';

        self::assertEquals(
            $template,
            $this->instance->execute($template, [
                'def' => 'this should not appear anywhere',
            ])
        );
    }

    public function testTreatsNullAsEmptyString(): void
    {
        $template = 'This {placeholder} has a null but nonetheless defined value.';

        self::assertEquals(
            'This  has a null but nonetheless defined value.',
            $this->instance->execute($template, [
                'placeholder' => null,
            ])
        );
    }
}
