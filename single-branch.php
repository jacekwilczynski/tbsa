<?php
get_header();
if (have_posts()) {
    the_post();
    echo do_shortcode('[branch_page]');
}
get_footer();
