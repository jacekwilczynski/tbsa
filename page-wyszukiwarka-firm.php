<?php
get_header();
if (have_posts()) {
    the_post();
    echo do_shortcode('[business_search_page]');
}
get_footer();
