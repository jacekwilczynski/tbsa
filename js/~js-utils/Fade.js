export default function Fade({ element, fadingClass, transitionDuration }) {
  return function fade(action, promise = Promise.resolve()) {
    element.classList.add(fadingClass);

    if (transitionDuration) {
      setTimeout(complete, transitionDuration);
    } else {
      element.addEventListener('transitionend', complete);
    }

    function complete() {
      promise
        .then(result => action(result))
        .then(function() {
          element.classList.remove(fadingClass);
          element.removeEventListener('transitionend', complete);
        });
    }
  };
}
