import throttle from 'lodash.throttle';

class AutoStyler {
  constructor(callback) {
    this.callback = callback;
  }

  enable({ refreshInterval = 1000 } = {}) {
    document.addEventListener('DOMContentLoaded', this.callback);
    window.addEventListener('load', this.callback);
    window.addEventListener('resize', throttle(this.callback, refreshInterval));
  }
}

export default AutoStyler;
