/**
 * @param {function(*)} condition
 * @return {function(*): Array[]}
 */
function SplitArray(condition) {
  return function splitArray(array) {
    const truthy = [];
    const falsy = [];
    for (let i = 0; i < array.length; i++) {
      if (condition(array[i])) {
        truthy.push(array[i]);
      } else {
        falsy.push(array[i]);
      }
    }
    return [truthy, falsy];
  };
}

export default SplitArray;
