export default class ShitCrypt {
  static decrypt(data, key) {
    return data
      .split('-')
      .map((code, index) =>
        String.fromCharCode(Number.parseInt(code, 16) - key - index)
      )
      .join('');
  }
}
