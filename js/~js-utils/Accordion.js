import forceTransition from './forceTransition';

export default function Accordion({
  blockSelector,
  activeClass,
  groupSelector,
  handleSelector,
  contentSelector,
  transitionDuration
}) {
  let groups = new WeakSet();

  function onGroupClick() {
    const clickedGroup = this;

    if (clickedGroup.classList.contains(activeClass)) {
      setGroupStatus(clickedGroup, false);
      return;
    }

    const allGroups = clickedGroup.parentNode.querySelectorAll(groupSelector);
    Array.prototype.forEach.call(allGroups, function(group) {
      setGroupStatus(group, group === clickedGroup);
    });
  }

  function setGroupStatus(group, active) {
    forceTransition({
      property: 'height',
      element: group.querySelector(contentSelector),
      action: () => group.classList.toggle(activeClass, active),
      transitionDuration
    });
  }

  return {
    activate() {
      Array.prototype.forEach.call(
        document.querySelectorAll(
          [...(blockSelector ? [blockSelector] : []), groupSelector].join(' ')
        ),
        function(group) {
          if (!groups.has(group)) {
            group
              .querySelector(handleSelector)
              .addEventListener('click', onGroupClick.bind(group));
            groups.add(group);
          }
        }
      );
    }
  };
}
