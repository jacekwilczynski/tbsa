import $ from 'jquery';

export default function forceTransition({
  property,
  element,
  action,
  transitionDuration
}) {
  element.style[property] = '';
  element.style.transition = 'none';
  const valueBefore = getComputedStyle(element)[property];
  action();
  const valueAfter = getComputedStyle(element)[property];

  element.style[property] = valueBefore;

  setTimeout(function() {
    $(element).animate({ [property]: valueAfter }, transitionDuration, () => {
      element.style.transition = '';
      element.style[property] = '';
    });
  });
}
