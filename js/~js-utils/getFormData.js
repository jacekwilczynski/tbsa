export default function getFormData($form) {
  return $form
    .serializeArray()
    .filter(field => field.value !== '')
    .reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;
    }, {});
}
