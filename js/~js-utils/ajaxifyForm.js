import $ from 'jquery';
import renderSpinner from '~templates/lds-spinner.twig';
import getFormData from './getFormData';

export default function ajaxifyForm($form, { onSuccess } = {}) {
  $form.on('submit', function(event) {
    const $spinner = $(renderSpinner());
    $('[type=submit]').append($spinner);
    event.preventDefault();
    const data = getFormData($form);
    $.ajax({
      url: $form.attr('action'),
      method: $form.attr('method'),
      data,
      success(...args) {
        $spinner.remove();
        if (onSuccess) {
          onSuccess(...args);
        }
      }
    });
  });
}
