/**
 * @param {Date} date
 * @returns {string}
 */
export const formatTime = date =>
  date.toLocaleTimeString(undefined, {
    hour12: false,
    hour: '2-digit',
    minute: '2-digit'
  });

/**
 * @param {Date} date
 * @returns {string}
 */
export const formatDate = date =>
  [
    date.getFullYear(),
    String(date.getMonth() + 1).padStart(2, '0'),
    String(date.getDate()).padStart(2, '0')
  ].join('-');
