import $ from 'jquery';
import ajaxifyForm from '~js-utils/ajaxifyForm';

const $form = $('.js-submission-comment-form');
if ($form.length > 0) {
  ajaxifyForm($form, { onSuccess: () => location.reload() });
}
