import $ from 'jquery';
import configureDatePicker from './configureDatePicker';
import preloadSubmitTalkPopup from './preloadSubmitTalkPopup';
import SetDateToNearest from './SetDateToNearest';

export default function configureDatesUpdates({ $branchPicker, $datePicker }) {
  let datesByBranchId = getDatesFromLocalStorage();
  const getDates = () => datesByBranchId[$branchPicker.val()] || {};

  $(window).on('submitTalkPopup/loadedData', update);

  configureDatePicker({ $datePicker, getDates });

  $datePicker.datepicker('setDate', tbsa_submitTalk.initialDate);

  const setDateToNearest = SetDateToNearest({ $datePicker, getDates });

  function update() {
    datesByBranchId = getDatesFromLocalStorage();
    $datePicker.datepicker('refresh');
  }
  $branchPicker.on('change', () => setDateToNearest($datePicker.val()));

  $(window).on(
    'submitTalkPopup/talkSubmission/done',
    preloadSubmitTalkPopup.data
  );
}

function getDatesFromLocalStorage() {
  const data = JSON.parse(localStorage.getItem('submitTalkPopup/data'));
  return (data && data.datesByBranchId) || {};
}
