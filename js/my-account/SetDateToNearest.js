import { formatDate } from '~js-utils/time';

export default function SetDateToNearest({ $datePicker, getDates }) {
  return function setDateToNearest(reference) {
    const nearestDate = findNearestDateString({
      dates: getDates(),
      reference: reference
    });

    if (nearestDate) {
      $datePicker.datepicker('setDate', nearestDate);
    } else {
      $datePicker.val('');
    }
  };
}

function findNearestDateString({ dates, reference = formatDate(new Date()) }) {
  if (dates[reference]) {
    return reference;
  }

  const dateStrings = [...Object.keys(dates), reference];
  dateStrings.sort();

  const referenceIndex = dateStrings.indexOf(reference);
  const next = dateStrings[referenceIndex + 1];
  const previous = dateStrings[referenceIndex - 1];

  if (next && !previous) {
    return next;
  }
  if (previous && !next) {
    return previous;
  }
  if (next && previous) {
    const referenceTimestamp = Date.parse(reference);
    const previousTimestamp = Date.parse(previous);
    const nextTimestamp = Date.parse(next);

    const previousIsCloserThanNext =
      referenceTimestamp - previousTimestamp <
      nextTimestamp - referenceTimestamp;

    return previousIsCloserThanNext ? previous : next;
  }

  return null;
}
