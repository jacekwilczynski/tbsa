import $ from 'jquery';
import activateEntrepreneurLoader from './editTalk/activateEntrepreneurLoader';
import activateResetButtons from './editTalk/activateResetButtons';
import enableGrabbingFieldFocus from './editTalk/enableGrabbingFieldFocus';
import './editTalk/activateAutoSubmit';
import './editTalk/disableNameFieldForRegularUsers';

const $form = $('#tbsa_talk-form');
enableGrabbingFieldFocus($form);
activateResetButtons($form);
activateEntrepreneurLoader($form);
