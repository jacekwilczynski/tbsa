import $ from 'jquery';
import getFormData from '~js-utils/getFormData';

export default function EventListUpdater({
  $form,
  $resultsContainer,
  beforeWait = () => {},
  afterWait = () => {}
}) {
  let talkId;
  const { getEvents, hasCached } = EventRepository();
  const initialResultsContainerHtml = $resultsContainer.html();

  return {
    setTalkId(newTalkId) {
      talkId = newTalkId;
    },
    update() {
      const params = { ...getFormData($form), talk_id: talkId };
      const hasValidParams = params.branch && params.date;
      const willTakeTime = hasValidParams && !hasCached(params);
      let heightBefore;

      if (willTakeTime) {
        beforeWait();
      }

      if (hasValidParams) {
        $resultsContainer.css({ opacity: 0 });
      }

      if (willTakeTime || !hasValidParams) {
        $resultsContainer.html(initialResultsContainerHtml);
      }

      heightBefore = $resultsContainer.height();
      if (heightBefore) $resultsContainer.height(heightBefore);

      getEvents(params)
        .then(html => $resultsContainer.html(html))
        .fail(() =>
          $resultsContainer.html(`<p>${tbsa_submitTalk.fetchErrorMessage}</p>`)
        )
        .done(() => {
          if (willTakeTime) afterWait();
          if (heightBefore) {
            $resultsContainer.height('');
            const heightAfter = $resultsContainer.height();
            $resultsContainer.height(heightBefore);
            $resultsContainer.animate(
              { height: heightAfter, opacity: 1 },
              function() {
                $resultsContainer.height('');
              }
            );
          }
          $(window).trigger('submitTalkPopup/eventsUpdated');
        });
    }
  };
}

function EventRepository() {
  let cache = {};

  $(window).on('submitTalkPopup/talkSubmission/done', clearCache);
  $(window).on('submitTalkPopup/opened', clearCache);

  function hasCached(params) {
    return cache[JSON.stringify(params)];
  }

  function getEvents(params) {
    const serializedParams = JSON.stringify(params);
    if (cache[serializedParams]) {
      return $.when(cache[serializedParams]);
    } else {
      return $.get(tbsa_submitTalk.eventsAjaxUrl, params).then(html => {
        cache[serializedParams] = html;
        return html;
      });
    }
  }

  function clearCache() {
    cache = {};
  }

  return { getEvents, hasCached };
}
