import $ from 'jquery';
import actions from './talksList/actions';
import './talksList/react';

$(window).on('submitTalkPopup/loadedHtml', function() {
  $(document.body).addClass('talksTableActions-ready');
});

$(window).on('click', function(event) {
  const $trigger = $(event.target);
  if (!$trigger.is('[data-talk-action]')) {
    return;
  }
  const action = actions[$trigger.attr('data-talk-action')];
  const $talkContainer = $trigger.closest('[data-talk]');
  const talk = JSON.parse($talkContainer.attr('data-talk'));
  action({ talk, $trigger, $talkContainer });
});
