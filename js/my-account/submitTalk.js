import $ from 'jquery';

export default function submitTalk(templateId, eventId) {
  $(window).trigger('submitTalkPopup/talkSubmission/started', { eventId });

  return $.ajax({
    url: tbsa_talk.rest.createSubmissionFromTemplate.url,
    method: 'POST',
    headers: { 'X-WP-NONCE': tbsa_talk.rest.createSubmissionFromTemplate.nonce },
    data: JSON.stringify({
      talkTemplate: templateId,
      event: eventId
    }),
    success: function({ notification } = {}) {
      $(window).trigger('submitTalkPopup/talkSubmission/done', {
        status: 'submitted',
        eventId,
        notification
      });
    },
    error: function(response) {
      $(window).trigger('submitTalkPopup/talkSubmission/done', {
        error: true,
        notification: JSON.parse(response.responseText)
      });
    }
  });
}

