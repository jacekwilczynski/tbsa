import $ from 'jquery';
import throttle from 'lodash.throttle';
import configureDatesUpdates from './configureDatesUpdates';
import EventListUpdater from './EventListUpdater';
import preloadSubmitTalkPopup from './preloadSubmitTalkPopup';
import submitTalk from './submitTalk';

if (window.tbsa_submitTalkPopup) {
  preloadSubmitTalkPopup();
}

let eventListUpdater;

$(window).on(
  'submitTalkPopup/inserted',
  function() {
    const $popup = $('#submitTalkPopup');
    const $form = $popup.find('.js-eventSearchForm');
    const $branchPicker = $form.find('select[name=branch]');
    const $datePicker = $form.find('input[name=date]');
    const $resultsContainer = $('#submitTalkPopup__events');

    configureDatesUpdates({ $branchPicker, $datePicker });

    eventListUpdater = EventListUpdater({
      $form,
      $resultsContainer,
      beforeWait: () => $popup.addClass('submitTalkPopup--loading'),
      afterWait: () => $popup.removeClass('submitTalkPopup--loading')
    });
    $form.on('change', throttle(eventListUpdater.update, 1000));
  }
);

$(window).on(
  'submitTalkPopup/opened',
  function(_, { talk }) {
    eventListUpdater.setTalkId(talk.id);

    addListenerToSubmitTalkButtons();
    $(window).on('submitTalkPopup/eventsUpdated', addListenerToSubmitTalkButtons);

    function addListenerToSubmitTalkButtons() {
      $('[data-submit-talk]').on('click', function() {
        const eventId = Number($(this).data('event-id'));
        submitTalk(talk.id, eventId);
      });
    }
  }
);


$(window).on(
  'submitTalkPopup/talkSubmission/started',
  function(_, { eventId }) {
    const $submitButton = $(`[data-submit-talk][data-event-id=${eventId}]`);
    const $eventWrapper = $submitButton.closest('.submitTalkEvent');
    $submitButton.replaceWith(
      `<p class="js-submission-status" data-event-id="${eventId}">${tbsa_submitTalk.submissionStatuses.sending}</p>`
    );

    const $warning = $eventWrapper.find('.submitTalkEventWarning');
    $warning.remove();
  }
);

$(window).on(
  'submitTalkPopup/talkSubmission/done',
  function(_, { eventId, error, notification }) {
    if (notification) {
      $.fancybox.open(notification);
    }

    if (!error) {
      $(`.js-submission-status[data-event-id=${eventId}]`).html(tbsa_submitTalk.submissionStatuses.success);
      $(document).on('beforeClose.fb', function(event) {
        if (
          event.target.querySelector('#submitTalkPopup') &&
          document.querySelector('.talksTable')
        ) {
          window.location.reload();
        }
      });
    }
  }
);

