import { formatDate } from '~js-utils/time';

/**
 * @param {jquery} $datePicker
 * @param {function} getDates
 * @param {object} options
 */
export default function configureDatePicker({
  $datePicker,
  getDates,
  options = {}
}) {
  $datePicker.datepicker('option', {
    minDate: 0,
    /**
     * @param {Date} date
     */
    beforeShowDay(date) {
      const dates = getDates();
      const status = getDateAvailability(dates, formatDate(date));
      const available = isAvaiable(status);
      return [
        available,
        [
          'date',
          'date--' + status,
          'date--' + (available ? 'available' : 'unavailable')
        ].join(' ')
      ];
    },
    ...options
  });
  $datePicker.datepicker('setDate', new Date());
}

/**
 * @param {{string: number}} dates
 * @param {string} date
 * @returns {string}
 */
function getDateAvailability(dates = {}, date) {
  return dates[date] ? dates[date] : 'empty';
}

/**
 * @param {string} availability
 * @returns {boolean}
 */
function isAvaiable(availability) {
  return availability === 'free' || availability === 'in_review';
}
