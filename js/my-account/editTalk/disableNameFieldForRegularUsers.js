const INPUT_ID = 'acf-field_5d57c69268a45-0-field_5d57c6a768a46';

document.addEventListener('DOMContentLoaded', function() {
  const nameInput = document.getElementById(INPUT_ID);
  if (nameInput && window.tbsa_talk && window.tbsa_talk.userLevel === 'regular') {
    nameInput.disabled = true;
  }
});
