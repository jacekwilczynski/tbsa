import openSubmitTalkPopup from '../openSubmitTalkPopup';
import $ from 'jquery';

const SESSION_STORAGE_KEY = 'tbsa_auto_submit_talk_id';

openPopupIfDesired();
setButtonListener();

function setButtonListener() {
  document.addEventListener('DOMContentLoaded', function() {
    Array.prototype.forEach.call(
      document.getElementsByClassName('js-auto-submit-talk'),
      function(button) {
        button.addEventListener('click', function() {
          sessionStorage.setItem(SESSION_STORAGE_KEY, window.tbsa_talk.id || 'new');
        });
      }
    );
  });
}

function openPopupIfDesired() {
  const autoSubmitTalkId = sessionStorage.getItem(SESSION_STORAGE_KEY);
  sessionStorage.removeItem(SESSION_STORAGE_KEY);
  if (autoSubmitTalkId) {
    getTemplateId(autoSubmitTalkId).then(templateId => {
      openSubmitTalkPopup({ id: templateId });
    });
  }
}

function getTemplateId(idFromStorage) {
  if (idFromStorage === 'new') {
    const { url, method } = window.tbsa_talk.rest.getNewestTalkTemplateId;
    return $.ajax({ url, method });
  }
  return Promise.resolve(Number(idFromStorage));
}
