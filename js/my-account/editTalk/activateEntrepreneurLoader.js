import $ from 'jquery';

const endpoint = window.tbsa_talk.rest.getEntrepreneurFields;

export default function activateEntrepreneurLoader($form) {
  $form.on('change', function(event) {
    const $eventTarget = $(event.target);
    if (!$eventTarget.is('.acf-field[data-name=entrepreneur] select')) {
      return;
    }
    const $row = $eventTarget.closest('.acf-field[data-name=speakers] .acf-row');
    const $photoField = $row.find('.acf-field[data-name=photo]');
    const $photoUploader = $photoField.find('.acf-image-uploader');
    const $photoImg = $photoField.find('img');
    const entrepreneurId = $(event.target).val();
    $.ajax({
      url: endpoint.url + '?entrepreneur_id=' + entrepreneurId,
      method: endpoint.method,
      headers: { 'X-WP-NONCE': endpoint.nonce }
    })
      .then(({ fields, photoUrl }) => {
        setAcfFields(fields, $row);
        $photoImg.attr('src', photoUrl);
        $photoUploader.toggleClass('has-value', Boolean(photoUrl));
      });
  });
}

function setAcfFields(fields, $wrapper) {
  Object.keys(fields).forEach(key => {
    const $field = acf.findField(key, $wrapper);
    if ($field.is('[data-name=entrepreneur]')) {
      return;
    }
    acf.getField($field).val(fields[key]);
  });
}
