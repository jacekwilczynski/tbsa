export default function activateResetButtons($form) {
  $form.find('input[type=reset]').on('click', function(event) {
    event.preventDefault();
    $form
      .find('.acf-field[data-type=image] a[data-name=remove]')
      .trigger('click');
    $form
      .find(
        'select,' +
        'textarea,' +
        'input:not([type=button]):not([type=submit]):not([type=reset]):not([type=hidden])'
      )
      .val('')
      .trigger('change');
    $form.find('input[type=checkbox]').prop('checked', false);
    clearTinyMces();
  });
}

function clearTinyMces() {
  let i = 0;
  let editor;
  do {
    editor = tinyMCE.get(i);
    if (editor) {
      editor.setContent('');
      i++;
    }
  } while (editor);
}
