import $ from 'jquery';

export default function enableGrabbingFieldFocus($form) {
  $form
    .find('.acf-field .acf-field')
    .on('click', function(event) {
      if (!$(event.target).is('.acf-repeater input')) {
        $(this)
          .find('input:not([type=hidden]), select, textarea')
          .first()
          .trigger('focus');
      }
    });
}
