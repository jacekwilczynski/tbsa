import $ from 'jquery';

export default function preloadSubmitTalkPopup() {
  preloadSubmitTalkPopup.html();
  preloadSubmitTalkPopup.data();
}

preloadSubmitTalkPopup.html = function() {
  $.ajax({
    url: tbsa_submitTalkPopup.htmlUrl,
    dataType: 'html'
  }).then(html => {
    localStorage.setItem('submitTalkPopup/html', html);
    $(window).trigger('submitTalkPopup/loadedHtml');
  });
};

preloadSubmitTalkPopup.data = function() {
  $.ajax({
    url: tbsa_submitTalkPopup.dataUrl,
    dataType: 'text'
  }).then(data => {
    localStorage.setItem('submitTalkPopup/data', data);
    $(window).trigger('submitTalkPopup/loadedData');
  });
};
