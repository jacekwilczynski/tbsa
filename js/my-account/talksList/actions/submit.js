import openSubmitTalkPopup from '../../openSubmitTalkPopup';

export default function({ talk }) {
  openSubmitTalkPopup(talk);
}
