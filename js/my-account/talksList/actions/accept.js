import $ from 'jquery';
import renderColoredStatus from '~templates/myAccount/coloredStatus.twig';

export default function({ talk, $talkContainer }) {
  const $button = $talkContainer.find('[data-talk-action=accept]');
  const $buttonBackupCopy = $button.clone(true);
  $button
    .off()
    .addClass('talksTableActions__action--disabled')
    .text('Akceptuję...');
  const { url, method, nonce } = tbsa_talk.rest.acceptSubmission;
  $.ajax({
    url: `${url}/${talk.id}`,
    method,
    headers: { 'X-WP-NONCE': nonce },
    success() {
      $talkContainer.find('.js-coloredStatus').replaceWith(renderColoredStatus({
        status: { raw: 'accepted', formatted: 'Zaakceptowany' }
      }));
      const talkSubject = $talkContainer.find('.js-submission-talk-subject').text();
      $talkContainer.find('.js-submission-event-title').text(talkSubject);
    },
    error(jqXHR) {
      $button.replaceWith($buttonBackupCopy);
      alert(
        `Podczas próby akceptacji wniosku wystąpił błąd. ` +
        `Status: ${jqXHR.status}. ` +
        `Treść odpowiedzi serwera: ${decodeUtf8(jqXHR.responseText)}`
      );
    },
    complete() {
      $button.text('Akceptuj');
    }
  });
}

function decodeUtf8(str) {
  try {
    return JSON.parse(str);
  } catch (e) {
    return str;
  }
}
