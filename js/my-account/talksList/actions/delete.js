import $ from 'jquery';

export default function({ talk, $talkContainer }) {
  const confirmText = tbsa_talks.confirmDeletionText.replace(
    '%s',
    talk.subject
  );
  if (confirm(confirmText)) {
    $talkContainer.find('td:last-child').text(tbsa_talks.deletingText);
    $talkContainer.animate({ opacity: 0.5 });
    $.ajax({
      url: `${tbsa_talks.ajaxUrl}/${talk.id}`,
      method: 'DELETE',
      headers: { 'X-WP-NONCE': tbsa_talks.nonce }
    }).then(() => $talkContainer.remove());
  }
}
