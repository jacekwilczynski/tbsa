export const getNumberOfPages = (limit, totalCount) => limit ? Math.ceil(totalCount / limit) : 1;

export const getPageNumber = (limit, offset) => limit ? Math.floor(offset / limit) + 1 : 1;

export const getOffset = (limit, pageNumber) => limit ? (pageNumber - 1) * limit : 0;
