import PropTypes from 'prop-types';

export const componentPropType = PropTypes.oneOfType([PropTypes.object, PropTypes.func]);
