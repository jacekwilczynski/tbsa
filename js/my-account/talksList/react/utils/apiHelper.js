import $ from 'jquery';

const endpoints = window.tbsa_talk.rest;

const createSearchHelper = ({ url, method, nonce }) => data =>
  new Promise((resolve, reject) =>
    $.ajax({
      url,
      method,
      data,
      headers: { 'X-WP-NONCE': nonce },
      success: resolve,
      error: reject
    })
  );

export const searchTemplates = createSearchHelper(endpoints.searchTemplates);

export const searchOwnSubmissions = createSearchHelper({
  ...endpoints.searchSubmissions,
  url: endpoints.searchSubmissions.url + '/own'
});

export const searchOthersSubmissions = createSearchHelper({
  ...endpoints.searchSubmissions,
  url: endpoints.searchSubmissions.url + '/others'
});
