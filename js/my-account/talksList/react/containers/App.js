import * as React from 'react';
import SubmissionsSection from '../components/submissions/SubmissionsSection';
import TemplatesSection from '../components/templates/TemplatesSection';

const App = () => (
  <>
    <TemplatesSection {...window.tbsa_talkTemplatesSectionData}/>
    <SubmissionsSection {...window.tbsa_talkSubmissionsSectionData}/>
  </>
);

export default App;
