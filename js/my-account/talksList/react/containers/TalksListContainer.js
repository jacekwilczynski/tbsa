import PropTypes from 'prop-types';
import * as React from 'react';
import TalksList from '../components/common/TalksList';
import { getNumberOfPages, getOffset, getPageNumber } from '../utils/paginationHelper';
import { componentPropType } from '../utils/propTypes';
import debounce from 'lodash.debounce';

class TalksListContainer extends React.Component {
  static propTypes = {
    Table: componentPropType.isRequired,
    config: PropTypes.object.isRequired,
    searchTalks: PropTypes.func.isRequired,
    messageIfNone: PropTypes.string.isRequired,
    allowCustomSearch: PropTypes.bool.isRequired,
    limit: PropTypes.number
  };

  static defaultProps = {
    limit: 10
  };

  state = {
    status: {
      ready: false,
      loading: false
    },
    talks: {
      anyExist: false,
      visibleItems: [],
      currentPage: 1,
      numberOfPages: 1
    },
  };

  loadTalks = async ({
    offset = 0,
    limit = this.props.limit,
    phrase
  } = {}) => {
    this.setState(({ status }) => ({ status: { ...status, loading: true } }));
    const responseData = await this.props.searchTalks({
      offset,
      limit,
      phrase
    });

    const noTalksExistRegardlessOfSearchParams = responseData === null;
    if (noTalksExistRegardlessOfSearchParams) {
      this.updateState({ anyExist: false });
    } else {
      const { totalCount, items } = responseData;
      this.updateState({ items, offset, totalCount });
    }
  };

  updateState = ({
    anyExist = true,
    items = [],
    offset = 0,
    totalCount = 0
  }) => {
    this.setState({
      status: {
        ready: true,
        loading: false
      },
      talks: {
        anyExist,
        visibleItems: items,
        currentPage: getPageNumber(this.props.limit, offset),
        numberOfPages: getNumberOfPages(this.props.limit, totalCount)
      }
    });
  };

  onPageChange = pageNumber => {
    this.loadTalks({ offset: getOffset(this.props.limit, pageNumber) });
  };

  onSearchChange = debounce(this.loadTalks, 1000);

  componentDidMount() {
    this.loadTalks();
  }

  render() {
    const { Table, config, messageIfNone, allowCustomSearch } = this.props;
    const { talks, status } = this.state;
    return (
      <TalksList
        messageIfNone={messageIfNone}
        tableConfig={config}
        status={status}
        talks={talks}
        onPageChange={this.onPageChange}
        onSearchChange={allowCustomSearch && this.onSearchChange}
        Table={Table}
      />
    );
  }
}

export default TalksListContainer;
