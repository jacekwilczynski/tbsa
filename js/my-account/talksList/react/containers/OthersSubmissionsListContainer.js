import * as React from 'react';
import SubmissionsTable from '../components/submissions/SubmissionsTable';
import { searchOthersSubmissions } from '../utils/apiHelper';
import TalksListContainer from './TalksListContainer';

const OthersSubmissionsListContainer = () => (
  <TalksListContainer
    searchTalks={searchOthersSubmissions}
    config={window.tbsa_talkSubmissionsSectionData}
    Table={SubmissionsTable}
    messageIfNone="Brak wniosków innych osób o prezentację w zarządzanych przez Ciebie towarzystwach."
    allowCustomSearch={true}
  />
);

export default OthersSubmissionsListContainer;
