import $ from 'jquery';
import * as React from 'react';

class HeightTransitioner extends React.Component {
  ref = React.createRef();

  getSnapshotBeforeUpdate() {
    const $root = $(this.ref.current);
    return $root.height();
  }

  componentDidUpdate(_, __, heightBeforeUpdate) {
    const $table = $(this.ref.current);
    $table.height('');
    const newHeight = $table.height();
    if (newHeight === heightBeforeUpdate) {
      return;
    }
    $table.height(heightBeforeUpdate);
    $table.animate({ height: newHeight }, () => {
      $table.height('');
    });
  }

  render() {
    return <div ref={this.ref} style={{ overflow: 'hidden' }}>{this.props.children}</div>;
  }
}

export default HeightTransitioner;
