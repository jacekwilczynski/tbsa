import * as React from 'react';
import TemplatesTable from '../components/templates/TemplatesTable';
import { searchTemplates } from '../utils/apiHelper';
import TalksListContainer from './TalksListContainer';

const TemplatesListContainer = () => (
  <TalksListContainer
    searchTalks={searchTemplates}
    config={window.tbsa_talkTemplatesSectionData}
    Table={TemplatesTable}
    messageIfNone="Na ten moment nie masz zapisanych żadnych szablonów prezentacji."
    allowCustomSearch={false}
  />
);

export default TemplatesListContainer;
