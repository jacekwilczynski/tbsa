import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';

class Pagination extends Component {
  static propTypes = {
    currentPage: PropTypes.number.isRequired,
    numberOfPages: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    classNameWhenUnnecessary: PropTypes.string
  };

  handlePageChange = ({ selected }) => {
    this.props.onPageChange(selected + 1);
  };

  render() {
    const { numberOfPages, currentPage } = this.props;
    return (
      <>
        <ReactPaginate
          previousLabel="<"
          nextLabel=">"
          pagesDisplayed={5}
          forcePage={currentPage}
          pageCount={numberOfPages}
          onPageChange={this.handlePageChange}
          pageClassName="page-number"
          containerClassName={this.getClassName()}
        />
      </>
    );
  }

  getClassName() {
    const { numberOfPages, className, classNameWhenUnnecessary } = this.props;
    const classes = [];
    if (className) {
      classes.push(className);
    }
    if (classNameWhenUnnecessary && numberOfPages < 2) {
      classes.push(classNameWhenUnnecessary);
    }
    return classes.join(' ');
  }
}

export default Pagination;
