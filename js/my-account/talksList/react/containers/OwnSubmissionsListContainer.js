import * as React from 'react';
import SubmissionsTable from '../components/submissions/SubmissionsTable';
import { searchOwnSubmissions } from '../utils/apiHelper';
import TalksListContainer from './TalksListContainer';

const OwnSubmissionsListContainer = () => (
  <TalksListContainer
    searchTalks={searchOwnSubmissions}
    config={window.tbsa_talkSubmissionsSectionData}
    Table={SubmissionsTable}
    messageIfNone="Na ten moment nie masz żadnych własnych wniosków o prezentację."
    allowCustomSearch={true}
  />
);

export default OwnSubmissionsListContainer;
