import * as React from 'react';
import { render } from 'react-dom';
import App from './containers/App';

const root = document.getElementById('talks-react-root');
if (root) {
  render(<App/>, root);
}
