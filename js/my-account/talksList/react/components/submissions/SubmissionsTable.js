import PropTypes from 'prop-types';
import * as React from 'react';
import SubmissionRow from './SubmissionRow';

const SubmissionsTable = ({
  talks,
  canEditSubmissions,
  editTalkUrl,
  canAcceptSubmissions
}) => (
  <table className="talksTable talksTable--submissions">
    <thead>
      <tr>
        <th>Data</th>
        <th>Wydarzenie</th>
        <th>Temat</th>
        <th className="talksTable-centered-column">Stan</th>
        {canEditSubmissions && <th>Działania</th>}
      </tr>
    </thead>
    <tbody>
      {talks.map(submission => (
        <SubmissionRow
          key={submission.id}
          submission={submission}
          canEditSubmissions={canEditSubmissions}
          editTalkUrl={editTalkUrl}
          canAcceptSubmissions={canAcceptSubmissions}
        />
      ))}
    </tbody>
  </table>
);

export default SubmissionsTable;

SubmissionsTable.propTypes = {
  canAcceptSubmissions: PropTypes.bool.isRequired,
  canEditSubmissions: PropTypes.bool.isRequired,
  editTalkUrl: PropTypes.string.isRequired,
  talks: PropTypes.arrayOf(SubmissionRow.propTypes.submission).isRequired
};
