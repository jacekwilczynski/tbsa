import PropTypes from 'prop-types';
import * as React from 'react';

const ColoredStatus = ({ status: { formatted, raw } }) => (
  <span className={`coloredStatus coloredStatus--${raw} js-coloredStatus`}>
    <span className="coloredStatus__icon" title={formatted}></span>
    <span className="coloredStatus__text talksTable-mobileOnly">{formatted}</span>
  </span>
);

export default ColoredStatus;

ColoredStatus.propTypes = {
  status: PropTypes.shape({
    formatted: PropTypes.string.isRequired,
    raw: PropTypes.string.isRequired
  }).isRequired
};
