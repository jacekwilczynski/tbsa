import * as React from 'react';
import LdsSpinner from '../common/LdsSpinner';

const SubmissionActions = ({ submission, editTalkUrl, canAcceptSubmissions }) => <>
  <span className="talksTable__inlineCaption">Działania:</span>
  <span className="talksTableActions">
    <Edit submission={submission} editTalkUrl={editTalkUrl}/>
    <div className="talksTableActions__spinner"><LdsSpinner/></div>
    <Copy/>
    {canAcceptSubmissions && <Accept submission={submission}/>}
  </span>
</>;

export default SubmissionActions;

const Edit = ({ submission, editTalkUrl }) => (
  <a
    className={`
      talksTableActions__action
      ${submission.editable ? '' : 'talksTableActions__action--disabled'}
    `}
    href={submission.editable ? editTalkUrl + submission.id : 'javascript:'}
  >Edytuj</a>
);

const Copy = () => (
  <a className="talksTableActions__action talksTableActions__action--dynamic"
     href="javascript:"
     data-talk-action="submit"
  >Kopiuj</a>
);

const Accept = ({ submission }) => (
  <a
    className={`
      talksTableActions__action
      talksTableActions__action--dynamic
      ${submission.acceptable ? '' : 'talksTableActions__action--disabled'}
    `}
    href="javascript:"
    data-talk-action={submission.acceptable && 'accept'}
  >Akceptuj</a>
);
