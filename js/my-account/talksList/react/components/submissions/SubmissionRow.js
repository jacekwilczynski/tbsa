import PropTypes from 'prop-types';
import * as React from 'react';
import ColoredStatus from './ColoredStatus';
import SubmissionActions from './SubmissionActions';

const SubmissionRow = ({
  submission,
  canEditSubmissions,
  editTalkUrl,
  canAcceptSubmissions
}) => (
  <tr data-talk={submission.manageable && JSON.stringify(submission)}>
    <td>
      <span className="talksTable__inlineCaption">Data:</span>
      {submission.eventDate}
    </td>
    <td>
      <span className="talksTable__inlineCaption">Wydarzenie:</span>
      <span dangerouslySetInnerHTML={{ __html: submission.eventLink }}></span>
    </td>
    <td>
      <span className="talksTable__inlineCaption">Temat:</span>
      <span className="js-submission-talk-subject">
        {submission.talkSubject}
      </span>
    </td>
    <td className="talksTable-centered-column">
      <span className="talksTable__inlineCaption">Stan:</span>
      <ColoredStatus status={submission.status}/>
    </td>
    {canEditSubmissions && (
      <td>
        {submission.manageable && (
          <SubmissionActions
            submission={submission}
            editTalkUrl={editTalkUrl}
            canAcceptSubmissions={canAcceptSubmissions}
          />
        )}
      </td>
    )}
  </tr>
);

export default SubmissionRow;

SubmissionRow.propTypes = {
  canAcceptSubmissions: PropTypes.bool.isRequired,
  canEditSubmissions: PropTypes.bool.isRequired,
  editTalkUrl: PropTypes.string.isRequired,
  submission: PropTypes.shape({
    id: PropTypes.number.isRequired,
    talkSubject: PropTypes.string.isRequired,
    status: ColoredStatus.propTypes.status,
    eventDate: PropTypes.string.isRequired,
    eventLink: PropTypes.string.isRequired,
    manageable: PropTypes.bool.isRequired
  }).isRequired
};
