import * as React from 'react';
import OthersSubmissionsListContainer from '../../containers/OthersSubmissionsListContainer';
import OwnSubmissionsListContainer from '../../containers/OwnSubmissionsListContainer';

const SubmissionsSection = ({ canEditSubmissions }) => {
  return (
    <section className="talksSection">
      <h3 className="talksSection__heading">
        {canEditSubmissions ? 'Twoje wnioski' : 'Wnioski'}
      </h3>
      <OwnSubmissionsListContainer/>
      {canEditSubmissions && (
        <>
          <h3 className="talksSection__heading" style={{ paddingTop: '1rem' }}>
            Wnioski innych
          </h3>
          <OthersSubmissionsListContainer/>
        </>
      )}
    </section>
  );
};

export default SubmissionsSection;
