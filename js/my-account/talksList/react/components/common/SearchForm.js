import PropTypes from 'prop-types';
import * as React from 'react';

const LABEL = 'Szukaj';

class SearchForm extends React.Component {
  state = { phrase: '' };

  onSubmit = event => {
    event.preventDefault();
  };

  onChange = event => {
    const phrase = event.target.value;
    this.setState({ phrase }, () => {
      this.props.onChange(this.state);
    });
  };

  render() {
    return (
      <form
        className="form-normal form-custom-sizes talkSearchForm"
        onSubmit={this.onSubmit}
      >
        <label>
          <span className="sr-only">{LABEL}</span>
          <input
            className="talkSearchForm__input"
            type="search"
            placeholder={LABEL}
            value={this.state.phrase}
            onChange={this.onChange}
          />
        </label>
      </form>
    );
  }
}

export default SearchForm;

SearchForm.propTypes = {
  onChange: PropTypes.func.isRequired
};
