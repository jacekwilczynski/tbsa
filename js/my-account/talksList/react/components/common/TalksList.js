import * as PropTypes from 'prop-types';
import * as React from 'react';
import HeightTransitioner from '../../containers/HeightTransitioner';
import Pagination from '../../containers/Pagination';
import LdsSpinner from './LdsSpinner';
import SearchForm from './SearchForm';

function TalksList({
  messageIfNone,
  tableConfig,
  status: { ready, loading },
  talks: { anyExist, visibleItems, currentPage, numberOfPages },
  Table,
  onPageChange,
  onSearchChange
}) {
  if (!ready) {
    return <HeightTransitioner><LdsSpinner/></HeightTransitioner>;
  }

  if (!anyExist) {
    return (
      <HeightTransitioner>
        <p style={{ paddingBottom: '1rem' }}>{messageIfNone}</p>
      </HeightTransitioner>
    );
  }

  return (
    <div className={`talksList ${onSearchChange ? 'talksList--searchable' : ''}`}>
      {onSearchChange && <SearchForm onChange={onSearchChange}/>}
      <HeightTransitioner>
        <div className={`talksLoadingWrapper ${loading ? 'talksLoadingWrapper--loading' : ''}`}>
          <div className="talksLoadingWrapper__spinner">
            <LdsSpinner/>
          </div>
          <div className="talksLoadingWrapper__content">
            <Table {...tableConfig} talks={visibleItems}/>
          </div>
        </div>
      </HeightTransitioner>
      <div className="text-center">
        <Pagination
          currentPage={currentPage - 1}
          numberOfPages={numberOfPages}
          onPageChange={onPageChange}
          className="talks-pagination"
          classNameWhenUnnecessary="talks-pagination--unnecessary"
        />
      </div>
    </div>
  );
}

export default TalksList;

TalksList.propTypes = {
  messageIfNone: PropTypes.string.isRequired,
  tableConfig: PropTypes.any,
  status: PropTypes.shape({
    ready: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired
  }).isRequired,
  talks: PropTypes.shape({
    anyExist: PropTypes.bool.isRequired,
    visibleItems: PropTypes.array.isRequired,
    currentPage: PropTypes.number.isRequired,
    numberOfPages: PropTypes.number.isRequired
  }).isRequired,
  Table: PropTypes.any.isRequired,
  onPageChange: PropTypes.func.isRequired,
  onSearchChange: PropTypes.func
};
