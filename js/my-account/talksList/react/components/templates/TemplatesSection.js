import PropTypes from 'prop-types';
import * as React from 'react';
import TemplatesListContainer from '../../containers/TemplatesListContainer';

const TemplatesSection = ({ guidebookUrl, newTalkUrl }) => (
  <section className="talksSection">
    <h3 className="talksSection__heading">Szablony</h3>
    <TemplatesListContainer/>
    <div className="talkSectionButtons">
      {guidebookUrl && guideBookLink(guidebookUrl)}
      {newTalkLink(newTalkUrl)}
    </div>
  </section>
);

export default TemplatesSection;

TemplatesSection.propTypes = {
  guidebookUrl: PropTypes.string.isRequired,
  newTalkUrl: PropTypes.string.isRequired
};

const guideBookLink = url => (
  <a
    className="et_pb_button btn-primary-inverse button-sm"
    href={url}
    rel="noopener noreferrer"
    target="_blank"
  >
    Poradnik tworzenia prezentacji
  </a>
);

const newTalkLink = url => (
  <a className="et_pb_button" href={url}>
    Dodaj nową prezentację
  </a>
);
