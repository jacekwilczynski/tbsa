import PropTypes from 'prop-types';
import * as React from 'react';
import LdsSpinner from '../common/LdsSpinner';

class TemplatesTable extends React.Component {
  render() {
    const { talks, editTalkUrl } = this.props;
    return (
      <table className="talksTable talksTable--templates">
        <thead>
          <tr>
            <th>Temat</th>
            <th>Działania</th>
          </tr>
        </thead>
        <tbody>
          {talks.map(template =>
            <tr data-talk={JSON.stringify(template)} key={template.id}>
              <td>{template.subject}</td>
              <td>
                <div className="talksTableActions">
                  <a className="talksTableActions__action"
                     href={editTalkUrl + template.id}>Edytuj</a>
                  <div className="talksTableActions__spinner"><LdsSpinner/></div>
                  <a className="talksTableActions__action talksTableActions__action--dynamic"
                     href="javascript:"
                     data-talk-action="submit">Wyślij</a>
                  <a className="talksTableActions__action talksTableActions__action--dynamic"
                     href="javascript:"
                     data-talk-action="delete">Usuń</a>
                </div>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }
}

export default TemplatesTable;

const templatePropType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  subject: PropTypes.string.isRequired
});

export const templatesPropType = PropTypes.arrayOf(templatePropType);

TemplatesTable.propTypes = {
  editTalkUrl: PropTypes.string.isRequired,
  talks: templatesPropType.isRequired
};
