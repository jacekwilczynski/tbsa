import deleteAction from './actions/delete';
import submitAction from './actions/submit';
import acceptAction from './actions/accept';

export default {
  delete: deleteAction,
  submit: submitAction,
  accept: acceptAction
};
