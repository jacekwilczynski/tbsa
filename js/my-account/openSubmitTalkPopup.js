import $ from 'jquery';

export default function openSubmitTalkPopup(talk) {
  const $popup = $('#submitTalkPopup');
  if ($popup.length) {
    openInserted();
  } else {
    openWhenLoaded();
  }

  function openWhenLoaded() {
    const loadedHtml = localStorage.getItem('submitTalkPopup/html');
    if (loadedHtml) {
      $(loadedHtml)
        .hide()
        .appendTo(document.body);
      $(window).trigger('submitTalkPopup/inserted');
      openInserted();
    } else {
      $(window).on('submitTalkPopup/loadedHtml', function() {
        openWhenLoaded();
      });
    }

  }

  function openInserted() {
    $.fancybox.open({
      src: '#submitTalkPopup',
      opts: { autoFocus: false, touch: false }
    });
    $(window).trigger('submitTalkPopup/opened', { talk });
  }
}
