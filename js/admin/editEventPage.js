import $ from 'jquery';

$(function() {
  const bookingsMetabox = $('#acf-group_5cab9f238b102');
  if (bookingsMetabox.length === 0) return;

  bookingsMetabox.prependTo('#acf_after_title-sortables');

  const rowsContainer = bookingsMetabox.find('.acf-field-repeater tbody');
  if (rowsContainer.length === 0) {
    return;
  }

  const quantityOutput = $('<span>');
  bookingsMetabox
    .find('.hndle > span')
    .append(' ')
    .append(quantityOutput);

  setInterval(updateQuantity, 2000);
  updateQuantity();

  function updateQuantity() {
    const rows = rowsContainer.children().slice(0, -1);
    const quantity = rows.toArray().reduce((quantity, row) => {
      const input = $(row).find(
        'td[data-name="number_of_places"] input[type=number]'
      );
      return quantity + (Number.parseInt(input.val()) || 0);
    }, 0);
    quantityOutput.text(`(${quantity})`);
  }
});
