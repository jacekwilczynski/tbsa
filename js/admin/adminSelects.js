// noinspection JSUnresolvedVariable
const homeUrl = window.tbsa_general.homeUrl;

function GetPostEditUrl(homeUrl) {
  return function getPostEditUrl(id) {
    return `${homeUrl}/wp-admin/post.php?post=${id}&action=edit`;
  };
}

function GetUserEditUrl(homeUrl) {
  return function getUserEditUrl(id) {
    return `${homeUrl}/wp-admin/user-edit.php?user_id=${id}`;
  };
}

function GetEditUrlFactory(homeUrl) {
  const byAcfFieldType = {
    user: GetUserEditUrl(homeUrl),
    post_object: GetPostEditUrl(homeUrl)
  };

  return {
    getByAcfFieldType: fieldType => byAcfFieldType[fieldType]
  };
}

const getEditUrlFactory = GetEditUrlFactory(homeUrl);

Array.prototype.forEach.call(
  document.querySelectorAll('.follow-to-edit__group'),
  function(group) {
    new MutationObserver(UpdateAll(group)).observe(group, {
      childList: true,
      subtree: true
    });
  }
);

window.addEventListener('load', UpdateAll(document.body));

function UpdateAll(root) {
  return function updateAll() {
    Array.prototype.forEach.call(
      root.querySelectorAll('.follow-to-edit'),
      function(field) {
        const fieldType = field.dataset.type;
        const getEditUrl = getEditUrlFactory.getByAcfFieldType(fieldType);
        const inputContainer =
          field.tagName.toLowerCase() === 'tr'
            ? field.querySelector('.acf-input')
            : field;
        const select = inputContainer.querySelector('select');

        let link = inputContainer.querySelector('.follow-to-edit__link');
        if (!link) {
          link = document.createElement('a');
          link.classList.add('follow-to-edit__link');
          link.innerText = 'Szczegóły';
          inputContainer.appendChild(link);
        }

        const id = select.value;
        if (id) {
          link.href = getEditUrl(id);
          inputContainer.classList.remove('follow-to-edit--empty');
        } else {
          inputContainer.classList.add('follow-to-edit--empty');
        }
      }
    );
  };
}
