import $ from 'jquery';
import 'slick-carousel';
import '~styles-loaded-from-js/slick.scss';

window.tbsa_simpleSlider = function() {
  $('.js-simple-slider').slick({ speed: 500 });
};

$(window.tbsa_simpleSlider);
