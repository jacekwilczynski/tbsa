import $ from 'jquery';

$(window).on('load', function() {
  $('.secondaryTopNav')
    .clone()
    .removeClass('secondaryTopNav--desktop')
    .addClass('secondaryTopNav--mobile')
    .appendTo('#mobile_menu')
    .wrap('<li class="menu-item"></li>');
});
