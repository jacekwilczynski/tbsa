export default findNearestEventfulCell;

function findNearestEventfulCell(calendar) {
  const eventfulToday = calendar.querySelector('.eventful-today');
  if (eventfulToday) {
    return eventfulToday;
  }

  const today = calendar.querySelector('.eventless-today');
  const showingCurrentMonth = Boolean(today);
  const interestingCells = calendar.querySelectorAll(
    '.eventful, .eventless-today'
  );

  for (let i = 0; i < interestingCells.length; i++) {
    if (!showingCurrentMonth || interestingCells[i - 1] === today) {
      return interestingCells[i];
    }
  }
}
