export default function setActiveCell(calendar, targetCell) {
  Array.prototype.forEach.call(
    calendar.querySelectorAll('.eventful, .eventful-today'),
    function(cell) {
      cell.classList.toggle('active', cell === targetCell);
    }
  );
}
