import $ from 'jquery';

$('input[name=min_date]').on('change', function() {
  const $min = $(this);
  const $max = $min.closest('form').find('input[name=max_date]');
  const maxDate = $max.val();
  const minDate = $min.val();
  if (maxDate && maxDate < minDate) $max.datepicker('setDate', minDate);
});

$('input[name=max_date]').on('change', function() {
  const $max = $(this);
  const $min = $max.closest('form').find('input[name=min_date]');
  const minDate = $min.val();
  const maxDate = $max.val();
  if (minDate && minDate > maxDate) $min.datepicker('setDate', maxDate);
});
