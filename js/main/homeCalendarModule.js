import $ from 'jquery';
import Fade from '~js-utils/Fade';
import forceTransition from '~js-utils/forceTransition';
import findNearestEventfulCell from './homeCalendarModule/findNearestEventfulCell';
import setActiveCell from './homeCalendarModule/setActiveCell';

// noinspection JSUnresolvedVariable
const ajaxRoute = window.tbsa_event.ajaxRoute;

document.addEventListener('DOMContentLoaded', function() {
  const calendarBlock = document.getElementById(
    'homeCalendarModule__calendarContainer'
  );

  if (!calendarBlock) {
    return;
  }

  const eventsBlock = document.getElementById('homeEvents');
  const eventList = document.getElementById('homeEvents__list');
  const selectionBlock = document.getElementById(
    'homeCalendarModule__selectionBlock'
  );
  const summaryBlock = document.getElementById(
    'homeCalendarModule__summaryContainer'
  );

  let currentEventId;

  const fadeSummary = Fade({
    element: summaryBlock,
    fadingClass: 'homeCalendarModule__summaryContainer--fading',
    transitionDuration: 250
  });

  const initialCell = findNearestEventfulCell(calendarBlock);
  if (initialCell) {
    selectCalendarCell(initialCell, initialCell.querySelector('a')).then(
      function() {
        const initialEventInfo = eventList.querySelector('.eventShortInfo');
        if (initialEventInfo) {
          setActiveEvent(
            eventList.querySelectorAll('.eventShortInfo'),
            initialEventInfo
          );
        }
      }
    );
  }

  calendarBlock.addEventListener('click', function(event) {
    const link = event.target;
    if (isDayLink(link)) {
      event.preventDefault();
      const cell = link.parentNode;

      // Disable clicking on past events
      if (cell.classList.contains('eventful-pre')) {
        return;
      }

      selectCalendarCell(cell, link);
    }
  });

  function selectCalendarCell(cell, link) {
    return new Promise(function(resolve) {
      eventsBlock.classList.add('homeEvents--loading');
      setActiveCell(calendarBlock, cell);
      const dateString = getDateFromUrl(link.getAttribute('href'));
      $.ajax(ajaxRoute + '?render_for=calendar&date=' + dateString).then(function(
        markup
      ) {
        forceTransition({
          element: selectionBlock,
          property: 'height',
          transitionDuration: 500,
          action: () => {
            eventList.innerHTML = markup;
            const items = eventList.querySelectorAll('.eventShortInfo');
            activateListItems(items);
            Array.prototype.forEach.call(items, function(item) {
              if (item.dataset && item.dataset.eventId === currentEventId) {
                item.classList.add('eventShortInfo--active');
              }
            });
            eventsBlock.classList.remove('homeEvents--loading');
            resolve();
          }
        });
      });
    });
  }

  function activateListItems(items) {
    Array.prototype.forEach.call(items, function(item) {
      item.addEventListener('click', function(event) {
        event.preventDefault();
        if (!eventsBlock.classList.contains('homeEvents--loading')) {
          setActiveEvent(items, item);
        }
      });
    });
  }

  function setActiveEvent(items, activeItem) {
    currentEventId = activeItem.dataset.eventId;
    Array.prototype.forEach.call(items, function(item) {
      if (item && item.classList) {
        item.classList.toggle('eventShortInfo--active', item === activeItem);
      }
    });
    fadeSummary(markup => {
      forceTransition({
        element: summaryBlock,
        property: 'height',
        transitionDuration: 500,
        action: () => {
          summaryBlock.innerHTML = markup;
        }
      });
    }, $.ajax(ajaxRoute + '?render_for=summary&id=' + currentEventId));
  }
});

const isDayLink = element => element.matches('td[class*=eventful] > a');

const getDateFromUrl = url => url.match(/\d{4}-\d{2}-\d{2}/)[0];
