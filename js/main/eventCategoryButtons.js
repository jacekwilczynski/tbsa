import Accordion from '~js-utils/Accordion';

const eventCategoryButtons = Accordion({
  blockSelector: '.js-eventCategoryButtons',
  activeClass: 'eventCategoryButtons__group--active',
  groupSelector: '.js-eventCategoryButtons__group',
  handleSelector: '.eventCategoryButtons__item--category',
  contentSelector: '.js-eventCategoryButtons__events',
  transitionDuration: 300
});

export default eventCategoryButtons;

eventCategoryButtons.activate();
