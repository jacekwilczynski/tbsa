import AutoStyler from '~js-utils/AutoStyler';
import SplitArray from '~js-utils/SplitArray';

new AutoStyler(updateHeights).enable();
const splitArray = new SplitArray(shouldBeSized);

window.tbsa_updateHeights = updateHeights;

function updateHeights() {
  const allElements = document.querySelectorAll('[data-height-group]');
  const elementsByGroup = arrangeByGroups(allElements);
  Object.keys(elementsByGroup).forEach(groupName => {
    const groupElements = elementsByGroup[groupName];
    const [elementsToSize, elementsToFree] = splitArray(groupElements);

    const heights = elementsToSize.map(getNaturalHeight);
    const topHeight = Math.max(...heights);
    elementsToSize.forEach(element => {
      element.style.height = topHeight + 'px';
    });

    elementsToFree.forEach(element => {
      element.style.height = '';
    });
  });
}

/**
 * @param {NodeList} elements
 */
function arrangeByGroups(elements) {
  const elementsByGroup = {};
  Array.prototype.forEach.call(elements, function(element) {
    const groupName = element.dataset.heightGroup;
    if (!elementsByGroup[groupName]) {
      elementsByGroup[groupName] = [];
    }
    elementsByGroup[groupName].push(element);
  });
  return elementsByGroup;
}

/**
 * @param {HTMLElement} element
 * @return {int}
 */
function getNaturalHeight(element) {
  element.style.height = 'auto';
  return Number.parseInt(getComputedStyle(element).height);
}

/**
 * @param {HTMLElement} element
 */
function shouldBeSized(element) {
  const { heightGroupIf } = element.dataset;
  if (heightGroupIf) {
    const minWidthRegex = /min-width: (\d+)px/;
    const minWidthMatches = heightGroupIf.match(minWidthRegex);
    if (minWidthMatches && minWidthMatches[1]) {
      const minWidth = Number.parseInt(minWidthMatches[1]);
      return window.innerWidth >= minWidth;
    }
  }
  return true;
}
