import $ from 'jquery';

$(function() {
  if (
    $('.woocommerce-checkout').length &&
    $(document.body).hasClass('woocommerce-only-virtual-products')
  ) {
    $('#billing_faktura').change(function() {
      const fields = $(
        '#billing_company_field, #billing_address_1_field, #billing_address_2_field, #billing_postcode_field, #billing_city_field'
      )
        .closest('p')
        .add('#tbsa_checkout-invoice-data-heading');
      if ($('#billing_faktura').is(':checked')) {
        fields.slideDown();
      } else {
        fields.slideUp();
      }
    });
  }
});
