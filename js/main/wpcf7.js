import $ from 'jquery';

function customizeCheckboxes() {
  $('.wpcf7-form-control-wrap input[type=checkbox]').each(function() {
    const input = $(this);
    if (!input.next().hasClass('checkbox')) {
      input.addClass('sr-only');
      input.after('<span class="checkbox"></span>');
    }
  });
}

customizeCheckboxes();
$(customizeCheckboxes);
