const thresholds = {
  landscape: 1.2,
  portrait: 0.8
};

const orientations = {
  landscape: ratio => ratio > thresholds.landscape,
  square: ratio =>
    ratio >= thresholds.portrait && ratio <= thresholds.landscape,
  portrait: ratio => ratio < thresholds.portrait
};

const orientationNames = Object.keys(orientations);

function detectImageOrientations(root = document) {
  const wrappers = root.querySelectorAll('.js-detect-image-orientation');

  Array.prototype.forEach.call(wrappers, function(wrapper) {
    const image =
      wrapper.tagName === 'img' ? wrapper : wrapper.querySelector('img');

    if (image) {
      image.addEventListener('load', function() {
        const ratio =
          Number.parseFloat(image.width) / Number.parseFloat(image.height);
        orientationNames.forEach(name =>
          wrapper.classList.toggle(
            'orientation-' + name,
            orientations[name](ratio)
          )
        );
      });
    }
  });
}

document.addEventListener('DOMContentLoaded', function() {
  detectImageOrientations();
});
