import $ from 'jquery';

$(function() {
  const owl = $('.affiliates .owl-carousel');
  owl.data('owl.carousel').options.autoWidth = true;
  owl.trigger('refresh.owl.carousel');
});
