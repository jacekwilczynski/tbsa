import $ from 'jquery';
import Fade from '~js-utils/Fade';
import forceTransition from '~js-utils/forceTransition';
import eventCategoryButtons from './eventCategoryButtons';

// noinspection JSUnresolvedVariable
const ajaxRoute = window.tbsa_event.ajaxRoute;

const initialRegion = 'mazowieckie';

function State(initial) {
  let state = initial;
  const callbacks = [];

  return {
    subscribe(callback) {
      callbacks.push(callback);
    },
    set(newState) {
      callbacks.forEach(callback => callback(newState, state));
      state = newState;
    },
    get() {
      return state;
    }
  };
}

document.addEventListener('DOMContentLoaded', function() {
  Array.prototype.forEach.call(
    document.getElementsByClassName('homeMapBlock'),
    function(mainBlock) {
      const state = State(initialRegion);
      state.subscribe(transitionOptions);

      const section = mainBlock.closest('.et_pb_section');
      const optionsBlock = mainBlock.querySelector('.homeMapBlock__options');
      const branchLabels = mainBlock.querySelectorAll('.homeMapBlock__branch');
      const mapRegions = mainBlock.querySelectorAll('.tbsa-map__region');
      const categoriesOuter = mainBlock.querySelector(
        '.homeMapBlock__eventCategoriesOuter'
      );
      const categoriesInner = mainBlock.querySelector(
        '.homeMapBlock__eventCategoriesInner'
      );

      const fadeCategories = Fade({
        element: categoriesOuter,
        fadingClass: 'homeMapBlock__eventCategoriesOuter--transitioning',
        transitionDuration: 250
      });

      Array.prototype.forEach.call(mapRegions, function(mapRegion) {
        mapRegion.addEventListener('click', function() {
          if (mapRegion.classList.contains('tbsa-map__region--disabled')) {
            return;
          }
          const region = mapRegion
            .getAttribute('class')
            .match(/tbsa-map__region=([^\s|]*)\s?/)[1];
          state.set(region);
        });
      });

      Array.prototype.forEach.call(branchLabels, function(branchLabel) {
        branchLabel
          .querySelector('.homeMapBlock__branchLink')
          .addEventListener('click', function(event) {
            event.preventDefault();
            updateCategories(branchLabel.dataset.branchId);
          });
      });

      const region = state.get();
      updateRegions(region);
      updateBranches(region);

      function transitionOptions(region, oldRegion) {
        if (region !== oldRegion) {
          updateRegions(region);
          optionsBlock.classList.add('homeMapBlock__options--transitioning');
          optionsBlock.addEventListener('transitionend', endTransition);
        }
        function endTransition() {
          updateBranches(region);
          optionsBlock.removeEventListener('transitionend', endTransition);
        }
      }

      function updateRegions(region) {
        Array.prototype.forEach.call(mapRegions, function(mapRegion) {
          mapRegion.classList.toggle(
            'tbsa-map__region--active',
            mapRegion.classList.contains('tbsa-map__region=' + region)
          );
        });
      }

      function updateBranches(region) {
        let isFirst = true;
        Array.prototype.forEach.call(branchLabels, function(element) {
          const isInRegion = element.dataset.region === region;
          element.classList.toggle('homeMapBlock__branch--hidden', !isInRegion);
          if (isInRegion && isFirst) {
            updateCategories(element.dataset.branchId);
            isFirst = false;
          }
        });
        optionsBlock.classList.remove('homeMapBlock__options--transitioning');
      }

      function updateCategories(branchId) {
        Array.prototype.forEach.call(branchLabels, function(branchLabel) {
          branchLabel.classList.toggle(
            'homeMapBlock__branch--active',
            branchLabel.dataset.branchId === branchId
          );
        });

        fadeCategories(function(markup) {
          forceTransition({
            property: 'height',
            element: section,
            transitionDuration: 500,
            action: () => {
              categoriesInner.innerHTML = markup;
              eventCategoryButtons.activate();
            }
          });
        }, $.ajax(ajaxRoute + '?render_for=map&branch=' + branchId));
      }
    }
  );
});
