function formatPrice(price) {
  return price.toFixed(2).replace('.', ',') + ' zł';
}

/**
 * @param {HTMLFormElement} form
 */
window.tbsa_onBookingFormChange = function(form) {
  const totalField = form.elements.namedItem('total');
  const quantity = Number(form.elements.namedItem('quantity').value);
  totalField.value = formatPrice(quantity * Number(form.dataset.price));
};

/**
 * @param {HTMLFormElement} form
 */
window.tbsa_onTicketsBookingFormChange = function(form) {
  const parsedTicketInputNames = [...form.elements]
    .map(input => ({
      input,
      matches: input.name.match(/^tickets\[(\d+)]\[([^\]]*)]$/)
    }))
    .filter(({ matches }) => matches)
    .map(({ input, matches }) => ({
      input,
      index: matches[1],
      key: matches[2]
    }));

  /**
   * @type {{id, price, quantity}[]}
   */
  const ticketLines = parsedTicketInputNames.reduce(
    (data, { input, index, key }) => {
      if (!data[index]) {
        data[index] = {};
      }
      data[index][key] = input.value;

      const price = input.dataset.price;
      if (price) {
        data[index].price = price;
      }
      return data;
    },
    []
  );

  const quantity = ticketLines.reduce(
    (quantity, ticketLine) => quantity + Number.parseInt(ticketLine.quantity),
    0
  );

  const total = ticketLines.reduce(
    (total, ticketLine) =>
      total +
      Number.parseFloat(ticketLine.price) *
        Number.parseInt(ticketLine.quantity),
    0
  );

  form.elements.namedItem('total').value = formatPrice(total);

  Array.prototype.forEach.call(form.querySelectorAll('[type=submit]'), function(
    submitButton
  ) {
    if (quantity) {
      submitButton.removeAttribute('disabled');
    } else {
      submitButton.setAttribute('disabled', true);
    }
  });
};
