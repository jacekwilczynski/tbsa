import $ from 'jquery';
import Popper from 'popper.js';
import ShitCrypt from '~js-utils/ShitCrypt';

const activatedLinks = new WeakSet();

window.tbsa_activatePopoverLinks = () => activatePopoverLinks($(document.body));

window.addEventListener('click', function(event) {
  const popoverLinks = $('.popoverLink');
  popoverLinks.each(function() {
    if (this !== event.target && !$.contains(this, event.target)) {
      $(this).blur();
    }
  });
});

function activatePopoverLinks(root) {
  const links = root.find('.popoverLink:not(.popoverLink--showOnHover)');
  links.each(function() {
    if (activatedLinks.has(this)) {
      return;
    }
    activatedLinks.add(this);
    const link = $(this);

    const popover = link.next('.popoverLink__popover--static');
    if (popover && popover.length === 1) {
      new Popper(link, popover);
    } else {
      let popover;
      link.on('click focus', function(event) {
        if (
          !popover ||
          !(
            $(event.target) === popover.get(0) ||
            $.contains(popover.get(0), event.target)
          )
        ) {
          event.preventDefault();
          if (!popover && link.data('link-key')) {
            const encodedHref = link.data('href');
            const decodedHref = ShitCrypt.decrypt(
              encodedHref,
              Number(link.data('link-key'))
            );
            const prefixless = decodedHref.replace(/^(tel:|mailto:)/, '');
            popover = $(
              `<span class="popoverLink__popover"><a href="${decodedHref}">${prefixless}</a></span>`
            );
            popover.appendTo(link);
            new Popper(link, popover);
            popover.addClass('popoverLink__popover--activated');
            link.removeAttr('data-link-key data-href');
          }
        }
      });
    }
  });
}
