import Accordion from '~js-utils/Accordion';

const accordion = new Accordion({
  groupSelector: '.subboard',
  handleSelector: '.subboard__heading',
  contentSelector: '.subboard__content',
  activeClass: 'subboard--active',
  transitionDuration: 300
});

document.addEventListener('DOMContentLoaded', function() {
  accordion.activate();
});
