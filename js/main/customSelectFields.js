import $ from 'jquery';
import 'selectric';

window.initializeCustomSelects = function() {
  const fields = $('.custom-select');
  fields.each(function() {
    const field = $(this);
    if (field.data('initialized')) {
      return;
    }

    field.on('selectric-init', function() {
      const placholderText = field.data('placeholder');
      const emptyLabel = field.data('empty-label');
      const label = field.closest('.selectric-wrapper').find('.label');
      update();
      field.on('selectric-select', update);

      function update() {
        clearIfNecessary();

        updateLabel();
      }

      function clearIfNecessary() {
        if (field.prop('multiple')) {
          field.on('selectric-select', function() {
            const selectedItems = field.val() || [];
            if (selectedItems.indexOf('') !== -1) {
              field.val([]);
              field.selectric('refresh');
            }
          });
        }
      }

      function updateLabel() {
        const labelText = label.text();
        if (labelText === emptyLabel) {
          label.text(placholderText);
          label.addClass('as-placeholder');
        } else {
          label.removeClass('as-placeholder');
        }
      }
    });

    field.data('initialized', true);
    field.selectric({ maxHeight: 240 });
  });
};

$(window.initializeCustomSelects);
