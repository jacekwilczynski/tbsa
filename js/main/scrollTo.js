import $ from 'jquery';

$(function() {
  const diviPageContainer = $('#page-container');

  $('[data-scroll-to]').on('click', function() {
    const trigger = $(this);
    const target = $(trigger.data('scroll-to'));
    const visibilityThreshold = trigger.data('scroll-threshold');
    if (
      !visibilityThreshold ||
      target.get(0).getBoundingClientRect().top >=
        $(window).innerHeight() - visibilityThreshold
    ) {
      const position =
        target.offset().top -
        Number.parseInt(diviPageContainer.css('padding-top'));
      const time = trigger.data('scroll-time');
      $('html, body').animate({ scrollTop: position }, time);
    }
  });
});
