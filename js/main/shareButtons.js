document.addEventListener('DOMContentLoaded', function() {
  const links = document.getElementsByClassName('js-share-button');
  Array.prototype.forEach.call(links, function(link) {
    link.addEventListener('click', function(e) {
      e.preventDefault();
      const href = this.getAttribute('href');
      if (href) {
        window.open(href, 'share', 'width=640,height=480');
      }
    });
  });
});
