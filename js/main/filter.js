import $ from 'jquery';

$(function() {
  const filterControls = $('.js-filter-control');

  // Prevent unwanted bubbling of the click event when switching between elements using keyboard arrows
  filterControls.on('click', function(e) {
    e.stopPropagation();
  });

  filterControls.on('change', onChange);
  filterControls.filter(':checked').each(onChange);

  function onChange() {
    const input = $(this);
    const key = input.attr('name');
    const value = input.val();

    $(`.js-filter-subject[data-filter-key=${key}]`).each(function() {
      const subject = $(this);
      const subjectValue = subject.data('filter-value');
      subject.toggle(!value || subjectValue === value);
    });
  }
});
