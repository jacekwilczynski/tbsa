import $ from 'jquery';

function IsVisible($window) {
  return function isVisible($element) {
    const top = $element.offset().top;
    const bottom = top + $element.outerHeight();
    const scrollTop = $window.scrollTop();
    const scrollBottom = scrollTop + $window.innerHeight();
    return bottom >= scrollTop && top <= scrollBottom;
  };
}

$(function() {
  const isVisible = IsVisible($(window));

  $('.bottomBar').each(function() {
    const $bottomBar = $(this);

    const $placeholder = $bottomBar.clone();
    $placeholder
      .removeClass('bottomBar')
      .addClass('bottomBar__placeholder')
      .appendTo(document.body)
      .hide();

    const $references = $(
      $bottomBar.data('bottom-bar-reference') || '.bottomBar__reference'
    ).filter(function() {
      return (
        $(this).closest('.bottomBar, .bottomBar__placeholder').length === 0
      );
    });

    let isBarVisible = false;

    setInterval(function() {
      let shouldBarBeVisibile = true;

      for (let i = 0; i < $references.length; i++) {
        const $reference = $references.eq(i);
        if (isVisible($reference)) {
          shouldBarBeVisibile = false;
          break;
        }
      }

      if (shouldBarBeVisibile) {
        if (!isBarVisible) {
          $bottomBar.addClass('bottomBar--visible');
          $placeholder.show();
          isBarVisible = true;
        }
      } else if (isBarVisible) {
        $bottomBar.removeClass('bottomBar--visible');
        $placeholder.hide();
        isBarVisible = false;
      }
    }, 500);
  });
});
