window.addEventListener('load', function() {
  setInterval(function() {
    const iframes = document.querySelectorAll('.fb-video iframe');
    Array.prototype.forEach.call(iframes, function(iframe) {
      const element = iframe.parentNode;

      // Get raw sizes
      const computedStyle = getComputedStyle(element);
      const sizes = {
        computedWidth: computedStyle.width,
        inlineWidth: element.style.width,
        inlineHeight: element.style.height
      };

      // Make sizes numeric; break if any value is invalid
      for (let key in sizes) {
        if (sizes.hasOwnProperty(key)) {
          sizes[key] = Number.parseInt(sizes[key]);
          if (!sizes[key]) {
            return;
          }
        }
      }

      // Adjust height
      const targetRatio = sizes.inlineWidth / sizes.inlineHeight;
      const targetHeight = sizes.computedWidth / targetRatio;
      element.style.maxHeight = targetHeight + 'px';
    });
  }, 1000);
});
