<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined('ABSPATH')) {
    exit;
}

if (preg_match('/error=\d/', $_SERVER['REQUEST_URI'])) {
    $order->set_status('failed');
    ?>
    <script>
      document.getElementsByClassName('main_title')[0].innerText = 'Błąd płatności';
    </script>
    <?php
}

$thankYou_before = get_field('thank_you_before', 'option');
$thankYou_middle = get_field('thank_you_middle', 'option');
$thankYou_after  = get_field('thank_you_after', 'option');

?>

<div class="woocommerce-order">

    <?php if ($order) : ?>

        <?php if ($order->has_status(['cancelled', 'failed'])) : ?>

            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.',
                    'woocommerce'); ?></p>

            <?php do_action('tbsa_order_again_button', $order) ?>

        <?php else : ?>

        <script>history.replaceState(null, null, '<?= home_url('/dziekujemy') ?>')</script>

            <?php if ($thankYou_before) :
                echo $thankYou_before;
            else : ?>
                <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text',
                        __('Thank you. Your order has been received.', 'woocommerce'), $order); ?></p>
            <?php endif ?>

            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
                <li class="woocommerce-order-overview__order order">
                    <?php _e('Order number:', 'woocommerce'); ?>
                    <strong><?php echo $order->get_order_number(); ?></strong>
                </li>
                <li class="woocommerce-order-overview__date date">
                    <?php _e('Date:', 'woocommerce'); ?>
                    <strong><?php echo wc_format_datetime($order->get_date_created()); ?></strong>
                </li>
                <?php if (is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email()) : ?>
                    <li class="woocommerce-order-overview__email email">
                        <?php _e('Email:', 'woocommerce'); ?>
                        <strong><?php echo $order->get_billing_email(); ?></strong>
                    </li>
                <?php endif; ?>
                <li class="woocommerce-order-overview__total total">
                    <?php _e('Total:', 'woocommerce'); ?>
                    <strong><?php echo $order->get_formatted_order_total(); ?></strong>
                </li>
                <?php if ($order->get_payment_method_title()) : ?>
                    <li class="woocommerce-order-overview__payment-method method">
                        <?php _e('Payment method:', 'woocommerce'); ?>
                        <strong><?php echo wp_kses_post($order->get_payment_method_title()); ?></strong>
                    </li>
                <?php endif; ?>
            </ul>

        <?php endif; ?>

        <?php echo $thankYou_middle ?>

        <?php do_action('woocommerce_thankyou', $order->get_id()); ?>

        <?php echo $thankYou_after ?>

    <?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text',
                __('Thank you. Your order has been received.', 'woocommerce'), null); ?></p>

    <?php endif; ?>

</div>
