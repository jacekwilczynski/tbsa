<?php
get_header();
if (have_posts()) {
    the_post();
    echo do_shortcode('[offer_page]');
}
get_footer();
