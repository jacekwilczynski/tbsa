<?php

namespace MobilitySoft\TBSA\ValueObject;

use MobilitySoft\TBSA\Entity\BusinessInterface;

final class BusinessRelation
{
    /**
     * @var string
     */
    private $description;

    /**
     * @var BusinessInterface
     */
    private $business;

    /**
     * @var string[]|null
     */
    private $locationsOverride;

    /**
     * BusinessRelation constructor.
     *
     * @param string $description
     * @param BusinessInterface $business
     * @param string[]|null $locationsOverride
     */
    public function __construct(string $description, BusinessInterface $business, ?array $locationsOverride = null)
    {
        $this->description       = $description;
        $this->business          = $business;
        $this->locationsOverride = $locationsOverride;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return BusinessInterface
     */
    public function getBusiness(): BusinessInterface
    {
        return $this->business;
    }

    /**
     * @return string[]
     */
    public function getLocations(): array
    {
        return $this->locationsOverride ?? $this->business->getLocations();
    }
}
