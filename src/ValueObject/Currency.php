<?php

namespace MobilitySoft\TBSA\ValueObject;

class Currency
{
    /**
     * @var float
     */
    private $value;

    /**
     * Currency constructor.
     *
     * @param float $value
     */
    private function __construct(float $value)
    {
        $this->value = $value;
    }

    /**
     * @param float $value
     *
     * @return Currency
     */
    public static function fromFloat(float $value): self
    {
        return new self($value);
    }

    public function __toString(): string
    {
        return (string)$this->getFloat();
    }

    /**
     * @return float
     */
    public function getFloat(): float
    {
        return $this->value;
    }

    public function add(Currency $other): self
    {
        return new self($this->value + $other->value);
    }
}
