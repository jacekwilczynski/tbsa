<?php

namespace MobilitySoft\TBSA\ValueObject;

use MobilitySoft\TBSA\Traits\SerializableToArray;

class EventLocation
{
    use SerializableToArray;

    /**
     * @var string
     */
    private $town;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $address;

    public function __construct(string $town, string $title, string $address)
    {
        $this->town    = $town;
        $this->title   = $title;
        $this->address = $address;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTown(): string
    {
        return $this->town;
    }
}
