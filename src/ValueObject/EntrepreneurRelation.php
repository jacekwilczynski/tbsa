<?php

namespace MobilitySoft\TBSA\ValueObject;

use MobilitySoft\TBSA\Entity\EntrepreneurInterface;

final class EntrepreneurRelation
{
    /**
     * @var string
     */
    private $description;

    /**
     * @var EntrepreneurInterface
     */
    private $entrepreneur;

    /**
     * EntrepreneurRelation constructor.
     *
     * @param string $description
     * @param EntrepreneurInterface $entrepreneur
     */
    public function __construct(string $description, EntrepreneurInterface $entrepreneur)
    {
        $this->description  = $description;
        $this->entrepreneur = $entrepreneur;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return EntrepreneurInterface
     */
    public function getEntrepreneur(): EntrepreneurInterface
    {
        return $this->entrepreneur;
    }
}
