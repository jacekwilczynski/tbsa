<?php

namespace MobilitySoft\TBSA\ValueObject;

final class BranchLocation
{
    /**
     * @var string
     */
    private $town;

    /**
     * @var string
     */
    private $voivodeship;

    /**
     * BranchLocation constructor.
     *
     * @param string $town
     * @param string $voivodeship
     */
    public function __construct(string $town, string $voivodeship)
    {
        $this->town        = $town;
        $this->voivodeship = $voivodeship;
    }

    /**
     * @return string
     */
    public function getTown(): string
    {
        return $this->town;
    }

    /**
     * @return string
     */
    public function getVoivodeship(): string
    {
        return $this->voivodeship;
    }
}
