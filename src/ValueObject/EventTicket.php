<?php

namespace MobilitySoft\TBSA\ValueObject;

class EventTicket
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Currency
     */
    private $price;

    /**
     * @var string
     */
    private $description;

    /**
     * EventTicket constructor.
     *
     * @param string $id
     * @param string $name
     * @param Currency $price
     * @param string $description
     */
    public function __construct(string $id, string $name, Currency $price, string $description = '')
    {
        $this->id          = sanitize_title($id);
        $this->name        = $name;
        $this->price       = $price;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Currency
     */
    public function getPrice(): Currency
    {
        return $this->price;
    }
}
