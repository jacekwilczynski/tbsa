<?php

namespace MobilitySoft\TBSA\ValueObject;

use DateTimeImmutable;
use MobilitySoft\TBSA\Traits\SerializableToArray;

final class Timespan
{
    use SerializableToArray;

    /**
     * @var bool
     */
    private $wholeDay;

    /**
     * @var DateTimeImmutable
     */
    private $start;

    /**
     * @var DateTimeImmutable
     */
    private $end;

    /**
     * TimeSpan constructor.
     *
     * @param bool $wholeDay
     * @param DateTimeImmutable $start
     * @param DateTimeImmutable|null $end
     */
    public function __construct(bool $wholeDay, DateTimeImmutable $start, ?DateTimeImmutable $end = null)
    {
        $this->wholeDay = $wholeDay;
        $this->start    = $start;
        $this->end      = $end ?? $start;
    }

    /**
     * @return bool
     */
    public function isWholeDay(): bool
    {
        return $this->wholeDay;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getStart(): DateTimeImmutable
    {
        return $this->start;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getEnd(): DateTimeImmutable
    {
        return $this->end;
    }
}
