<?php

namespace MobilitySoft\TBSA\ValueObject;

class EventContact
{
    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $email;

    public function __construct(string $phone, string $email)
    {
        $this->phone = $phone;
        $this->email = $email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
