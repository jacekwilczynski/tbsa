<?php

namespace MobilitySoft\TBSA;

const EVENT_PRODUCT_ID                 = 2473;
const MAGAZINE_SUBSCRIPTION_PRODUCT_ID = 4025;
const NEWSLETTER_FORM_ID               = 92;
const MAGAZINE_FORM_ID                 = 4232;
const CONTACT_FORM_ID                  = 4145;

const INITIATIVE_POST_TYPE_SLUG       = 'initiative';
const MAGAZINE_POST_TYPE_SLUG         = 'magazine_issue';
const ENTREPRENEUR_POST_TYPE_SLUG     = 'entrepreneur';
const BUSINESS_POST_TYPE_SLUG         = 'business';
const INDUSTRY_TAXONOMY_SLUG          = 'tbsa_industry';
const LOCATION_TAXONOMY_SLUG          = 'tbsa_location';
const BRANCH_POST_TYPE_SLUG           = 'branch';
const NETWORKING_GROUP_POST_TYPE_SLUG = 'networking_group';
const BOARD_POST_TYPE_SLUG            = 'tbsa_board';
const EVENT_POST_TYPE_SLUG            = 'event';
const EVENT_CATEGORY_POST_TYPE_SLUG   = 'event-categories';
const JOB_OFFER_POST_TYPE_SLUG        = 'tbsa_job_offer';
const TALK_TEMPLATE_POST_TYPE_SLUG    = 'tbsa_talk';
const TALK_SUBMISSION_POST_TYPE_SLUG  = 'tbsa_pres_sub';
