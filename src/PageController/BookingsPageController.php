<?php

namespace MobilitySoft\TBSA\PageController;

use MobilitySoft\TBSA\Entity\Branch;
use MobilitySoft\TBSA\Renderer\BookingsPageRenderer;
use MobilitySoft\TBSA\Repository\BranchRepository;

class BookingsPageController
{
    /**
     * @var BookingsPageRenderer
     */
    private $bookingsPageRenderer;

    /**
     * @var BranchRepository
     */
    private $branchRepository;

    public function __construct(BookingsPageRenderer $bookingsPageRenderer, BranchRepository $branchRepository)
    {
        $this->bookingsPageRenderer = $bookingsPageRenderer;
        $this->branchRepository = $branchRepository;
    }

    public function execute(): void
    {
        $isAdministrator = current_user_can('administrator');
        if (!$isAdministrator && !current_user_can('board_member')) {
            http_response_code(403);
            echo $this->bookingsPageRenderer->renderError(__('Strona dostępna wyłącznie dla członków zarządu oraz administratorów.'));
            exit();
        }

        $time = time();
        $year = (int) ($_GET['rok'] ?? date('Y', $time));
        $month = (int) ($_GET['miesiac'] ?? date('m', $time));

        if (!$year) {
            echo $this->bookingsPageRenderer->renderError('Nieprawidłowy rok.');
            http_response_code(400);
            exit();
        }

        if (!$month) {
            echo $this->bookingsPageRenderer->renderError('Nieprawidłowy miesiąc.');
            http_response_code(400);
            exit();
        }

        $branchId = ($_GET['towarzystwo'] ?? null) ? (int) $_GET['towarzystwo'] : null;
        $showUnavailables = isset($_GET['pokaz_niedostepne']);

        $branches = $this->branchRepository->getNonVirtualBranches();

        if ($branchId > 0) {
            /** @var Branch|null $currentBranch */
            $currentBranch = array_find($branches, function (Branch $branch) use ($branchId) {
                return $branch->getId() == $branchId;
            });

            if ($currentBranch === null) {
                http_response_code(404);
                echo $this->bookingsPageRenderer->renderError(__('Towarzystwo o id ' . $branchId . ' nie istnieje.'));
                exit();
            }
        }

        echo $this->bookingsPageRenderer->render($year, $month, $branches, $branchId, $showUnavailables);
    }
}
