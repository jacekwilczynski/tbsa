<?php

namespace MobilitySoft\TBSA\PageController;

use MobilitySoft\TBSA\Renderer\EventPageRenderer;

class EventPageController
{
    /**
     * @var EventPageRenderer
     */
    private $eventPageRenderer;

    public function __construct(EventPageRenderer $eventPageRenderer)
    {
        $this->eventPageRenderer = $eventPageRenderer;
    }

    public function execute(): void
    {
        get_header();
        echo $this->eventPageRenderer->render(get_the_ID());
        get_footer();
    }
}
