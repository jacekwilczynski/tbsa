<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage;

use MobilitySoft\TBSA\Common\Application\InitializableRenderer;
use MobilitySoft\TBSA\Common\View\Renderer;
use MobilitySoft\TBSA\MyAccountPage\Application\EditEntrepreneur\EditEntrepreneurTab;
use MobilitySoft\TBSA\MyAccountPage\Application\TbsaDashboard;
use MobilitySoft\TBSA\Talk\Application\EditTalkPage\EditTalkRenderer;
use MobilitySoft\TBSA\Talk\Application\Controller\EventsController;
use MobilitySoft\TBSA\Talk\Application\Controller\PopupController;
use MobilitySoft\TBSA\Talk\Application\TalksPage\TalksPageRenderer;
use Psr\Container\ContainerInterface;
use WP_REST_Request;

class MyAccountPage
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var string
     */
    private $currentEndpointTitle;

    /**
     * @var string
     */
    private $currentEndpointSlug;

    /**
     * @var array
     */
    private $parentEndpoints;

    /**
     * @var array
     */
    private $subEndpoints = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function isEnabled(): bool
    {
        return (
            current_user_can('administrator') ||
            (defined('TBSA_ACCOUNT_MANAGEMENT') && TBSA_ACCOUNT_MANAGEMENT)
        );
    }

    public function enable(): void
    {
        add_action('template_redirect', function () {
            if (is_account_page() && !self::isEnabled()) {
                wp_redirect(home_url());
                exit();
            }
        });

        add_action('get_header', function () {
            if (is_account_page()) {
                $this->setupView();
            }
        });

        if (is_user_logged_in()) {
            $this->setupEndpoints();

            add_action('rest_api_init', function () {
                register_rest_route('/tbsa/v1', '/talks/(?P<id>[\d]+)', [
                    'methods'  => 'DELETE',
                    'callback' => function (WP_REST_Request $request) {
                        wp_trash_post($request->get_param('id'));

                        return [
                            'nonce' => wp_create_nonce('wp_rest'),
                        ];
                    },
                ]);
            });

            add_action('wp_ajax_submit_talk_popup_html', function () {
                $this->container->get(PopupController::class)->html();
            });

            add_action('wp_ajax_submit_talk_popup_data', function () {
                $this->container->get(PopupController::class)->data();
            });

            add_action('wp_ajax_submit_talk_events', function () {
                $this->container->get(EventsController::class)->execute();
            });
        }
    }

    private function setupEndpoints(): void
    {
        $menuPosition = 2;

        if (defined('TBSA_ENTREPRENEUR_EDITOR') && TBSA_ENTREPRENEUR_EDITOR) {
            $this->addEndpoint(
                'edit-entrepreneur',
                __('profil-przedsiebiorcy'),
                __('Profil przedsiębiorcy'),
                $this->container->get(EditEntrepreneurTab::class),
                $menuPosition++
            );
        }

        $this->addEndpoint(
            'talks',
            __('prezentacje'),
            __('Prezentacje'),
            $this->container->get(TalksPageRenderer::class),
            $menuPosition
        );

        $this->addEndpoint(
            'create_talk',
            __('utworz-prezentacje'),
            __('Utwórz prezentację'),
            $this->container->get(EditTalkRenderer::class)
                ->withRedirect(wc_get_account_endpoint_url('talks')),
            'talks'
        );

        $this->addEndpoint(
            'edit_talk',
            __('edytuj-prezentacje'),
            __('Edytuj prezentację'),
            $this->container->get(EditTalkRenderer::class),
            'talks'
        );
    }

    private function addEndpoint(
        string $slug,
        string $urlSlug,
        string $title,
        Renderer $renderer,
        $menuPosition = null
    ): void {
        // Make WordPress recognize the new tab URL
        add_action('init', function () use ($urlSlug): void {
            add_rewrite_endpoint($urlSlug, EP_PAGES);
        });

        if (is_string($menuPosition)) {
            $this->parentEndpoints[$menuPosition][] = $slug;
            $this->subEndpoints[$slug] = $title;
        }

        if (is_int($menuPosition)) {
            add_filter(
                'woocommerce_account_menu_items',
                function (array $items) use ($slug, $title, $menuPosition): array {
                    return array_insert($items, [$slug => $title], $menuPosition);
                }
            );
        }

        // Map the fixed slug used in code to the user-friendy & translatable slug visible in production
        add_filter(
            'woocommerce_get_query_vars',
            function (array $vars) use ($slug, $urlSlug): array {
                $vars[$slug] = $urlSlug;
                return $vars;
            }
        );

        add_action("woocommerce_account_{$slug}_endpoint", function () use ($renderer): void {
            echo $renderer->render();
        });

        if ($renderer instanceof InitializableRenderer) {
            add_action('get_header', function () use ($slug, $renderer): void {
                if ($this->isCurrentEndpoint($slug)) {
                    $renderer->initialize();
                }
            });
        }
    }

    private function isCurrentEndpoint(string $slug, bool $includeSubEndpoints = false): bool
    {
        global $wp;
        $queryVars = $wp->query_vars ?? [];

        $isCurrent = array_key_exists($slug, $queryVars);

        if (!$isCurrent && $includeSubEndpoints && isset($this->parentEndpoints[$slug])) {
            $isCurrent = (bool) array_intersect(array_keys($queryVars), $this->parentEndpoints[$slug]);
        }

        if (!$isCurrent && isset($this->currentEndpointSlug)) {
            $isCurrent = $slug === 'dashboard' && $this->currentEndpointSlug === 'dashboard';
        }

        return $isCurrent;
    }

    private function setupView(): void
    {
        add_action('woocommerce_before_account_navigation', function (): void {
            printf('<div class="my-account-panel__menu"><h1>%s</h1>', get_the_title());
        });

        add_action('woocommerce_after_account_navigation', function (): void {
            echo '</div>';
        });

        add_filter('woocommerce_account_menu_items', function (array $items): array {
            unset($items['downloads']);

            $combinedItems = array_merge($items, $this->subEndpoints);
            $this->currentEndpointSlug = array_find(array_keys($combinedItems), function (string $slug) {
                    return $this->isCurrentEndpoint($slug);
                }) ?? 'dashboard';

            $this->currentEndpointTitle = $combinedItems[$this->currentEndpointSlug];

            return $items;
        });

        add_action('woocommerce_account_menu_item_classes', function (array $classes, string $endpointSlug): array {
            if (
                $this->isCurrentEndpoint($endpointSlug, true) &&
                !in_array('is-active', $classes)
            ) {
                $classes[] = 'is-active';
            }

            return $classes;
        }, 10, 2);

        add_action('woocommerce_account_content', function (): void {
            printf('<h2>%s</h2>', $this->currentEndpointTitle);
        }, 0);

        add_action('woocommerce_account_content', function (): void {
            if (is_url(wc_get_account_endpoint_url('dashboard'))) {
                echo $this->container->get(TbsaDashboard::class)->render();
                remove_all_actions('woocommerce_account_content');
            }
        }, 0);

        add_action('wp_enqueue_scripts', function () {
            wp_enqueue_style(
                'my-account-style',
                get_theme_file_uri('/build/myAccount-style.css'),
                defined('TBSA_DEV_MODE') && TBSA_DEV_MODE ? time() : '1.1'
            );

            wp_enqueue_script(
                'my-account-script',
                get_theme_file_uri('/build/myAccount-script.js'),
                'jquery',
                defined('TBSA_DEV_MODE') && TBSA_DEV_MODE ? time() : '1.1',
                true
            );
        });
    }
}
