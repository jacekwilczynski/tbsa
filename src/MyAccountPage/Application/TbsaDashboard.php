<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Application;

use MobilitySoft\TBSA\MyAccountPage\Domain\User\User;
use Twig\Environment as Twig;

class TbsaDashboard
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var User
     */
    private $user;

    public function __construct(Twig $twig, User $user)
    {
        $this->twig = $twig;
        $this->user = $user;
    }

    public function render(): string
    {
        return $this->twig->render('tbsaDashboard.twig', [
            'texts' => [
                'welcome' => sprintf(
                    __('Hello %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'woocommerce'),
                    '<strong>' . esc_html($this->user->getName()->getDisplayName()) . '</strong>',
                    esc_url(wc_logout_url(wc_get_page_permalink('myaccount')))
                ),
            ],
        ]);
    }
}
