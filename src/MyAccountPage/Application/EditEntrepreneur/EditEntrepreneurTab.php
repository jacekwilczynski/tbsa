<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Application\EditEntrepreneur;

use MobilitySoft\TBSA\Common\Application\InitializableRenderer;
use MobilitySoft\TBSA\MyAccountPage\Domain\User\User;
use Twig\Environment as Twig;

class EditEntrepreneurTab implements InitializableRenderer
{
    /**
     * @var EntrepreneurForm
     */
    private $form;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var User|null
     */
    private $user;

    public function __construct(EntrepreneurForm $form, Twig $twig, ?User $user)
    {
        $this->form = $form;
        $this->twig = $twig;
        $this->user = $user;
    }

    public function render(): string
    {
        $entrepreneur = $this->user->getEntrepreneur();

        if ($entrepreneur) {
            $context = [
                'form' => $this->form->render($entrepreneur),
            ];
        } else {
            $context = [
                'message' => __(
                    'Nie posiadasz swojego profilu w wyszukiwarce firm Towarzystw Biznesowych.',
                    'tbsa_my-account'
                ),
            ];
        }

        return $this->twig->render('myAccount/editEntrepreneur.twig', $context);
    }

    public function initialize(): void
    {
        acf_form_head();
    }
}
