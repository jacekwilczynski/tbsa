<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Application\EditEntrepreneur;

use MobilitySoft\TBSA\MyAccountPage\Domain\Entrepreneur\Entrepreneur;

class EntrepreneurForm
{
    public function render(Entrepreneur $entrepreneur): string
    {
        ob_start();
        acf_form([
            'post_id'    => $entrepreneur->getId(),
            'fields'     => [
                'photo',
                'description',
                'external_links',
            ],
        ]);

        return ob_get_clean();
    }
}
