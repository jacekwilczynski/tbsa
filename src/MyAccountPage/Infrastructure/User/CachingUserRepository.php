<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Infrastructure\User;

use MobilitySoft\TBSA\MyAccountPage\Domain\User\User;
use MobilitySoft\TBSA\MyAccountPage\Domain\User\UserRepository as UserRepositoryInterface;

class CachingUserRepository implements UserRepositoryInterface
{
    /**
     * @var UserRepository
     */
    private $source;

    /**
     * @var array
     */
    private $usersById = [];

    public function __construct(UserRepository $source)
    {
        $this->source = $source;
    }

    public function getUser(int $id): ?User
    {
        if (empty($this->usersById[$id])) {
            $this->usersById[$id] = $this->source->getUser($id);
        }

        return $this->usersById[$id];
    }
}
