<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Infrastructure\User;

use MobilitySoft\TBSA\MyAccountPage\Domain\Entrepreneur\EntrepreneurRepository;
use MobilitySoft\TBSA\MyAccountPage\Domain\User\User;
use MobilitySoft\TBSA\MyAccountPage\Domain\User\UserName;
use MobilitySoft\TBSA\MyAccountPage\Domain\User\UserRepository as UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    public function __construct(EntrepreneurRepository $entrepreneurRepository)
    {
        $this->entrepreneurRepository = $entrepreneurRepository;
    }

    public function getUser(int $id): ?User
    {
        $wpUser = get_userdata($id);

        if ($wpUser) {
            $name           = new UserName($wpUser->display_name);
            $entrepreneurId = get_field('entrepreneur', $wpUser);
            $entrepreneur   = is_int($entrepreneurId) ? $this->entrepreneurRepository->getById($entrepreneurId) : null;

            return new User($id, $name, $entrepreneur);
        }

        return null;
    }
}
