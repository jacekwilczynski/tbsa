<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Infrastructure\Entrepreneur;

use MobilitySoft\TBSA\MyAccountPage\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\MyAccountPage\Domain\Entrepreneur\EntrepreneurRepository as EntrepreneurRepositoryInterface;

class EntrepreneurRepository implements EntrepreneurRepositoryInterface
{
    public function getById(int $id): ?Entrepreneur
    {
        $postStatus = get_post_status($id);

        if ($postStatus && $postStatus !== 'trash') {
            return new Entrepreneur($id);
        }

        return null;
    }
}
