<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Domain\User;

interface UserRepository
{
    public function getUser(int $id): ?User;
}
