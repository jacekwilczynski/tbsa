<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Domain\User;

use MobilitySoft\TBSA\MyAccountPage\Domain\Entrepreneur\Entrepreneur;

class User
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var Entrepreneur|null
     */
    private $entrepreneur;

    public function __construct(int $id, UserName $name, ?Entrepreneur $entrepreneur)
    {
        $this->id           = $id;
        $this->name         = $name;
        $this->entrepreneur = $entrepreneur;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): UserName
    {
        return $this->name;
    }

    public function getEntrepreneur(): ?Entrepreneur
    {
        return $this->entrepreneur;
    }
}
