<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Domain\User;

class UserName
{
    /**
     * @var string
     */
    private $displayName;

    public function __construct(string $displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }
}
