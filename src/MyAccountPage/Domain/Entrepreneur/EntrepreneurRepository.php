<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\MyAccountPage\Domain\Entrepreneur;

interface EntrepreneurRepository
{
    public function getById(int $id): ?Entrepreneur;
}
