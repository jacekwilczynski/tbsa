<?php

namespace MobilitySoft\TBSA\FilterHandler;

/**
 * Interface Filterer
 * @package MobilitySoft\TBSA\HtmlInjector
 */
interface FilterHandler
{
    /**
     * @param string $content
     *
     * @return string
     */
    public function filter(string $content): string;
}
