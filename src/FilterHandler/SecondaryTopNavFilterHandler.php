<?php

namespace MobilitySoft\TBSA\FilterHandler;

use MobilitySoft\TBSA\Renderer\Renderer;
use Wa72\HtmlPageDom\HtmlPageCrawler;

/**
 * Class SecondaryTopNavInjector
 * @package MobilitySoft\TBSA\FilterHandler
 */
final class SecondaryTopNavFilterHandler implements FilterHandler
{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * SecondaryTopNavInjector constructor.
     *
     * @param Renderer $renderer
     */
    public function __construct(Renderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public function filter(string $content): string
    {
        $dom        = HtmlPageCrawler::create($content);
        $topMenuNav = $dom->filter('#top-menu-nav');
        $topMenuNav->prepend($this->renderer->render());

        return $dom->saveHTML();
    }
}
