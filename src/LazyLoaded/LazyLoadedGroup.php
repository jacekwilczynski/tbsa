<?php

namespace MobilitySoft\TBSA\LazyLoaded;

use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Entity\GroupInterface;
use MobilitySoft\TBSA\Repository\GroupRepository;
use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

final class LazyLoadedGroup implements GroupInterface
{
    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * @var GroupInterface
     */
    private $group;

    /**
     * @var string
     */
    private $id;

    /**
     * LazyLoadedGroup constructor.
     *
     * @param GroupRepository $groupRepository
     * @param string $id
     */
    public function __construct(GroupRepository $groupRepository, string $id)
    {
        $this->groupRepository = $groupRepository;
        $this->id              = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        $this->ensureData();

        return $this->group->getId();
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->ensureData();
        $this->group->setId($id);
    }

    private function ensureData(): void
    {
        if ( ! isset($this->group)) {
            $this->group = $this->groupRepository->getById($this->id);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $this->ensureData();

        return $this->group->getName();
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->ensureData();
        $this->group->setName($name);
    }

    /**
     * @return EntrepreneurRelation[]
     */
    public function getMembers(): array
    {
        $this->ensureData();

        return $this->group->getMembers();
    }

    /**
     * @param EntrepreneurRelation[] $members
     */
    public function setMembers(array $members): void
    {
        $this->ensureData();
        $this->group->setMembers($members);
    }

    /**
     * @return BranchInterface|null
     */
    public function getBranch(): ?BranchInterface
    {
        $this->ensureData();

        return $this->group->getBranch();
    }

    /**
     * @param BranchInterface|null $branch
     */
    public function setBranch(?BranchInterface $branch): void
    {
        $this->ensureData();
        $this->group->setBranch($branch);
    }

    /**
     * @return BoardInterface|null
     */
    public function getLocalBoard(): ?BoardInterface
    {
        $this->ensureData();

        return $this->group->getLocalBoard();
    }

    /**
     * @param BoardInterface|null $localBoard
     */
    public function setLocalBoard(?BoardInterface $localBoard): void
    {
        $this->ensureData();
        $this->group->setLocalBoard($localBoard);
    }
}
