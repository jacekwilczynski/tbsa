<?php

namespace MobilitySoft\TBSA\LazyLoaded;

use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Entity\EventCategory;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Entity\GroupInterface;
use MobilitySoft\TBSA\EntityGroupInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\ValueObject\EventBooking;
use MobilitySoft\TBSA\ValueObject\EventContact;
use MobilitySoft\TBSA\ValueObject\EventLocation;
use MobilitySoft\TBSA\ValueObject\EventTicket;
use MobilitySoft\TBSA\ValueObject\Timespan;

final class LazyLoadedEvent implements EventInterface
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var EventInterface
     */
    private $event;

    /**
     * @var string
     */
    private $id;

    /**
     * LazyLoadedEvent constructor.
     *
     * @param EventRepositoryInterface $eventRepository
     * @param string $id
     */
    public function __construct(EventRepositoryInterface $eventRepository, string $id)
    {
        $this->eventRepository = $eventRepository;
        $this->id              = $id;
    }

    /**
     * @return GroupInterface|null
     */
    public function getGroup(): ?GroupInterface
    {
        $this->ensureData();

        return $this->event->getGroup();
    }

    /**
     * @param GroupInterface|null $group
     */
    public function setGroup(?GroupInterface $group): void
    {
        $this->ensureData();
        $this->event->setGroup($group);
    }

    /**
     * @return string
     */
    public function getExcerpt(): string
    {
        $this->ensureData();

        return $this->event->getExcerpt();
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt): void
    {
        $this->ensureData();
        $this->event->setExcerpt($excerpt);
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        $this->ensureData();

        return $this->event->getImage();
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->ensureData();
        $this->event->setImage($image);
    }

    /**
     * @return EventLocation|null
     */
    public function getLocation(): ?EventLocation
    {
        $this->ensureData();

        return $this->event->getLocation();
    }

    /**
     * @param EventLocation|null $location
     */
    public function setLocation(?EventLocation $location): void
    {
        $this->ensureData();
        $this->event->setLocation($location);
    }

    /**
     * @return Timespan|null
     */
    public function getTimespan(): ?Timespan
    {
        $this->ensureData();

        return $this->event->getTimespan();
    }

    /**
     * @param Timespan|null $timespan
     */
    public function setTimespan(?Timespan $timespan): void
    {
        $this->ensureData();
        $this->event->setTimespan($timespan);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        $this->ensureData();

        return $this->event->getId();
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->ensureData();
        $this->event->setId($id);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        $this->ensureData();

        return $this->event->getTitle();
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->ensureData();
        $this->event->setTitle($title);
    }

    /**
     * @return EventCategory|null
     */
    public function getCategory(): ?EventCategory
    {
        $this->ensureData();

        return $this->event->getCategory();
    }

    /**
     * @param EventCategory|null $category
     */
    public function setCategory(?EventCategory $category): void
    {
        $this->ensureData();
        $this->event->setCategory($category);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $this->ensureData();

        return $this->event->toArray();
    }

    /**
     * @return EventContact
     */
    public function getContact(): EventContact
    {
        $this->ensureData();

        return $this->event->getContact();
    }

    /**
     * @param EventContact $links
     */
    public function setContact(EventContact $links): void
    {
        $this->ensureData();
        $this->event->setContact($links);
    }

    /**
     * @return BranchInterface|null
     */
    public function getBranch(): ?BranchInterface
    {
        $this->ensureData();

        return $this->event->getBranch();
    }

    /**
     * @param BranchInterface|null $branch
     */
    public function setBranch(?BranchInterface $branch): void
    {
        $this->ensureData();
        $this->event->setBranch($branch);
    }

    /**
     * @return EventBooking[]
     */
    public function getBookings(): array
    {
        $this->ensureData();

        return $this->event->getBookings();
    }

    /**
     * @param EventBooking[] $bookings
     */
    public function setBookings(array $bookings): void
    {
        $this->ensureData();
        $this->event->setBookings($bookings);
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        $this->ensureData();

        return $this->event->getPrice();
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->ensureData();
        $this->event->setPrice($price);
    }

    /**
     * @return bool
     */
    public function isExternal(): bool
    {
        $this->ensureData();

        return $this->event->isExternal();
    }

    /**
     * @param bool $isExternal
     */
    public function setIsExternal(bool $isExternal): void
    {
        $this->ensureData();
        $this->setIsExternal($isExternal);
    }

    /**
     * @return string|null
     */
    public function getExternalUrl(): ?string
    {
        $this->ensureData();

        return $this->event->getExternalUrl();
    }

    /**
     * @param string|null $externalUrl
     */
    public function setExternalUrl(?string $externalUrl): void
    {
        $this->ensureData();
        $this->setExternalUrl($externalUrl);
    }

    /**
     * @return EventTicket[]
     */
    public function getTickets(): array
    {
        $this->ensureData();

        return $this->event->getTickets();
    }

    /**
     * @param EventTicket[] $tickets
     */
    public function setTickets(array $tickets): void
    {
        $this->ensureData();
        $this->event->setTickets($tickets);
    }

    public function hasBookingsEnabled(): bool
    {
        $this->ensureData();

        return $this->event->hasBookingsEnabled();
    }

    public function setBookingsEnabled(bool $bookable): void
    {
        $this->ensureData();
        $this->event->setBookingsEnabled($bookable);
    }

    private function ensureData(): void
    {
        if ( ! isset($this->event)) {
            $this->event = $this->eventRepository->getById($this->id);
        }
    }
}
