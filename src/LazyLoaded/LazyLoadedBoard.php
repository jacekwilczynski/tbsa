<?php

namespace MobilitySoft\TBSA\LazyLoaded;

use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\Group;
use MobilitySoft\TBSA\Repository\BoardRepository;
use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

final class LazyLoadedBoard implements BoardInterface
{
    /**
     * @var BoardRepository
     */
    private $boardRepository;

    /**
     * @var BoardInterface
     */
    private $board;

    /**
     * @var string
     */
    private $id;

    /**
     * LazyLoadedBoard constructor.
     *
     * @param BoardRepository $boardRepository
     * @param string $id
     */
    public function __construct(BoardRepository $boardRepository, string $id)
    {
        $this->boardRepository = $boardRepository;
        $this->id              = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        $this->ensureData();

        return $this->board->getId();
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->ensureData();
        $this->board->setId($id);
    }

    private function ensureData(): void
    {
        if ( ! isset($this->board)) {
            $this->board = $this->boardRepository->getById($this->id);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $this->ensureData();

        return $this->board->getName();
    }

    /**
     * @return EntrepreneurRelation[]
     */
    public function getMembers(): array
    {
        $this->ensureData();

        return $this->board->getMembers();
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->ensureData();
        $this->board->setName($name);
    }

    /**
     * @param array $members
     */
    public function setMembers(array $members): void
    {
        $this->ensureData();
        $this->board->setMembers($members);
    }

    /**
     * @return Group[]
     */
    public function getGroups(): array
    {
        $this->ensureData();

        return $this->board->getGroups();
    }

    /**
     * @param Group[] $groups
     */
    public function setGroups(array $groups): void
    {
        $this->ensureData();
        $this->board->setGroups($groups);
    }
}
