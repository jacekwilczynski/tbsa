<?php

namespace MobilitySoft\TBSA\LazyLoaded;

use MobilitySoft\TBSA\Entity\BusinessInterface;
use MobilitySoft\TBSA\Repository\BusinessRepository;
use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

final class LazyLoadedBusiness implements BusinessInterface
{
    /**
     * @var BusinessRepository
     */
    private $businessRepository;

    /**
     * @var BusinessInterface
     */
    private $business;

    /**
     * @var string
     */
    private $id;

    /**
     * LazyLoadedBusiness constructor.
     *
     * @param BusinessRepository $businessRepository
     * @param string $id
     */
    public function __construct(BusinessRepository $businessRepository, string $id)
    {
        $this->businessRepository = $businessRepository;
        $this->id                 = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        $this->ensureData();

        return $this->business->getId();
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->ensureData();
        $this->business->setId($id);
    }

    private function ensureData(): void
    {
        if ( ! isset($this->business)) {
            $this->business = $this->businessRepository->getById($this->id);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $this->ensureData();

        return $this->business->getName();
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->ensureData();
        $this->business->setName($name);
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        $this->ensureData();

        return $this->business->getLogo();
    }

    /**
     * @param string $logo
     */
    public function setLogo(string $logo): void
    {
        $this->ensureData();
        $this->business->setLogo($logo);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        $this->ensureData();

        return $this->business->getUrl();
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->ensureData();
        $this->business->setUrl($url);
    }

    /**
     * @return string[]
     */
    public function getIndustries(): array
    {
        $this->ensureData();

        return $this->business->getIndustries();
    }

    /**
     * @param string[] $industries
     */
    public function setIndustries(array $industries): void
    {
        $this->ensureData();
        $this->business->setIndustries($industries);
    }

    /**
     * @return string[]
     */
    public function getLocations(): array
    {
        $this->ensureData();

        return $this->business->getLocations();
    }

    /**
     * @param string[] $locations
     */
    public function setLocations(array $locations): void
    {
        $this->ensureData();
        $this->business->setLocations($locations);
    }

    /**
     * @return EntrepreneurRelation[]
     */
    public function getEntrepreneurRelations(): array
    {
        $this->ensureData();

        return $this->business->getEntrepreneurRelations();
    }

    /**
     * @param EntrepreneurRelation[] $entrepreneurRelations
     */
    public function setEntrepreneurRelations(array $entrepreneurRelations): void
    {
        $this->ensureData();
        $this->business->setEntrepreneurRelations($entrepreneurRelations);
    }
}
