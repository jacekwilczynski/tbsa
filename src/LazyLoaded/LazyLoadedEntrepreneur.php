<?php

namespace MobilitySoft\TBSA\LazyLoaded;

use MobilitySoft\TBSA\Entity\BusinessRelation;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\Entity\GroupInterface;
use MobilitySoft\TBSA\Entity\Link;
use MobilitySoft\TBSA\Entity\MediaLink;
use MobilitySoft\TBSA\Repository\EntrepreneurRepository;

final class LazyLoadedEntrepreneur implements EntrepreneurInterface
{
    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var EntrepreneurInterface
     */
    private $entrepreneur;

    /**
     * @var string
     */
    private $id;

    /**
     * LazyLoadedEntrepreneur constructor.
     *
     * @param EntrepreneurRepository $entrepreneurRepository
     * @param string $id
     */
    public function __construct(EntrepreneurRepository $entrepreneurRepository, string $id)
    {
        $this->entrepreneurRepository = $entrepreneurRepository;
        $this->id                     = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        $this->ensureData();

        return $this->entrepreneur->getId();
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->ensureData();
        $this->entrepreneur->setId($id);
    }

    private function ensureData(): void
    {
        if ( ! isset($this->entrepreneur)) {
            $this->entrepreneur = $this->entrepreneurRepository->getById($this->id);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $this->ensureData();

        return $this->entrepreneur->getName();
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->ensureData();
        $this->entrepreneur->setName($name);
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        $this->ensureData();

        return $this->entrepreneur->getPhoto();
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void
    {
        $this->ensureData();
        $this->entrepreneur->setPhoto($photo);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        $this->ensureData();

        return $this->entrepreneur->getDescription();
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->ensureData();
        $this->entrepreneur->setDescription($description);
    }

    /**
     * @return string[]
     */
    public function getActivities(): array
    {
        $this->ensureData();

        return $this->entrepreneur->getActivities();
    }

    /**
     * @param string[] $activities
     */
    public function setActivities(array $activities): void
    {
        $this->ensureData();
        $this->entrepreneur->setActivities($activities);
    }

    /**
     * @return MediaLink[]
     */
    public function getMedia(): array
    {
        $this->ensureData();

        return $this->entrepreneur->getMedia();
    }

    /**
     * @param MediaLink[] $media
     */
    public function setMedia(array $media): void
    {
        $this->ensureData();
        $this->entrepreneur->setMedia($media);
    }

    /**
     * @return Link[]
     */
    public function getLinks(): array
    {
        $this->ensureData();

        return $this->entrepreneur->getLinks();
    }

    /**
     * @param Link[] $links
     */
    public function setLinks(array $links): void
    {
        $this->ensureData();
        $this->entrepreneur->setLinks($links);
    }

    /**
     * @return BusinessRelation[]
     */
    public function getBusinessRelations(): array
    {
        $this->ensureData();

        return $this->entrepreneur->getBusinessRelations();
    }

    /**
     * @param BusinessRelation[] $businessRelations
     */
    public function setBusinessRelations(array $businessRelations): void
    {
        $this->ensureData();
        $this->entrepreneur->setBusinessRelations($businessRelations);
    }

    /**
     * @return bool
     */
    public function isStarred(): bool
    {
        $this->ensureData();

        return $this->entrepreneur->isStarred();
    }

    /**
     * @param bool $isStarred
     */
    public function setIsStarred(bool $isStarred): void
    {
        $this->entrepreneur->setIsStarred($isStarred);
    }

    /**
     * @return GroupInterface[]
     */
    public function getGroups(): array
    {
        $this->ensureData();

        return $this->entrepreneur->getGroups();
    }

    /**
     * @param GroupInterface[] $groups
     */
    public function setGroups(array $groups): void
    {
        $this->ensureData();
        $this->entrepreneur->setGroups($groups);
    }
}
