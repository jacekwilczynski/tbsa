<?php

namespace MobilitySoft\TBSA\LazyLoaded;

use MobilitySoft\TBSA\DTO\Noun;
use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\Entity\GroupInterface;
use MobilitySoft\TBSA\Repository\BranchRepository;
use MobilitySoft\TBSA\ValueObject\BranchLocation;

final class LazyLoadedBranch implements BranchInterface
{
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var BranchInterface
     */
    private $branch;

    /**
     * @var string
     */
    private $id;

    /**
     * LazyLoadedBranch constructor.
     *
     * @param BranchRepository $branchRepository
     * @param string $id
     */
    public function __construct(BranchRepository $branchRepository, string $id)
    {
        $this->branchRepository = $branchRepository;
        $this->id               = $id;
    }

    /**
     * @return string[]
     */
    public function getPhotos(): array
    {
        $this->ensureData();

        return $this->branch->getPhotos();
    }

    private function ensureData(): void
    {
        if ( ! isset($this->branchp)) {
            $this->branch = $this->branchRepository->getById($this->id);
        }
    }

    /**
     * @param string[] $photos
     */
    public function setPhotos(array $photos): void
    {
        $this->ensureData();
        $this->branch->setPhotos($photos);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        $this->ensureData();

        return $this->branch->getId();
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->ensureData();
        $this->branch->setId($id);
    }

    /**
     * @return Noun
     */
    public function getName(): Noun
    {
        $this->ensureData();

        return $this->branch->getName();
    }

    /**
     * @param Noun $name
     */
    public function setName(Noun $name): void
    {
        $this->ensureData();
        $this->branch->setName($name);
    }

    /**
     * @return BoardInterface|null
     */
    public function getRegionalBoard(): ?BoardInterface
    {
        $this->ensureData();

        return $this->branch->getRegionalBoard();
    }

    /**
     * @param BoardInterface|null $regionalBoard
     */
    public function setRegionalBoard(?BoardInterface $regionalBoard): void
    {
        $this->ensureData();
        $this->branch->setRegionalBoard($regionalBoard);
    }

    /**
     * @return GroupInterface[]
     */
    public function getGroups(): array
    {
        $this->ensureData();

        return $this->branch->getGroups();
    }

    /**
     * @param GroupInterface[] $groups
     */
    public function setGroups(array $groups): void
    {
        $this->ensureData();
        $this->branch->setGroups($groups);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        $this->ensureData();

        return $this->branch->getUrl();
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->ensureData();
        $this->branch->setUrl($url);
    }

    /**
     * @return BranchLocation|null
     */
    public function getLocation(): ?BranchLocation
    {
        $this->ensureData();

        return $this->branch->getLocation();
    }

    /**
     * @param BranchLocation|null $location
     */
    public function setLocation(?BranchLocation $location): void
    {
        $this->ensureData();
        $this->branch->setLocation($location);
    }

    /**
     * @return EntrepreneurInterface|null
     */
    public function getContact(): ?EntrepreneurInterface
    {
        $this->ensureData();

        return $this->branch->getContact();
    }

    /**
     * @param EntrepreneurInterface|null $contact
     */
    public function setContact(?EntrepreneurInterface $contact): void
    {
        $this->ensureData();
        $this->branch->setContact($contact);
    }
}
