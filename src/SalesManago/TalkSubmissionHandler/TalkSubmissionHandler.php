<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\TalkSubmissionHandler;

use MobilitySoft\TBSA\SalesManago\Client\Contact;
use MobilitySoft\TBSA\SalesManago\Client\SalesManagoClient;
use MobilitySoft\TBSA\Talk\Domain\Speaker\Speaker;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;

class TalkSubmissionHandler
{
    /**
     * @var SalesManagoClient
     */
    private $client;

    public function __construct(SalesManagoClient $client)
    {
        $this->client = $client;
    }

    public function execute(Submission $submission): void
    {
        foreach ($submission->getSpeakers()->getItems() as $speaker) {
            $contact = $this->createContactFromSpeaker($speaker);
            $this->client->upsertContact($contact);
        }
    }

    private function createContactFromSpeaker(Speaker $speaker): Contact
    {
        return (new Contact($speaker->getContact()->getEmail()))
            ->setPhone($speaker->getContact()->getPhone())
            ->setName($speaker->getName())
            ->addTags('TB_PRELEGENT');
    }
}
