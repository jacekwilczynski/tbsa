<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\BookingCount;

class BuyerRepository
{
    /**
     * @var \wpdb
     */
    private $wpdb;

    /**
     * @var Buyer[]
     */
    private $buyersByEmail = [];

    public function __construct(\wpdb $wpdb)
    {
        $this->wpdb = $wpdb;
    }

    /**
     * @throws \RuntimeException
     */
    public function put(Buyer $buyer): void
    {
        if (in_array($buyer, $this->buyersByEmail, true)) {
            $query = $this->wpdb->prepare(
                'UPDATE ' . BuyerMapping::getTableName() .
                ' SET ' . BuyerMapping::COLUMN_NUMBER_OF_EXPORTED_ORDERS . ' = %d' .
                ' WHERE ' . BuyerMapping::COLUMN_EMAIL . ' = %s',
                $buyer->getNumberOfExportedOrders(),
                $buyer->getEmail()
            );
            file_put_contents('log.txt', print_r($query, true) . PHP_EOL, FILE_APPEND);
            $success = $this->wpdb->query($query);
        } else {
            $success = $this->wpdb->query($this->wpdb->prepare(
                'INSERT INTO ' . BuyerMapping::getTableName() .
                '(' . BuyerMapping::COLUMN_EMAIL . ', ' . BuyerMapping::COLUMN_NUMBER_OF_EXPORTED_ORDERS . ') ' .
                'VALUES (%s, %d)',
                $buyer->getEmail(),
                $buyer->getNumberOfExportedOrders()
            ));
        }

        if (!$success) {
            throw new \RuntimeException($this->wpdb->last_error);
        }
    }

    public function findByEmail(string $email): ?Buyer
    {
        $query = $this->wpdb->prepare(
            'SELECT * FROM ' . BuyerMapping::getTableName() .
            ' WHERE ' . BuyerMapping::COLUMN_EMAIL . ' = %s',
            $email
        );

        $result = $this->wpdb->get_row($query, ARRAY_A);

        if ($result === null) {
            return null;
        }

        $buyer = new Buyer(
            $result[BuyerMapping::COLUMN_EMAIL],
            (int) $result[BuyerMapping::COLUMN_NUMBER_OF_EXPORTED_ORDERS]
        );

        $this->buyersByEmail[$buyer->getEmail()] = $buyer;

        return $buyer;
    }
}
