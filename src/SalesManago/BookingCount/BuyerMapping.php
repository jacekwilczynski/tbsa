<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\BookingCount;

class BuyerMapping
{
    private const TABLE = 'salesmanago_buyers';
    public const COLUMN_EMAIL = 'email';
    public const COLUMN_NUMBER_OF_EXPORTED_ORDERS = 'number_of_exported_orders';

    public static function getTableName(): string
    {
        global $table_prefix;
        return $table_prefix . self::TABLE;
    }
}
