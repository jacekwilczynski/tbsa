<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\BookingCount;

class Buyer
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $numberOfExportedOrders;

    /**
     * @throws \InvalidArgumentException
     */
    public function __construct(string $email, int $numberOfExportedOrders = 0)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException("'$email' is not a valid e-mail address.");
        }
        $this->email = $email;
        $this->numberOfExportedOrders = $numberOfExportedOrders;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getNumberOfExportedOrders(): int
    {
        return $this->numberOfExportedOrders;
    }

    public function addExportedOrder(): void
    {
        $this->numberOfExportedOrders++;
    }
}
