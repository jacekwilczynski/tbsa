<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\BookingCount;

class BuyerMigration
{
    /**
     * @var \wpdb
     */
    private $wpdb;

    public function __construct(\wpdb $wpdb)
    {
        $this->wpdb = $wpdb;
    }

    /**
     * @throws \RuntimeException
     */
    public function up(): void
    {
        $success = $this->wpdb->query(sprintf(
            '
                CREATE TABLE %s (
                    %s VARCHAR(255) NOT NULL PRIMARY KEY,
                    %s INT NOT NULL DEFAULT 0
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ',
            BuyerMapping::getTableName(),
            BuyerMapping::COLUMN_EMAIL,
            BuyerMapping::COLUMN_NUMBER_OF_EXPORTED_ORDERS
        ));
        if (!$success) {
            throw new \RuntimeException('Failed to add buyers table. ' . $this->wpdb->last_error);
        }
    }

    /**
     * @throws \RuntimeException
     */
    public function down(): void
    {
        $success = $this->wpdb->query('DROP TABLE ' . BuyerMapping::getTableName());
        if (!$success) {
            throw new \RuntimeException('Failed to drop buyers table. ' . $this->wpdb->last_error);
        }
    }
}
