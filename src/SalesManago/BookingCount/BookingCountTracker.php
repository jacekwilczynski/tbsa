<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\BookingCount;

class BookingCountTracker
{
    /**
     * @var BuyerRepository
     */
    private $buyerRepository;

    public function __construct(BuyerRepository $buyerRepository)
    {
        $this->buyerRepository = $buyerRepository;
    }

    public function getNumberOfBookingsSoFar(string $email): int
    {
        $buyer = $this->buyerRepository->findByEmail($email);
        return $buyer !== null ? $buyer->getNumberOfExportedOrders() : 0;
    }

    public function addBooking(\WC_Order $order): void
    {
        $order->update_meta_data('exported_to_salesmanago', 1);
        $order->save();

        $email = $order->get_billing_email();
        $buyer = $this->buyerRepository->findByEmail($email);
        if ($buyer === null) {
            $buyer = new Buyer($email);
        }
        $buyer->addExportedOrder();
        $this->buyerRepository->put($buyer);
    }
}
