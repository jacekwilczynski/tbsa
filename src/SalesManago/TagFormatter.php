<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago;

class TagFormatter
{
    public function format(string $input): string
    {
        return strtoupper(str_replace('-', '_', sanitize_title($input)));
    }
}
