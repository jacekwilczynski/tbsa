<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\IdentificationDataProvider;

use MobilitySoft\TBSA\SalesManago\Client\SalesManagoIdentificationData;

class SalesManagoIdentificationDataProvider
{
    private const CONSTANTS = [
        'SALESMANAGO_CLIENT_ID',
        'SALESMANAGO_API_KEY',
        'SALESMANAGO_SHA',
        'SALESMANAGO_OWNER',
    ];

    public function getData(): SalesManagoIdentificationData
    {
        $this->ensureNoMissingConstants();
        return new SalesManagoIdentificationData(
            SALESMANAGO_CLIENT_ID,
            SALESMANAGO_API_KEY,
            SALESMANAGO_SHA,
            SALESMANAGO_OWNER
        );
    }

    private function ensureNoMissingConstants(): void
    {
        $problems = $this->findProblems();
        if (count($problems) > 0) {
            throw new UnusableCredentialsException($problems);
        }
    }

    private function findProblems(): array
    {
        $problems = [];
        foreach (self::CONSTANTS as $constant) {
            $problem = $this->findProblem($constant);
            if ($problem !== '') {
                $problems[$constant] = $problem;
            }
        }
        return $problems;
    }

    private function findProblem(string $constant): string
    {
        if (!defined($constant)) {
            return 'not defined';
        }
        $value = constant($constant);
        if (!is_string($value)) {
            return 'not string';
        }
        if (trim($value) === '') {
            return 'empty';
        }
        return '';
    }
}
