<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\IdentificationDataProvider;

class UnusableCredentialsException extends \RuntimeException
{
    public function __construct(array $problems)
    {
        $problemList = implode('; ', array_map(
            static function (string $problem, string $constant): string {
                return "$constant: $problem";
            }, $problems, array_keys($problems)
        ));
        parent::__construct(
            'Following problems found with SalesManago credentials: ' .
            $problemList . '. ' .
            'Please ensure the above PHP global constants are set with appropriate values.'
        );
    }
}
