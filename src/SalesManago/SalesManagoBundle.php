<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago;

use MobilitySoft\TBSA\SalesManago\OrderHandler\OrderHandler;
use Psr\Container\ContainerInterface;

class SalesManagoBundle
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function enable(): void
    {
        add_action('woocommerce_order_status_processing', function ($orderId): void {
            try {
                $handler = $this->container->get(OrderHandler::class);
                $handler->handleOrderWithId((int) $orderId);
            } catch (\Exception $e) {
                error_log((string) $e);
            }
        });
    }
}
