<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\Client;

use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;

class SalesManagoException extends RuntimeException
{
    public const UPSERT = 'upsert contact information';

    public function __construct(
        string $operation,
        GuzzleException $previous,
        ?array $requestData = null,
        string $responseBody = ''
    ) {
        $message = $this->composeMessage($operation, $requestData, $responseBody);
        parent::__construct($message, 1, $previous);
    }

    private function composeMessage(string $operation, ?array $requestData, string $responseBody): string
    {
        $message = "Error when trying to $operation.";

        if ($requestData !== null) {
            $serialized = print_r($requestData, true);
            $message .= "\nRequest data: \n$serialized\n";
        }

        if ($responseBody !== '') {
            $message .= "\nResponse body: \n$responseBody\n";
        }

        return $message;
    }
}
