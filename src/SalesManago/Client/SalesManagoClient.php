<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

class SalesManagoClient
{
    private const BASE_URL = 'https://www.salesmanago.pl/api';
    private const ENDPOINT_CONTACT_UPSERT = '/contact/upsert';

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var SalesManagoIdentificationData
     */
    private $identificationData;

    public function __construct(ClientInterface $client, SalesManagoIdentificationData $identificationData)
    {
        $this->client = $client;
        $this->identificationData = $identificationData;
    }

    /**
     * @throws SalesManagoException
     */
    public function upsertContact(Contact $contact): void
    {
        $requestData = [
            'async'            => !defined('TBSA_DEV_MODE') || !TBSA_DEV_MODE,
            'contact'          => [
                'name'  => $contact->getName(),
                'email' => $contact->getEmail(),
                'phone' => $contact->getPhone(),
            ],
            'owner'            => $this->identificationData->getOwner(),
            'forceOptIn'       => true,
            'forceOptOut'      => false,
            'forcePhoneOptIn'  => true,
            'forcePhoneOptOut' => false,
        ];

        if ($contact->getTagsToApply() !== []) {
            $requestData['tags'] = $contact->getTagsToApply();
        }

        if ($contact->getProperties() !== []) {
            $properties = $contact->getProperties();
            $requestData['properties'] = $properties;
        }

        try {
            $this->sendRequest(self::ENDPOINT_CONTACT_UPSERT, $requestData);
        } catch (RequestException $requestException) {
            $response = $requestException->getResponse();
            $responseBody = $response !== null ? (string) $response->getBody() : '';
            throw new SalesManagoException(
                SalesManagoException::UPSERT,
                $requestException,
                $requestData,
                $responseBody
            );
        } catch (GuzzleException $guzzleException) {
            throw new SalesManagoException(SalesManagoException::UPSERT, $guzzleException);
        }
    }

    /**
     * @throws GuzzleException
     */
    private function sendRequest(string $path, array $data): void
    {
        $this->client->request(
            'POST',
            self::BASE_URL . $path,
            ['json' => $this->prepareRequestData($data)]
        );
    }

    private function prepareRequestData(array $data): array
    {
        $data['clientId'] = $this->identificationData->getClientId();
        $data['apiKey'] = $this->identificationData->getApiKey();
        $data['sha'] = $this->identificationData->getSha();
        $data['requestTime'] = time();
        return $data;
    }
}
