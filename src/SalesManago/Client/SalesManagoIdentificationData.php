<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\Client;

class SalesManagoIdentificationData
{
    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $sha;

    /**
     * @var string
     */
    private $owner;

    public function __construct(string $clientId, string $apiKey, string $sha, string $owner)
    {
        $this->clientId = $clientId;
        $this->apiKey = $apiKey;
        $this->sha = $sha;
        $this->owner = $owner;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function getSha(): string
    {
        return $this->sha;
    }

    public function getOwner(): string
    {
        return $this->owner;
    }
}
