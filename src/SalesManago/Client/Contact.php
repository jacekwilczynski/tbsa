<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\Client;

class Contact
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string[]
     */
    private $tagsToApply = [];

    /**
     * @var array
     */
    private $properties = [];

    /**
     * @throws InvalidEmailException
     */
    public function __construct(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException($email);
        }
        $this->email = $email;
    }

    public function setPhone(string $phone): Contact
    {
        $this->phone = $phone;
        return $this;
    }

    public function setName(string $name): Contact
    {
        $this->name = $name;
        return $this;
    }

    public function addTags(string ...$tags): Contact
    {
        $this->tagsToApply = array_merge($this->tagsToApply, $tags);
        return $this;
    }

    public function setProperty(string $key, $value, ?int $index = null): Contact
    {
        if ($index !== null) {
            $key .= " ($index)";
        }
        $this->properties[$key] = $value;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string[]
     */
    public function getTagsToApply(): array
    {
        return $this->tagsToApply;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }
}
