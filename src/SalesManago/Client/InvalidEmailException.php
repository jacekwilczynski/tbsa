<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\Client;

use InvalidArgumentException;

class InvalidEmailException extends InvalidArgumentException
{
    public function __construct(string $email)
    {
        parent::__construct("'$email' is not a valid e-mail address.");
    }
}
