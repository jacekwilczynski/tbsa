<?php
declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\OrderHandler;

use MobilitySoft\TBSA\SalesManago\Client\Contact;

abstract class CreateContactFromOrder
{
    protected function getBasicContact(\WC_Order $order): Contact
    {
        $name = $order->get_billing_first_name() . ' ' . $order->get_billing_last_name();
        return (new Contact($order->get_billing_email()))
            ->setPhone($order->get_billing_phone())
            ->setName($name);
    }
}
