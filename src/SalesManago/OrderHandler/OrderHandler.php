<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\OrderHandler;

use MobilitySoft\TBSA\SalesManago\BookingCount\BookingCountTracker;
use MobilitySoft\TBSA\SalesManago\Client\SalesManagoClient;
use const MobilitySoft\TBSA\MAGAZINE_SUBSCRIPTION_PRODUCT_ID;

class OrderHandler
{
    /**
     * @var SalesManagoClient
     */
    private $client;

    /**
     * @var CreateContactFromEventOrder
     */
    private $createContactFromEvent;

    /**
     * @var CreateContactFromMagazineSubscriptionOrder
     */
    private $contactFromMagazineSubscription;

    /**
     * @var BookingCountTracker
     */
    private $bookingCountTracker;

    public function __construct(
        SalesManagoClient $client,
        CreateContactFromEventOrder $createContactFromEvent,
        CreateContactFromMagazineSubscriptionOrder $contactFromMagazineSubscription,
        BookingCountTracker $bookingCountTracker
    ) {
        $this->client = $client;
        $this->createContactFromEvent = $createContactFromEvent;
        $this->contactFromMagazineSubscription = $contactFromMagazineSubscription;
        $this->bookingCountTracker = $bookingCountTracker;
    }

    public function handleOrderWithId(int $orderId): void
    {
        $order = wc_get_order($orderId);
        if (!$order instanceof \WC_Order || $order->meta_exists('exported_to_salesmanago')) {
            return;
        }

        foreach ($order->get_items() as $item) {
            if (!$item instanceof \WC_Order_Item_Product) {
                continue;
            }

            if ($item->meta_exists('_tbsa_eventId')) {
                $this->handleEventOrder($order, $item);
                return;
            }

            if ($item->get_product_id() === MAGAZINE_SUBSCRIPTION_PRODUCT_ID) {
                $this->handleMagazineOrder($order);
                return;
            }
        }
    }

    private function handleEventOrder(\WC_Order $order, \WC_Order_Item_Product $item): void
    {
        $index = $this->bookingCountTracker->getNumberOfBookingsSoFar($order->get_billing_email()) + 1;
        $contact = $this->createContactFromEvent->execute($order, $item, $index);
        $this->client->upsertContact($contact);
        $this->bookingCountTracker->addBooking($order);
    }

    private function handleMagazineOrder(\WC_Order $order): void
    {
        $contact = $this->contactFromMagazineSubscription->execute($order);
        $this->client->upsertContact($contact);
    }
}
