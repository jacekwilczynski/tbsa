<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\OrderHandler;

use MobilitySoft\TBSA\SalesManago\Client\Contact;

class CreateContactFromMagazineSubscriptionOrder extends CreateContactFromOrder
{
    public function execute(\WC_Order $order): Contact
    {
        return $this->getBasicContact($order)
            ->addTags('MERKURYUSZ_PRENUMERATA');
    }
}
