<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\SalesManago\OrderHandler;

use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\SalesManago\Client\Contact;
use MobilitySoft\TBSA\SalesManago\TagFormatter;

class CreateContactFromEventOrder extends CreateContactFromOrder
{
    private const DATE_FORMAT = 'Y-m-d';

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var TagFormatter
     */
    private $tagFormatter;

    public function __construct(EventRepositoryInterface $eventRepository, TagFormatter $tagFormatter)
    {
        $this->eventRepository = $eventRepository;
        $this->tagFormatter = $tagFormatter;
    }

    public function execute(\WC_Order $order, \WC_Order_Item_Product $item, int $index): Contact
    {
        $event = $this->getEvent($item);
        $city = $this->getCity($event);

        $contact = $this->getBasicContact($order)
            ->addTags('TB_GOSCIE')
            ->setProperty(
                'data_zamowienia',
                $order->get_date_created()->format(self::DATE_FORMAT),
                $index
            )
            ->setProperty(
                'data_wydarzenia',
                $event->getTimespan()->getStart()->format(self::DATE_FORMAT),
                $index
            );

        if ($city !== '') {
            $contact->addTags('TB_GOSCIE_' . $this->tagFormatter->format($city));
            $contact->setProperty('miasto', $city, $index);
        }

        return $contact;
    }

    private function getEvent(\WC_Order_Item_Product $item): EventInterface
    {
        $eventId = (int) $item->get_meta('_tbsa_eventId');
        $event = $this->eventRepository->getById($eventId);
        if ($event === null) {
            throw new \RuntimeException("Event $eventId not found.");
        }
        return $event;
    }

    private function getCity(EventInterface $event): string
    {
        $branch = $event->getBranch();
        if ($branch === null) {
            return '';
        }
        $location = $branch->getLocation();
        if ($location === null) {
            return '';
        }
        return $location->getTown();
    }
}
