<?php
/**
 * Created by PhpStorm.
 * User: jw
 * Date: 28.03.19
 * Time: 07:37
 */

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\ValueObject\EventBooking;
use MobilitySoft\TBSA\ValueObject\EventContact;
use MobilitySoft\TBSA\ValueObject\EventLocation;
use MobilitySoft\TBSA\ValueObject\EventTicket;
use MobilitySoft\TBSA\ValueObject\Timespan;

interface EventInterface
{
    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @return GroupInterface|null
     */
    public function getGroup(): ?GroupInterface;

    /**
     * @param GroupInterface|null $group
     */
    public function setGroup(?GroupInterface $group): void;

    /**
     * @return EventBooking[]
     */
    public function getBookings(): array;

    /**
     * @param EventBooking[] $bookings
     */
    public function setBookings(array $bookings): void;

    /**
     * @return BranchInterface|null
     */
    public function getBranch(): ?BranchInterface;

    /**
     * @param BranchInterface|null $branch
     */
    public function setBranch(?BranchInterface $branch): void;

    /**
     * @return string
     */
    public function getExcerpt(): string;

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt): void;

    /**
     * @return string
     */
    public function getImage(): string;

    /**
     * @param string $image
     */
    public function setImage(string $image): void;

    /**
     * @return EventLocation|null
     */
    public function getLocation(): ?EventLocation;

    /**
     * @param EventLocation|null $location
     */
    public function setLocation(?EventLocation $location): void;

    /**
     * @return Timespan|null
     */
    public function getTimespan(): ?Timespan;

    /**
     * @param Timespan|null $timespan
     */
    public function setTimespan(?Timespan $timespan): void;

    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @param string $id
     */
    public function setId(string $id): void;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $title
     */
    public function setTitle(string $title): void;

    /**
     * @return EventCategory|null
     */
    public function getCategory(): ?EventCategory;

    /**
     * @param EventCategory|null $category
     */
    public function setCategory(?EventCategory $category): void;

    /**
     * @return EventContact
     */
    public function getContact(): EventContact;

    /**
     * @param EventContact $links
     */
    public function setContact(EventContact $links): void;

    /**
     * @return int
     */
    public function getPrice(): int;

    /**
     * @param int $price
     */
    public function setPrice(int $price): void;

    /**
     * @return bool
     */
    public function isExternal(): bool;

    /**
     * @param bool $isExternal
     */
    public function setIsExternal(bool $isExternal): void;

    /**
     * @return string|null
     */
    public function getExternalUrl(): ?string;

    /**
     * @param string|null $externalUrl
     */
    public function setExternalUrl(?string $externalUrl): void;

    /**
     * @return EventTicket[]
     */
    public function getTickets(): array;

    /**
     * @param EventTicket[] $tickets
     */
    public function setTickets(array $tickets): void;

    public function setBookingsEnabled(bool $bookable): void;

    public function hasBookingsEnabled(): bool;
}
