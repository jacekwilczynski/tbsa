<?php

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\DTO\Noun;
use MobilitySoft\TBSA\ValueObject\BranchLocation;

final class Branch implements BranchInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var Noun
     */
    private $name;

    /**
     * @var BoardInterface|null
     */
    private $regionalBoard;

    /**
     * @var string[]
     */
    private $photos;

    /**
     * @var GroupInterface[]
     */
    private $groups;

    /**
     * @var string
     */
    private $url;

    /**
     * @var BranchLocation|null
     */
    private $location;

    /**
     * @var EntrepreneurInterface
     */
    private $contact;

    /**
     * @return EntrepreneurInterface|null
     */
    public function getContact(): ?EntrepreneurInterface
    {
        return $this->contact;
    }

    /**
     * @param EntrepreneurInterface|null $contact
     */
    public function setContact(?EntrepreneurInterface $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return BranchLocation|null
     */
    public function getLocation(): ?BranchLocation
    {
        return $this->location;
    }

    /**
     * @param BranchLocation|null $location
     */
    public function setLocation(?BranchLocation $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string[]
     */
    public function getPhotos(): array
    {
        return $this->photos;
    }

    /**
     * @param string[] $photos
     */
    public function setPhotos(array $photos): void
    {
        $this->photos = $photos;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Noun
     */
    public function getName(): Noun
    {
        return $this->name;
    }

    /**
     * @param Noun $name
     */
    public function setName(Noun $name): void
    {
        $this->name = $name;
    }

    /**
     * @return BoardInterface|null
     */
    public function getRegionalBoard(): ?BoardInterface
    {
        return $this->regionalBoard;
    }

    /**
     * @param BoardInterface|null $regionalBoard
     */
    public function setRegionalBoard(?BoardInterface $regionalBoard): void
    {
        $this->regionalBoard = $regionalBoard;
    }

    /**
     * @return GroupInterface[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param GroupInterface[] $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }
}
