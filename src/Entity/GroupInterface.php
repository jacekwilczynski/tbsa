<?php
/**
 * Created by PhpStorm.
 * User: jw
 * Date: 11.02.19
 * Time: 20:24
 */

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

interface GroupInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @param string $id
     */
    public function setId(string $id): void;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     */
    public function setName(string $name): void;

    /**
     * @return EntrepreneurRelation[]
     */
    public function getMembers(): array;

    /**
     * @param EntrepreneurRelation[] $members
     */
    public function setMembers(array $members): void;

    /**
     * @return BranchInterface|null
     */
    public function getBranch(): ?BranchInterface;

    /**
     * @param BranchInterface|null $branch
     */
    public function setBranch(?BranchInterface $branch): void;

    /**
     * @return BoardInterface|null
     */
    public function getLocalBoard(): ?BoardInterface;

    /**
     * @param BoardInterface|null $localBoard
     */
    public function setLocalBoard(?BoardInterface $localBoard): void;
}
