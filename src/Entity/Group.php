<?php

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

final class Group implements GroupInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var EntrepreneurRelation[]
     */
    private $members;

    /**
     * @var BranchInterface|null
     */
    private $branch;

    /**
     * @var BoardInterface|null
     */
    private $localBoard;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return EntrepreneurRelation[]
     */
    public function getMembers(): array
    {
        return $this->members;
    }

    /**
     * @param EntrepreneurRelation[] $members
     */
    public function setMembers(array $members): void
    {
        $this->members = $members;
    }

    /**
     * @return BranchInterface|null
     */
    public function getBranch(): ?BranchInterface
    {
        return $this->branch;
    }

    /**
     * @param BranchInterface|null $branch
     */
    public function setBranch(?BranchInterface $branch): void
    {
        $this->branch = $branch;
    }

    /**
     * @return BoardInterface|null
     */
    public function getLocalBoard(): ?BoardInterface
    {
        return $this->localBoard;
    }

    /**
     * @param BoardInterface|null $localBoard
     */
    public function setLocalBoard(?BoardInterface $localBoard): void
    {
        $this->localBoard = $localBoard;
    }
}
