<?php

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\DTO\Link;
use MobilitySoft\TBSA\DTO\MediaLink;
use MobilitySoft\TBSA\ValueObject\BusinessRelation;

final class Entrepreneur implements EntrepreneurInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string[]
     */
    private $activities;

    /**
     * @var MediaLink[]
     */
    private $media;

    /**
     * @var Link[]
     */
    private $links;

    /**
     * @var BusinessRelation[]
     */
    private $businessRelations;

    /**
     * @var bool
     */
    private $isStarred;

    /**
     * @var GroupInterface[]
     */
    private $groups;

    /**
     * @return GroupInterface[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param GroupInterface[] $groups
     */
    public function setGroups(array $groups): void
    {
        $this->groups = $groups;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string[]
     */
    public function getActivities(): array
    {
        return $this->activities;
    }

    /**
     * @param string[] $activities
     */
    public function setActivities(array $activities): void
    {
        $this->activities = $activities;
    }

    /**
     * @return MediaLink[]
     */
    public function getMedia(): array
    {
        return $this->media;
    }

    /**
     * @param MediaLink[] $media
     */
    public function setMedia(array $media): void
    {
        $this->media = $media;
    }

    /**
     * @return Link[]
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * @param Link[] $links
     */
    public function setLinks(array $links): void
    {
        $this->links = $links;
    }

    /**
     * @return BusinessRelation[]
     */
    public function getBusinessRelations(): array
    {
        return $this->businessRelations;
    }

    /**
     * @param BusinessRelation[] $businessRelations
     */
    public function setBusinessRelations(array $businessRelations): void
    {
        $this->businessRelations = $businessRelations;
    }

    /**
     * @return bool
     */
    public function isStarred(): bool
    {
        return $this->isStarred;
    }

    /**
     * @param bool $isStarred
     */
    public function setIsStarred(bool $isStarred): void
    {
        $this->isStarred = $isStarred;
    }
}
