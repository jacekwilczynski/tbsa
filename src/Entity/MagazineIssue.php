<?php

namespace MobilitySoft\TBSA\Entity;

final class MagazineIssue
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var string|null
     */
    private $link;

    /**
     * @var string
     */
    private $cover;

    /**
     * @var string
     */
    private $about;

    /**
     * @var string[]
     */
    private $contents;

    public function __construct(
        string $number,
        array $contents,
        string $cover,
        string $about,
        ?string $link = null
    ) {
        $this->number = $number;
        $this->cover = $cover;
        $this->about = $about;
        $this->contents = $contents;
        $this->link = $link;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getCover(): string
    {
        return $this->cover;
    }

    public function getAbout(): string
    {
        return $this->about;
    }

    /**
     * @return string[]
     */
    public function getContents(): array
    {
        return $this->contents;
    }
}
