<?php

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\Traits\SerializableToArray;
use MobilitySoft\TBSA\ValueObject\EventBooking;
use MobilitySoft\TBSA\ValueObject\EventContact;
use MobilitySoft\TBSA\ValueObject\EventLocation;
use MobilitySoft\TBSA\ValueObject\EventTicket;
use MobilitySoft\TBSA\ValueObject\Timespan;

final class Event implements EventInterface
{
    use SerializableToArray;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $title;

    /**
     * @var EventCategory|null
     */
    private $category;

    /**
     * @var EventLocation|null
     */
    private $location;

    /**
     * @var Timespan|null
     */
    private $timespan;

    /**
     * @var string
     */
    private $excerpt;

    /**
     * @var GroupInterface|null
     */
    private $group;

    /**
     * @var EventBooking[]
     */
    private $bookings;

    /**
     * @var int
     */
    private $price;

    /**
     * @var EventTicket[]
     */
    private $tickets = [];

    /**
     * @var BranchInterface|null
     */
    private $branch;

    /**
     * @var string
     */
    private $description;

    /**
     * @var EventContact
     */
    private $contact;

    /**
     * @var bool
     */
    private $isExternal;

    /**
     * @var string|null
     */
    private $externalUrl;

    /**
     * @var bool
     */
    private $bookingsEnabled;

    /**
     * @return EventTicket[]
     */
    public function getTickets(): array
    {
        return $this->tickets;
    }

    /**
     * @param EventTicket[] $tickets
     */
    public function setTickets(array $tickets): void
    {
        $this->tickets = $tickets;
    }

    /**
     * @return bool
     */
    public function isExternal(): bool
    {
        return $this->isExternal;
    }

    /**
     * @param bool $isExternal
     */
    public function setIsExternal(bool $isExternal): void
    {
        $this->isExternal = $isExternal;
    }

    /**
     * @return string|null
     */
    public function getExternalUrl(): ?string
    {
        return $this->externalUrl;
    }

    /**
     * @param string|null $externalUrl
     */
    public function setExternalUrl(?string $externalUrl): void
    {
        $this->externalUrl = $externalUrl;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return EventBooking[]
     */
    public function getBookings(): array
    {
        return $this->bookings;
    }

    /**
     * @param EventBooking[] $bookings
     */
    public function setBookings(array $bookings): void
    {
        $this->bookings = $bookings;
    }

    /**
     * @return BranchInterface|null
     */
    public function getBranch(): ?BranchInterface
    {
        return $this->branch;
    }

    /**
     * @param BranchInterface|null $branch
     */
    public function setBranch(?BranchInterface $branch): void
    {
        $this->branch = $branch;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return EventContact
     */
    public function getContact(): EventContact
    {
        return $this->contact;
    }

    /**
     * @param EventContact $contact
     */
    public function setContact(EventContact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return GroupInterface|null
     */
    public function getGroup(): ?GroupInterface
    {
        return $this->group;
    }

    /**
     * @param GroupInterface|null $group
     */
    public function setGroup(?GroupInterface $group): void
    {
        $this->group = $group;
    }

    /**
     * @return string
     */
    public function getExcerpt(): string
    {
        return $this->excerpt;
    }

    /**
     * @param string $excerpt
     */
    public function setExcerpt(string $excerpt): void
    {
        $this->excerpt = $excerpt;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return EventLocation|null
     */
    public function getLocation(): ?EventLocation
    {
        return $this->location;
    }

    /**
     * @param EventLocation|null $location
     */
    public function setLocation(?EventLocation $location): void
    {
        $this->location = $location;
    }

    /**
     * @return Timespan|null
     */
    public function getTimespan(): ?Timespan
    {
        return $this->timespan;
    }

    /**
     * @param Timespan|null $timespan
     */
    public function setTimespan(?Timespan $timespan): void
    {
        $this->timespan = $timespan;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return EventCategory|null
     */
    public function getCategory(): ?EventCategory
    {
        return $this->category;
    }

    /**
     * @param EventCategory|null $category
     */
    public function setCategory(?EventCategory $category): void
    {
        $this->category = $category;
    }

    public function hasBookingsEnabled(): bool
    {
        return $this->bookingsEnabled;
    }

    public function setBookingsEnabled(bool $bookable): void
    {
        $this->bookingsEnabled = $bookable;
    }
}
