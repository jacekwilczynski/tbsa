<?php

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

interface BoardInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return EntrepreneurRelation[]
     */
    public function getMembers(): array;

    /**
     * @param string $id
     */
    public function setId(string $id): void;

    /**
     * @param string $name
     */
    public function setName(string $name): void;

    /**
     * @param EntrepreneurRelation[] $members
     */
    public function setMembers(array $members): void;

    /**
     * @return Group[]
     */
    public function getGroups(): array;

    /**
     * @param Group[] $groups
     */
    public function setGroups(array $groups): void;
}
