<?php

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

final class Business implements BusinessInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $logo;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string[]
     */
    private $industries;

    /**
     * @var string[]
     */
    private $locations;

    /**
     * @var EntrepreneurRelation[]
     */
    private $entrepreneurRelations;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo(string $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string[]
     */
    public function getIndustries(): array
    {
        return $this->industries;
    }

    /**
     * @param string[] $industries
     */
    public function setIndustries(array $industries): void
    {
        $this->industries = $industries;
    }

    /**
     * @return string[]
     */
    public function getLocations(): array
    {
        return $this->locations;
    }

    /**
     * @param string[] $locations
     */
    public function setLocations(array $locations): void
    {
        $this->locations = $locations;
    }

    /**
     * @return EntrepreneurRelation[]
     */
    public function getEntrepreneurRelations(): array
    {
        return $this->entrepreneurRelations;
    }

    /**
     * @param EntrepreneurRelation[] $entrepreneurRelations
     */
    public function setEntrepreneurRelations(array $entrepreneurRelations): void
    {
        $this->entrepreneurRelations = $entrepreneurRelations;
    }
}
