<?php
/**
 * Created by PhpStorm.
 * User: jw
 * Date: 11.02.19
 * Time: 20:44
 */

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\DTO\Noun;
use MobilitySoft\TBSA\ValueObject\BranchLocation;

interface BranchInterface
{
    /**
     * @return string[]
     */
    public function getPhotos(): array;

    /**
     * @param string[] $photos
     */
    public function setPhotos(array $photos): void;

    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @param string $id
     */
    public function setId(string $id): void;

    /**
     * @return Noun
     */
    public function getName(): Noun;

    /**
     * @param Noun $name
     */
    public function setName(Noun $name): void;

    /**
     * @return BoardInterface|null
     */
    public function getRegionalBoard(): ?BoardInterface;

    /**
     * @param BoardInterface|null $regionalBoard
     */
    public function setRegionalBoard(?BoardInterface $regionalBoard): void;

    /**
     * @return GroupInterface[]
     */
    public function getGroups(): array;

    /**
     * @param GroupInterface[] $groups
     */
    public function setGroups(array $groups): void;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $url
     */
    public function setUrl(string $url): void;

    /**
     * @return BranchLocation|null
     */
    public function getLocation(): ?BranchLocation;

    /**
     * @param BranchLocation|null $location
     */
    public function setLocation(?BranchLocation $location): void;

    /**
     * @return EntrepreneurInterface|null
     */
    public function getContact(): ?EntrepreneurInterface;

    /**
     * @param EntrepreneurInterface|null $contact
     */
    public function setContact(?EntrepreneurInterface $contact): void;
}
