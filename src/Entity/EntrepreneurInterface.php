<?php
/**
 * Created by PhpStorm.
 * User: jw
 * Date: 08.02.19
 * Time: 20:17
 */

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\DTO\Link;
use MobilitySoft\TBSA\DTO\MediaLink;
use MobilitySoft\TBSA\ValueObject\BusinessRelation;

interface EntrepreneurInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @param string $id
     */
    public function setId(string $id): void;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     */
    public function setName(string $name): void;

    /**
     * @return string
     */
    public function getPhoto(): string;

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $description
     */
    public function setDescription(string $description): void;

    /**
     * @return string[]
     */
    public function getActivities(): array;

    /**
     * @param string[] $activities
     */
    public function setActivities(array $activities): void;

    /**
     * @return MediaLink[]
     */
    public function getMedia(): array;

    /**
     * @param MediaLink[] $media
     */
    public function setMedia(array $media): void;

    /**
     * @return Link[]
     */
    public function getLinks(): array;

    /**
     * @param Link[] $links
     */
    public function setLinks(array $links): void;

    /**
     * @return BusinessRelation[]
     */
    public function getBusinessRelations(): array;

    /**
     * @param BusinessRelation[] $businessRelations
     */
    public function setBusinessRelations(array $businessRelations): void;

    /**
     * @return bool
     */
    public function isStarred(): bool;

    /**
     * @param bool $isStarred
     */
    public function setIsStarred(bool $isStarred): void;

    /**
     * @return GroupInterface[]
     */
    public function getGroups(): array;

    /**
     * @param GroupInterface[] $groups
     */
    public function setGroups(array $groups): void;
}
