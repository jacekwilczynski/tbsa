<?php

namespace MobilitySoft\TBSA\Entity;

use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

interface BusinessInterface
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @param string $id
     */
    public function setId(string $id): void;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     */
    public function setName(string $name): void;

    /**
     * @return string
     */
    public function getLogo(): string;

    /**
     * @param string $logo
     */
    public function setLogo(string $logo): void;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $url
     */
    public function setUrl(string $url): void;

    /**
     * @return string[]
     */
    public function getIndustries(): array;

    /**
     * @param string[] $industries
     */
    public function setIndustries(array $industries): void;

    /**
     * @return string[]
     */
    public function getLocations(): array;

    /**
     * @param string[] $locations
     */
    public function setLocations(array $locations): void;

    /**
     * @return EntrepreneurRelation[]
     */
    public function getEntrepreneurRelations(): array;

    /**
     * @param EntrepreneurRelation[] $entrepreneurRelations
     */
    public function setEntrepreneurRelations(array $entrepreneurRelations): void;
}
