<?php

namespace MobilitySoft\TBSA;

use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Entity\GroupInterface;

final class GetBranchLocalBoards
{
    /**
     * @param BranchInterface $branch
     *
     * @return mixed
     */
    public function execute(BranchInterface $branch)
    {
        $groups = $branch->getGroups();

        if ( ! $groups) {
            return [];
        }

        $boards = array_map(function (GroupInterface $group) {
            return $group->getLocalBoard();
        }, $groups);

        $boards = array_filter($boards, function (?BoardInterface $board) {
            return $board;
        });

        $uniqueBoards = unique_sorted_by_quantity($boards, function (BoardInterface $board) {
            return $board->getId();
        });

        return count($uniqueBoards) === 1 ? $boards[0] : array_map(function (GroupInterface $group) {
            return [
                'group' => $group,
                'board' => $group->getLocalBoard(),
            ];
        }, $groups);
    }
}
