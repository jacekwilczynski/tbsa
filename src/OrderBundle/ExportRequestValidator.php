<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\OrderBundle;

use DateTimeImmutable;
use Exception;

class ExportRequestValidator
{
    private const DATE_FORMAT_PRESENTATION = 'YYYY-MM-DD';
    private const DATE_FORMAT_REGEXP = '/\d{4}-\d{2}-\d{2}/';

    /**
     * @return string[]
     */
    public static function collectValidationErrors(array $data): array
    {
        $errors = [];

        if (!self::isElementValidDate($data, 'from')) {
            $errors[] = sprintf(
                'Invalid "from" date. Date must be formatted as %s or omitted, ' .
                'which will be understood as from the first order ever placed.',
                self::DATE_FORMAT_PRESENTATION
            );
        }

        if (!self::isElementValidDate($data, 'to')) {
            $errors[] = sprintf(
                'Invalid "to" date. Date must be formatted as %s or omitted, ' .
                'which will be understood as till now.',
                self::DATE_FORMAT_PRESENTATION
            );
        }

        return $errors;
    }

    private static function isElementValidDate(array $array, string $key): bool
    {
        if (empty($array[$key])) {
            return true;
        }

        $value = $array[$key];

        return (
            is_string($value) &&
            self::matchesExpectedDateFormat($value) &&
            self::isActualDate($value)
        );
    }

    private static function matchesExpectedDateFormat(string $value): bool
    {
        return preg_match(self::DATE_FORMAT_REGEXP, $value);
    }

    private static function isActualDate(string $value): bool
    {
        try {
            new DateTimeImmutable($value);
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }
}
