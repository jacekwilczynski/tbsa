<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\OrderBundle;

class ExportRequest
{
    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    private function __construct(string $from, string $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public static function fromArray(array $data): ExportRequest
    {
        return new ExportRequest($data['from'] ?? '', $data['to'] ?? '');
    }
}
