<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\OrderBundle;

use Psr\Container\ContainerInterface;

class OrderBundle
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function enable(): void
    {
        add_action('wp_ajax_export_event_orders', function (): void {
            $this->container->get(OrderController::class)->exportOrdersToXlsx();
        });
    }
}
