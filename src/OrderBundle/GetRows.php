<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\OrderBundle;

use Generator;
use WC_Order;

class GetRows
{
    private $columnLabels = [
        'Imię i nazwisko',
        'Telefon',
        'E-mail',
        'Miasto (towarzystwo)',
        'Data zamówienia',
        'Data wydarzenia',
    ];

    private const MAX_ORDERS_PER_DB_QUERY = 100;

    /**
     * @return string[][]
     */
    public function execute(ExportRequest $request): array
    {
        $rows = [$this->columnLabels];
        foreach ($this->getOrdersInBatches($request) as $ordersBatch) {
            foreach ($ordersBatch as $order) {
                $rows[] = $this->makeRowFromOrder($order);
            }
        }
        return $rows;
    }

    /**
     * @return Generator|WC_Order[][]
     */
    private function getOrdersInBatches(ExportRequest $request): Generator
    {
        $page = 1;
        do {
            $result = wc_get_orders($this->getWcOrderQueryArgs($request, $page));
            yield $result->orders;
        } while ($page++ < $result->max_num_pages);
    }

    private function getWcOrderQueryArgs(ExportRequest $request, int $page): array
    {
        return [
            'type'         => 'shop_order',
            'status'       => 'completed',
            'date_created' => $this->getDateQuery($request),
            'paginate'     => true,
            'limit'        => self::MAX_ORDERS_PER_DB_QUERY,
            'page'         => $page,
        ];
    }

    private function getDateQuery(ExportRequest $request): string
    {
        $from = $request->getFrom();
        $to = $request->getTo();

        if ($from && $to) {
            return "$from...$to";
        }

        if ($from) {
            return ">= $from";
        }

        if ($to) {
            return "<= $to";
        }

        return '';
    }

    private function makeRowFromOrder(WC_Order $order): ?array
    {
        $eventId = ModelHelper::getEventIdFromOrder($order);
        if (!$eventId) {
            return null;
        }

        return [
            $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
            $order->get_billing_phone(),
            $order->get_billing_email(),
            ModelHelper::getBranchCityFromEventId($eventId),
            $order->get_date_created()->format('Y-m-d'),
            ModelHelper::getEventDateFromEventId($eventId)
        ];
    }
}
