<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\OrderBundle;

class OrderController
{
    /**
     * @var OutputOrdersXlsx
     */
    private $outputOrdersXlsx;

    public function __construct(OutputOrdersXlsx $outputOrdersXlsx)
    {
        $this->outputOrdersXlsx = $outputOrdersXlsx;
    }

    public function exportOrdersToXlsx(): void
    {
        if (!current_user_can('administrator')) {
            $this->terminate(
                403,
                'Forbidden',
                'You must be logged in as an administrator to export orders.'
            );
        }

        $validationErrors = ExportRequestValidator::collectValidationErrors($_GET);
        if (count($validationErrors) > 0) {
            $this->terminate(
                400,
                'Bad Request',
                implode(PHP_EOL, $validationErrors)
            );
        }

        $this->outputOrdersXlsx->execute(
            ExportRequest::fromArray($_GET),
            'zamowienia.xlsx'
        );
        wp_die();
    }

    private function terminate(int $statusCode, string $message, string $details = ''): void
    {
        http_response_code($statusCode);
        header('Content-Type: text/plain; charset=utf-8');
        echo "$statusCode $message";
        if (trim($details) !== '') {
            echo PHP_EOL . PHP_EOL . $details;
        }
        wp_die();

        // Make sure no malicious plugin can unlock access to further operations
        // by overriding application termination in wp_die's hooks
        exit();
    }
}
