<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\OrderBundle;

use WC_Order;
use const MobilitySoft\TBSA\BRANCH_POST_TYPE_SLUG;

class ModelHelper
{
    public static function getEventIdFromOrder(WC_Order $order): ?int
    {
        foreach ($order->get_items() as $item) {
            $eventId = (int) $item->get_meta('_tbsa_eventId');
            if ($eventId) {
                return $eventId;
            }
        }

        return null;
    }

    public static function getBranchCityFromEventId(int $eventId): string
    {
        $branchIds = get_field('tbsa_event_branches', $eventId);
        if (!is_array($branchIds) || count($branchIds) === 0) {
            return '';
        }

        $branchId = reset($branchIds);
        if (!is_int($branchId) || get_post_type($branchId) !== BRANCH_POST_TYPE_SLUG) {
            return '';
        }

        return (string) get_field('town', $branchId);
    }

    public static function getEventDateFromEventId(int $eventId): string
    {
        $event = em_get_event($eventId, 'post_id');
        if (!$event) {
            return '';
        }

        return $event->get_datetime()->format('Y-m-d');
    }
}
