<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\OrderBundle;

use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OutputOrdersXlsx
{
    /**
     * @var GetRows
     */
    private $getRows;

    public function __construct(GetRows $getRows)
    {
        $this->getRows = $getRows;
    }

    public function execute(ExportRequest $request, string $fileName): void
    {
        $rows = $this->getRows->execute($request);
        $spreadsheet = $this->createSpreadsheet($rows);
        $this->send($spreadsheet, $fileName);
    }

    private function createSpreadsheet($rows): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $this->fillWorksheet($spreadsheet->getActiveSheet(), $rows);
        return $spreadsheet;
    }

    private function fillWorksheet(Worksheet $worksheet, array $rows): void
    {
        foreach ($rows as $rowIndex => $row) {
            foreach ($row as $columnIndex => $value) {
                $cell = $worksheet->getCellByColumnAndRow($columnIndex + 1, $rowIndex + 1);
                $cell->setValue((string) $value);
                $cell->setDataType(DataType::TYPE_STRING);
            }
        }
    }

    private function send(Spreadsheet $spreadsheet, string $fileName): void
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header(sprintf('Content-Disposition: attachment;filename="%s"', $fileName));
        header('Cache-Control: no-cache');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
