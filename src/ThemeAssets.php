<?php

namespace MobilitySoft\TBSA;

class ThemeAssets
{
    /**
     * @var array
     */
    private $jsVars = [];

    public function addJsVar(string $key, $value, bool $inline = false): void
    {
        if ($inline) {
            $json = json_encode($value);
            echo "<script>window.$key=$json</script>";
        } else {
            if (is_callable($value)) {
                $value = $value();
            }
            if (isset($this->jsVars[$key]) && is_array($this->jsVars[$key])) {
                $this->jsVars[$key] = array_merge($this->jsVars[$key], $value);
            } else {
                $this->jsVars[$key] = $value;
            }
        }
    }

    public function enqueue(): void
    {
        // Styles
        wp_enqueue_style(
            'parent-style',
            get_template_directory_uri() . '/style.css'
        );

        wp_enqueue_style(
            'slick-style',
            get_theme_file_uri('/build/slick.css')
        );

        wp_enqueue_style(
            'fancybox-style',
            get_theme_file_uri('/build/jquery.fancybox.min.css')
        );

        wp_enqueue_style(
            'mCustomScrollbar-style',
            get_theme_file_uri('/build/jquery.mCustomScrollbar.css')
        );

        wp_enqueue_style(
            'child-style',
            get_theme_file_uri('/build/main-style.css'),
            'parent-style',
            defined('TBSA_DEV_MODE') && TBSA_DEV_MODE ? time() : '1.8'
        );

        // Scripts
        wp_enqueue_script(
            'slick-script',
            get_theme_file_uri('/build/slick.js'),
            'jquery',
            null,
            true
        );

        wp_enqueue_script(
            'fancybox-script',
            get_theme_file_uri('/build/jquery.fancybox.min.js'),
            'jquery',
            null,
            true
        );

        wp_enqueue_script(
            'mCustomScrollbar-script',
            get_theme_file_uri('/build/jquery.mCustomScrollbar.js'),
            'jquery',
            null,
            true
        );

        wp_enqueue_script(
            'child-script',
            get_theme_file_uri('/build/main-script.js'),
            'jquery',
            defined('TBSA_DEV_MODE') && TBSA_DEV_MODE ? time() : '1.1',
            true
        );

        foreach ($this->jsVars as $key => $value) {
            wp_localize_script('child-script', $key, $value);
        }
    }
}
