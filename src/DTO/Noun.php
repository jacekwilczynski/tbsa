<?php

namespace MobilitySoft\TBSA\DTO;

class Noun
{
    /**
     * @var string
     */
    public $nominative;

    /**
     * @var string
     */
    public $genitive;

    /**
     * @var string
     */
    public $locative;

    public function __toString(): string
    {
        return $this->nominative;
    }
}
