<?php

namespace MobilitySoft\TBSA\DTO;

final class InitiativeCategory
{
    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $longTitle;

    /**
     * @var string
     */
    public $description;
}
