<?php

namespace MobilitySoft\TBSA\DTO;

final class MediaLink
{
    /**
     * @var string
     */
    public $image;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $subtitle;

    /**
     * @var string
     */
    public $url;
}
