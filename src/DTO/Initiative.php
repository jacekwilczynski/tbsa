<?php

namespace MobilitySoft\TBSA\DTO;

final class Initiative
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $thumbnail;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $applyUrl;

    /**
     * @var string
     */
    public $subscribeUrl;
}
