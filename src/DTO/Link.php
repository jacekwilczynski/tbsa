<?php

namespace MobilitySoft\TBSA\DTO;

final class Link
{
    /**
     * @var string
     */
    public $href;

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $type;
}
