<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Utils;

class ReplacePlaceholders
{
    public function execute(string $template, array $data): string
    {
        return mb_ereg_replace_callback(
            '\{([^}]+)\}',
            static function (array $matches) use ($data): string {
                if (array_key_exists($matches[1], $data)) {
                    return (string) $data[$matches[1]];
                }
                return $matches[0];
            },
            $template
        );
    }
}
