<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Utils;

use OutOfBoundsException;

trait DeserializableEnum
{
    /**
     * @return static
     * @throws OutOfBoundsException
     */
    public static function fromString(string $key)
    {
        $methodName = strtoupper($key);
        if (method_exists(static::class, $methodName)) {
            return call_user_func([static::class, $methodName]);
        }

        throw new OutOfBoundsException("Value $methodName not defined on enum " . static::class);
    }
}
