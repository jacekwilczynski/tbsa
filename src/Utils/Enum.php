<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Utils;

class Enum
{
    /**
     * @var array
     */
    private static $instances = [];

    /**
     * @var string
     */
    private $key;

    private $data;

    private function __construct(string $key, $data)
    {
        $this->key  = strtolower($key);
        $this->data = $data;
    }

    /**
     * @return static
     */
    protected static function getInstance(string $key, $data = null)
    {
        if ( ! isset(self::$instances[static::class])) {
            self::$instances[static::class] = [];
        }

        if ( ! isset(self::$instances[static::class][$key])) {
            self::$instances[static::class][$key] = new static($key, $data);
        }

        return self::$instances[static::class][$key];
    }

    public function __toString(): string
    {
        return $this->getKey();
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getData()
    {
        return $this->data;
    }
}
