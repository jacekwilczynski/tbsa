<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Utils\Pagination;

class PaginationState
{
    /**
     * @var int
     */
    private $requestedOffset;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var int
     */
    private $numberOfItems;

    public function __construct(int $requestedOffset, int $pageSize, int $numberOfItems)
    {
        $this->requestedOffset = $requestedOffset;
        $this->pageSize        = $pageSize;
        $this->numberOfItems   = $numberOfItems;
    }

    public function getLastIndex(): int
    {
        return min(
            $this->getFirstIndex() - 1 + $this->getPageSize(),
            $this->getNumberOfItems()
        );
    }

    public function getFirstIndex(): int
    {
        $pageSize = $this->getPageSize();

        return intdiv($this->getRequestedOffset(), $pageSize) * $pageSize + 1;
    }

    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function getRequestedOffset(): int
    {
        return $this->requestedOffset;
    }

    public function getNumberOfItems(): int
    {
        return $this->numberOfItems;
    }

    public function getNextPage(): ?int
    {
        $currentPage = $this->getCurrentPage();

        return $currentPage < $this->getNumberOfPages() ? $currentPage + 1 : null;
    }

    public function getCurrentPage(): int
    {
        return $this->computePage($this->getRequestedOffset());
    }

    public function computePage(int $offset): int
    {
        return intdiv($offset, $this->getPageSize()) + 1;
    }

    public function getNumberOfPages(): int
    {
        return (int)ceil($this->getNumberOfItems() / $this->getPageSize());
    }

    public function getPreviousPage(): ?int
    {
        $currentPage = $this->getCurrentPage();

        return $currentPage > 1 ? $currentPage - 1 : null;
    }

    public function computeOffset(int $page): int
    {
        return ($page - 1) * $this->getPageSize();
    }
}
