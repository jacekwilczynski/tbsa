<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Utils\Pagination;

interface GetPageUrl
{
    public function execute(int $page): ?string;
}
