<?php

namespace MobilitySoft\TBSA\RestController;

use MobilitySoft\TBSA\Entity\EventCategory;
use MobilitySoft\TBSA\Repository\EventCategoryRepositoryInterface;
use WP_REST_Response;

class EventCategoryRestController
{
    /**
     * @var EventCategoryRepositoryInterface
     */
    private $eventCategoryRepository;

    public function __construct(EventCategoryRepositoryInterface $eventCategoryRepository)
    {
        $this->eventCategoryRepository = $eventCategoryRepository;
    }

    public function getAll(): WP_REST_Response
    {
        $categories = $this->eventCategoryRepository->getAll();

        return rest_ensure_response(array_map(function (EventCategory $category) {
            return self::getEventCategoryData($category);
        }, $categories));
    }

    public static function getEventCategoryData(EventCategory $category): array
    {
        return [
            'id'         => (int)$category->getId(),
            'longTitle'  => $category->getTitle(),
            'shortTitle' => $category->getShortTitle(),
            'slug'       => $category->getSlug(),
            'iconUrl'    => $category->getIcon(),
        ];
    }
}
