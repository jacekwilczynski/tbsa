<?php

namespace MobilitySoft\TBSA\RestController;

use MobilitySoft\TBSA\Domain\Booking\BookingRepository;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\Service\CreateBookingFromWcOrderItem;
use MobilitySoft\TBSA\Service\CustomCheckoutData;
use MobilitySoft\TBSA\Service\GetEventTicketById;
use MobilitySoft\TBSA\Service\IsEventBookable;
use MobilitySoft\TBSA\ValueObject\EventTicket;
use Throwable;
use WC_Cart;
use WC_Order_Item;
use WC_Product;
use const MobilitySoft\TBSA\EVENT_PRODUCT_ID;

class EventCheckoutController
{
    private const DATE_FORMAT = 'Y-m-d, H:i';

    /**
     * @var int
     */
    private $productId;

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var CreateBookingFromWcOrderItem
     */
    private $createBookingFromWcOrderItem;

    /**
     * @var BookingRepository
     */
    private $bookingRepository;

    /**
     * BookingRestController constructor.
     *
     * @param int $productId
     * @param EventRepositoryInterface $eventRepository
     * @param CreateBookingFromWcOrderItem $createBookingFromWcOrderItem
     * @param BookingRepository $bookingRepository
     */
    public function __construct(
        int $productId,
        EventRepositoryInterface $eventRepository,
        CreateBookingFromWcOrderItem $createBookingFromWcOrderItem,
        BookingRepository $bookingRepository
    ) {
        $this->productId = $productId;
        $this->eventRepository = $eventRepository;
        $this->createBookingFromWcOrderItem = $createBookingFromWcOrderItem;
        $this->bookingRepository = $bookingRepository;
    }

    public function execute(): void
    {
        $this->bindToHooks();

        if (isset($_POST['event_checkout']) && $_POST['event_checkout']) {
            add_action('wp_loaded', function () {
                $event = $this->eventRepository->getById((int) $_POST['event_id']);

                if ($event && IsEventBookable::execute($event)) {
                    $cart = WC()->cart;
                    $cart->empty_cart();

                    $cartItemData = ['tbsa_eventId' => $event->getId()];

                    if (isset($_POST['tickets']) && is_array($_POST['tickets'])) {
                        foreach ($this->sanitizeTickets($_POST['tickets']) as $ticket) {
                            $cartItemData['tbsa_ticketId'] = $ticket['id'];
                            $ticketQuantity = $ticket['quantity'];

                            $cart->add_to_cart(
                                $this->productId,
                                $ticketQuantity,
                                null,
                                null,
                                $cartItemData
                            );
                        }
                    } else {
                        $cart->add_to_cart(
                            $this->productId,
                            (int) $_POST['quantity'],
                            null,
                            null,
                            $cartItemData
                        );

                        $free = !$event->getTickets() && !$event->getPrice();
                        if ($free) {
                            $checkout = wc()->checkout();
                            $params = $_POST;
                            CustomCheckoutData::save($params);
                            $orderId = $checkout->create_order([
                                'billing_first_name'   => $params['first_name'],
                                'billing_last_name'    => $params['last_name'],
                                'billing_email'        => $params['email'],
                                'billing_phone'        => $params['phone'],
                                'payment_method'       => 'none',
                                'payment_method_title' => 'brak',
                            ]);
                            $order = wc_get_order($orderId);
                            update_post_meta($orderId, '_customer_user', get_current_user_id());
                            $order->calculate_totals();
                            $order->set_status('completed');
                            $order->save();

                            wp_redirect($order->get_checkout_order_received_url());
                            exit;
                        }
                    }

                    wp_redirect(wc_get_checkout_url());
                } else {
                    wp_redirect(home_url());
                    exit();
                }
            });
        }
    }

    public function bindToHooks(): void
    {
        // Make events purchasable even if their associated product is private
        add_filter(
            'woocommerce_is_purchasable',
            static function (bool $defaultAnswer, WC_Product $product): bool {
                if ($product->get_id() === EVENT_PRODUCT_ID) {
                    return true;
                }

                return $defaultAnswer;
            },
            10,
            2
        );

        // Set cart item prices based on event price
        add_action('woocommerce_before_calculate_totals', function (WC_Cart $cart) {
            $cartItems = $cart->get_cart();
            foreach ($cartItems as $cartItemKey => $cartItem) {
                if ($this->isEvent($cartItem)) {
                    $eventId = $cartItem['tbsa_eventId'];
                    $event = $this->eventRepository->getById($eventId);

                    if ($event) {
                        /** @var WC_Product $product */
                        $product = $cartItem['data'];

                        $ticketId = $cartItem['tbsa_ticketId'] ?? null;
                        if ($ticketId) {
                            $ticket = GetEventTicketById::execute($event, $ticketId);
                        }

                        try {
                            $price = isset($ticket) ? $ticket->getPrice()->getFloat() : $event->getPrice();
                            $product->set_price($price);
                        } catch (Throwable $e) {
                            $cart->remove_cart_item($cartItemKey);
                        }
                    }
                }
            }
        }, 99999, 1);

        // Attach event-related data to cart item
        add_filter('woocommerce_get_item_data', function (array $data, array $cartItem) {
            if ($this->isEvent($cartItem)) {
                $eventId = $cartItem['tbsa_eventId'];
                $ticketId = $cartItem['tbsa_ticketId'] ?? null;
                $event = $this->eventRepository->getById($eventId);

                if ($event) {
                    $data = array_merge(
                        $data,
                        $this->getBookingMeta(
                            $event,
                            GetEventTicketById::execute($event, $ticketId)
                        )
                    );
                }
            }

            return $data;
        }, 10, 2);

        // Attach event-related data to order item
        add_action('woocommerce_checkout_create_order_line_item', function (WC_Order_Item $item, $_, array $cartItem) {
            if ($this->isEvent($cartItem)) {
                $eventId = $cartItem['tbsa_eventId'];
                $ticketId = $cartItem['tbsa_ticketId'] ?? null;
                $event = $this->eventRepository->getById($eventId);

                if ($event) {
                    $meta = $this->getBookingMeta(
                        $event,
                        GetEventTicketById::execute($event, $ticketId)
                    );

                    foreach ($meta as $property) {
                        $item->add_meta_data($property['key'], $property['value']);
                    }
                }

                $item->add_meta_data('_tbsa_eventId', $eventId);

                if ($ticketId) {
                    $item->add_meta_data('_tbsa_ticketId', $ticketId);
                }

                if ($event->getBranch()) {
                    $item->add_meta_data('_tbsa_organizerId', $event->getBranch()->getId());
                }
            }
        }, 10, 3);

        // Set up how event-related data is displayed in order details
        add_filter(
            'woocommerce_order_item_display_meta_value',
            function (string $displayValue, object $metaLine, WC_Order_Item $item) {
                $eventId = $item->get_meta('_tbsa_eventId');

                if ($eventId) {
                    $key = $metaLine->key;

                    if ($key === 'Wydarzenie') {
                        $href = get_the_permalink($eventId);
                    }

                    if ($key === 'Adres') {
                        $href = 'https://maps.google.com/maps?q=' . urlencode_deep($metaLine->value);
                    }

                    if ($key === 'Organizator') {
                        $organizerId = $item->get_meta('_tbsa_organizerId');
                        $href = get_the_permalink($organizerId);
                    }

                    if ($key === 'Telefon') {
                        $href = 'tel:' . $metaLine->value;
                    }

                    if ($key === 'E-mail') {
                        $href = 'mailto:' . $metaLine->value;
                    }
                }

                return isset($href)
                    ? '<a href="' . $href . '" target="_blank" rel="noopener noreferrer">' . $displayValue . '</a>'
                    : $displayValue;
            },
            10,
            3
        );

        // Set order status to completed once payment is successful
        add_action('woocommerce_order_status_processing', function (int $orderId) {
            $order = wc_get_order($orderId);
            $items = $order->get_items();
            $eventItems = array_filter($items, function (WC_Order_Item $item) {
                return $item->get_data()['product_id'] === $this->productId;
            });
            $isPureEventOrder = count($items) === count($eventItems);

            if ($isPureEventOrder) {
                $order->set_status('completed');
                $order->save();
            }
        }, 10, 1);

        // Add booking when order is complete
        add_action('woocommerce_order_status_completed', function (int $orderId) {
            $order = wc_get_order($orderId);

            if (!get_post_meta($orderId, 'tbsa_order_has_booking')) {
                foreach ($order->get_items() as $item) {
                    $isEvent = $item->meta_exists('_tbsa_eventId');
                    if ($isEvent) {
                        $booking = $this->createBookingFromWcOrderItem->execute($item);
                        $this->bookingRepository->add($booking);
                    }
                }

                update_post_meta($orderId, 'tbsa_order_has_booking', 1);
            }
        }, 10, 1);
    }

    private function isEvent(array $cartItem): bool
    {
        return $cartItem['product_id'] === $this->productId && isset($cartItem['tbsa_eventId']);
    }

    private function getBookingMeta(EventInterface $event, ?EventTicket $ticket = null): array
    {
        $start = $event->getTimespan()->getStart();
        $end = $event->getTimespan()->getEnd();
        $location = $event->getLocation();
        $branch = $event->getBranch();
        $contact = $event->getContact();

        $data = [];

        if ($ticket) {
            $data[] = ['key' => 'Rodzaj biletu', 'value' => $ticket->getName()];
        }

        $data[] = ['key' => 'Wydarzenie', 'value' => $event->getTitle()];

        if ($location) {
            $data[] = ['key' => 'Adres', 'value' => $location->getAddress()];
        }

        $data[] = ['key' => 'Rozpoczęcie', 'value' => $start->format(self::DATE_FORMAT)];

        if ($end) {
            $data[] = ['key' => 'Zakończenie', 'value' => $end->format(self::DATE_FORMAT)];
        }

        if ($branch) {
            $data[] = ['key' => 'Organizator', 'value' => $branch->getName()];
        }

        if ($contact) {
            $phone = $contact->getPhone();
            $email = $contact->getEmail();
            if ($phone) {
                $data[] = ['key' => 'Telefon', 'value' => $phone];
            }
            if ($email) {
                $data[] = ['key' => 'E-mail', 'value' => $email];
            }
        }

        return $data;
    }

    private function sanitizeTickets(array $tickets): array
    {
        return array_map(function (array $ticket) {
            return [
                'id'       => sanitize_title($ticket['id']),
                'quantity' => (int) $ticket['quantity'],
            ];
        }, $tickets);
    }
}
