<?php

namespace MobilitySoft\TBSA\RestController;

use DateTimeImmutable;
use MobilitySoft\TBSA\Common\Application\Event\CreateEventQueryFromHttpRequestParams;
use MobilitySoft\TBSA\Common\Domain\Event\Speaker;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetSpeakersFromAcfFields;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\Service\GetDays;
use MobilitySoft\TBSA\Service\GetPriceString;
use MobilitySoft\TBSA\Service\TranslateMonth;
use Throwable;
use WP_REST_Controller;
use WP_REST_Request;
use WP_REST_Response;

class EventRestController extends WP_REST_Controller
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var GetDays
     */
    private $getDays;

    /**
     * @var CreateEventQueryFromHttpRequestParams
     */
    private $createEventQueryFromHttpRequestParams;

    /**
     * @var GetSpeakersFromAcfFields
     */
    private $getSpeakersFromAcfFields;

    public function __construct(
        CreateEventQueryFromHttpRequestParams $createEventQueryFromHttpRequestParams,
        EventRepositoryInterface $eventRepository,
        GetDays $getDays,
        GetSpeakersFromAcfFields $getSpeakersFromAcfFields
    ) {
        $this->eventRepository = $eventRepository;
        $this->getDays = $getDays;
        $this->createEventQueryFromHttpRequestParams = $createEventQueryFromHttpRequestParams;
        $this->getSpeakersFromAcfFields = $getSpeakersFromAcfFields;
    }

    public function query(WP_REST_Request $request): WP_REST_Response
    {
        $params = $request->get_params();
        // For unknown reasons, EmEventRepository limits to one fewer than specified.
        // I didn't want to risk braking its other uses by changing it.
        $params['limit']++;
        $events = $this->eventRepository->query($this->createEventQueryFromHttpRequestParams->execute($params));

        $response = rest_ensure_response(array_map(function (EventInterface $event) {
            return $this->getEventData($event);
        }, $events));

        $this->setHeaders($response);

        return $response;
    }

    private function getEventData(EventInterface $event): array
    {
        $customFields = get_fields($event->getId());

        $start = $event->getTimespan()->getStart();

        try {
            $data = [
                'id'       => (int) $event->getId(),
                'title'    => $event->getTitle(),
                'timespan' => [
                    'start'      => [
                        'text'      => TranslateMonth::execute($start->format('j F Y, H:i')),
                        'timestamp' => $start->getTimestamp(),
                    ],
                    'isWholeDay' => $event->getTimespan()->isWholeDay(),
                ],
                'url'      => $event->isExternal() ? $event->getExternalUrl() : get_the_permalink($event->getId()),
                'speakers' => $this->getEventSpeakers($customFields),
                'images'   => $this->getEventImages($event, $customFields),
            ];
        } catch (Throwable $e) {
            error_log("Error while constructing event {$event->getId()} for the Events REST API.");
            throw $e;
        }

        $end = $event->getTimespan()->getEnd();
        if ($end) {
            $data['timespan']['end'] = [
                'text'      => TranslateMonth::execute($end->format('j F Y, H:i')),
                'timestamp' => $end->getTimestamp(),
            ];
        }

        $location = $event->getLocation();
        if ($location) {
            $data['location'] = [
                'name'    => $location->getTitle(),
                'town'    => $location->getTown(),
                'address' => $location->getAddress(),
            ];
        }

        $price = plain_text(GetPriceString::execute($event, true));
        if ($price) {
            $data['price'] = $price;
        }

        $description = $this->getEventDescription($event);
        if ($description) {
            $data['description'] = [
                'html'  => $description,
                'plain' => plain_text($description),
            ];
        }

        $category = $event->getCategory();
        if ($category) {
            $data['category'] = EventCategoryRestController::getEventCategoryData($category);
        }

        return $data;
    }

    private function getEventSpeakers(array $customFields): array
    {
        return array_map(function (Speaker $speaker) {
            $photo = $speaker->getPhoto();
            $business = $speaker->getBusiness();
            $description = $speaker->getDescription();

            return array_filter([
                'name'        => $speaker->getName(),
                'imageUrl'    => $photo ? $photo->getUrl() : null,
                'business'    => $business ? [
                    'name' => $business->getName(),
                    'url'  => $business->getUrl(),
                ] : null,
                'description' => $description ? [
                    'html'  => $description,
                    'plain' => plain_text($description),
                ] : null,
            ]);
        }, $this->getSpeakersFromAcfFields->execute($customFields));
    }

    private function getEventImages(EventInterface $event, array $customFields): array
    {
        return array_filter([
            'thumbnail' => $this->getImage(get_post_thumbnail_id($event->getId())),
            'cover'     => $this->getImage($this->getEventCoverImageId($event)),
            'gallery'   => $this->getEventGallery($customFields),
        ], function ($item) {
            return $item;
        });
    }

    private function getImage($id): ?array
    {
        return $id ? [
            'thumbnail' => wp_get_attachment_image_url($id, 'thumbnail'),
            'small'     => wp_get_attachment_image_url($id, 'medium'),
            'medium'    => wp_get_attachment_image_url($id, 'medium_large'),
            'large'     => wp_get_attachment_image_url($id, 'large'),
            'full'      => wp_get_attachment_image_url($id, 'full'),
        ] : null;
    }

    private function getEventCoverImageId(EventInterface $event): ?int
    {
        $imageId = $customFields['event_page_top_image'] ?? null;
        if (!$imageId) {
            $eventCategory = $event->getCategory();
            $imageId = $eventCategory
                ? get_field('event_page_top_image', get_term($eventCategory->getId()))
                : null;
        }

        return $imageId;
    }

    private function getEventGallery(array $customFields): array
    {
        $hasGallery = isset($customFields['event_page_gallery']) &&
                      is_array($customFields['event_page_gallery']);

        return $hasGallery
            ? array_map(function (array $imageData) {
                return $this->getImage($imageData['ID']);
            }, $customFields['event_page_gallery'])
            : [];
    }

    private function getEventDescription(EventInterface $event): string
    {
        $text = apply_filters('the_content', get_post($event->getId())->post_content);
        if ($text) {
            return $text;
        }

        $category = $event->getCategory();
        if ($category) {
            return $category->getDescription();
        }

        return '';
    }

    private function setHeaders(WP_REST_Response $response): void
    {
    }

    public function getDays(WP_REST_Request $request): WP_REST_Response
    {
        $params = $request->get_params();

        if (isset($params['year'], $params['month'])) {
            $year = (int) $params['year'];
            $month = (int) $params['month'];
            $currentDate = new DateTimeImmutable("$year-$month");
            $endDate = $currentDate->modify('+1 year');
        } else {
            $currentDate = new DateTimeImmutable($params['start']);
            $endDate = isset($params['end']) ? new DateTimeImmutable($params['end']) : null;
        }

        $days = $this->getDays->execute(
            $params,
            $currentDate,
            $endDate,
            function (EventInterface $event) {
                return $this->getEventData($event);
            }
        );

        $response = rest_ensure_response($days);
        $this->setHeaders($response);
        return $response;
    }

    public function getById(WP_REST_Request $request): WP_REST_Response
    {
        $id = (int) $request->get_param('id');
        $event = $this->eventRepository->getById($id);

        if (!$event) {
            return new WP_REST_Response("Event with id $id does not exist.", 404);
        }

        $response = rest_ensure_response($this->getEventData($event));

        $this->setHeaders($response);

        return $response;
    }
}
