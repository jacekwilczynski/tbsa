<?php

namespace MobilitySoft\TBSA\RestController;

use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\Service\CustomCheckoutData;
use MobilitySoft\TBSA\Service\IsEventBookable;
use MobilitySoft\TBSA\Service\TranslateMonth;
use MobilitySoft\TBSA\ValueObject\EventTicket;
use Twig\Environment as Twig;
use WP_REST_Request;
use WP_REST_Response;

class BookingFormController
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * BookingController constructor.
     *
     * @param Twig $twig
     * @param EventRepositoryInterface $eventRepository
     */
    public function __construct(Twig $twig, EventRepositoryInterface $eventRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->twig = $twig;
    }

    /**
     * @param WP_REST_Request $request
     *
     * @return string|WP_REST_Response
     */
    public function renderForm(WP_REST_Request $request)
    {
        $eventId = $request->get_param('event_id');
        $event = $this->eventRepository->getById($eventId);

        if (!$event) {
            return new WP_REST_Response('Event with id ' . $eventId . ' does not exist or is invalid', 204);
        }

        if (!IsEventBookable::execute($event)) {
            return new WP_REST_Response('Event with id ' . $eventId . ' has already taken place.', 410);
        }

        $tickets = $event->getTickets();
        $price = $tickets ? null : $event->getPrice();
        $location = $event->getLocation();

        return do_shortcode($this->twig->render('bookingForm.twig', [
            'eventId'                   => $event->getId(),
            'date'                      => TranslateMonth::execute($event->getTimespan()->getStart()->format('j F Y, H:i')),
            'address'                   => $location ? $location->getAddress() : null,
            'title'                     => 'Rezerwacja: ' . $event->getTitle(),
            'formattedPrice'            => $price ? wc_price($price) : null,
            'price'                     => $price,
            'customCheckout'            => !$tickets && !$price ? CustomCheckoutData::load() : null,
            'tickets'                   => $this->getTicketsData($tickets),
            'targetUrl'                 => home_url('/zamowienie'),
            'buyText'                   => $tickets ? 'Rezerwuj' : $price ? 'Kup teraz' : 'Zapisz się bezpłatnie',
            'showTicketDescriptionText' => 'Więcej',
        ]));
    }

    /**
     * @param EventTicket[] $tickets
     *
     * @return array
     */
    private function getTicketsData(array $tickets): array
    {
        return array_map(function (EventTicket $ticket) {
            $price = $ticket->getPrice()->getFloat();

            return [
                'label'          => $ticket->getName(),
                'id'             => $ticket->getId(),
                'price'          => $price,
                'formattedPrice' => wc_price($price),
                'description'    => $ticket->getDescription(),
            ];
        }, $tickets);
    }
}
