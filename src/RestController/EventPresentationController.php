<?php

namespace MobilitySoft\TBSA\RestController;

use MobilitySoft\TBSA\Common\Application\Event\CreateEventQueryFromHttpRequestParams;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Renderer\EventPageRenderer;
use MobilitySoft\TBSA\Renderer\EventSummaryRenderer;
use MobilitySoft\TBSA\Renderer\SmallEventListRenderer;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use WP_REST_Request;

/**
 * Class EventPresentationController
 * @package MobilitySoft\TBSA\RestController
 */
final class EventPresentationController
{
    /**
     * @var string[]
     */
    private $allowedParams = ['branch', 'category', 'date', 'render_for', 'id'];

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var SmallEventListRenderer
     */
    private $smallEventListRenderer;

    /**
     * @var EventSummaryRenderer
     */
    private $eventSummaryRenderer;

    /**
     * @var EventPageRenderer
     */
    private $eventPageRenderer;

    /**
     * @var CreateEventQueryFromHttpRequestParams
     */
    private $createEventQueryFromHttpRequestParams;

    public function __construct(
        CreateEventQueryFromHttpRequestParams $createEventQueryFromHttpRequestParams,
        EventPageRenderer $eventPageRenderer,
        EventSummaryRenderer $eventSummaryRenderer,
        EventRepositoryInterface $eventRepository,
        SmallEventListRenderer $smallEventListRenderer
    ) {
        $this->createEventQueryFromHttpRequestParams = $createEventQueryFromHttpRequestParams;
        $this->eventPageRenderer                     = $eventPageRenderer;
        $this->eventRepository                       = $eventRepository;
        $this->eventSummaryRenderer                  = $eventSummaryRenderer;
        $this->smallEventListRenderer                = $smallEventListRenderer;
    }

    /**
     * @param WP_REST_Request $request
     *
     * @return mixed
     */
    public function query(WP_REST_Request $request)
    {
        $params = $this->sanitizeParams($request->get_params());

        if (isset($params['render_for'])) {
            switch ($params['render_for']) {
                case 'map':
                    ob_start();
                    $noEventsMessage = get_field('tbsa_no_events_message', 'option');
                    $markup          = do_shortcode('[event_categories style="shadow" branch_link="1" branch="' . $params['branch'] . '" no_events_message="' . $noEventsMessage . '"]');
                    ob_clean();

                    return $markup;
                case 'calendar':
                    ob_start();
                    $query  = $this->createEventQueryFromHttpRequestParams->execute($params);
                    $events = $this->eventRepository->query($query);
                    $markup = $this->smallEventListRenderer->renderEvents($events);
                    ob_clean();

                    return $markup;
                case 'summary':
                    ob_start();
                    $markup = $this->eventSummaryRenderer->renderEvent($this->eventRepository->getById($params['id']));
                    ob_clean();

                    return $markup;
                case 'details':
                    ob_start();
                    $markup = $this->eventPageRenderer->render($params['id'], true);
                    ob_clean();

                    return $markup;
            }
        }

        return array_map(
            function (EventInterface $event) { return $event->toArray(); },
            $this->eventRepository->query($this->createEventQueryFromHttpRequestParams->execute($params))
        );
    }

    /**
     * @param array $params
     *
     * @return array
     */
    private function sanitizeParams(array $params): array
    {
        return array_filter($params, function ($key) {
            return in_array($key, $this->allowedParams);
        }, ARRAY_FILTER_USE_KEY);
    }
}
