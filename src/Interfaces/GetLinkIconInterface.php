<?php

namespace MobilitySoft\TBSA\Interfaces;

/**
 * Interface GetLinkIconInterface
 * @package MobilitySoft\TBSA\Interfaces
 */
interface GetLinkIconInterface
{
    /**
     * @param string $type
     *
     * @return string
     */
    public function execute(string $type): string;
}
