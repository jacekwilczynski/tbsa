<?php

namespace MobilitySoft\TBSA\Interfaces;

interface StringToIntFunction
{
    public function execute(string $input): int;
}
