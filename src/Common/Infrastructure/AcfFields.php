<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure;

class AcfFields
{
    public function setValuesByFieldKey(array $map): void
    {
        foreach ($map as $key => $value) {
            add_filter("acf/prepare_field/key=$key", function ($field) use ($value) {
                $field['value'] = $value;
                return $field;
            });
        }
    }
}
