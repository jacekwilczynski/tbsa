<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure;

class BranchRepositoryHelper
{
    public static function getNonVirtualBranchesMetaQuery(): array
    {
        return [
            [
                'key'     => 'town',
                'compare' => 'EXISTS',
            ],
            [
                'key'     => 'town',
                'compare' => '!=',
                'value'   => '',
            ],
            [
                'key'     => 'voivodeship',
                'compare' => 'EXISTS',
            ],
            [
                'key'     => 'voivodeship',
                'compare' => '!=',
                'value'   => '',
            ],
        ];
    }
}
