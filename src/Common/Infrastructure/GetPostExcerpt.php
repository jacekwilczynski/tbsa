<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure;

use WP_Post;

class GetPostExcerpt
{
    /**
     * @param WP_Post|int $post
     * @param int|null $trimWords
     *
     * @return string
     */
    public function execute($post, ?int $trimWords = 18): string
    {
        $post = get_post($post);

        return has_excerpt($post)
            ? $post->post_excerpt
            : wp_trim_words($post->post_content, $trimWords);
    }
}
