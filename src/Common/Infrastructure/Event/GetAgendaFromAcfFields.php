<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use MobilitySoft\TBSA\Common\Domain\Event\Agenda\Agenda;
use MobilitySoft\TBSA\Common\Domain\Event\Agenda\Item;
use MobilitySoft\TBSA\Common\Domain\Event\Agenda\Part;
use MobilitySoft\TBSA\Utils\Enum;

class GetAgendaFromAcfFields
{
    public function execute(?array $fields): ?Agenda
    {
        $type = $this->getAgendaType($fields);

        if ($type === AgendaType::SINGLE()) {
            return new Agenda(new Part('', ...$this->parseItems($fields['tbsa_agenda_items'])));
        }

        if ($type === AgendaType::MULTI_PART()) {
            return new Agenda(...$this->parseParts($fields['tbsa_agenda_parts']));
        }

        return null;
    }

    private function getAgendaType(array $fields): AgendaType
    {
        $useMulti  = $fields['tbsa_agenda_split'] ?? null;
        $hasSingle = $fields['tbsa_agenda_items'] ?? null;
        $hasMulti  = $fields['tbsa_agenda_parts'] ?? null;

        if ( ! $useMulti && $hasSingle) {
            return AgendaType::SINGLE();
        }

        if ($useMulti && $hasMulti) {
            return AgendaType::MULTI_PART();
        }

        return AgendaType::NONE();
    }

    /**
     * @param $rawItems
     *
     * @return Item[]
     */
    private function parseItems($rawItems): array
    {
        if (is_array($rawItems)) {
            return array_map(function ($rawItem): Item {
                $icon = $rawItem['icon'] ?? '';
                $text = $rawItem['normal_text'] ?? '';

                $time        = $rawItem['time'] ?? '';
                $description = "{$icon} {$text}";

                return new Item($time, $description);
            }, $rawItems);
        }

        return [];
    }

    /**
     * @param $rawParts
     *
     * @return Part[]
     */
    private function parseParts($rawParts): array
    {
        if (is_array($rawParts)) {
            return array_map(function ($partData): Part {
                $title = $partData['heading'] ?? '';
                $items = $this->parseItems($partData['items'] ?? []);

                return new Part($title, ...$items);
            }, $rawParts);
        }

        return [];
    }
}

class AgendaType extends Enum
{
    public static function NONE(): AgendaType
    {
        return self::getInstance('NONE');
    }

    public static function SINGLE(): AgendaType
    {
        return self::getInstance('SINGLE');
    }

    public static function MULTI_PART(): AgendaType
    {
        return self::getInstance('MULTI_PART');
    }
}
