<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

class GetBranchId
{
    public function execute($acfFields): ?int
    {
        return $acfFields['tbsa_event_branches'][0] ?? null;
    }
}
