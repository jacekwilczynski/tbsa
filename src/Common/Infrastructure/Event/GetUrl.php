<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

class GetUrl
{
    public function execute(int $eventId, $acfFields = null): string
    {
        if ($acfFields === null) {
            $acfFields = get_fields($eventId);
        }

        if (!$acfFields instanceof SimpleAcfFields) {
            $acfFields = SimpleAcfFields::prepare($acfFields);
        }

        if ($acfFields->isExternal()) {
            return $acfFields->getExternalUrl();
        }

        return get_the_permalink($eventId);
    }
}
