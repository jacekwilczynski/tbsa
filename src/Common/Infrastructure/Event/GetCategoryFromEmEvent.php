<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use EM_Event;
use WP_Term;

class GetCategoryFromEmEvent
{
    public function getTerm(EM_Event $event): ?WP_Term
    {
        $id = $this->getId($event);

        return $id ? get_term($id) : null;
    }

    public function getId(EM_Event $event): ?int
    {
        $category = $event->get_categories()->get_first();

        if ($category && is_numeric($category->id)) {
            return (int)$category->id;
        }

        return null;
    }
}
