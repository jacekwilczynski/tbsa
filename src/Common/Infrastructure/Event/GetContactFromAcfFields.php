<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use MobilitySoft\TBSA\ValueObject\EventContact;

class GetContactFromAcfFields
{
    public function execute($fields): ?EventContact
    {
        if (isset($fields['tbsa_event_contact'])) {
            $field = $fields['tbsa_event_contact'];

            return new EventContact($field['phone'] ?? '', $field['email'] ?? '');
        }

        return null;
    }
}
