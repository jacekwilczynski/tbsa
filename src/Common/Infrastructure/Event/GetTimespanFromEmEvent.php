<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use DateTimeImmutable;
use EM_Event;
use MobilitySoft\TBSA\ValueObject\Timespan;

class GetTimespanFromEmEvent
{
    public function execute(EM_Event $emEvent): ?Timespan
    {
        $emStart = $emEvent->start();
        $emEnd   = $emEvent->end();

        if ( ! $emStart) {
            return null;
        }

        $wholeDay = (bool)$emEvent->event_all_day;
        $start    = new DateTimeImmutable($emStart->getDateTime(), $emStart->getTimezone());
        $end      = $emEnd ? new DateTimeImmutable($emEnd->getDateTime(), $emEnd->getTimezone()) : null;

        return new Timespan($wholeDay, $start, $end);
    }
}
