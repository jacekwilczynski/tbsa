<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Repository\BranchRepository;

class GetBranchFromAcfFields
{
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var GetBranchId
     */
    private $getBranchId;

    public function __construct(BranchRepository $branchRepository, GetBranchId $getBranchId)
    {
        $this->branchRepository = $branchRepository;
        $this->getBranchId      = $getBranchId;
    }

    public function getBranch($acfFields): ?BranchInterface
    {
        $id = $this->getId($acfFields);

        return $id ? $this->branchRepository->getById($id) : null;
    }

    public function getId($acfFields): ?int
    {
        return $this->getBranchId->execute($acfFields);
    }
}
