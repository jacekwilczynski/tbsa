<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use MobilitySoft\TBSA\Common\Domain\Event\Speaker;
use MobilitySoft\TBSA\Common\Domain\Event\SpeakerBusiness;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\Repository\EntrepreneurRepository;
use MobilitySoft\TBSA\View\Medium\MediumFactory;

class GetSpeakersFromAcfFields
{
    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var MediumFactory
     */
    private $mediumFactory;

    public function __construct(EntrepreneurRepository $entrepreneurRepository, MediumFactory $mediumFactory)
    {
        $this->entrepreneurRepository = $entrepreneurRepository;
        $this->mediumFactory = $mediumFactory;
    }

    /**
     * @return Speaker[]
     */
    public function execute(array $acfFields): array
    {
        return array_filter(array_map(function ($speakerData): ?Speaker {
            return $this->getSpeaker($speakerData);
        }, get_array_field('tbsa_speakers', $acfFields)));
    }

    private function getSpeaker(array $acfRow): ?Speaker
    {
        $type = $acfRow['type'] ?? 'none';

        if ($type === 'member' && (is_int($acfRow['entrepreneur'] ?? null))) {
            $entrepreneur = $this->entrepreneurRepository->getById($acfRow['entrepreneur']);
            if ($entrepreneur === null) {
                return null;
            }

            return new Speaker(
                $entrepreneur->getName(),
                get_the_permalink($entrepreneur->getId()),
                $this->mediumFactory->createMedium($entrepreneur->getPhoto()),
                $this->getDescription($acfRow, $entrepreneur),
                $this->getBusinessFromEntrepreneur($entrepreneur)
            );
        }

        if ($type === 'custom_data') {
            $customData = $acfRow['data'] ?? [];
            if (empty($customData['name'])) {
                return null;
            }

            return new Speaker(
                $customData['name'],
                $customData['url'] ?? '',
                $this->mediumFactory->createMedium($customData['imageUrl'] ?? null),
                $acfRow['description'] ?? null,
                $this->getBusinessFromCustomData($customData)
            );
        }

        return null;
    }

    private function getDescription(array $acfRow, ?EntrepreneurInterface $entrepreneur): ?string
    {
        $useCustomDescription = $acfRow['use_custom_description'] ?? false;
        return $useCustomDescription
            ? ($acfRow['description'] ?? null)
            : $entrepreneur->getDescription();
    }

    private function getBusinessFromEntrepreneur(EntrepreneurInterface $entrepreneur): ?SpeakerBusiness
    {
        $relation = $entrepreneur->getBusinessRelations()[0] ?? null;
        if ($relation === null) {
            return null;
        }

        $business = $relation->getBusiness();
        return new SpeakerBusiness($business->getName(), $business->getUrl());
    }

    private function getBusinessFromCustomData(array $customData): ?SpeakerBusiness
    {
        if (isset($customData['business']['name'], $customData['business']['url'])) {
            return new SpeakerBusiness(
                $customData['business']['name'],
                $customData['business']['url']
            );
        }

        return null;
    }
}
