<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use const MobilitySoft\TBSA\EVENT_POST_TYPE_SLUG;

class MigrateEventSpeakersToMulti
{
    private const FIELD_PREFIX = 'tbsa_speaker_';

    /**
     * @return int[] affected Event ids
     */
    public function execute(int $offset, int $limit, bool $dryRun): array
    {
        $ids = get_posts([
            'post_type'      => EVENT_POST_TYPE_SLUG,
            'posts_per_page' => $limit ?: -1,
            'offset'         => $offset,
            'fields'         => 'ids',
        ]);
        $affectedIds = [];
        foreach ($ids as $id) {
            $acfFields = get_fields($id);
            if ($this->usesMultipleSpeakers($acfFields)) {
                continue;
            }
            if (!$dryRun) {
                update_field('tbsa_speakers_multi', true, $id);
                update_field('tbsa_speakers', [$this->migrateSpeaker($acfFields)], $id);
            }
            $affectedIds[] = $id;
        }

        return $affectedIds;
    }

    private function migrateSpeaker(array $acfFields): array
    {
        $fieldPrefixLength = strlen(self::FIELD_PREFIX);
        $fields = [];
        foreach ($acfFields as $key => $value) {
            if (strpos($key, self::FIELD_PREFIX) === 0) {
                $fields[substr($key, $fieldPrefixLength)] = $value;
            }
        }
        return $fields;
    }

    private function usesMultipleSpeakers($acfFields): bool
    {
        $flagIsSetToMulti = get_field_safe('tbsa_speakers_multi', $acfFields);
        if (!$flagIsSetToMulti) {
            return false;
        }

        $speakers = get_array_field('tbsa_speakers', $acfFields);
        if (count($speakers) === 0) {
            return false;
        }

        $validSpeakers = array_filter($speakers, function ($speaker): bool {
            if (empty($speaker['type'])) {
                return false;
            }

            $isValidEntrepreneur = (
                $speaker['type'] === 'member' &&
                isset($speaker['entrepreneur']) &&
                is_int($speaker['entrepreneur'])
            );
            if ($isValidEntrepreneur) {
                return true;
            }

            $isValidCustomData = (
                $speaker['type'] === 'custom_data' &&
                isset($speaker['data']['name']) &&
                trim($speaker['data']['name']) !== ''
            );
            if ($isValidCustomData) {
                return true;
            }

            return false;
        });

        return count($validSpeakers) > 0;
    }
}
