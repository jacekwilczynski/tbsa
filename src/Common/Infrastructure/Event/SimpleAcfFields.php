<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use MobilitySoft\TBSA\ValueObject\Currency;
use MobilitySoft\TBSA\ValueObject\EventContact;
use MobilitySoft\TBSA\ValueObject\EventTicket;

class SimpleAcfFields
{
    /**
     * @var array
     */
    private $fields;

    private function __construct($fields = [])
    {
        $this->fields = $fields;
    }

    public static function prepare($fields): SimpleAcfFields
    {
        return new SimpleAcfFields($fields);
    }

    public function getGroupId(): ?int
    {
        return $this->fields['tbsa_event_groups'][0] ?? null;
    }

    public function getBranchId(): ?int
    {
        return $this->fields['tbsa_event_branches'][0] ?? null;
    }

    public function getContact(): ?EventContact
    {
        if (isset($this->fields['tbsa_event_contact'])) {
            $field = $this->fields['tbsa_event_contact'];

            return new EventContact($field['phone'] ?? '', $field['email'] ?? '');
        }

        return null;
    }

    public function getPrice(): int
    {
        $field = $this->fields['tbsa_event_price'] ?? null;

        return is_numeric($field) ? (int) $field : 0;
    }

    /**
     * @return EventTicket[]
     */
    public function getTickets(): array
    {
        $field = $this->fields['tbsa_event_tickets'] ?? null;

        if ( ! is_array($field)) {
            return [];
        }

        return array_map(function (array $ticketData) {
            $label = $ticketData['label'] ?? '';

            return new EventTicket(
                $label,
                $label,
                Currency::fromFloat((float) ($ticketData['price'] ?? 0)),
                $ticketData['description'] ?? ''
            );
        }, $field);
    }

    public function isExternal(): bool
    {
        return $this->fields['tbsa_event_is_external'] ?? false;
    }

    public function getExternalUrl(): ?string
    {
        return $this->fields['tbsa_external_event_url'] ?? null;
    }

    public function hasBookingsEnabled(): bool
    {
        return ($this->fields['tbsa_event_bookings_toggle'] ?? 'on') !== 'off';
    }
}
