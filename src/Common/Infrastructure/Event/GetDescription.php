<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use EM_Event;

class GetDescription
{
    /**
     * @param EM_Event $event
     * @param WP_Term|int|array|null
     *
     * @return string
     */
    public function execute(EM_Event $event, $category): string
    {
        $value = '';

        if ( ! empty($event->post_content)) {
            $value = $event->post_content;
        } elseif (isset($category)) {
            $value = (string)get_field_safe('description', $category, 'category');
        }

        return (string)apply_filters('the_content', $value);
    }
}
