<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use DateTimeImmutable;
use EM_Event;
use EM_Events;
use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use stdClass;
use const MobilitySoft\TBSA\INDUSTRY_TAXONOMY_SLUG;

class FindEmEvents
{
    /**
     * @param EventQuery $query
     * @param array|null $wpQueryOverride
     *
     * @return EM_Event[]
     */
    public function execute(EventQuery $query, ?array $wpQueryOverride = null): array
    {
        // This should be handled as a default parameter value but it doesn't work
        if ($wpQueryOverride === null) {
            $wpQueryOverride = [];
        }

        $useWpQuery = (bool) $wpQueryOverride;

        // Ids to which our queries will be limited
        $ids = [];

        // If necessary, we'll use WP query to query for ACF fields, which EM doesn't support
        $wpQueryArgs = [
            'post_type'      => 'event',
            'posts_per_page' => -1,
            'fields'         => 'ids',
        ];

        // Get ids by date
        // $ids will be passed to the following query, whether it's WP's or EM's
        // - in the case of a WP query, $ids will be overwritten afterwards,
        // and EM will be queried using the new list
        $startDate = $query->getStartDate();
        $endDate = $query->getEndDate();
        if ($startDate || $endDate) {
            $ids = $this->getIdsInDateRange($startDate, $endDate);
            $wpQueryArgs['post__in'] = $ids;
        }

        if ($query->getBranches() !== null) {
            $useWpQuery = true;
            if ($query->getBranches() === BranchesInQuery::ANY()) {
                $wpQueryArgs = array_merge($wpQueryArgs, [
                    'meta_key'     => 'tbsa_event_branches',
                    'meta_compare' => 'REGEXP',
                    'meta_value'   => '^$',
                ]);
            } else {
                $metaQuery = ['relation' => 'OR'];
                foreach ($query->getBranches()->getItems() as $branchId) {
                    $metaQuery[] = [
                        'key'     => 'tbsa_event_branches',
                        'compare' => 'LIKE',
                        'value'   => '"' . $branchId . '"',
                    ];
                }
                $wpQueryArgs = array_merge_recursive($wpQueryArgs, ['meta_query' => [$metaQuery]]);
            }
        }

        if ($query->getExternal() !== null) {
            $useWpQuery = true;
            $wpQueryArgs = array_merge_recursive($wpQueryArgs, [
                'meta_query' => [
                    [
                        'key'   => 'tbsa_event_is_external',
                        'value' => (int) $query->getExternal(),
                    ],
                ],
            ]);
        }

        if ($query->getIndustries()) {
            $useWpQuery = true;
            $wpQueryArgs['tax_query'] = [
                [
                    'taxonomy' => INDUSTRY_TAXONOMY_SLUG,
                    'field'    => 'name',
                    'terms'    => $query->getIndustries(),
                ],
            ];
        }

        $emQueryArgs = [
            'scope'  => $query->getScope()->getKey(),
            'offset' => $query->getOffset(),
        ];

        if ($useWpQuery) {
            // Overwriting $ids with the WP query results
            $args = array_merge_recursive($wpQueryArgs, $wpQueryOverride);
            $ids = get_posts($args);

            if (!$ids) {
                return [];
            }
        }

        if (!empty($ids)) {
            $emQueryArgs['post_id'] = implode(',', $ids);
        }

        if (!empty($query->getPhrase())) {
            $emQueryArgs['search'] = $query->getPhrase();
        }

        if (!empty($query->getCategories())) {
            $emQueryArgs['category'] = implode(',', $query->getCategories());
        }

        if ($query->getLimit() !== null) {
            // Times two because we will have to filter out Event Manager's
            // buggy events and we don't want to drop below the limit then
            $emQueryArgs['limit'] = $query->getLimit() * 2;
        }

        /** @var EM_Event[] $emEvents */
        $emEvents = EM_Events::get($emQueryArgs);

        // Filter away trash entries (they exist probably due to some EM bug,
        // or perhaps EM insists on returning also recurring event templates)
        $emEvents = array_values(array_filter($emEvents, function (EM_Event $event) { return $event->post_id; }));

        // Trim the array to the actual desired length
        if ($query->getLimit() !== null) {
            $emEvents = array_slice($emEvents, 0, $query->getLimit());
        }

        return $emEvents;
    }

    /**
     * @param DateTimeImmutable $startDate
     * @param DateTimeImmutable $endDate
     *
     * @return int[]
     */
    private function getIdsInDateRange(?DateTimeImmutable $startDate, ?DateTimeImmutable $endDate): array
    {
        global $wpdb;

        if ($startDate) {
            $startDateString = $startDate->format('Y-m-d');
            $startDateCondition = "event_end_date >= '$startDateString'";
        }

        if ($endDate) {
            $endDateString = $endDate->format('Y-m-d');
            $endDateCondition = "event_start_date <= '$endDateString'";
        }

        $condition = implode(' AND ', array_filter([$startDateCondition ?? null, $endDateCondition ?? null]));

        $dbQueryResult = $wpdb->get_results('SELECT post_id FROM ' . EM_EVENTS_TABLE . " WHERE $condition;");

        return array_filter(array_map(function (stdClass $item): ?int {
            return isset($item->post_id)
                ? (int) $item->post_id
                : null;
        }, $dbQueryResult));
    }
}
