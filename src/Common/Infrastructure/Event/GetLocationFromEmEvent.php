<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use EM_Event;
use MobilitySoft\TBSA\ValueObject\EventLocation;

class GetLocationFromEmEvent
{
    public function execute(EM_Event $event): ?EventLocation
    {
        $emLocation       = $event->get_location();
        $hasValidLocation = $emLocation && $emLocation->location_name;

        return $hasValidLocation
            ? new EventLocation(
                $emLocation->location_town,
                $emLocation->location_name,
                "$emLocation->location_address, $emLocation->location_town"
            )
            : null;
    }
}
