<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Infrastructure\Event;

use MobilitySoft\TBSA\Common\Domain\Event\Pricing\EventPricing;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\Free;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\OnePrice;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\TicketBased;
use MobilitySoft\TBSA\ValueObject\Currency;
use MobilitySoft\TBSA\ValueObject\EventTicket;

class GetPricingFromAcfFields
{
    public function execute($fields): EventPricing
    {
        $tickets = $this->getTickets($fields);
        if ($tickets) {
            return new TicketBased(...$tickets);
        }

        $price = $fields['tbsa_event_price'] ?? null;
        if (is_numeric($price) && $price > 0) {
            return new OnePrice(Currency::fromFloat((float)$price));
        }

        return new Free();
    }

    /**
     * @param $fields
     *
     * @return EventTicket[]
     */
    private function getTickets($fields): array
    {
        $field = $fields['tbsa_event_tickets'] ?? null;

        if ( ! is_array($field)) {
            return [];
        }

        return array_map(function (array $ticketData) {
            $label = $ticketData['label'] ?? '';

            return new EventTicket(
                $label,
                $label,
                Currency::fromFloat((float)($ticketData['price'] ?? 0)),
                $ticketData['description'] ?? ''
            );
        }, $field);
    }
}
