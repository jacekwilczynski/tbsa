<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain;

class BranchesInQuery
{
    /**
     * @var BranchesInQuery
     */
    private static $any;

    /**
     * @var int[]
     */
    private $items;

    public function __construct(int ...$items)
    {
        $this->items = $items;
    }

    public static function ANY(): BranchesInQuery
    {
        if ( ! self::$any) {
            self::$any = new self();
        }

        return self::$any;
    }

    public function getFirst(): ?int
    {
        return $this->items[0] ?? null;
    }

    /**
     * @return int[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
