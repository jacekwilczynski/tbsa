<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Submission;

use MobilitySoft\TBSA\Utils\DeserializableEnum;
use MobilitySoft\TBSA\Utils\Enum;

class SubmissionStatus extends Enum
{
    use DeserializableEnum;

    public static function ACCEPTED(): SubmissionStatus
    {
        return self::getInstance('ACCEPTED', ['hopefulness' => 2]);
    }

    public static function AWAITING(): SubmissionStatus
    {
        return self::getInstance('AWAITING', ['hopefulness' => 1]);
    }

    public static function REJECTED(): SubmissionStatus
    {
        return self::getInstance('REJECTED', ['hopefulness' => 0]);
    }

    public static function compare(SubmissionStatus $a, SubmissionStatus $b): int
    {
        return $a->getData()['hopefulness'] <=> $b->getData()['hopefulness'];
    }
}
