<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Exceptions;

use RuntimeException;

class EntityNotFoundException extends RuntimeException
{
    public function __construct(string $entityName, int $queriedId)
    {
        parent::__construct("$entityName with id $queriedId not found.");
    }
}
