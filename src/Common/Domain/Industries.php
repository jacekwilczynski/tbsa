<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain;

class Industries
{
    /**
     * @var string[]
     */
    private $items;

    public function __construct(string... $items)
    {
        $this->items = array_unique($items);
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public static function merge(Industries... $collections): Industries
    {
        $potentiallyRepeatingIndustriesArray = array_map(function (Industries $collection): array {
            return $collection->getItems();
        }, $collections);

        if (count($potentiallyRepeatingIndustriesArray) === 0) {
            return new Industries();
        }

        return new Industries(...array_merge(...$potentiallyRepeatingIndustriesArray));
    }
}
