<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event;

use DateTimeImmutable;
use InvalidArgumentException;
use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;

class EventQuery
{
    /**
     * @var string|null
     */
    protected $phrase;

    /**
     * @var DateTimeImmutable|null
     */
    protected $startDate;

    /**
     * @var DateTimeImmutable|null
     */
    protected $endDate;

    /**
     * @var BranchesInQuery|null
     */
    protected $branches;

    /**
     * @var EventQueryScope
     */
    protected $scope;

    /**
     * @var int[]|null
     */
    protected $categories;

    /**
     * @var int|null
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @var bool|null
     */
    protected $external;

    /**
     * @var string[]|null
     */
    protected $industries;

    public function __construct()
    {
        $this->scope = EventQueryScope::ALL();
    }

    public function getPhrase(): ?string
    {
        return $this->phrase;
    }

    public function setPhrase(?string $phrase): void
    {
        $this->phrase = $phrase;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    public function getStartDate(): ?DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTimeImmutable $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): ?DateTimeImmutable
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTimeImmutable $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function setDate(?DateTimeImmutable $date): void
    {
        $this->startDate = $date;
        $this->endDate   = $date;
    }

    public function getBranch(): ?int
    {
        $branches = $this->getBranches();

        return $branches ? $branches->getFirst() : null;
    }

    public function getBranches(): ?BranchesInQuery
    {
        return $this->branches;
    }

    public function setBranches(?BranchesInQuery $branches): void
    {
        $this->branches = $branches;
    }

    public function getScope(): EventQueryScope
    {
        return $this->scope;
    }

    public function setScope(EventQueryScope $scope): void
    {
        $this->scope = $scope;
    }

    /**
     * @return int[]|null
     */
    public function getCategories(): ?array
    {
        return $this->categories;
    }

    /**
     * @param int[]|null $categories
     */
    public function setCategories(?array $categories): void
    {
        $this->categories = $categories;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): void
    {
        if (is_int($limit) && $limit <= 0) {
            throw new InvalidArgumentException('Limit must be 1 or greater. For no limit, use null.');
        }

        $this->limit = $limit;
    }

    public function getExternal(): ?bool
    {
        return $this->external;
    }

    public function setExternal(?bool $external): void
    {
        $this->external = $external;
    }

    /**
     * @return string[]|null
     */
    public function getIndustries(): ?array
    {
        return $this->industries;
    }

    /**
     * @param string[]|null $industries
     */
    public function setIndustries(?array $industries): void
    {
        $this->industries = $industries;
    }
}
