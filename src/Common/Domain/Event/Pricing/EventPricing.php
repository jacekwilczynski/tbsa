<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Pricing;

interface EventPricing
{
    public function accept(EventPricingVisitor $visitor);
}
