<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Pricing;

class Free implements EventPricing
{
    public function accept(EventPricingVisitor $visitor)
    {
        return $visitor->visitFree($this);
    }
}
