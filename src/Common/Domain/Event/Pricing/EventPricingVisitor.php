<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Pricing;

interface EventPricingVisitor
{
    public function visitFree(Free $eventPricing);

    public function visitOnePrice(OnePrice $eventPricing);

    public function visitTicketBased(TicketBased $eventPricing);
}
