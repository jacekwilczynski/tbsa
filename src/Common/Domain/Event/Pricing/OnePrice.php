<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Pricing;

use MobilitySoft\TBSA\ValueObject\Currency;

class OnePrice implements EventPricing
{
    /**
     * @var Currency
     */
    private $price;

    public function __construct(Currency $price)
    {
        $this->price = $price;
    }

    /**
     * @return Currency
     */
    public function getPrice(): Currency
    {
        return $this->price;
    }

    public function accept(EventPricingVisitor $visitor)
    {
        return $visitor->visitOnePrice($this);
    }
}
