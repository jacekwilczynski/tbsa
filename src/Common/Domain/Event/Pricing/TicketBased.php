<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Pricing;

use MobilitySoft\TBSA\ValueObject\EventTicket;

class TicketBased implements EventPricing
{
    /**
     * @var EventTicket[]
     */
    private $tickets;

    public function __construct(EventTicket... $tickets)
    {
        $this->tickets = $tickets;
    }

    /**
     * @return EventTicket[]
     */
    public function getTickets(): array
    {
        return $this->tickets;
    }

    public function accept(EventPricingVisitor $visitor)
    {
        return $visitor->visitTicketBased($this);
    }
}
