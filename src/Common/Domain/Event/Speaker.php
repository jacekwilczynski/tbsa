<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event;

use MobilitySoft\TBSA\View\Medium\Medium;

class Speaker
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var Medium|null
     */
    private $photo;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var SpeakerBusiness|null
     */
    private $business;

    public function __construct(
        string $name,
        string $url,
        ?Medium $photo,
        ?string $description,
        ?SpeakerBusiness $business
    ) {
        $this->name = $name;
        $this->url = $url;
        $this->photo = $photo;
        $this->description = $description;
        $this->business = $business;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getPhoto(): ?Medium
    {
        return $this->photo;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getBusiness(): ?SpeakerBusiness
    {
        return $this->business;
    }
}
