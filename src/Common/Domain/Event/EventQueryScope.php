<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event;

use MobilitySoft\TBSA\Utils\DeserializableEnum;
use MobilitySoft\TBSA\Utils\Enum;

class EventQueryScope extends Enum
{
    use DeserializableEnum;

    public static function ALL(): EventQueryScope
    {
        return self::getInstance('ALL');
    }

    public static function FUTURE(): EventQueryScope
    {
        return self::getInstance('FUTURE');
    }
}
