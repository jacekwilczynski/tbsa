<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Agenda;

class Part
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var Item[]
     */
    private $items;

    public function __construct(string $title, Item... $items)
    {
        $this->title = $title;
        $this->items = $items;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
