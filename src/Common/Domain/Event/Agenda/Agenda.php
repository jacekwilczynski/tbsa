<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Agenda;

class Agenda
{
    /**
     * @var Part[]
     */
    private $parts;

    public function __construct(Part... $parts)
    {
        $this->parts = $parts;
    }

    /**
     * @return Part[]
     */
    public function getParts(): array
    {
        return $this->parts;
    }
}
