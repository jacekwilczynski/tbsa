<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Domain\Event\Agenda;

class Item
{
    /**
     * @var string
     */
    private $startTime;

    /**
     * @var string
     */
    private $description;

    /**
     * Item constructor.
     *
     * @param string $startTime
     * @param string $description
     */
    public function __construct(string $startTime, string $description)
    {
        $this->startTime   = $startTime;
        $this->description = $description;
    }

    public function getStartTime(): string
    {
        return $this->startTime;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
