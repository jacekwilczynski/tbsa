<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\Application\Event;

use MobilitySoft\TBSA\Common\Domain\Event\Pricing\EventPricingVisitor;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\Free;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\OnePrice;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\TicketBased;
use MobilitySoft\TBSA\ValueObject\EventTicket;

class GetFormattedEventPrice implements EventPricingVisitor
{
    /**
     * @var bool
     */
    private $emptyStringForZero;

    public function __construct(bool $emptyStringForZero = false)
    {
        $this->emptyStringForZero = $emptyStringForZero;
    }

    public function visitFree(Free $eventPricing): string
    {
        return $this->emptyStringForZero ? '' : wc_price(0);
    }

    public function visitOnePrice(OnePrice $eventPricing): string
    {
        return wc_price($eventPricing->getPrice()->getFloat());
    }

    public function visitTicketBased(TicketBased $eventPricing)
    {
        $tickets = $eventPricing->getTickets();

        if ($tickets) {
            if (count($tickets) > 1) {
                $prices = array_map(function (EventTicket $ticket) {
                    return $ticket->getPrice()->getFloat();
                }, $tickets);

                return wc_format_price_range(min(...$prices), max(...$prices));
            }

            return wc_price($tickets[0]->getPrice()->getFloat());
        }

        return wc_price(0);
    }
}
