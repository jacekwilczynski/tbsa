<?php

namespace MobilitySoft\TBSA\Common\Application\Event;

use DateTimeImmutable;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\Event\EventQueryScope;
use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;

class CreateEventQueryFromHttpRequestParams
{
    public function execute(array $params): EventQuery
    {
        $query = new EventQuery();

        if (isset($params['branch'])) {
            if ($params['branch'] === -1) {
                $query->setBranches(BranchesInQuery::ANY());
            } elseif (is_array($params['branch'])) {
                $query->setBranches(new BranchesInQuery(...$params['branch']));
            } else {
                $query->setBranches(new BranchesInQuery($params['branch']));
            }
        }

        if (isset($params['categories'])) {
            $query->setCategories($params['categories']);
        }

        if (isset($params['date'])) {
            $query->setDate(new DateTimeImmutable($params['date']));
        }

        if (isset($params['limit'])) {
            $query->setLimit($params['limit']);
        }

        if (isset($params['scope'])) {
            $query->setScope(EventQueryScope::fromString($params['scope']));
        }

        return $query;
    }
}
