<?php

namespace MobilitySoft\TBSA\Common\Application;

use MobilitySoft\TBSA\Common\View\Renderer;

interface InitializableRenderer extends Renderer
{
    public function initialize(): void;
}
