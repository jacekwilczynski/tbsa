<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\View;

class AutoHref
{
    public static function build(string $address): string
    {
        if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
            return 'mailto:' . $address;
        }

        if (preg_match('/^[\d+()*]/', $address)) {
            return 'tel:' . $address;
        }

        return $address;
    }
}
