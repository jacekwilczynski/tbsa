<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\View;

class Anchor implements Renderer
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var array
     */
    private $attributes = [];

    private function __construct(string $address, string $content, bool $external, array $attributes)
    {
        $this->content            = $content;
        $this->attributes['href'] = AutoHref::build($address);
        if ($external) {
            $this->attributes['target'] = '_blank';
            $this->attributes['rel']    = 'noopener noreferrer';
        }
        $this->attributes = array_merge($this->attributes, $attributes);
    }

    public static function simple(string $address, ?bool $external = false): Anchor
    {
        return new Anchor($address, $address, $external, []);
    }

    public static function presentable(
        string $address,
        string $content,
        ?bool $external = false,
        ?array $attributes = []
    ): Anchor {
        return new Anchor($address, $content, $external, $attributes);
    }

    public function __toString(): string
    {
        return $this->render();
    }

    public function render(): string
    {
        $attributeString = implode(' ', array_map(static function ($key, $value) {
            return sprintf('%s="%s"', $key, $value);
        }, array_keys($this->attributes), $this->attributes));

        return sprintf(
            '<a %s>%s</a>',
            $attributeString,
            $this->content
        );
    }
}
