<?php

namespace MobilitySoft\TBSA\Common\View;

interface Renderer
{
    public function render(): string;
}
