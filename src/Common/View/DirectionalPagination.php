<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\View;

use MobilitySoft\TBSA\Utils\Pagination\GetPageUrl;
use MobilitySoft\TBSA\Utils\Pagination\PaginationState;

class DirectionalPagination
{
    /**
     * @var GetPageUrl
     */
    private $getPageUrl;

    /**
     * @var PaginationState
     */
    private $state;

    public function __construct(GetPageUrl $getPageUrl, PaginationState $state)
    {
        $this->getPageUrl = $getPageUrl;
        $this->state      = $state;
    }

    public function getCurrentPage(): int
    {
        return $this->state->getCurrentPage();
    }

    public function getNumberOfPages(): int
    {
        return $this->state->getNumberOfPages();
    }

    public function getPreviousPageUrl(): ?string
    {
        $previousPage = $this->state->getPreviousPage();

        return $previousPage ? $this->getPageUrl->execute($previousPage) : null;
    }

    public function getNextPageUrl(): ?string
    {
        $previousPage = $this->state->getNextPage();

        return $previousPage ? $this->getPageUrl->execute($previousPage) : null;
    }
}
