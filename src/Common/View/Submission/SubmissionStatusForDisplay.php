<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Common\View\Submission;

use InvalidArgumentException;
use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;

class SubmissionStatusForDisplay
{
    /**
     * @var string
     */
    private $raw;

    /**
     * @var string
     */
    private $formatted;

    public function __construct(SubmissionStatus $status)
    {
        $this->raw = (string) $status;
        $this->formatted = $this->translate($status);
    }

    public function getRaw(): string
    {
        return $this->raw;
    }

    public function getFormatted(): string
    {
        return $this->formatted;
    }

    private function translate(SubmissionStatus $status): string
    {
        switch ($status) {
            case SubmissionStatus::AWAITING():
                return __('Oczekuje na akceptację');
            case SubmissionStatus::ACCEPTED():
                return __('Zaakceptowany');
            case SubmissionStatus::REJECTED():
                return __('Odrzucony');
            default:
                throw new InvalidArgumentException("Invalid submission status: {$status}");
        }
    }
}
