<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\Medium;

class WpImage implements Medium
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $defaultSize;

    public function __construct(int $id, ?string $defaultSize = 'full')
    {
        $this->id          = $id;
        $this->defaultSize = $defaultSize;
    }

    public function getUrl(...$args): string
    {
        return wp_get_attachment_image_url($this->id, ...($args ?: [$this->defaultSize])) ?: '';
    }

    public function getMarkup(...$args): string
    {
        return wp_get_attachment_image($this->id, ...($args ?: [$this->defaultSize]));
    }

    public function getId(): int
    {
        return $this->id;
    }
}
