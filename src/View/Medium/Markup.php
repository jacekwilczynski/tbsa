<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\Medium;

class Markup implements Medium
{
    /**
     * @var string
     */
    private $markup;

    /**
     * @var string|null
     */
    private $url;

    public function __construct(string $markup)
    {
        $this->markup = $markup;
    }

    public function getUrl(...$args): string
    {
        if (empty($this->url)) {
            preg_match('/src="([^"]*)"/i', $this->markup, $matches);
            $this->url = $matches[1] ?? '';
        }

        return $this->url;
    }

    public function getMarkup(...$args): string
    {
        return $this->markup;
    }
}
