<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\Medium;

class ImageUrl implements Medium
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $imgElementAtts;

    public function __construct(string $url, ?array $imgElementAtts = [])
    {
        $this->url            = $url;
        $this->imgElementAtts = $imgElementAtts;
    }

    public function getMarkup(...$args): string
    {
        /** @noinspection ALL */
        $format = '<img src="%s" %s>';

        return sprintf(
            $format,
            $this->getUrl(),
            render_xml_attributes($this->imgElementAtts)
        );
    }

    public function getUrl(...$args): string
    {
        return $this->url;
    }
}
