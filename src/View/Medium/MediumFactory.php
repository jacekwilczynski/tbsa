<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\Medium;

class MediumFactory
{
    private const MARKUP_PATTERN = '/^\s*</';

    public function createMedium($data): ?Medium
    {
        if (is_array($data)) {
            return new WpImage($data['ID']);
        }

        if (is_numeric($data)) {
            return new WpImage($data);
        }

        if (is_string($data)) {
            if (preg_match(self::MARKUP_PATTERN, $data)) {
                return new Markup($data);
            }

            return new ImageUrl($data);
        }

        return null;
    }

    public function serializeMedium(?Medium $medium)
    {
        if (!$medium) {
            return null;
        }

        if ($medium instanceof WpImage) {
            return $medium->getId();
        }

        return $medium->getUrl();
    }
}
