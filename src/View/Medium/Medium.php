<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\Medium;

interface Medium
{
    public function getUrl(...$args): string;

    public function getMarkup(...$args): string;
}
