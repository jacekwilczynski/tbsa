<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Infrastructure;

use MobilitySoft\TBSA\View\EventPage\Domain\EventCategory;
use MobilitySoft\TBSA\View\EventPage\Domain\EventCategoryRepository as EventCategoryRepositoryInterface;
use WP_Term;

class EventCategoryRepository implements EventCategoryRepositoryInterface
{
    public function getById(int $id): ?EventCategory
    {
        $term = get_term($id);

        return $term ? $this->getFromTerm($term) : null;
    }

    private function getFromTerm(WP_Term $term): EventCategory
    {
        return new EventCategory($term->term_id, $term->slug, $term->name);
    }
}
