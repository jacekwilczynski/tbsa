<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Infrastructure;

use MobilitySoft\TBSA\View\EventPage\Domain\Media;
use MobilitySoft\TBSA\View\Medium\MediumFactory;

class GetMedia
{
    /**
     * @var MediumFactory
     */
    private $mediumFactory;

    /**
     * GetMediaFromAcfFields constructor.
     *
     * @param MediumFactory $mediumFactory
     */
    public function __construct(MediumFactory $mediumFactory)
    {
        $this->mediumFactory = $mediumFactory;
    }

    /**
     * @param array|null $eventAcfFields
     * @param WP_Term|int|array|null $category
     *
     * @return Media
     */
    public function execute($eventAcfFields, $category = null): Media
    {
        $cover = $this->mediumFactory->createMedium(
            fallback(
                get_field_safe('event_page_top_image', $eventAcfFields),
                get_field_safe('event_page_top_image', $category, 'category')
            )
        );

        $supplementary = get_field_safe('event_page_description_fullwidth', $eventAcfFields)
            ? null
            : $this->mediumFactory->createMedium(fallback(
                get_field_safe('event_page_description_image', $eventAcfFields),
                get_field_safe('video', $category, 'category')
            ));

        $galleryField = $eventAcfFields['event_page_gallery'] ?? null;
        $gallery      = is_array($galleryField)
            ? array_map(function ($item) { return $this->mediumFactory->createMedium($item); }, $galleryField)
            : [];

        return new Media($cover, $supplementary, ...$gallery);
    }
}
