<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Infrastructure;

use InvalidArgumentException;
use MobilitySoft\TBSA\Common\Domain\Event\Agenda\Agenda;
use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetAgendaFromAcfFields;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetBranchFromAcfFields;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetCategoryFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetContactFromAcfFields;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetDescription;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetLocationFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetPricingFromAcfFields;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetSpeakersFromAcfFields;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetTimespanFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\SimpleAcfFields;
use MobilitySoft\TBSA\Domain\Booking\BookingRepository;
use MobilitySoft\TBSA\View\EventPage\Domain\Benefits;
use MobilitySoft\TBSA\View\EventPage\Domain\Event;
use MobilitySoft\TBSA\View\EventPage\Domain\EventCategoryRepository as EventCategoryRepositoryInterface;
use MobilitySoft\TBSA\View\EventPage\Domain\EventRepository as EventRepositoryInterface;
use MobilitySoft\TBSA\View\EventPage\Domain\EventWithBookings;
use const MobilitySoft\TBSA\INDUSTRY_TAXONOMY_SLUG;

class EventRepository implements EventRepositoryInterface
{
    /**
     * @var BookingRepository
     */
    private $bookingRepository;

    /**
     * @var EventCategoryRepositoryInterface
     */
    private $eventCategoryRepository;

    /**
     * @var GetAgendaFromAcfFields
     */
    private $getAgendaFromAcfFields;

    /**
     * @var GetBranchFromAcfFields
     */
    private $getBranchFromAcfFields;

    /**
     * @var GetCategoryFromEmEvent
     */
    private $getCategoryFromEmEvent;

    /**
     * @var GetContactFromAcfFields
     */
    private $getContactFromAcfFields;

    /**
     * @var GetDescription
     */
    private $getDescription;

    /**
     * @var GetLocationFromEmEvent
     */
    private $getLocationFromEmEvent;

    /**
     * @var GetMedia
     */
    private $getMedia;

    /**
     * @var GetPricingFromAcfFields
     */
    private $getPricingFromAcfFields;

    /**
     * @var GetSpeakersFromAcfFields
     */
    private $getSpeakersFromAcfFields;

    /**
     * @var GetTimespanFromEmEvent
     */
    private $getTimespanFromEmEvent;

    public function __construct(
        BookingRepository $bookingRepository,
        GetAgendaFromAcfFields $getAgendaFromAcfFields,
        GetBranchFromAcfFields $getBranchFromAcfFields,
        GetCategoryFromEmEvent $getCategoryFromEmEvent,
        EventCategoryRepositoryInterface $eventCategoryRepository,
        GetContactFromAcfFields $getContactFromAcfFields,
        GetDescription $getDescription,
        GetLocationFromEmEvent $getLocationFromEmEvent,
        GetMedia $getMedia,
        GetPricingFromAcfFields $getPricingFromAcfFields,
        GetSpeakersFromAcfFields $getSpeakersFromAcfFields,
        GetTimespanFromEmEvent $getTimespanFromEmEvent
    ) {
        $this->bookingRepository = $bookingRepository;
        $this->eventCategoryRepository = $eventCategoryRepository;
        $this->getAgendaFromAcfFields = $getAgendaFromAcfFields;
        $this->getBranchFromAcfFields = $getBranchFromAcfFields;
        $this->getCategoryFromEmEvent = $getCategoryFromEmEvent;
        $this->getContactFromAcfFields = $getContactFromAcfFields;
        $this->getDescription = $getDescription;
        $this->getLocationFromEmEvent = $getLocationFromEmEvent;
        $this->getMedia = $getMedia;
        $this->getPricingFromAcfFields = $getPricingFromAcfFields;
        $this->getSpeakersFromAcfFields = $getSpeakersFromAcfFields;
        $this->getTimespanFromEmEvent = $getTimespanFromEmEvent;
    }

    public function getEvent(int $id): ?Event
    {
        return $this->buildEvent($id);
    }

    public function getEventWithBookings(int $id): ?EventWithBookings
    {
        /** @var EventWithBookings $event */
        $event = $this->buildEvent($id, EventWithBookings::class);
        $event->setBookings(...$this->bookingRepository->getByEventId($id));

        return $event;
    }

    private function buildEvent(int $id, ?string $class = Event::class): ?Event
    {
        if ($class !== Event::class && !is_subclass_of($class, Event::class)) {
            throw new InvalidArgumentException(sprintf(
                'Supplied class %s does not fulfill the requirement of being substitutable for %s.',
                $class,
                Event::class
            ));
        }

        $emEvent = em_get_event($id, 'post_id');

        if (!$emEvent) {
            return null;
        }

        $categoryId = $this->getCategoryFromEmEvent->getId($emEvent);
        $acfFields = get_fields($id);

        /** @var Event $event */
        $event = new $class(
            $id,
            $emEvent->post_title,
            $this->getTimespanFromEmEvent->execute($emEvent)
        );

        if (is_int($categoryId)) {
            $event->setCategory($this->eventCategoryRepository->getById($categoryId));
        }
        $event->setPricing($this->getPricingFromAcfFields->execute($acfFields));
        $event->setMedia($this->getMedia->execute($acfFields, $categoryId));
        $event->setUrl(get_the_permalink($id));
        $event->setLocation($this->getLocationFromEmEvent->execute($emEvent));
        $event->setBranch($this->getBranchFromAcfFields->getBranch($acfFields));
        $event->setContact($this->getContactFromAcfFields->execute($acfFields));
        $event->setDescription($this->getDescription->execute($emEvent, $categoryId));
        $event->setSpeakers(...$this->getSpeakersFromAcfFields->execute($acfFields));
        $event->setAgenda($this->getAgenda($acfFields, $categoryId));
        $event->setIndustries(new Industries(...get_term_names(INDUSTRY_TAXONOMY_SLUG, $id)));
        $event->setBenefits($this->getBenefits($id));
        if (!SimpleAcfFields::prepare($acfFields)->hasBookingsEnabled()) {
            $event->disableBookings();
        }

        return $event;
    }

    private function getAgenda($acfFields, $category = null): ?Agenda
    {
        $fromEvent = $this->getAgendaFromAcfFields->execute($acfFields);
        if ($fromEvent) {
            return $fromEvent;
        }

        if ($category) {
            return $this->getAgendaFromAcfFields->execute(get_fields("category_$category"));
        }

        return null;
    }

    private function getBenefits(int $id): Benefits
    {
        $items = get_array_field('tbsa_event_benefits', $id);
        return new Benefits(...array_filter(array_map(function (array $item): string {
            return $item['text'] ?? '';
        }, $items)));
    }
}
