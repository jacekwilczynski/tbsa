<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Domain;

class EventCategory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $title;

    public function __construct(int $id, string $slug, string $title)
    {
        $this->id    = $id;
        $this->slug  = $slug;
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
