<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Domain;

use MobilitySoft\TBSA\View\Medium\Medium;

class Media
{
    /**
     * @var Medium|null
     */
    private $cover;

    /**
     * @var Medium|null
     */
    private $supplementary;

    /**
     * @var Medium[]
     */
    private $gallery;

    /**
     * EventPageMedia constructor.
     *
     * @param Medium|null $cover
     * @param Medium|null $supplementary
     * @param Medium[] $gallery
     */
    public function __construct(?Medium $cover = null, ?Medium $supplementary = null, Medium... $gallery)
    {
        $this->cover         = $cover;
        $this->supplementary = $supplementary;
        $this->gallery       = $gallery;
    }

    /**
     * @return Medium|null
     */
    public function getCover(): ?Medium
    {
        return $this->cover;
    }

    /**
     * @return Medium|null
     */
    public function getSupplementary(): ?Medium
    {
        return $this->supplementary;
    }

    /**
     * @return Medium[]
     */
    public function getGallery(): array
    {
        return $this->gallery;
    }
}
