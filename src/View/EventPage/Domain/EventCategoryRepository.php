<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Domain;

interface EventCategoryRepository
{
    public function getById(int $id): ?EventCategory;
}
