<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Domain;

interface EventRepository
{
    public function getEvent(int $id): ?Event;

    public function getEventWithBookings(int $id): ?EventWithBookings;
}
