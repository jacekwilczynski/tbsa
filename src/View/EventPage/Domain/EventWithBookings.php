<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Domain;

use MobilitySoft\TBSA\Domain\Booking\Booking;

class EventWithBookings extends Event
{
    /**
     * @var Booking[]
     */
    private $bookings;

    /**
     * @return Booking[]
     */
    public function getBookings(): array
    {
        return $this->bookings;
    }

    /**
     * @param Booking[] $bookings
     */
    public function setBookings(Booking... $bookings): void
    {
        $this->bookings = $bookings;
    }
}
