<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Domain;

class Benefits implements \IteratorAggregate, \Countable
{
    /**
     * @var string[]
     */
    private $items;

    public function __construct(string... $items)
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function count(): int
    {
        return count($this->items);
    }
}
