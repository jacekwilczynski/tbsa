<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\View\EventPage\Domain;

use MobilitySoft\TBSA\Common\Domain\Event\Agenda\Agenda;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\EventPricing;
use MobilitySoft\TBSA\Common\Domain\Event\Pricing\Free;
use MobilitySoft\TBSA\Common\Domain\Event\Speaker;
use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\ValueObject\EventContact;
use MobilitySoft\TBSA\ValueObject\EventLocation;
use MobilitySoft\TBSA\ValueObject\Timespan;

class Event
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Timespan
     */
    private $timespan;

    /**
     * @var EventCategory
     */
    private $category;

    /**
     * @var EventPricing
     */
    private $pricing;

    /**
     * @var Media
     */
    private $media;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var EventLocation|null
     */
    private $location;

    /**
     * @var BranchInterface|null
     */
    private $branch;

    /**
     * @var EventContact|null
     */
    private $contact;

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var Speaker[]
     */
    private $speakers;

    /**
     * @var Agenda|null
     */
    private $agenda;

    /**
     * @var bool
     */
    private $bookingsEnabled = true;

    /**
     * @var Industries
     */
    private $industries;

    /**
     * @var Benefits
     */
    private $benefits;

    public function __construct(int $id, string $title, Timespan $timespan)
    {
        $this->id = $id;
        $this->title = $title;
        $this->timespan = $timespan;
        $this->pricing = new Free();
        $this->media = new Media();
        $this->industries = new Industries();
    }

    public function getCategory(): EventCategory
    {
        return $this->category;
    }

    public function setCategory(EventCategory $category): void
    {
        $this->category = $category;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPricing(): EventPricing
    {
        return $this->pricing;
    }

    public function setPricing(EventPricing $pricing): void
    {
        $this->pricing = $pricing;
    }

    public function getMedia(): Media
    {
        return $this->media;
    }

    public function setMedia(Media $media): void
    {
        $this->media = $media;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function getLocation(): ?EventLocation
    {
        return $this->location;
    }

    public function setLocation(?EventLocation $location): void
    {
        $this->location = $location;
    }

    public function getBranch(): ?BranchInterface
    {
        return $this->branch;
    }

    public function setBranch(?BranchInterface $branch): void
    {
        $this->branch = $branch;
    }

    public function getContact(): ?EventContact
    {
        return $this->contact;
    }

    public function setContact(?EventContact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return Speaker[]
     */
    public function getSpeakers(): array
    {
        return $this->speakers;
    }

    /**
     * @param Speaker[] $speakers
     */
    public function setSpeakers(Speaker... $speakers): void
    {
        $this->speakers = $speakers;
    }

    public function getAgenda(): ?Agenda
    {
        return $this->agenda;
    }

    public function setAgenda(?Agenda $agenda): void
    {
        $this->agenda = $agenda;
    }

    public function disableBookings(): void
    {
        $this->bookingsEnabled = false;
    }

    public function isBookable(): bool
    {
        return (
            $this->bookingsEnabled &&
            $this->getTimespan()->getStart()->getTimestamp() > time()
        );
    }

    public function getTimespan(): Timespan
    {
        return $this->timespan;
    }

    public function getIndustries(): Industries
    {
        return $this->industries;
    }

    public function setIndustries(Industries $industries): void
    {
        $this->industries = $industries;
    }

    public function getBenefits(): Benefits
    {
        return $this->benefits;
    }

    public function setBenefits(Benefits $benefits): void
    {
        $this->benefits = $benefits;
    }
}
