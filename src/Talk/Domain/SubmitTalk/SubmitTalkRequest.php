<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\SubmitTalk;

class SubmitTalkRequest
{
    /**
     * @var int
     */
    private $eventId;

    /**
     * @var int
     */
    private $templateId;

    public function __construct(int $eventId, int $templateId)
    {
        $this->eventId    = $eventId;
        $this->templateId = $templateId;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }

    /**
     * @return int
     */
    public function getTemplateId(): int
    {
        return $this->templateId;
    }
}
