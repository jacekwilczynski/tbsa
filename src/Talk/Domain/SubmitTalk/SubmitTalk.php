<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\SubmitTalk;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;
use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Event\EventNotFoundException;
use MobilitySoft\TBSA\Talk\Domain\Event\EventRepository;
use MobilitySoft\TBSA\Talk\Domain\Exception\NotAuthenticatedException;
use MobilitySoft\TBSA\Talk\Domain\Exception\NotAllowedToSubmitTalkException;
use MobilitySoft\TBSA\Talk\Domain\Submission\CreateSubmissionFromTemplate;
use MobilitySoft\TBSA\Talk\Domain\Exception\EventNotAvailableException;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\SubmissionConsumer;
use MobilitySoft\TBSA\Talk\Domain\TalkAccessChecker;
use MobilitySoft\TBSA\Talk\Domain\Template\Template;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateNotFoundException;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateRepository;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;

class SubmitTalk
{
    /**
     * @var TalkAccessChecker
     */
    private $talkAccessChecker;

    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * @var SubmissionRepository
     */
    private $submissionRepository;

    /**
     * @var TemplateRepository
     */
    private $templateRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var SubmissionConsumer|null
     */
    private $onSuccess;

    public function __construct(
        TalkAccessChecker $talkAccessChecker,
        EventRepository $eventRepository,
        SubmissionRepository $submissionRepository,
        TemplateRepository $templateRepository,
        UserRepository $userRepository,
        ?SubmissionConsumer $onSuccess
    ) {
        $this->talkAccessChecker = $talkAccessChecker;
        $this->eventRepository = $eventRepository;
        $this->submissionRepository = $submissionRepository;
        $this->templateRepository = $templateRepository;
        $this->userRepository = $userRepository;
        $this->onSuccess = $onSuccess;
    }

    /**
     * @throws EntityNotFoundException
     * @throws NotAuthenticatedException
     * @throws NotAllowedToSubmitTalkException
     * @throws EventNotAvailableException
     */
    public function execute(SubmitTalkRequest $request): void
    {
        $submitter = $this->userRepository->getCurrentUser();
        if (!$submitter) {
            throw new NotAuthenticatedException();
        }

        $template = $this->getTemplate($request, $submitter);
        $reasonsToPreventSubmission = $submitter->getReasonsToPreventSubmission($template);
        if (count($reasonsToPreventSubmission) > 0) {
            throw new NotAllowedToSubmitTalkException(
                $template->getId(),
                $submitter->getId(),
                $reasonsToPreventSubmission
            );
        }

        $event = $this->getEvent($request);
        if (!$event->acceptsSubmissions()) {
            throw new EventNotAvailableException($event);
        }

        $submission = $this->createSubmissionFromTemplate($submitter, $template, $event);
        $this->submissionRepository->save($submission);

        if ($this->onSuccess) {
            $this->onSuccess->execute($submission);
        }
    }

    private function getTemplate(SubmitTalkRequest $request, User $submitter): Template
    {
        $id = $request->getTemplateId();
        $template = $this->templateRepository->findTemplateById($id);
        if (!$template) {
            throw new TemplateNotFoundException($id);
        }

        if (!$this->talkAccessChecker->canUserEditTalkWithId($submitter, $id)) {
            throw new NotAllowedToSubmitTalkException(
                $template->getId(),
                $submitter->getId(),
                ['The user does not have access to the template.']
            );
        }

        return $template;
    }

    private function getEvent(SubmitTalkRequest $request): Event
    {
        $event = $this->eventRepository->getEventById($request->getEventId());
        if (!$event) {
            throw new EventNotFoundException($request->getEventId());
        }

        return $event;
    }

    private function createSubmissionFromTemplate(?User $submitter, Template $template, Event $event): Submission
    {
        return new Submission(
            0,
            $event,
            $template->getSpeakers(),
            $template->getTalk(),
            $submitter,
            SubmissionStatus::AWAITING()
        );
    }
}
