<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use DateInterval;
use DateTimeImmutable;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\Event\EventQueryScope;
use MobilitySoft\TBSA\Talk\Domain\Event\EventQuery as TalkEventQuery;
use MobilitySoft\TBSA\Talk\Domain\Event\EventRepository;
use MobilitySoft\TBSA\Talk\Domain\Event\Events;
use MobilitySoft\TBSA\Talk\Domain\Event\EventStatuses;
use MobilitySoft\TBSA\Talk\Domain\Event\FindEvents;

class FindAvailableEvents implements FindEvents
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function execute(EventQuery $query): Events
    {
        return $this->eventRepository->findEvents($this->ensureQueryOnlyForAvailables($query));
    }

    private function ensureQueryOnlyForAvailables(EventQuery $query): TalkEventQuery
    {
        $query = TalkEventQuery::fromCommon($query);
        $query->setStatuses(EventStatuses::available());
        $query->setScope(EventQueryScope::FUTURE());

        $today = (new DateTimeImmutable())->setTime(0, 0, 0);
        $tomorrow = $today->add(new DateInterval('P1D'));
        if (!$query->getStartDate() || $query->getStartDate() < $tomorrow) {
            $query->setStartDate($tomorrow);
        }

        return $query;
    }
}
