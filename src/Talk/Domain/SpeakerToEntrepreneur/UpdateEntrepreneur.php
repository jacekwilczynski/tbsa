<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\SpeakerToEntrepreneur;

use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Contact;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\Talk\Domain\Speaker\Speaker;
use MobilitySoft\TBSA\ValueObject\Image;
use MobilitySoft\TBSA\View\Medium\MediumFactory;

class UpdateEntrepreneur
{
    /**
     * @var MediumFactory
     */
    private $mediumFactory;

    public function __construct(MediumFactory $mediumFactory)
    {
        $this->mediumFactory = $mediumFactory;
    }

    public function exceute(Entrepreneur $entrepreneur, Speaker $speaker): Entrepreneur
    {
        return new Entrepreneur(
            $entrepreneur->getId(),
            $entrepreneur->getName(),
            new Contact(
                $speaker->getContact()->getEmail(),
                $speaker->getContact()->getPhone()
            ),
            new Image((int)$this->mediumFactory->serializeMedium($speaker->getPhoto())),
            $speaker->getDescription(),
            $entrepreneur->getBusinesses()
        );
    }
}
