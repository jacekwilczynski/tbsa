<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\SpeakerToEntrepreneur;

use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\EntrepreneurRepository;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;

class UpdateEntrepreneursFromSubmission
{
    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var UpdateEntrepreneur
     */
    private $updateEntrepreneur;

    public function __construct(EntrepreneurRepository $entrepreneurRepository, UpdateEntrepreneur $applyData)
    {
        $this->entrepreneurRepository = $entrepreneurRepository;
        $this->updateEntrepreneur = $applyData;
    }

    public function execute(Submission $submission): void
    {
        foreach ($submission->getSpeakers()->getItems() as $speaker) {
            if (
                !$speaker->shouldBeCopiedToEntrepreneur() ||
                $speaker->getEntrepreneurId() === null
            ) {
                continue;
            }

            $entrepreneur = $this->entrepreneurRepository->findById($speaker->getEntrepreneurId());
            if (!$entrepreneur) {
                continue;
            }

            $updatedEntrepreneur = $this->updateEntrepreneur->exceute($entrepreneur, $speaker);
            $this->entrepreneurRepository->update($updatedEntrepreneur);
        }
    }
}
