<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class SubmissionAccessChecker
{
    public function canUserEditSubmission(User $user, Submission $submission): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($submission->getStatus() !== SubmissionStatus::AWAITING()) {
            return false;
        }

        $eventBranch     = $submission->getEvent()->getBranch();
        $managedBranches = $user->getManagedBranches();

        if ($eventBranch !== null) {
            return $managedBranches->includes($eventBranch);
        }

        return false;
    }
}
