<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Branch;

interface BranchRepository
{
    public function getBranchById(int $id): ?Branch;

    public function getNonVirtualBranches(): Branches;

    /**
     * @return int[]
     */
    public function getNonVirtualBranchIds(): array;

    public function getBranchesByIds(int... $ids): Branches;
}
