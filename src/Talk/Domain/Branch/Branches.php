<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Branch;

class Branches
{
    /**
     * @var Branch[]
     */
    private $items;

    public function __construct(Branch... $items)
    {
        $this->items = $items;
        usort($this->items, function (Branch $a, Branch $b) { return strcoll($a->getName(), $b->getName()); });
    }

    /**
     * @return Branch[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function includes(Branch $branch): bool
    {
        return (bool)array_find($this->items, function (Branch $item) use ($branch) {
            return $item->equals($branch);
        });
    }

    public function getFirst(): ?Branch
    {
        return $this->items[0] ?? null;
    }

    /**
     * @return int[]
     */
    public function getIds(): array
    {
        return array_map(function (Branch $branch) {
            return $branch->getId();
        }, $this->items);
    }
}
