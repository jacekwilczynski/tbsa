<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Branch;

class Branch
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $town;

    public function __construct(
        int $id,
        string $name,
        string $url,
        string $town
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->town = $town;
    }

    public function getTown(): string
    {
        return $this->town;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function equals(Branch $other): bool
    {
        return $this->getId() === $other->getId();
    }

    public function getId(): int
    {
        return $this->id;
    }
}
