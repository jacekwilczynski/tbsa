<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

interface DetectTemplateVsSubmission
{
    public function execute(?int $talkId): TemplateVsSubmission;
}
