<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneurs;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use MobilitySoft\TBSA\Talk\Domain\User\Users;

class FindIndustriesInBranch
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(int $branchId): Industries
    {
        $users = $this->userRepository->getBranchMembers($branchId);
        $entrepreneurs = $this->extractEntrepreneursFromUsers($users);
        return $this->extractIndustriesFromEntrepreneurs($entrepreneurs);
    }

    private function extractEntrepreneursFromUsers(Users $users): Entrepreneurs
    {
        return new Entrepreneurs(...array_filter(array_map(function (User $user): ?Entrepreneur {
            return $user->getEntrepreneur();
        }, $users->getItems())));
    }

    private function extractIndustriesFromEntrepreneurs(Entrepreneurs $entrepreneurs): Industries
    {
        return Industries::merge(...array_map(function (Entrepreneur $entrepreneur): Industries {
            return $entrepreneur->getIndustries();
        }, $entrepreneurs->getItems()));
    }
}
