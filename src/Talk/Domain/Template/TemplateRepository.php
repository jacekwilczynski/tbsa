<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Template;

interface TemplateRepository
{
    public function findTemplates(TemplateQuery $query): Templates;

    public function countTemplates(TemplateQuery $query): int;

    public function findTemplateById(int $id): ?Template;

    public function getLatestCreatedTemplateId(int $userId): ?int;
}
