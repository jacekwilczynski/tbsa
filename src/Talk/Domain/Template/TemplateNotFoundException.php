<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Template;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;

class TemplateNotFoundException extends EntityNotFoundException
{
    public function __construct(int $queriedId)
    {
        parent::__construct('Talk template', $queriedId);
    }
}
