<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Template;

use MobilitySoft\TBSA\Talk\Domain\Speaker\Speakers;
use MobilitySoft\TBSA\Talk\Domain\Talk\Talk;
use MobilitySoft\TBSA\Talk\Domain\User\Users;

class Template
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Speakers
     */
    private $speakers;

    /**
     * @var Talk
     */
    private $talk;

    /**
     * @var Users
     */
    private $owners;

    public function __construct(int $id, Speakers $speakers, Talk $talk, Users $owners)
    {
        $this->id = $id;
        $this->speakers = $speakers;
        $this->talk = $talk;
        $this->owners = $owners;
    }

    public function getOwners(): Users
    {
        return $this->owners;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSpeakers(): Speakers
    {
        return $this->speakers;
    }

    public function getTalk(): Talk
    {
        return $this->talk;
    }
}
