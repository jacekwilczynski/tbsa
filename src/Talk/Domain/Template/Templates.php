<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Template;

class Templates
{
    /**
     * @var int Total count, regardless of pagination
     */
    private $totalCount;

    /**
     * @var Template[]
     */
    private $items;

    public function __construct(int $totalCount, Template... $items)
    {
        $this->totalCount = $totalCount;
        $this->items = $items;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
