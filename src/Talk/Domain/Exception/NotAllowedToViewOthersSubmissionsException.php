<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Exception;

class NotAllowedToViewOthersSubmissionsException extends \DomainException
{
    public function __construct()
    {
        parent::__construct("You are not allowed to view other people's talk submissions.");
    }
}
