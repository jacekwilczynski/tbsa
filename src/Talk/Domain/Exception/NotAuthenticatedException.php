<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Exception;

use RuntimeException;

class NotAuthenticatedException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('You need to be logged in to submit a talk.');
    }
}
