<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Exception;

use MobilitySoft\TBSA\Talk\Domain\Event\Event;

class EventNotAvailableException extends \DomainException
{
    public function __construct(Event $event)
    {
        parent::__construct(
            "Event {$event->getId()}, whose status is {$event->getStatus()} " .
            'does not currently accept the submissions.'
        );
    }
}
