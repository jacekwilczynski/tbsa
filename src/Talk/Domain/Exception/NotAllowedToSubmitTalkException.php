<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Exception;

class NotAllowedToSubmitTalkException extends \DomainException
{
    /**
     * @param string[] $reasons
     */
    public function __construct(int $templateId, int $submitterId, array $reasons)
    {
        $list = array_map(function (string $reason): string {
            return " - $reason";
        }, $reasons);

        parent::__construct(
            "User $submitterId is not allowed " .
            "to submit talk tempalte $templateId. " .
            "Reasons:\n" .
            implode(PHP_EOL, $list)
        );
    }
}
