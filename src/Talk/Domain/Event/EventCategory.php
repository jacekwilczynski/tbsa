<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

class EventCategory
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $shortTitle;

    /**
     * @var bool
     */
    private $openToSubmissions;

    public function __construct(string $slug, string $title, string $shortTitle, bool $openToSubmissions)
    {
        $this->slug              = $slug;
        $this->title             = $title;
        $this->shortTitle        = $shortTitle;
        $this->openToSubmissions = $openToSubmissions;
    }

    /**
     * @return bool
     */
    public function isOpenToSubmissions(): bool
    {
        return $this->openToSubmissions;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    public function getShortTitle(): string
    {
        return $this->shortTitle;
    }
}
