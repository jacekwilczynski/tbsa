<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

use MobilitySoft\TBSA\Utils\Enum;

class EventStatus extends Enum
{
    public static function FREE(): EventStatus
    {
        return self::getInstance('FREE');
    }

    public static function IN_REVIEW(): EventStatus
    {
        return self::getInstance('IN_REVIEW');
    }

    public static function TAKEN(): EventStatus
    {
        return self::getInstance('TAKEN');
    }
}
