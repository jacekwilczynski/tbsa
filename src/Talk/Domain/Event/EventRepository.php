<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;

interface EventRepository
{
    public function getEventById(int $id): ?Event;

    public function findEvents(EventQuery $query): Events;

    public function getBranchIdFromEventId(int $id): ?int;
}
