<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery as CommonEventQuery;

class EventQuery extends CommonEventQuery
{
    /**
     * @var EventStatuses
     */
    private $statuses;

    public function __construct()
    {
        parent::__construct();
        $this->statuses = new EventStatuses();
    }

    public static function fromCommon(CommonEventQuery $source): EventQuery
    {
        $instance = new EventQuery();

        foreach (get_object_vars($source) as $key => $value) {
            $instance->$key = $value;
        }

        return $instance;
    }

    public function getStatuses(): EventStatuses
    {
        return $this->statuses;
    }

    public function setStatuses(EventStatuses $statuses): void
    {
        $this->statuses = $statuses;
    }
}
