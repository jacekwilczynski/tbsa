<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

use MobilitySoft\TBSA\Talk\Domain\User\User;

class Events
{
    /**
     * @var Event[]
     */
    private $items;

    public function __construct(Event... $items)
    {
        $this->items = $items;
    }

    public function exceptThoseWhereUserHasAnAwaitingSubmission(User $user): Events
    {
        return new Events(...array_filter(
            $this->getItems(),
            function (Event $event) use ($user): bool { return !$event->getAwaitingSubmitters()->includes($user); }
        ));
    }

    /**
     * @return Event[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
