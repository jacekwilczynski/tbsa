<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;

class EventNotFoundException extends EntityNotFoundException
{
    public function __construct(int $queriedId)
    {
        parent::__construct('Event', $queriedId);
    }
}
