<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

class EventStatuses
{
    /**
     * @var EventStatus[]
     */
    private $statuses;

    public function __construct(EventStatus... $statuses)
    {
        $this->statuses = $statuses;
    }

    public static function available(): EventStatuses
    {
        return new EventStatuses(EventStatus::FREE(), EventStatus::IN_REVIEW());
    }

    public function match(EventStatus $status): bool
    {
        return $this->statuses ? in_array($status, $this->statuses) : true;
    }
}
