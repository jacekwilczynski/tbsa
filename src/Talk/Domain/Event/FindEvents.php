<?php

namespace MobilitySoft\TBSA\Talk\Domain\Event;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;

interface FindEvents
{
    public function execute(EventQuery $query): Events;
}
