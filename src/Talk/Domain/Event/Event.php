<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Event;

use MobilitySoft\TBSA\Talk\Domain\Branch\Branch;
use MobilitySoft\TBSA\Talk\Domain\Submission\Speakers;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submissions;
use MobilitySoft\TBSA\Talk\Domain\User\Users;
use MobilitySoft\TBSA\ValueObject\EventLocation;
use MobilitySoft\TBSA\ValueObject\Timespan;

class Event
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Timespan
     */
    private $timespan;

    /**
     * @var string
     */
    private $url;

    /**
     * @var EventLocation|null
     */
    private $location;

    /**
     * @var EventCategory|null
     */
    private $category;

    /**
     * @var Branch|null
     */
    private $branch;

    /**
     * @var Submissions
     */
    private $submissions;

    public function __construct(
        int $id,
        string $title,
        Timespan $timespan,
        string $url,
        ?EventLocation $location,
        ?EventCategory $category,
        ?Branch $branch,
        ?Submissions $submissions
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->timespan = $timespan;
        $this->url = $url;
        $this->location = $location;
        $this->category = $category;
        $this->branch = $branch;
        $this->submissions = $submissions ?? new Submissions();
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTimespan(): Timespan
    {
        return $this->timespan;
    }

    public function getLocation(): ?EventLocation
    {
        return $this->location;
    }

    public function getCategory(): ?EventCategory
    {
        return $this->category;
    }

    public function getBranch(): ?Branch
    {
        return $this->branch;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function acceptsSubmissions(): bool
    {
        return $this->getStatus() !== EventStatus::TAKEN();
    }

    public function getStatus(): EventStatus
    {
        if ($this->getSubmissions()->isAnyAccepted()) {
            return EventStatus::TAKEN();
        }

        if ($this->getSubmissions()->isAnyAwaiting()) {
            return EventStatus::IN_REVIEW();
        }

        return EventStatus::FREE();
    }

    public function getSubmissions(): Submissions
    {
        return $this->submissions;
    }

    public function getAwaitingSubmitters(): Users
    {
        return $this->getSubmissions()->getAwaiting()->getSubmitters();
    }
}
