<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Utils\Enum;

class TemplateVsSubmission extends Enum
{
    public static function TEMPLATE(): self
    {
        return self::getInstance('TEMPLATE');
    }

    public static function SUBMISSION(): self
    {
        return self::getInstance('SUBMISSION');
    }
}
