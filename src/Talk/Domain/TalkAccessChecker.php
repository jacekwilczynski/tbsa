<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateRepository;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class TalkAccessChecker
{
    /**
     * @var TemplateRepository
     */
    private $templateRepository;

    /**
     * @var SubmissionAccessChecker
     */
    private $submissionAccessChecker;

    /**
     * @var TemplateAccessChecker
     */
    private $templateAccessChecker;

    /**
     * @var DetectTemplateVsSubmission
     */
    private $detectTemplateVsSubmission;

    /**
     * @var SubmissionRepository
     */
    private $submissionRepository;

    public function __construct(
        TemplateRepository $templateRepository,
        SubmissionRepository $submissionRepository,
        TemplateAccessChecker $templateAccessChecker,
        SubmissionAccessChecker $submissionAccessChecker,
        DetectTemplateVsSubmission $detectTemplateVsSubmission
    ) {
        $this->templateRepository = $templateRepository;
        $this->submissionRepository = $submissionRepository;
        $this->templateAccessChecker = $templateAccessChecker;
        $this->submissionAccessChecker = $submissionAccessChecker;
        $this->detectTemplateVsSubmission = $detectTemplateVsSubmission;
    }

    public function canUserEditTalkWithId(User $user, int $talkId): bool
    {
        $talkType = $this->detectTemplateVsSubmission->execute($talkId);
        if ($talkType === TemplateVsSubmission::TEMPLATE()) {
            return $this->checkTemplate($user, $talkId);
        }

        return $this->checkSubmission($user, $talkId);
    }

    private function checkTemplate(User $user, int $talkId): bool
    {
        $template = $this->templateRepository->findTemplateById($talkId);
        return $this->templateAccessChecker->canUserEditTemplate($user, $template);
    }

    private function checkSubmission(User $user, int $talkId): bool
    {
        $submission = $this->submissionRepository->findById($talkId);
        return $this->submissionAccessChecker->canUserEditSubmission($user, $submission);
    }
}
