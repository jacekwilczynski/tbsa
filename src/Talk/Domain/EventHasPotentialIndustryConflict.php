<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Talk\Domain\Event\EventRepository;

class EventHasPotentialIndustryConflict
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * @var BranchHasPotentialIndustryConflict
     */
    private $branchHasPotentialIndustryConflict;

    public function __construct(
        EventRepository $eventRepository,
        BranchHasPotentialIndustryConflict $branchHasPotentialIndustryConflict
    ) {
        $this->eventRepository = $eventRepository;
        $this->branchHasPotentialIndustryConflict = $branchHasPotentialIndustryConflict;
    }

    public function execute(int $talkId, int $eventId, int $userId): bool
    {
        $branchId = $this->eventRepository->getBranchIdFromEventId($eventId);
        if ($branchId === null) {
            return false;
        }

        return $this->branchHasPotentialIndustryConflict->execute($talkId, $branchId, $userId);
    }
}
