<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\User;

class Users
{
    /**
     * @var User[]
     */
    private $items;

    public function __construct(User... $items)
    {
        $this->items = $items;
    }

    public static function union(Users... $sets): Users
    {
        $list = array_merge(...array_map(function (Users $users) {
            return $users->getItems();
        }, $sets));

        return new Users(...unique_sorted_by_quantity($list, function (User $user) {
            return $user->getId();
        }));
    }

    /**
     * @return User[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function includes(User $user): bool
    {
        return (bool) array_find(
            $this->items,
            function (User $item) use ($user) { return $item->equals($user); }
        );
    }
}
