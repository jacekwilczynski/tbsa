<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\User;

abstract class UserRepository
{
    public function getByIds(int ...$ids): Users
    {
        return new Users(...array_filter(array_map(function (int $id): ?User {
            return $this->getById($id);
        }, $ids)));
    }

    abstract public function getById(int $id): ?User;

    abstract public function getCurrentUser(): ?User;

    abstract public function getBranchMembers(int $branchId): Users;
}
