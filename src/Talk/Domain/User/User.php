<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\User;

use MobilitySoft\TBSA\Talk\Domain\Branch\Branches;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\Talk\Domain\Template\Template;

class User
{
    private const PRIVILEGED_USER_MAX_SPEAKERS = 3;
    private const REGULAR_USER_MAX_SPEAKERS = 1;

    /**
     * @var int
     */
    private $id;

    /**
     * @var Entrepreneur|null
     */
    private $entrepreneur;

    /**
     * @var string
     */
    private $email;

    /**
     * @var bool
     */
    private $admin;

    /**
     * @var bool
     */
    private $boardMember;

    /**
     * @var Branches
     */
    private $memberBranches;

    /**
     * @var Branches
     */
    private $managedBranches;

    /**
     * @var string
     */
    private $displayName;

    public function __construct(
        int $id,
        string $displayName,
        string $email,
        ?Entrepreneur $entrepreneur,
        bool $admin,
        bool $boardMember,
        Branches $memberBranches,
        ?Branches $managedBranches = null
    ) {
        $this->id = $id;
        $this->displayName = $displayName;
        $this->email = $email;
        $this->entrepreneur = $entrepreneur;
        $this->admin = $admin;
        $this->boardMember = $boardMember;
        $this->memberBranches = $memberBranches;
        $this->managedBranches = $managedBranches ?? new Branches();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getEntrepreneur(): ?Entrepreneur
    {
        return $this->entrepreneur;
    }

    public function isAdmin(): bool
    {
        return $this->admin;
    }

    public function isBoardMember(): bool
    {
        return $this->boardMember;
    }

    public function getMaxNumberOfSpeakersUserCanAdd(): int
    {
        return $this->isPrivileged()
            ? self::PRIVILEGED_USER_MAX_SPEAKERS
            : self::REGULAR_USER_MAX_SPEAKERS;
    }

    public function getReasonsToPreventSubmission(Template $template): array
    {
        $reasons = [];

        $speakers = $template->getSpeakers();

        $numberOfSpeakers = $speakers->count();
        $maxSpeakers = $this->getMaxNumberOfSpeakersUserCanAdd();
        if ($numberOfSpeakers > $maxSpeakers) {
            $reasons[] = "Too many speakers: max $maxSpeakers, got $numberOfSpeakers.";
        }

        if (!$this->isPrivileged()) {
            foreach ($speakers->getItems() as $speaker) {
                $speakerName = $speaker->getName();
                if (
                    $speakerName !== $this->getDisplayName() &&
                    $speakerName !== $this->getEntrepreneur()->getName()
                ) {
                    $reasons[] = "The speaker's name ('$speakerName') differs from that of the user " .
                                 "while the user not allowed to submit on others' behalf.";
                }
            }
        }

        return $reasons;
    }

    /**
     * @return Branches
     */
    public function getMemberBranches(): Branches
    {
        return $this->memberBranches;
    }

    public function getManagedBranches(): Branches
    {
        return $this->managedBranches;
    }

    public function equals(User $other): bool
    {
        return $other->getId() === $this->getId();
    }

    public function isPrivileged(): bool
    {
        return $this->isAdmin() || $this->isBoardMember();
    }

    public function canEditSubmissions(): bool
    {
        return $this->isPrivileged();
    }

    public function canAcceptSubmissions(): bool
    {
        return $this->isAdmin();
    }
}
