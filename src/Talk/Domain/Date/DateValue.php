<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Date;

use DateTimeImmutable;

class DateValue
{
    /**
     * @var DateTimeImmutable
     */
    private $dateTime;

    private function __construct(DateTimeImmutable $dateTime)
    {
        $this->dateTime = new DateTimeImmutable($dateTime->format('Y-m-d'));
    }

    public static function fromDateTime(DateTimeImmutable $dateTime): DateValue
    {
        return new DateValue($dateTime);
    }

    public function equals(DateValue $other): bool
    {
        return $this->compare($other) === 0;
    }

    public function compare(DateValue $other): int
    {
        return $this->getDateTime() <=> $other->getDateTime();
    }

    public function getDateTime(): DateTimeImmutable
    {
        return $this->dateTime;
    }

    public function isPast($now = 'now'): bool
    {
        return $this->getDateTime() < new DateTimeImmutable($now);
    }

    public function __toString(): string
    {
        return $this->format();
    }

    public function format(?string $format = 'Y-m-d'): string
    {
        return $this->getDateTime()->format($format);
    }
}
