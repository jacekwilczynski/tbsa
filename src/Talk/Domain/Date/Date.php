<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Date;

class Date
{
    /**
     * @var DateValue
     */
    private $value;

    /**
     * @var DateStatus
     */
    private $status;

    public function __construct(DateValue $value, DateStatus $status)
    {
        $this->value = $value;
        $this->status = $status;
    }

    public function coalesce(Date $other): Date
    {
        if (!$this->getValue()->equals($other->getValue())) {
            throw new \InvalidArgumentException(sprintf(
                "Cannot coalesce Dates with different values.\n  this: %s\n  other: %s",
                $this->getValue(),
                $other->getValue()
            ));
        }

        return new Date(
            $this->getValue(),
            DateStatus::pickMostAvailable($this->getStatus(), $other->getStatus())
        );
    }

    public function getValue(): DateValue
    {
        return $this->value;
    }

    public function getStatus(): DateStatus
    {
        return $this->status;
    }
}
