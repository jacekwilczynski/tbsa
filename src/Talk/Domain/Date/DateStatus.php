<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Date;

use InvalidArgumentException;
use MobilitySoft\TBSA\Utils\Enum;

class DateStatus extends Enum
{
    public static function PAST(): DateStatus
    {
        return self::getInstance('PAST', 0);
    }

    public static function TAKEN(): DateStatus
    {
        return self::getInstance('TAKEN', 0);
    }

    public static function EMPTY(): DateStatus
    {
        return self::getInstance('EMPTY', 0);
    }

    public static function IN_REVIEW(): DateStatus
    {
        return self::getInstance('IN_REVIEW', 1);
    }

    public static function FREE(): DateStatus
    {
        return self::getInstance('FREE', 2);
    }

    public static function pickMostAvailable(DateStatus... $statuses): DateStatus
    {
        if ( ! $statuses) {
            throw new InvalidArgumentException('You need to pass at least one Date Status.');
        }

        usort($statuses, [self::class, 'compare']);

        return $statuses[0];
    }

    public static function compare(DateStatus $a, DateStatus $b): int
    {
        return $a->getData() - $b->getData();
    }

    public function isAvailable(): bool
    {
        return $this->getData() > 0;
    }
}
