<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Date;

use Generator;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Talk\Domain\Event\Events;
use MobilitySoft\TBSA\Talk\Domain\Event\FindEvents;

class DateRepository
{
    /**
     * @var FindEvents
     */
    private $findAvailableEvents;

    /**
     * @var GetDatesFromEvents
     */
    private $getDatesFromEvents;

    public function __construct(FindEvents $findAvailableEvents, GetDatesFromEvents $getDatesFromEvents)
    {
        $this->findAvailableEvents = $findAvailableEvents;
        $this->getDatesFromEvents = $getDatesFromEvents;
    }

    /**
     * @return Date[]
     */
    public function findAvailableDatesByBranchIds(int... $branchIds): array
    {
        $dates = [];

        foreach ($this->findEventsInChunks(new EventQuery()) as $events) {
            foreach ($branchIds as $branchId) {
                $branchEvents = new Events(...array_filter(
                    $events->getItems(),
                    function (Event $event) use ($branchId) {
                        return $event->getBranch() && $event->getBranch()->getId() === $branchId;
                    }
                ));
                if (!array_key_exists($branchId, $dates)) {
                    $dates[$branchId] = [];
                }
                $dates[$branchId] = array_merge(
                    $dates[$branchId],
                    $this->getDatesFromEvents->execute($branchEvents)
                );
            }
        }

        return $dates;
    }

    /**
     * @return Generator<Events>
     */
    public function findEventsInChunks(EventQuery $query, ?int $chunkSize = 20): Generator
    {
        $limit = $query->getLimit();
        $total = 0;
        $query->setLimit($chunkSize);
        do {
            $events = $this->findAvailableEvents->execute($query);
            yield $events;
            $count = count($events->getItems());
            $total += $count;
            $query->setOffset($query->getOffset() + $chunkSize);
        } while ($count > 0 && (!$limit || $total < $limit));
    }
}
