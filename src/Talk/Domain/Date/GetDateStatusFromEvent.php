<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Date;

use DateTimeImmutable;
use InvalidArgumentException;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Event\EventStatus;

class GetDateStatusFromEvent
{
    public function execute(?Event $event, $now = 'now'): DateStatus
    {
        if (!$event) {
            return DateStatus::EMPTY();
        }

        if ($event->getTimespan()->getStart() < new DateTimeImmutable($now)) {
            return DateStatus::PAST();
        }

        switch ($event->getStatus()) {
            case EventStatus::TAKEN():
                return DateStatus::TAKEN();
            case EventStatus::IN_REVIEW():
                return DateStatus::IN_REVIEW();
            case EventStatus::FREE():
                return DateStatus::FREE();
        }

        throw new InvalidArgumentException('Invalid EventStatus: ' . print_r($event->getStatus(), true));
    }
}
