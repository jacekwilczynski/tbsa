<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Date;

use MobilitySoft\TBSA\Talk\Domain\Event\Events;

class GetDatesFromEvents
{
    /**
     * @var GetDateStatusFromEvent
     */
    private $getDateStatusFromEvent;

    public function __construct(GetDateStatusFromEvent $getDateStatusFromEvent)
    {
        $this->getDateStatusFromEvent = $getDateStatusFromEvent;
    }

    /**
     * @param Events $events
     *
     * @return Date[]
     */
    public function execute(Events $events): array
    {
        $dates = [];

        foreach ($events->getItems() as $event) {
            $dateTime   = $event->getTimespan()->getStart();
            $dateString = $dateTime->format('Y-m-d');
            $date       = new Date(
                DateValue::fromDateTime($dateTime),
                $this->getDateStatusFromEvent->execute($event)
            );

            if (empty($dates[$dateString])) {
                $dates[$dateString] = $date;
            } else {
                $dates[$dateString] = $dates[$dateString]->coalesce($date);
            }
        }

        return $dates;
    }
}
