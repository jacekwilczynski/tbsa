<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Entrepreneur;

use MobilitySoft\TBSA\Common\Domain\Industries;

class Businesses
{
    /**
     * @var Business[]
     */
    private $items;

    public function __construct(Business... $items)
    {
        $this->items = $items;
    }

    /**
     * @return Business[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getMain(): ?Business
    {
        return $this->items[0] ?? null;
    }

    public function getIndustries(): Industries
    {
        return Industries::merge(...array_map(function (Business $business): Industries {
            return $business->getIndustries();
        }, $this->getItems()));
    }
}
