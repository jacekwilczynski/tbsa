<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Entrepreneur;

interface EntrepreneurRepository
{
    public function findById(int $id): ?Entrepreneur;

    public function update(Entrepreneur $entrepreneur): void;
}
