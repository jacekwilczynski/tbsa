<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Entrepreneur;

use MobilitySoft\TBSA\Common\Domain\Industries;

class Business
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var Industries
     */
    private $industries;

    public function __construct(int $id, string $name, string $url, Industries $industries)
    {
        $this->id = $id;
        $this->name = $name;
        $this->url = $url;
        $this->industries = $industries;
    }

    public function getIndustries(): Industries
    {
        return $this->industries;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
