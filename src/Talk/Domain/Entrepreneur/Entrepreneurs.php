<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Entrepreneur;

use MobilitySoft\TBSA\Common\Domain\Industries;

class Entrepreneurs
{
    /**
     * @var Entrepreneur[]
     */
    private $items;

    public function __construct(Entrepreneur... $items)
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getIndustries(): Industries
    {
        return Industries::merge(...array_merge(...array_map(function (Entrepreneur $entrepreneur): Industries {
            return $entrepreneur->getIndustries();
        }, $this->getItems())));
    }
}
