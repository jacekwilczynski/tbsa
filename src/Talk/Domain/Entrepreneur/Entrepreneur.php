<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Entrepreneur;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\ValueObject\Image;

class Entrepreneur
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Contact
     */
    private $contact;

    /**
     * @var Image|null
     */
    private $photo;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Businesses
     */
    private $businesses;

    public function __construct(
        int $id,
        string $name,
        ?Contact $contact,
        ?Image $photo,
        string $description,
        Businesses $businesses
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->contact = $contact ?? new Contact();
        $this->photo = $photo;
        $this->description = $description;
        $this->businesses = $businesses;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Contact
     */
    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function getPhoto(): ?Image
    {
        return $this->photo;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getBusinesses(): Businesses
    {
        return $this->businesses;
    }

    /**
     * @return Industries
     */
    public function getIndustries(): Industries
    {
        return $this->getBusinesses()->getIndustries();
    }
}
