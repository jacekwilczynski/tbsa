<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Entrepreneur;

class Contact
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    public function __construct(?string $email = '', ?string $phone = '')
    {
        $this->email = $email;
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
}
