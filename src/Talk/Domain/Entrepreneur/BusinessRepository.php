<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Entrepreneur;

interface BusinessRepository
{
    public function getById(int $id): ?Business;
}
