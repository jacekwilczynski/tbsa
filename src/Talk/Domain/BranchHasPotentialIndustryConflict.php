<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;
use MobilitySoft\TBSA\Talk\Domain\Talk\TalkRepository;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;

class BranchHasPotentialIndustryConflict
{
    /**
     * @var FindIndustriesInBranch
     */
    private $findIndustriesInBranch;

    /**
     * @var TalkRepository
     */
    private $talkRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        FindIndustriesInBranch $findIndustriesInBranch,
        TalkRepository $talkRepository,
        UserRepository $userRepository
    ) {
        $this->findIndustriesInBranch = $findIndustriesInBranch;
        $this->talkRepository = $talkRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function execute(int $submissionId, int $branchId, int $userId): bool
    {
        $talk = $this->talkRepository->getById($submissionId);
        $user = $this->userRepository->getById($userId);

        if (in_array(
            $branchId,
            $user->getMemberBranches()->getIds(),
            true
        )) {
            return false;
        }

        $talkIndustry = $talk->getIndustry();

        if ($talkIndustry !== null) {
            return in_array(
                $talkIndustry->getTitle(),
                $this->findIndustriesInBranch->execute($branchId)->getItems(),
                true
            );
        }

        return false;
    }
}
