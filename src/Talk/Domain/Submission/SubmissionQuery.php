<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Submission;

use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;

class SubmissionQuery
{
    /**
     * @var int|null
     */
    private $event;

    /**
     * @var int|null - negative number if should find all except those submitted by submitter with the id
     */
    private $submitter;

    /**
     * @var BranchesInQuery|null
     */
    private $branches;

    /**
     * @var bool
     */
    private $onlyForFutureEvents = true;

    /**
     * @var int
     */
    private $offset = 0;

    /**
     * @var int
     */
    private $limit = 0;

    /**
     * @var string
     */
    private $phrase = '';

    public function getBranches(): ?BranchesInQuery
    {
        return $this->branches;
    }

    public function setBranches(?BranchesInQuery $branches): void
    {
        $this->branches = $branches;
    }

    public function getSubmitter(): ?int
    {
        return $this->submitter;
    }

    public function setSubmitter(?int $submitter): void
    {
        $this->submitter = $submitter;
    }

    public function getEvent(): ?int
    {
        return $this->event;
    }

    public function setEvent(?int $event): void
    {
        $this->event = $event;
    }

    public function isOnlyForFutureEvents(): bool
    {
        return $this->onlyForFutureEvents;
    }

    public function setOnlyForFutureEvents(bool $onlyForFutureEvents): void
    {
        $this->onlyForFutureEvents = $onlyForFutureEvents;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }

    public function setPhrase(string $phrase): void
    {
        $this->phrase = $phrase;
    }
}
