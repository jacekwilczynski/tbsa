<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Submission;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;

interface SubmissionRepository
{
    /**
     * @throws EntityNotFoundException
     */
    public function getById(int $id): Submission;

    public function findById(int $id): ?Submission;

    public function findSubmissions(SubmissionQuery $query): PaginatedSubmissions;

    public function save(Submission... $submissions): void;
}
