<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Submission;

class PaginatedSubmissions extends Submissions
{
    /**
     * @var int
     */
    private $totalCount;

    public function __construct(int $totalCount, Submission... $items)
    {
        parent::__construct(...$items);
        $this->totalCount = $totalCount;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }
}
