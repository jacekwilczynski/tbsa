<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Submission;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Speaker\Speakers;
use MobilitySoft\TBSA\Talk\Domain\Talk\Talk;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class Submission
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Event
     */
    private $event;

    /**
     * @var Speakers
     */
    private $speakers;

    /**
     * @var Talk
     */
    private $talk;

    /**
     * @var User
     */
    private $submitter;

    /**
     * @var SubmissionStatus
     */
    private $status;

    /**
     * @var string string
     */
    private $rejectionComment;

    public function __construct(
        int $id,
        Event $event,
        Speakers $speakers,
        Talk $talk,
        User $submitter,
        ?SubmissionStatus $status = null,
        string $rejectionComment = ''
    ) {
        $this->id = $id;
        $this->event = $event;
        $this->speakers = $speakers;
        $this->talk = $talk;
        $this->submitter = $submitter;
        $this->status = $status ?? SubmissionStatus::AWAITING();
        $this->rejectionComment = $rejectionComment;
    }

    /**
     * @return User
     */
    public function getSubmitter(): User
    {
        return $this->submitter;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEvent(): Event
    {
        return $this->event;
    }

    public function getSpeakers(): Speakers
    {
        return $this->speakers;
    }

    public function getTalk(): Talk
    {
        return $this->talk;
    }

    public function isAccepted(): bool
    {
        return $this->getStatus() === SubmissionStatus::ACCEPTED();
    }

    public function getStatus(): SubmissionStatus
    {
        return $this->status;
    }

    public function getRejectionComment(): string
    {
        return $this->rejectionComment;
    }

    public function setStatus(SubmissionStatus $status): void
    {
        $this->status = $status;
    }
}
