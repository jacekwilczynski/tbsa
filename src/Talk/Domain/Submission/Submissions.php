<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Submission;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\Users;

class Submissions
{
    /**
     * @var Submission[]
     */
    private $items;

    public function __construct(Submission... $items)
    {
        $this->items = $items;
    }

    public function isAnyAccepted(): bool
    {
        return (bool) array_find(
            $this->getItems(),
            function (Submission $submission) { return $submission->isAccepted(); }
        );
    }

    public function isAnyAwaiting(): bool
    {
        return (bool) array_find(
            $this->getItems(),
            function (Submission $submission) {
                return $submission->getStatus() === SubmissionStatus::AWAITING();
            }
        );
    }

    public function getMostHopefulStatus(): ?SubmissionStatus
    {
        if (!$this->hasItems()) {
            return null;
        }

        $statuses = array_map(function (Submission $submission): SubmissionStatus {
            return $submission->getStatus();
        }, $this->items);
        usort($statuses, [SubmissionStatus::class, 'compare']);
        return $statuses[0];

    }

    /**
     * @return Submission[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getAwaiting(): Submissions
    {
        return new Submissions(...array_filter(
            $this->getItems(),
            function (Submission $submission): bool {
                return $submission->getStatus() === SubmissionStatus::AWAITING();
            }
        ));
    }

    public function hasItems(): bool
    {
        return count($this->getItems()) > 0;
    }

    public function getSubmitters(): Users
    {
        return new Users(...array_map(function (Submission $submission): User {
            return $submission->getSubmitter();
        }, $this->getItems()));
    }
}
