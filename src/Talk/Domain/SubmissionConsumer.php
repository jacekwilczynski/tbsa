<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;

interface SubmissionConsumer
{
    public function execute(Submission $submission): void;
}
