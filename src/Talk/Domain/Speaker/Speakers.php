<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Speaker;

class Speakers
{
    /**
     * @var Speaker[]
     */
    private $items;

    public function __construct(Speaker... $items)
    {
        if (count($items) === 0) {
            throw new \DomainException('A talk must have at least one speaker.');
        }
        $this->items = $items;
    }

    /**
     * @return Speaker[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getFirst(): Speaker
    {
        return $this->items[0];
    }

    public function count(): int
    {
        return count($this->items);
    }
}
