<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Speaker;

class Contact
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    public function __construct(string $email, string $phone)
    {
        $this->email = $email;
        $this->phone = $phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
