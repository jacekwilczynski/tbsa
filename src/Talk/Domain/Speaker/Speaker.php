<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Speaker;

use MobilitySoft\TBSA\View\Medium\Medium;

class Speaker
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Contact
     */
    private $contact;

    /**
     * @var Medium
     */
    private $photo;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Business
     */
    private $business;

    /**
     * @var bool
     */
    private $shouldBeCopiedToEntrepreneur;

    /**
     * @var int|null
     */
    private $entrepreneurId;

    public function __construct(
        string $name,
        Contact $contact,
        Medium $photo,
        string $description,
        Business $business,
        bool $shouldBeCopiedToEntrepreneur,
        ?int $entrepreneurId
    ) {
        $this->name = $name;
        $this->contact = $contact;
        $this->photo = $photo;
        $this->description = $description;
        $this->business = $business;
        $this->shouldBeCopiedToEntrepreneur = $shouldBeCopiedToEntrepreneur;
        $this->entrepreneurId = $entrepreneurId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function getPhoto(): Medium
    {
        return $this->photo;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getBusiness(): Business
    {
        return $this->business;
    }

    public function shouldBeCopiedToEntrepreneur(): bool
    {
        return $this->shouldBeCopiedToEntrepreneur;
    }

    public function getEntrepreneurId(): ?int
    {
        return $this->entrepreneurId;
    }
}
