<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Talk\Domain\Event\Events;
use MobilitySoft\TBSA\Talk\Domain\Event\FindEvents;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class FindEventsUserCanApplyFor implements FindEvents
{
    /**
     * @var FindEvents
     */
    private $source;

    /**
     * @var User
     */
    private $user;

    public function __construct(FindEvents $source, User $user)
    {
        $this->source = $source;
        $this->user = $user;
    }

    public function execute(EventQuery $query): Events
    {
        return $this->source->execute($query)
            ->exceptThoseWhereUserHasAnAwaitingSubmission($this->user);
    }
}
