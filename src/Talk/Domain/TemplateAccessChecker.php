<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Talk\Domain\Template\Template;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class TemplateAccessChecker
{
    public function canUserEditTemplate(User $user, Template $template): bool
    {
        return $user->isAdmin() || $template->getOwners()->includes($user);
    }
}
