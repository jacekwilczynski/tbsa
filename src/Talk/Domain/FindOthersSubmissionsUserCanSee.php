<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain;

use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;
use MobilitySoft\TBSA\Talk\Domain\Submission\PaginatedSubmissions;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionQuery;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class FindOthersSubmissionsUserCanSee
{
    /**
     * @var SubmissionRepository
     */
    private $submissionRepository;

    public function __construct(SubmissionRepository $submissionRepository)
    {
        $this->submissionRepository = $submissionRepository;
    }

    public function execute(User $user, int $offset, int $limit, string $phrase): PaginatedSubmissions
    {
        $query = new SubmissionQuery();
        $query->setSubmitter(-$user->getId());
        $query->setOffset($offset);
        $query->setLimit($limit);
        $query->setPhrase($phrase);

        if (!$user->isAdmin()) {
            $query->setBranches(new BranchesInQuery(...$user->getManagedBranches()->getIds()));
        }

        return $this->submissionRepository->findSubmissions($query);
    }
}
