<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Talk;

class Benefits
{
    /**
     * @var string[]
     */
    private $items;

    public function __construct(string ...$items)
    {
        $this->items = $items;
    }

    /**
     * @return string[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
