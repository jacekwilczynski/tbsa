<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Talk;

class Talk
{
    /**
     * @var string
     */
    private $subject;

    /**
     * @var Industry|null
     */
    private $industry;

    /**
     * @var Benefits
     */
    private $benefits;

    /**
     * @var string
     */
    private $description;

    public function __construct(string $subject, ?Industry $industry, Benefits $benefits, string $description)
    {
        $this->subject     = $subject;
        $this->industry    = $industry;
        $this->benefits    = $benefits;
        $this->description = $description;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getIndustry(): ?Industry
    {
        return $this->industry;
    }

    public function getBenefits(): Benefits
    {
        return $this->benefits;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
