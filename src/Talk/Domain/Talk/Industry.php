<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Talk;

class Industry
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    public function __construct(int $id, string $title)
    {
        $this->title = $title;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
