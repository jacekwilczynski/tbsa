<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Domain\Talk;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;

interface TalkRepository
{
    /**
     * @throws EntityNotFoundException
     */
    public function getById(int $id): Talk;
}
