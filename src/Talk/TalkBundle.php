<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk;

use MobilitySoft\TBSA\Talk\Application\DomainEventHandling\OnSubmissionStatusChange;
use MobilitySoft\TBSA\Talk\Application\DomainEventHandling\SubmissionStatusChangeListener;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\SubmissionAccessChecker;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use Psr\Container\ContainerInterface;
use WP_REST_Request;
use const MobilitySoft\TBSA\TALK_SUBMISSION_POST_TYPE_SLUG;
use const MobilitySoft\TBSA\TALK_TEMPLATE_POST_TYPE_SLUG;

class TalkBundle
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function enable(): void
    {
        if (!is_user_logged_in()) {
            return;
        }

        $this->registerRestRoutes();
        $this->container->get(SubmissionStatusChangeListener::class)
            ->addCallback($this->container->get(OnSubmissionStatusChange::class))
            ->listen();

        add_action('acf/save_post', static function ($postId): void {
            if (get_post_type($postId) === TALK_TEMPLATE_POST_TYPE_SLUG) {
                $userId = get_current_user_id();

                $speakers = get_array_field('speakers', $postId);
                if (count($speakers) === 1) {
                    $speaker = reset($speakers);
                    if (empty($speaker['name'])) {
                        $speaker['name'] = get_userdata($userId)->display_name;
                        update_row('speakers', 1, $speaker, $postId);
                    }
                }

                if (!get_field('owners', $postId)) {
                    update_field('owners', $userId, $postId);
                }

                if (get_post_status($postId) === 'draft') {
                    wp_publish_post($postId);
                }
            }
        }, 11);

        add_action('acf/validate_save_post', function (): void {
            if (empty($_POST['post_id'])) {
                return;
            }
            $postId = (int) $_POST['post_id'];
            if (get_post_type($postId) === TALK_SUBMISSION_POST_TYPE_SLUG) {
                $submission = $this->container->get(SubmissionRepository::class)->getById($postId);

                $user = $this->container->get(User::class);
                if (!(new SubmissionAccessChecker())->canUserEditSubmission($user, $submission)) {
                    acf_add_validation_error('', __('Nie możesz zmodyfikować zamkniętego wniosku.'));
                }

                $eventStart = $submission->getEvent()->getTimespan()->getStart();
                $now = new \DateTimeImmutable();
                if ($eventStart < $now && !$user->isAdmin()) {
                    acf_add_validation_error('', __('Nie możesz zmodyfikować wniosku na przeszłe wydarzenie.'));
                }
            }
        });

        // Prevent validation error when non-admin edits a submission
        add_filter('acf/validate_value/key=field_5d6972f8717e2', static function ($valid): bool {
            if (!current_user_can('administrator')) {
                return true;
            }
            return (bool) $valid;
        });

        add_filter('admin_body_class', static function (string $classes): string {
            if (get_post_type() === TALK_SUBMISSION_POST_TYPE_SLUG) {
                $classes .= ' follow-to-edit__group ';
            }

            return $classes;
        });
    }

    private function registerRestRoutes(): void
    {
        $routes = include __DIR__ . '/config/rest-routes.php';

        add_action('rest_api_init', function () use ($routes): void {
            foreach ($routes as $route) {
                $route['callback'] = function (WP_REST_Request $request) use ($route) {
                    [$class, $method] = $route['callback'];
                    return $this->container->get($class)->$method($request);
                };

                register_rest_route($route['namespace'], $route['resource'], $route);
            }
        });

        add_action('wp_enqueue_scripts', function () use ($routes): void {
            $nonce = wp_create_nonce('wp_rest');
            $jsRoutesData = array_map(function (array $route) use ($nonce): array {
                $url = $route['namespace'] . $route['resource'];
                $url = mb_ereg_replace('\(.*\)', '', $url);
                $url = mb_ereg_replace('/$', '', $url);
                return [
                    'url'    => rest_url($url),
                    'method' => $route['methods'],
                    'nonce'  => $nonce,
                ];
            }, $routes);
            ?>
            <script>
              if (!window.tbsa_talk) {
                window.tbsa_talk = {};
              }
              window.tbsa_talk.rest = <?= json_encode($jsRoutesData) ?>;
              window.tbsa_submitTalkPopup = {
                htmlUrl: '<?= admin_url('/admin-ajax.php/?action=submit_talk_popup_html') ?>',
                dataUrl: '<?= admin_url('/admin-ajax.php/?action=submit_talk_popup_data') ?>'
              };
            </script>
            <?php
        });
    }
}
