<?php

use MobilitySoft\TBSA\Talk\Application\Controller\EntrepreneurFieldsController;
use MobilitySoft\TBSA\Talk\Application\Controller\SubmissionController;
use MobilitySoft\TBSA\Talk\Application\Controller\TemplateController;

return [
    'searchTemplates'              => [
        'namespace' => '/tbsa/v1',
        'resource'  => '/talk-templates',
        'methods'   => 'GET',
        'callback'  => [TemplateController::class, 'search'],
    ],
    'getNewestTalkTemplateId'      => [
        'namespace' => '/tbsa/v1',
        'resource'  => '/talk-templates/last-id',
        'methods'   => 'GET',
        'callback'  => [TemplateController::class, 'getNewestId'],
    ],
    'searchSubmissions'            => [
        'namespace' => '/tbsa/v1',
        'resource'  => '/submissions/(?P<ownership>(own|others))',
        'methods'   => 'GET',
        'callback'  => [SubmissionController::class, 'search'],
    ],
    'createSubmissionFromTemplate' => [
        'namespace' => '/tbsa/v1',
        'resource'  => '/submissions/from-template',
        'methods'   => 'POST',
        'callback'  => [SubmissionController::class, 'submit'],
    ],
    'acceptSubmission'             => [
        'namespace' => '/tbsa/v1',
        'resource'  => '/submissions/accept/(?P<id>\d+)',
        'methods'   => 'PATCH',
        'callback'  => [SubmissionController::class, 'accept'],
        'args'      => [
            'id' => [
                'required'    => true,
                'type'        => 'integer',
                'description' => 'The id of the submission you want to accept.',
            ],
        ],
    ],
    'getEntrepreneurFields'        => [
        'namespace' => '/tbsa/v1',
        'resource'  => '/edit-talk/entrepreneur-fields',
        'methods'   => 'POST',
        'callback'  => [EntrepreneurFieldsController::class, 'getEntrepreneurFields'],
    ],
];
