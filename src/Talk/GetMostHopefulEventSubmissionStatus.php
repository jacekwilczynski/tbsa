<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\Event\EventRepository;

class GetMostHopefulEventSubmissionStatus
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function execute(int $eventId): ?SubmissionStatus
    {
        $event = $this->eventRepository->getEventById($eventId);
        if ($event === null) {
            return null;
        }
        return $event->getSubmissions()->getMostHopefulStatus();
    }
}
