<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\DomainEventHandling;

use MobilitySoft\TBSA\Common\View\Anchor;
use MobilitySoft\TBSA\Service\TranslateMonth;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\Talk\Benefits;
use Twig\Environment as Twig;

class GetSubmissionDataForNotification
{
    /**
     * @var Twig
     */
    private $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function execute(Submission $submission): array
    {
        $event = $submission->getEvent();

        return [
            'wnioskodawca'       => $submission->getSubmitter()->getDisplayName(),
            'email_wnioskodawcy' => $submission->getSubmitter()->getEmail(),
            'email_prelegenta'   => $submission->getSpeakers()->getFirst()->getContact()->getEmail(),
            'wydarzenie'         => Anchor::presentable($event->getUrl(), $event->getTitle(), true),
            'data'               => TranslateMonth::execute($event->getTimespan()->getStart()->format('j F Y')),
            'link_do_panelu'     => $this->getEditLink($submission),
            'pola_formularza'    => $this->renderFormData($submission),
            'komentarz'          => $submission->getRejectionComment(),
        ];
    }

    private function getEditLink(Submission $submission): string
    {
        return sprintf(
            '<a href="%s" target="_blank">%s</a>',
            admin_url("/post.php?post={$submission->getId()}&action=edit"),
            __('Zobacz w panelu administracyjnym')
        );
    }

    private function renderFormData(Submission $submission): string
    {
        return $this->twig->render('myAccount/submissionDataInEmail.twig', [
            'speakers' => $this->getSpeakersData($submission),
            'talk'     => $this->getTalkData($submission),
        ]);
    }

    private function renderBenefits(Benefits $benefits): string
    {
        $items = implode('', array_map(function (string $item): string {
            return "<li>$item</li>";
        }, $benefits->getItems()));

        return "<ol>$items</ol>";
    }

    private function getSpeakersData(Submission $submission): array
    {
        $speakersData = [];
        foreach ($submission->getSpeakers()->getItems() as $speaker) {
            $business = $speaker->getBusiness();
            $speakersData[$speaker->getName()] = [
                'E-mail'       => Anchor::simple($speaker->getContact()->getEmail()),
                'Telefon'      => Anchor::simple($speaker->getContact()->getPhone()),
                'Zdjęcie'      => $speaker->getPhoto()->getMarkup(),
                'Biogram'      => $speaker->getDescription(),
                'Nazwa firmy'  => $business->getName(),
                'Strona firmy' => $business->getUrl(),
            ];
        }
        return $speakersData;
    }

    private function getTalkData(Submission $submission): array
    {
        $talk = $submission->getTalk();
        return [
            'Temat'    => $talk->getSubject(),
            'Branża'   => $talk->getIndustry() ? $talk->getIndustry()->getTitle() : '-',
            'Korzyści' => $this->renderBenefits($talk->getBenefits()),
            'Opis'     => $talk->getDescription(),
        ];
    }
}
