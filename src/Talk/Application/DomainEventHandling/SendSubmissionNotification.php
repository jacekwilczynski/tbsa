<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\DomainEventHandling;

use MobilitySoft\TBSA\Notifications\AcfToWcEmailNotificationSenderFactory;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;

class SendSubmissionNotification
{
    /**
     * @var AcfToWcEmailNotificationSenderFactory
     */
    private $senderFactory;

    /**
     * @var GetSubmissionDataForNotification
     */
    private $getSubmissionDataForNotification;

    public function __construct(
        AcfToWcEmailNotificationSenderFactory $senderFactory,
        GetSubmissionDataForNotification $getSubmissionDataForNotification
    ) {
        $this->senderFactory = $senderFactory;
        $this->getSubmissionDataForNotification = $getSubmissionDataForNotification;
    }

    public function execute(string $slug, Submission $submission): void
    {
        $data = $this->getSubmissionDataForNotification->execute($submission);
        $this->senderFactory->create($slug)->send($data);
    }
}
