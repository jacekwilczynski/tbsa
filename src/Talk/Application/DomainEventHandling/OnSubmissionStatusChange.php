<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\DomainEventHandling;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\SpeakerToEntrepreneur\UpdateEntrepreneursFromSubmission;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\SubmissionConsumer;
use MobilitySoft\TBSA\Talk\Infrastructure\ApplySubmissionToEvent;

class OnSubmissionStatusChange implements SubmissionConsumer
{
    /**
     * @var SendSubmissionNotification
     */
    private $sendSubmissionNotification;

    /**
     * @var ApplySubmissionToEvent
     */
    private $applySubmissionToEvent;

    /**
     * @var UpdateEntrepreneursFromSubmission
     */
    private $updateEntrepreneurFromSubmission;

    public function __construct(
        SendSubmissionNotification $sendSubmissionNotification,
        ApplySubmissionToEvent $applySubmissionToEvent,
        UpdateEntrepreneursFromSubmission $updateEntrepreneurFromSubmission
    ) {
        $this->sendSubmissionNotification = $sendSubmissionNotification;
        $this->applySubmissionToEvent = $applySubmissionToEvent;
        $this->updateEntrepreneurFromSubmission = $updateEntrepreneurFromSubmission;
    }

    public function execute(Submission $submission): void
    {
        if ($submission->getStatus() === SubmissionStatus::ACCEPTED()) {
            $this->updateEntrepreneurFromSubmission->execute($submission);
            $this->applySubmissionToEvent->execute($submission);
            $this->sendSubmissionNotification->execute(
                'submission_accepted',
                $submission
            );
        }
        if ($submission->getStatus() === SubmissionStatus::REJECTED()) {
            $this->sendSubmissionNotification->execute(
                'submission_rejected',
                $submission
            );
        }
    }
}
