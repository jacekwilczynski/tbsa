<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\DomainEventHandling;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\SubmissionConsumer;
use const MobilitySoft\TBSA\TALK_SUBMISSION_POST_TYPE_SLUG;

class SubmissionStatusChangeListener
{
    /**
     * @var SubmissionRepository
     */
    private $submissionRepository;

    /**
     * @var SubmissionStatus[]
     */
    private $statuses;

    /**
     * @var SubmissionConsumer[]
     */
    private $callbacks;

    public function __construct(SubmissionRepository $submissionRepository)
    {
        $this->submissionRepository = $submissionRepository;
    }

    public function addCallback(SubmissionConsumer $submissionConsumer): self
    {
        $this->callbacks[] = $submissionConsumer;
        return $this;
    }

    public function listen(): void
    {
        add_action('acf/save_post', function ($id): void {
            if (get_post_type($id) === TALK_SUBMISSION_POST_TYPE_SLUG) {
                $submission = $this->submissionRepository->findById((int) $id);
                if ($submission !== null) {
                    $this->statuses[$submission->getId()] = $submission->getStatus();
                }
            }
        }, 9, 1);

        add_action('acf/save_post', function ($id): void {
            if (get_post_type($id) === TALK_SUBMISSION_POST_TYPE_SLUG) {
                $submission = $this->submissionRepository->findById($id);
                if ($submission !== null) {
                    $oldStatus = $this->statuses[(int) $id];
                    if ($submission->getStatus() !== $oldStatus) {
                        foreach ($this->callbacks as $callback) {
                            $callback->execute($submission);
                        }
                    }
                }
            }
        }, 11, 1);
    }
}
