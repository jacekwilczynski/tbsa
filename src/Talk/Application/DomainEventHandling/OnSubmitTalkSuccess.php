<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\DomainEventHandling;

use MobilitySoft\TBSA\SalesManago\TalkSubmissionHandler\TalkSubmissionHandler;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\SubmissionConsumer;

class OnSubmitTalkSuccess implements SubmissionConsumer
{
    /**
     * @var SendSubmissionNotification
     */
    private $sendSubmissionNotification;

    /**
     * @var TalkSubmissionHandler
     */
    private $salesManagoHandler;

    public function __construct(
        SendSubmissionNotification $sendSubmissionNotification,
        TalkSubmissionHandler $salesManagoHandler
    ) {
        $this->sendSubmissionNotification = $sendSubmissionNotification;
        $this->salesManagoHandler = $salesManagoHandler;
    }

    public function execute(Submission $submission): void
    {
        $this->sendSubmissionNotification->execute(
            'new_submission',
            $submission
        );

        $this->salesManagoHandler->execute($submission);
    }
}
