<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\Controller;

use Closure;
use MobilitySoft\TBSA\Common\Application\Event\CreateEventQueryFromHttpRequestParams;
use MobilitySoft\TBSA\Service\TranslateMonth;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Event\FindEvents;
use MobilitySoft\TBSA\Talk\Domain\EventHasPotentialIndustryConflict;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use MobilitySoft\TBSA\Talk\View\ViewEventFactory;
use Twig\Environment as Twig;
use WP_Error;

class EventsController
{
    /**
     * @var CreateEventQueryFromHttpRequestParams
     */
    private $createEventQuery;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var FindEvents
     */
    private $findAvailableEvents;

    /**
     * @var EventHasPotentialIndustryConflict
     */
    private $eventHasPotentialIndustryConflict;

    public function __construct(
        CreateEventQueryFromHttpRequestParams $createEventQuery,
        FindEvents $findAvailableEvents,
        Twig $twig,
        UserRepository $userRepository,
        EventHasPotentialIndustryConflict $eventHasPotentialIndustryConflict
    ) {
        $this->createEventQuery = $createEventQuery;
        $this->twig = $twig;
        $this->userRepository = $userRepository;
        $this->findAvailableEvents = $findAvailableEvents;
        $this->eventHasPotentialIndustryConflict = $eventHasPotentialIndustryConflict;
    }

    public function execute(): void
    {
        $query = $this->createEventQuery->execute($_GET);

        if ($query->getStartDate()) {
            $user = $this->userRepository->getCurrentUser();
            if (!$user) {
                wp_die(new WP_Error(401, 'You must be logged in to browse events for talk submissions.'));
            }

            $events = $this->findAvailableEvents->execute($query);
            $date = TranslateMonth::execute($query->getStartDate()->format('j F Y'));
            $context = [
                'events' => (new ViewEventFactory($this->getConflictChecker($user)))->convertMultiple($events),
                'date'   => $date,
            ];

        } else {
            $context = [];
        }

        echo $this->twig->render('myAccount/submitTalkEvents.twig', $context);

        wp_die();
    }

    private function getConflictChecker(User $user): ?Closure
    {
        if (is_numeric($_GET['talk_id'] ?? null)) {
            return function (Event $event) use ($user): bool {
                return $this->eventHasPotentialIndustryConflict->execute(
                    (int) $_GET['talk_id'],
                    $event->getId(),
                    $user->getId()
                );
            };
        }

        return null;
    }
}
