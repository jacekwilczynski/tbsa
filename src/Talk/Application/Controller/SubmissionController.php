<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\Controller;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Common\View\Anchor;
use MobilitySoft\TBSA\Talk\Application\DomainEventHandling\OnSubmissionStatusChange;
use MobilitySoft\TBSA\Talk\Application\TalksPage\SubmissionListDataProvider;
use MobilitySoft\TBSA\Talk\Domain\Event\EventNotFoundException;
use MobilitySoft\TBSA\Talk\Domain\EventHasPotentialIndustryConflict;
use MobilitySoft\TBSA\Talk\Domain\Exception\EventNotAvailableException;
use MobilitySoft\TBSA\Talk\Domain\Exception\NotAllowedToSubmitTalkException;
use MobilitySoft\TBSA\Talk\Domain\Exception\NotAuthenticatedException;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\SubmitTalk\SubmitTalk;
use MobilitySoft\TBSA\Talk\Domain\SubmitTalk\SubmitTalkRequest;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateNotFoundException;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use WP_REST_Request;
use WP_REST_Response;

class SubmissionController
{
    /**
     * @var SubmitTalk
     */
    private $submitTalk;

    /**
     * @var EventHasPotentialIndustryConflict
     */
    private $eventHasPotentialIndustryConflict;

    /**
     * @var User
     */
    private $user;

    /**
     * @var SubmissionRepository
     */
    private $submissionRepository;

    /**
     * @var OnSubmissionStatusChange
     */
    private $onSubmissionStatusChange;

    /**
     * @var SubmissionListDataProvider
     */
    private $submissionListDataProvider;

    public function __construct(
        SubmitTalk $submitTalk,
        EventHasPotentialIndustryConflict $eventHasPotentialIndustryConflict,
        User $user,
        SubmissionRepository $submissionRepository,
        OnSubmissionStatusChange $onSubmissionStatusChange,
        SubmissionListDataProvider $submissionListDataProvider
    ) {
        $this->submitTalk = $submitTalk;
        $this->eventHasPotentialIndustryConflict = $eventHasPotentialIndustryConflict;
        $this->user = $user;
        $this->submissionRepository = $submissionRepository;
        $this->onSubmissionStatusChange = $onSubmissionStatusChange;
        $this->submissionListDataProvider = $submissionListDataProvider;
    }

    public function submit(WP_REST_Request $request): WP_REST_Response
    {
        $body = json_decode($request->get_body(), true);

        $validationErrors = $this->findValidationErrors($body);
        if ($validationErrors) {
            $this->log($this->getValidationMessage($validationErrors, $request->get_body()));
            return new WP_REST_Response($this->getSubmissionErrorMessage(), 400);
        }

        $templateId = (int) $body['talkTemplate'];
        $eventId = (int) $body['event'];
        $submitTalkRequest = new SubmitTalkRequest($eventId, $templateId);

        try {
            $this->submitTalk->execute($submitTalkRequest);
            if ($this->eventHasPotentialIndustryConflict->execute(
                $templateId,
                $eventId,
                get_current_user_id()
            )) {
                $notification = __('We wskazanym mieście występuje potencjalny konflikt ' .
                                   'branżowy z Twoją prezentacją. Po otrzymaniu Twojego ' .
                                   'wniosku, skontaktujemy się z Członkiem TB reprezentującym ' .
                                   'wybraną przez Ciebie branżę.');
            } else {
                $notification = __('Dziękujemy! Wniosek został wysłany. Gdy ' .
                                   'zostanie opublikowany, otrzymasz powiadomienie ' .
                                   'na adres mailowy podany w formularzu.');
            }
            return new WP_REST_Response(['notification' => $notification], 200);
        } catch (NotAuthenticatedException $e) {
            $this->log($e);
            return new WP_REST_Response(
                'Musisz być zalogowany, aby zgłosić wniosek o prezentację.',
                401
            );
        } catch (NotAllowedToSubmitTalkException $e) {
            $this->log($e);
            return new WP_REST_Response(
                'Nie masz wystarczających uprawnień, aby zgłosić wniosek o prezentację.',
                403
            );
        } catch (TemplateNotFoundException $e) {
            $this->log($e);
            return new WP_REST_Response(
                'Wskazany szablon prezentacji nie istnieje.',
                404
            );
        } catch (EventNotFoundException $e) {
            $this->log($e);
            return new WP_REST_Response(
                'Wskazane wydarzenie nie istnieje.',
                404
            );
        } catch (EventNotAvailableException $e) {
            $this->log($e);
            return new WP_REST_Response(
                'Na wybrane wydarzenie nie można już zgłaszać prezentacji.',
                410
            );
        } catch (\Throwable $e) {
            $this->log($e);
            return new WP_REST_Response($this->getSubmissionErrorMessage(), 500);
        }
    }

    public function accept(WP_REST_Request $request): WP_REST_Response
    {
        $submissionId = (int) $request->get_param('id');

        if (!$this->user->canAcceptSubmissions()) {
            return new WP_REST_Response(
                'Nie masz uprawnień do akceptowania wniosków.',
                403
            );
        }

        $submission = $this->submissionRepository->findById($submissionId);

        if ($submission === null) {
            return new WP_REST_Response(
                "Wniosek o numerze $submissionId nie został znaleziony.",
                404
            );
        }

        if ($submission->isAccepted()) {
            return new WP_REST_Response(
                "Wniosek o numerze $submissionId jest już zaakceptowany.",
                403
            );
        }

        $eventStart = $submission->getEvent()->getTimespan()->getStart();
        if ($eventStart < new \DateTimeImmutable()) {
            return new WP_REST_Response(
                sprintf(
                    'Nie można zaakceptować wniosku na przeszłe wydarzenie (z %s)',
                    $eventStart->format('Y-m-d H:i')
                ),
                403
            );
        }

        $submission->setStatus(SubmissionStatus::ACCEPTED());
        $this->submissionRepository->save($submission);
        $this->onSubmissionStatusChange->execute($submission);

        return new WP_REST_Response('', 204);
    }

    public function search(WP_REST_Request $request): array
    {
        return $this->submissionListDataProvider->getSubmissions(
            (string) $request->get_param('ownership'),
            (int) $request->get_param('offset'),
            (int) $request->get_param('limit'),
            (string) $request->get_param('phrase')
        );
    }

    private function findValidationErrors($body): array
    {
        $errors = [];

        if (!$body) {
            $errors[] = 'Unparseable request body. Please supply JSON with "talkTemplate" and "event" fields saying which talk template you want to submit to which event.';
            return $errors;
        }

        if (isset($body['talkTemplate'])) {
            $talkTemplateId = (int) $body['talkTemplate'];
            if ($talkTemplateId != $body['talkTemplate'] || $talkTemplateId < 0) {
                $errors[] = 'talkTemplate field must be a natural number representing ' .
                            "the id of the template, got: {$body['talkTemplate']}";
            }
        } else {
            $errors[] = "Missing talkTemplate field with the talk template's id.";
        }

        if (isset($body['event'])) {
            $eventId = (int) $body['event'];
            if ($eventId != $body['event'] || $eventId < 0) {
                $errors[] = 'event field must be a natural number representing ' .
                            "the id if the event, got: {$body['event']}";
            }
        } else {
            $errors[] = "Missing event field with the talk event's id.";
        }

        return $errors;
    }

    private function getSubmissionErrorMessage(): string
    {
        return sprintf(
            __('Wystąpił błąd. Spróbuj odświeżyć stronę i zgłosić ponownie, a jeśli błąd się powtórzy, skontaktuj się z %s.'),
            Anchor::simple((string) get_option('admin_email'))
        );
    }

    private function getValidationMessage(array $validationErrors, string $rawBody): string
    {
        $list = array_map(function (string $error): string {
            return ' - ' . $error;
        }, $validationErrors);

        return (
            "SubmissionController received an invalid request.\n" .
            "Validation errors:\n" .
            implode(PHP_EOL, $list) . PHP_EOL .
            "Request body:\n" .
            $rawBody . PHP_EOL
        );
    }

    private function log($content): void
    {
        $timestamp = (new \DateTimeImmutable())->format(\DateTimeImmutable::ATOM);
        error_log("[$timestamp] $content");
    }
}
