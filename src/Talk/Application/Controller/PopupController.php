<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\Controller;

use MobilitySoft\TBSA\Common\View\Anchor;
use MobilitySoft\TBSA\EventSearchCommon\Form\EventSearchForm;
use MobilitySoft\TBSA\EventSearchCommon\Form\EventSearchFormBuilderFactory;
use MobilitySoft\TBSA\Talk\Application\EventSearch\BranchField;
use MobilitySoft\TBSA\Talk\Application\EventSearch\DateField;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branches;
use MobilitySoft\TBSA\Talk\Domain\Branch\BranchRepository;
use MobilitySoft\TBSA\Talk\Domain\Date\Date;
use MobilitySoft\TBSA\Talk\Domain\Date\DateRepository;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use Twig\Environment as Twig;
use WP_Error;

class PopupController
{
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var DateRepository
     */
    private $dateRepository;

    /**
     * @var EventSearchFormBuilderFactory
     */
    private $eventSearchFormBuilderFactory;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        BranchRepository $branchRepository,
        DateRepository $dateRepository,
        EventSearchFormBuilderFactory $eventSearchFormBuilderFactory,
        Twig $twig,
        UserRepository $userRepository
    ) {
        $this->branchRepository = $branchRepository;
        $this->dateRepository = $dateRepository;
        $this->eventSearchFormBuilderFactory = $eventSearchFormBuilderFactory;
        $this->twig = $twig;
        $this->userRepository = $userRepository;
    }

    public function html(): void
    {
        $user = $this->userRepository->getCurrentUser();

        if (!$user) {
            wp_die(new WP_Error(401, 'You must be logged in to browse events for talk submissions.'));
        }

        $branches = $this->branchRepository->getNonVirtualBranches();
        $initialBranchId = $this->getInitialBranchId($branches, $user);

        echo $this->twig->render('myAccount/submitTalkPopup.twig', [
            'form' => $this->getForm($branches, $initialBranchId),
        ]);
        ?>
        <script>
          if (!window.tbsa_submitTalk) {
            window.tbsa_submitTalk = {};
          }
          window.tbsa_submitTalk.eventsAjaxUrl = '<?= admin_url('/admin-ajax.php/?action=submit_talk_events') ?>';
          window.tbsa_submitTalk.fetchErrorMessage = '<?= $this->getFetchErrorMessage() ?>';
          window.tbsa_submitTalk.initialDate = '';
          window.tbsa_submitTalk.submissionStatuses = {
            sending: '<?= __('Wysyłam...') ?>',
            success: '<?= __('Wniosek przesłany.') ?>'
          };
          jQuery(window).trigger('submitTalkPopup/inserted');
        </script>
        <?php

        wp_die();
    }

    private function getInitialBranchId(Branches $allBranches, User $user): int
    {
        if ($user) {
            $branch = $user->getMemberBranches()->getFirst();

            if ($branch) {
                return $branch->getId();
            }
        }

        return $allBranches->getFirst()->getId();
    }

    private function getForm(Branches $branches, int $initialBranchId): EventSearchForm
    {
        $builder = $this->eventSearchFormBuilderFactory->createBuilder();
        $builder->addField(new BranchField($branches, $initialBranchId));
        $builder->addField(new DateField());

        return $builder->buildForm();
    }

    private function getFetchErrorMessage(): string
    {
        return sprintf(
            __('Przepraszamy, wystąpił błąd przy próbie wczytania wydarzeń. Prosimy o kontakt na %s.'),
            Anchor::simple((string) get_option('admin_email'))
        );
    }

    public function data(): void
    {
        $branchIds = $this->branchRepository->getNonVirtualBranchIds();

        echo json_encode([
            'datesByBranchId' => $this->getFormattedDatesByBranchId($branchIds),
        ]);

        wp_die();
    }

    /**
     * @param int[] $branchIds
     *
     * @return string[][]
     */
    private function getFormattedDatesByBranchId(array $branchIds): array
    {
        $datesByBranchId = $this->dateRepository->findAvailableDatesByBranchIds(...$branchIds);

        return $this->formatDatesByBranchId($datesByBranchId);
    }

    /**
     * @param Date[][] $datesByBranchId
     *
     * @return string[][]
     */
    private function formatDatesByBranchId(array $datesByBranchId): array
    {
        return array_map(function (array $dates) {
            return array_map(function (Date $date) {
                return (string) $date->getStatus();
            }, $dates);
        }, $datesByBranchId);
    }
}
