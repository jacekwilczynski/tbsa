<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\Controller;

use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\EntrepreneurRepository;
use MobilitySoft\TBSA\Talk\Infrastructure\GetSpeakerAcfFieldsMap;
use WP_REST_Request;
use WP_REST_Response;

class EntrepreneurFieldsController
{
    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var GetSpeakerAcfFieldsMap
     */
    private $getSpeakerAcfFieldsMap;

    public function __construct(
        EntrepreneurRepository $entrepreneurRepository,
        GetSpeakerAcfFieldsMap $getSpeakerAcfFieldsMap
    ) {
        $this->entrepreneurRepository = $entrepreneurRepository;
        $this->getSpeakerAcfFieldsMap = $getSpeakerAcfFieldsMap;
    }

    public function getEntrepreneurFields(WP_REST_Request $request): WP_REST_Response
    {
        $rawEntrepreneurId = $request->get_param('entrepreneur_id');
        if (empty($rawEntrepreneurId)) {
            return new WP_REST_Response('Missing entrepreneur_id.', 400);
        }
        $entrepreneurId = (int) $rawEntrepreneurId;

        $entrepreneur = $this->entrepreneurRepository->findById($entrepreneurId);
        if (!$entrepreneur) {
            return new WP_REST_Response("Entrepreneur with id $entrepreneurId not found.", 404);
        }

        $data = [
            'fields' => $this->getSpeakerAcfFieldsMap->fromEntrepreneur($entrepreneur),
        ];
        if ($entrepreneur->getPhoto()) {
            $data['photoUrl'] = wp_get_attachment_image_url($entrepreneur->getPhoto()->getId(), 'full');
        }

        return new WP_REST_Response($data, 200);
    }
}
