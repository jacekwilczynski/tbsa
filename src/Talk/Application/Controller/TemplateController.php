<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\Controller;

use MobilitySoft\TBSA\Talk\Domain\Template\Template;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateQuery;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateRepository;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class TemplateController
{
    /**
     * @var TemplateRepository
     */
    private $templateRepository;

    /**
     * @var User
     */
    private $user;

    public function __construct(TemplateRepository $templateRepository, User $user)
    {
        $this->templateRepository = $templateRepository;
        $this->user = $user;
    }

    public function getNewestId(): ?int
    {
        return $this->templateRepository->getLatestCreatedTemplateId($this->user->getId());
    }

    public function search(): ?array
    {
        $query = new TemplateQuery();
        $query->setUserId($this->user->getId());

        if ($this->templateRepository->countTemplates($query) === 0) {
            return null;
        }

        $this->setQueryParamsFromRequest($query);
        $templates = $this->templateRepository->findTemplates($query);

        return [
            'totalCount' => $templates->getTotalCount(),
            'items'      => array_map(
                static function (Template $talk): array {
                    return [
                        'id'      => $talk->getId(),
                        'subject' => $talk->getTalk()->getSubject(),
                    ];
                },
                $templates->getItems()
            ),
        ];
    }

    private function setQueryParamsFromRequest(TemplateQuery $query): void
    {
        if (isset($_GET['offset'])) {
            $query->setOffset((int) $_GET['offset']);
        }
        if (isset($_GET['limit'])) {
            $query->setLimit((int) $_GET['limit']);
        }
    }
}
