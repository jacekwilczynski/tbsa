<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\EditTalkPage;

use MobilitySoft\TBSA\Common\Application\InitializableRenderer;
use MobilitySoft\TBSA\Common\Infrastructure\AcfFields;
use MobilitySoft\TBSA\Talk\Domain\DetectTemplateVsSubmission;
use MobilitySoft\TBSA\Talk\Domain\TalkAccessChecker;
use MobilitySoft\TBSA\Talk\Domain\TemplateVsSubmission;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Infrastructure\GetSpeakerAcfFieldsMap;
use MobilitySoft\TBSA\Talk\Infrastructure\Template\CanCurrentUserAccessTalkTemplate;
use Twig\Environment as Twig;

class EditTalkRenderer implements InitializableRenderer
{
    private const SPEAKERS_FIELD_KEY = 'field_5d57c69268a45';

    /**
     * @var AcfFields
     */
    private $acfFields;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var int|null
     */
    private $talkId;

    /**
     * @var EditTalkForm
     */
    private $form;

    /**
     * @var User
     */
    private $user;

    /**
     * @var TalkAccessChecker
     */
    private $accessChecker;

    /**
     * @var string|null
     */
    private $redirect;

    /**
     * @var DetectTemplateVsSubmission
     */
    private $detectTemplateVsSubmission;

    /**
     * @var CommentsRenderer
     */
    private $commentsRenderer;

    public function __construct(
        AcfFields $acfFields,
        Twig $twig,
        User $user,
        TalkAccessChecker $accessChecker,
        DetectTemplateVsSubmission $detectTemplateVsSubmission,
        CommentsRenderer $commentsRenderer
    ) {
        $this->acfFields = $acfFields;
        $this->twig = $twig;
        $this->user = $user;
        $this->accessChecker = $accessChecker;
        $this->detectTemplateVsSubmission = $detectTemplateVsSubmission;
        $this->commentsRenderer = $commentsRenderer;
    }

    public function withRedirect(string $redirect): EditTalkRenderer
    {
        $this->redirect = $redirect;
        return $this;
    }

    public function render(): string
    {
        $creatingNew = $this->talkId === null;
        if ($creatingNew || $this->accessChecker->canUserEditTalkWithId($this->user, $this->talkId)) {
            $context = ['form' => $this->form->render()];
            if ($this->detectTemplateVsSubmission->execute($this->talkId) === TemplateVsSubmission::SUBMISSION()) {
                $context['comments'] = $this->commentsRenderer->render($this->talkId);
            }
        } else {
            $context = [
                'message' => __('Nie masz dostępu do podglądu ani edycji tej prezentacji.'),
            ];
        }

        return $this->twig->render('myAccount/talk.twig', $context);
    }

    public function initialize(): void
    {
        acf_form_head();

        $this->talkId = isset($_GET['id']) ? (int) $_GET['id'] : null;
        $this->form = new EditTalkForm($this->talkId, $this->user->getId());

        ?>
        <script>
          if (!window.tbsa_talk) {
            window.tbsa_talk = {};
          }
          window.tbsa_talk.userLevel = '<?= $this->getUserLevel() ?>';
        </script>
        <?php

        if ($this->talkId === null) {
            $this->form->setRedirect($this->redirect);

            $prefills = (new GetSpeakerAcfFieldsMap())->fromUser($this->user);
            $this->acfFields->setValuesByFieldKey($prefills);

            if ($this->user->isPrivileged()) {
                $this->form->allowClearing();
            }
        } else {
            ?>
            <script>
              window.tbsa_talk.id = <?= $this->talkId ?>;
            </script>
            <?php
        }

        add_filter('acf/load_field/key=' . self::SPEAKERS_FIELD_KEY, function (array $field): array {
            $field['max'] = $this->user->getMaxNumberOfSpeakersUserCanAdd();
            return $field;
        });
    }

    private function getUserLevel(): string
    {
        if ($this->user->isAdmin()) {
            $userLevel = 'admin';
        } elseif ($this->user->isBoardMember()) {
            $userLevel = 'board_member';
        } else {
            $userLevel = 'regular';
        }
        return $userLevel;
    }
}
