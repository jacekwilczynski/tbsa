<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\EditTalkPage;

use Twig\Environment as Twig;

class CommentsRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function render(int $talkId): string
    {
        if (comments_open($talkId)) {
            return $this->twig->render('myAccount/submissionComments.twig', [
                'form'     => $this->renderForm($talkId),
                'comments' => $this->renderComments($talkId),
            ]);
        }

        return '';
    }

    private function renderForm(int $talkId): string
    {
        ob_start();
        comment_form([
            'class_form'    => 'js-submission-comment-form',
            'comment_field' => $this->renderTextArea(),
            'logged_in_as'  => '',
            'label_submit'  => __('Publish'),
        ], $talkId);
        return ob_get_clean();
    }

    private function renderComments(int $talkId): string
    {
        return wp_list_comments(
            [
                'style' => 'div',
                'echo'  => false,
            ],
            get_comments([
                'post_id'  => $talkId,
                'per_page' => -1,
            ])
        ) ?? '';
    }

    private function renderTextArea(): string
    {
        return '<textarea
            id="comment"
            name="comment" 
            rows="8" 
            maxlength
            placeholder="Komentarz" 
            required
        ></textarea>';
    }
}
