<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\EditTalkPage;

use MobilitySoft\TBSA\Talk\Domain\TemplateVsSubmission;
use MobilitySoft\TBSA\Talk\Infrastructure\DetectTemplateVsSubmission;
use const MobilitySoft\TBSA\TALK_TEMPLATE_POST_TYPE_SLUG;

class EditTalkForm
{
    /**
     * @var int|null
     */
    private $talkId;

    /**
     * @var string = ''
     */
    private $redirect;

    /**
     * @var bool
     */
    private $allowClearing;

    /**
     * @var TemplateVsSubmission
     */
    private $talkClass;

    /**
     * @var int
     */
    private $currentUserId;

    public function __construct(?int $talkId, int $currentUserId)
    {
        $this->talkId = $talkId;
        $this->talkClass = (new DetectTemplateVsSubmission())->execute($talkId);
        $this->currentUserId = $currentUserId;
    }

    public function setRedirect(string $redirect): void
    {
        $this->redirect = $redirect;
    }

    public function allowClearing(): void
    {
        $this->allowClearing = true;
    }

    public function render(): string
    {
        $context = [
            'id'                 => 'tbsa_talk-form',
            'post_id'            => $this->talkId ?? 'new_post',
            'new_post'           => [
                'post_type'   => TALK_TEMPLATE_POST_TYPE_SLUG,
                'post_status' => 'publish',
                'meta_input'  => [
                    'owners' => [(string) $this->currentUserId],
                ],
            ],
            'updated_message'    => __('Zaktualizowano.'),
            'html_submit_button' => $this->renderButtons(),
        ];

        if ($this->redirect) {
            $context['return'] = $this->redirect;
        }

        ob_start();
        acf_form($context);
        return ob_get_clean();
    }

    private function renderButtons(): string
    {
        ob_start();
        ?>
        <div class="acf-bottom-buttons-row">
            <?php if ($this->allowClearing) : ?>
                <input type="reset"
                       class="acf-button button button-large"
                       value="<?= __('Wyczyść formularz') ?>">
            <?php endif ?>
            <input type="submit"
                   class="acf-button button button-large"
                   value="<?= $this->getSubmitText() ?>"/>
            <?php if ($this->talkClass === TemplateVsSubmission::TEMPLATE()) : ?>
                <input type="submit"
                       class="acf-button button button-primary button-large js-auto-submit-talk"
                       value="<?= __('Zapisz i wyślij') ?>"/>
            <?php endif ?>
        </div>
        <?php
        return ob_get_clean();
    }

    private function getSubmitText(): string
    {
        return $this->talkClass === TemplateVsSubmission::TEMPLATE()
            ? __('Zapisz szablon')
            : __('Zaktualizuj wniosek');
    }
}
