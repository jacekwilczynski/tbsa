<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\EventSearch;

use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\DatePicker;

class DateField extends DatePicker
{
    public function getName(): string
    {
        return 'date';
    }

    public function getLabel(): string
    {
        return __('Data');
    }

    public function keepDatePickerWithInput(): bool
    {
        return true;
    }
}
