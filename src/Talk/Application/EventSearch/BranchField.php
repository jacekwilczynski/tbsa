<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\EventSearch;

use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\Select;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branches;

class BranchField extends Select
{
    /**
     * @var Branches
     */
    private $branches;

    /**
     * @var array
     */
    private $kvPairs;

    /**
     * @var int
     */
    private $defaultId;

    public function __construct(Branches $branches, int $defaultId)
    {
        $this->branches  = $branches;
        $this->defaultId = $defaultId;
    }

    public function getLabel(): string
    {
        return __('Miasto');
    }

    public function getName(): string
    {
        return 'branch';
    }

    public function getValue(): int
    {
        return isset($_GET['branch']) ? (int)$_GET['branch'] : $this->defaultId;
    }

    public function getKvPairs(): array
    {
        if ($this->kvPairs === null) {
            $this->kvPairs = [];

            foreach ($this->branches->getItems() as $branch) {
                $this->kvPairs[$branch->getId()] = $branch->getTown();
            }
        }

        return $this->kvPairs;
    }

    public function getNoEmpty(): bool
    {
        return true;
    }
}
