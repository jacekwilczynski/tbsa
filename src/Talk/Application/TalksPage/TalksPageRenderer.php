<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\TalksPage;

use MobilitySoft\TBSA\Common\Application\InitializableRenderer;
use MobilitySoft\TBSA\Talk\Domain\CanUserEditSubmissions;

class TalksPageRenderer implements InitializableRenderer
{

    /**
     * @var TemplateListDataProvider
     */
    private $templateListDataProvider;

    /**
     * @var SubmissionListDataProvider
     */
    private $submissionListDataProvider;

    public function __construct(
        TemplateListDataProvider $templateListDataProvider,
        SubmissionListDataProvider $submissionListDataProvider
    ) {
        $this->templateListDataProvider = $templateListDataProvider;
        $this->submissionListDataProvider = $submissionListDataProvider;
    }

    public function render(): string
    {
        return '<div id="talks-react-root"></div>';
    }

    public function initialize(): void
    {
        acf_form_head();

        add_action('wp_enqueue_scripts', function () {
            wp_localize_script(
                'my-account-script',
                'tbsa_talks',
                [
                    'nonce'               => wp_create_nonce('wp_rest'),
                    'ajaxUrl'             => home_url('/wp-json/tbsa/v1/talks'),
                    'confirmDeletionText' => __('Czy na pewno chcesz usunąć szablon prezentacji "%s"? (Nie spowoduje to zniknięcia danych prezentacji z już przesłanych zgłoszeń.)'),
                    'deletingText'        => __('Usuwam...'),
                    'acceptingText'       => __('Akceptuję...'),
                    'acceptedText'        => __('Zaakceptowano'),
                ]
            );

            $this->templateListDataProvider->passJsData();
            $this->submissionListDataProvider->passJsData();
        }, 11);
    }
}
