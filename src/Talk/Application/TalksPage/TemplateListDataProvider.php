<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\TalksPage;

class TemplateListDataProvider extends AbstractTalkListDataProvider
{
    public function passJsData(): void
    {
        wp_localize_script('my-account-script', 'tbsa_talkTemplatesSectionData', [
            'newTalkUrl'    => wc_get_account_endpoint_url('create_talk'),
            'editTalkUrl'   => $this->getEditTalkBaseUrl(),
            'guidebookUrl'  => get_field('tbsa_talk_guidebook', 'option'),
        ]);
    }
}
