<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\TalksPage;

class AbstractTalkListDataProvider
{
    protected function getEditTalkBaseUrl(): string
    {
        return wc_get_account_endpoint_url('edit_talk') . '?id=';
    }
}
