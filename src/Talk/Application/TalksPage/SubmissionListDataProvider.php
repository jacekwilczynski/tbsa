<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Application\TalksPage;

use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Common\View\Anchor;
use MobilitySoft\TBSA\Common\View\Submission\SubmissionStatusForDisplay;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Exception\NotAllowedToViewOthersSubmissionsException;
use MobilitySoft\TBSA\Talk\Domain\FindOthersSubmissionsUserCanSee;
use MobilitySoft\TBSA\Talk\Domain\FindUsersSubmissions;
use MobilitySoft\TBSA\Talk\Domain\Submission\PaginatedSubmissions;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\SubmissionAccessChecker;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class SubmissionListDataProvider extends AbstractTalkListDataProvider
{
    private const OWNERSHIP_OWN = 'own';
    private const OWNERSHIP_OTHERS = 'others';
    private const OWNERSHIPS = [self::OWNERSHIP_OWN, self::OWNERSHIP_OTHERS];

    /**
     * @var User
     */
    private $user;

    /**
     * @var FindUsersSubmissions
     */
    private $findUsersSubmissions;

    /**
     * @var FindOthersSubmissionsUserCanSee
     */
    private $findOthersSubmissionsUserCanSee;

    public function __construct(
        User $user,
        FindUsersSubmissions $findUsersSubmissions,
        FindOthersSubmissionsUserCanSee $findOthersSubmissionsUserCanSee
    ) {
        $this->user = $user;
        $this->findUsersSubmissions = $findUsersSubmissions;
        $this->findOthersSubmissionsUserCanSee = $findOthersSubmissionsUserCanSee;
    }

    public function passJsData(): void
    {
        $data = [
            'canEditSubmissions'   => $this->user->canEditSubmissions(),
            'canAcceptSubmissions' => $this->user->canAcceptSubmissions(),
            'editTalkUrl'          => $this->getEditTalkBaseUrl(),
        ];

        ?>
        <script>
          window.tbsa_talkSubmissionsSectionData = <?= json_encode($data) ?>;
        </script>
        <?php
    }

    public function getSubmissions(string $ownership, int $offset, int $limit, string $phrase): array
    {
        if ($ownership === self::OWNERSHIP_OTHERS) {
            if (!$this->user->canEditSubmissions()) {
                throw new NotAllowedToViewOthersSubmissionsException();
            }
            $submissions = $this->findOthersSubmissionsUserCanSee->execute($this->user, $offset, $limit, $phrase);
        } elseif ($ownership === self::OWNERSHIP_OWN) {
            $submissions = $this->findUsersSubmissions->execute($this->user, $offset, $limit, $phrase);
        } else {
            throw new \InvalidArgumentException(
                'Submission ownership must be one of: ' .
                implode(', ', self::OWNERSHIPS)
            );
        }

        return $this->createSubmissionsViewData($submissions);
    }

    private function createSubmissionsViewData(PaginatedSubmissions $submissions): array
    {
        $items = $submissions->getItems();
        usort($items, function (Submission $a, Submission $b): int {
            return (
                $a->getEvent()->getTimespan()->getStart() <=>
                $b->getEvent()->getTimespan()->getStart()
            );
        });
        return [
            'totalCount' => $submissions->getTotalCount(),
            'items'      => array_map(function (Submission $submission) {
                return $this->createSubmissionViewData($submission);
            }, $items),
        ];
    }

    private function createSubmissionViewData(Submission $submission): array
    {
        $event = $submission->getEvent();
        $canManage = (
            $this->user->isAdmin() ||
            $this->user->getManagedBranches()->includes($event->getBranch())
        );
        $canEdit = (new SubmissionAccessChecker())->canUserEditSubmission($this->user, $submission);

        $displayStatus = new SubmissionStatusForDisplay($submission->getStatus());
        return [
            'id'          => $submission->getId(),
            'eventDate'   => $event->getTimespan()->getStart()->format('Y-m-d'),
            'eventLink'   => (string) $this->getEventLink($event),
            'talkSubject' => $submission->getTalk()->getSubject(),
            'status'      => [
                'raw'       => $displayStatus->getRaw(),
                'formatted' => $displayStatus->getFormatted(),
            ],
            'manageable'  => $canManage,
            'editable'    => $canEdit,
            'acceptable'  => $submission->getStatus() === SubmissionStatus::AWAITING(),
        ];
    }

    private function getEventLink(Event $event): Anchor
    {
        return Anchor::presentable(
            $event->getUrl(),
            $event->getTitle(),
            true,
            ['class' => 'js-submission-event-title']
        );
    }
}
