<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Entrepreneur;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Business;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\BusinessRepository as BusinessRepositoryInterface;
use const MobilitySoft\TBSA\BUSINESS_POST_TYPE_SLUG;
use const MobilitySoft\TBSA\INDUSTRY_TAXONOMY_SLUG;

class BusinessRepository implements BusinessRepositoryInterface
{
    public function getById(int $id): ?Business
    {
        if (get_post_type($id) === BUSINESS_POST_TYPE_SLUG) {
            $name = get_the_title($id);
            $url = get_field('url', $id);

            return new Business(
                $id,
                $name,
                $url ?: '',
                new Industries(...get_term_names(INDUSTRY_TAXONOMY_SLUG, $id))
            );
        }

        return null;
    }
}
