<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Entrepreneur;

use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Business;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Businesses;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Contact;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\EntrepreneurRepository as EntrepreneurRepositoryInterface;
use MobilitySoft\TBSA\ValueObject\Image;
use const MobilitySoft\TBSA\ENTREPRENEUR_POST_TYPE_SLUG;

class EntrepreneurRepository implements EntrepreneurRepositoryInterface
{
    /**
     * @var BusinessRepository
     */
    private $businessRepository;

    public function __construct(BusinessRepository $businessRepository)
    {
        $this->businessRepository = $businessRepository;
    }

    public function findById(int $id): ?Entrepreneur
    {
        if (get_post_type($id) !== ENTREPRENEUR_POST_TYPE_SLUG) {
            return null;
        }

        $acfFields = get_fields($id);
        $name = get_the_title($id);
        $contact = $this->getContact($acfFields);
        $photo = is_int($acfFields['photo'] ?? null) ? new Image($acfFields['photo']) : null;
        $description = $acfFields['description'] ?? '';
        $businesses = $this->getBusinesses($acfFields);

        return new Entrepreneur($id, $name, $contact, $photo, $description, $businesses);
    }

    public function getContact($acfFields): Contact
    {
        $links = $acfFields['external_links'] ?? null;
        if (is_array($links)) {
            $emailLink = $this->findLinkByType('email', $links);
            $phoneLink = $this->findLinkByType('tel', $links);
        }

        return new Contact(
            $emailLink['url'] ?? '',
            $phoneLink['url'] ?? ''
        );
    }

    public function findLinkByType(string $type, array $links)
    {
        return array_find($links, function ($link) use ($type) {
            return ($link['type']['value'] ?? null) === $type;
        });
    }

    private function getBusinesses($acfFields): Businesses
    {
        $relations = isset($acfFields['businesses']) && is_array($acfFields['businesses'])
            ? $acfFields['businesses']
            : [];

        /** @var int[] $ids */
        $ids = array_filter(array_map(function ($relation): ?int {
            return $relation['business'] ?? null;
        }, $relations));

        /** @var Business[] $businesses */
        $businesses = array_filter(array_map(function (int $id): ?Business {
            return $this->businessRepository->getById($id);
        }, $ids));

        return new Businesses(...$businesses);
    }

    public function update(Entrepreneur $entrepreneur): void
    {
        $id = $entrepreneur->getId();

        $links = get_field('external_links', $id);
        foreach ($links as &$link) {
            if (($link['type']['value'] ?? null) === 'email') {
                $link['url'] = $entrepreneur->getContact()->getEmail();
                break;
            }
        }
        foreach ($links as &$link) {
            if (($link['type']['value'] ?? null) === 'tel') {
                $link['url'] = $entrepreneur->getContact()->getPhone();
                break;
            }
        }

        update_field('external_links', $links, $id);
        update_field('photo', $entrepreneur->getPhoto()->getId(), $id);
        update_field('description', $entrepreneur->getDescription(), $id);
    }
}
