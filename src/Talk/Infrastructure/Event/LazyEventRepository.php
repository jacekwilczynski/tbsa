<?php /** @noinspection PhpUnusedParameterInspection */

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Event;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Event\EventRepository as EventRepositoryInterface;
use MobilitySoft\TBSA\Talk\Domain\Event\Events;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;

class LazyEventRepository implements EventRepositoryInterface
{
    /**
     * @var EventRepositoryInterface
     */
    private $source;

    /**
     * @var LazyLoadingValueHolderFactory
     */
    private $valueHolderFactory;

    public function __construct(
        EventRepositoryInterface $source,
        LazyLoadingValueHolderFactory $valueHolderFactory
    ) {
        $this->source = $source;
        $this->valueHolderFactory = $valueHolderFactory;
    }

    public function getEventById(int $id): Event
    {
        /** @var Event $lazyEvent */
        $lazyEvent = $this->valueHolderFactory->createProxy(
            Event::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($id) {
                $initializer = null;
                $wrappedObject = $this->source->getEventById($id);

                return true;
            }
        );

        return $lazyEvent;
    }

    public function findEvents(EventQuery $query): Events
    {
        /** @var Events $lazyEvents */
        $lazyEvents = $this->valueHolderFactory->createProxy(
            Events::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($query) {
                $initializer = null;
                $wrappedObject = $this->source->findEvents($query);

                return true;
            }
        );

        return $lazyEvents;
    }

    public function getBranchIdFromEventId(int $id): ?int
    {
        return $this->source->getBranchIdFromEventId($id);
    }
}
