<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Event;

use EM_Event;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Infrastructure\Event\FindEmEvents;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetBranchId;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetCategoryFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetLocationFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetTimespanFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetUrl;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branch;
use MobilitySoft\TBSA\Talk\Domain\Branch\BranchRepository;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Event\EventCategory;
use MobilitySoft\TBSA\Talk\Domain\Event\EventQuery as TalkEventQuery;
use MobilitySoft\TBSA\Talk\Domain\Event\EventRepository as EventRepositoryInterface;
use MobilitySoft\TBSA\Talk\Domain\Event\Events;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionQuery;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submissions;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;

class EventRepository implements EventRepositoryInterface
{
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var FindEmEvents
     */
    private $findEmEvents;

    /**
     * @var GetBranchId
     */
    private $getBranchId;

    /**
     * @var GetCategoryFromEmEvent
     */
    private $getCategoryFromEmEvent;

    /**
     * @var GetLocationFromEmEvent
     */
    private $getLocationFromEmEvent;

    /**
     * @var GetTimespanFromEmEvent
     */
    private $getTimespanFromEmEvent;

    /**
     * @var GetUrl
     */
    private $getUrl;

    /**
     * @var SubmissionRepository
     */
    private $submissionRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        BranchRepository $branchRepository,
        FindEmEvents $findEmEvents,
        GetBranchId $getBranchId,
        GetCategoryFromEmEvent $getCategoryFromEmEvent,
        GetLocationFromEmEvent $getLocationFromEmEvent,
        GetTimespanFromEmEvent $getTimespanFromEmEvent,
        GetUrl $getUrl
    ) {
        $this->branchRepository = $branchRepository;
        $this->findEmEvents = $findEmEvents;
        $this->getBranchId = $getBranchId;
        $this->getCategoryFromEmEvent = $getCategoryFromEmEvent;
        $this->getLocationFromEmEvent = $getLocationFromEmEvent;
        $this->getTimespanFromEmEvent = $getTimespanFromEmEvent;
        $this->getUrl = $getUrl;
    }

    public function inject(SubmissionRepository $submissionRepository, UserRepository $userRepository): void
    {
        $this->submissionRepository = $submissionRepository;
        $this->userRepository = $userRepository;
    }

    public function getEventById(int $id): ?Event
    {
        $emEvent = em_get_event($id, 'post_id');

        return $emEvent ? $this->fromEmEvent($emEvent) : null;
    }

    public function findEvents(EventQuery $query): Events
    {
        $emEvents = $this->findEmEvents->execute($query);

        $events = array_filter(array_map(function (EM_Event $emEvent): ?Event {
            return $this->fromEmEvent($emEvent);
        }, $emEvents));

        if ($query instanceof TalkEventQuery) {
            $events = array_filter($events, function (Event $event) use ($query) {
                return $query->getStatuses()->match($event->getStatus());
            });
        }

        return new Events(...$events);
    }

    public function getBranchIdFromEventId(int $id): ?int
    {
        $branchId = get_field('tbsa_event_branches', $id)[0] ?? null;
        if (is_int($branchId)) {
            return $branchId;
        }
        return null;
    }

    private function fromEmEvent(EM_Event $emEvent): ?Event
    {
        $eventId = $emEvent->post_id;
        $acfFields = get_fields($eventId);

        $category = $this->getCategory($emEvent);
        if (!$this->isEventOpenToSubmissions($acfFields, $category)) {
            return null;
        }

        $branch = $this->getBranch($acfFields);
        $title = $emEvent->post_title;
        $timespan = $this->getTimespanFromEmEvent->execute($emEvent);
        $url = $this->getUrl->execute($eventId, $acfFields);
        $location = $this->getLocationFromEmEvent->execute($emEvent);
        $submissions = $this->getSubmissions($eventId);

        return new Event($eventId, $title, $timespan, $url, $location, $category, $branch, $submissions);
    }

    private function getCategory(EM_Event $emEvent): ?EventCategory
    {
        $term = $this->getCategoryFromEmEvent->getTerm($emEvent);

        if ($term) {
            return new EventCategory(
                $term->slug,
                $term->name,
                get_field('short_title', $term),
                (bool) get_field('accepts_talk_submissions', "category_{$term->term_id}")
            );
        }

        return null;
    }

    private function isEventOpenToSubmissions($acfFields, ?EventCategory $category): bool
    {
        $opennessToSubmissions = $acfFields['accepts_talk_submissions'] ?? 'inherit';
        if ($opennessToSubmissions === 'inherit' && $category) {
            return $category->isOpenToSubmissions();
        }

        return $opennessToSubmissions === 'open';
    }

    private function getBranch($acfFields): ?Branch
    {
        $id = $this->getBranchId->execute($acfFields);

        return $id ? $this->branchRepository->getBranchById($id) : null;
    }

    private function getSubmissions($eventId): Submissions
    {
        $submissionQuery = new SubmissionQuery();
        $submissionQuery->setEvent($eventId);

        return $this->submissionRepository->findSubmissions($submissionQuery);
    }
}
