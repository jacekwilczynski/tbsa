<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\User;

use MobilitySoft\TBSA\Talk\Domain\Branch\Branches;
use MobilitySoft\TBSA\Talk\Domain\Branch\BranchRepository;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\EntrepreneurRepository;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository as AbstractUserRepository;
use MobilitySoft\TBSA\Talk\Domain\User\Users;

class UserRepository extends AbstractUserRepository
{
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    public function inject(
        BranchRepository $branchRepository,
        EntrepreneurRepository $entrepreneurRepository
    ): void {
        $this->branchRepository = $branchRepository;
        $this->entrepreneurRepository = $entrepreneurRepository;
    }

    public function getCurrentUser(): ?User
    {
        return $this->getById(get_current_user_id());
    }

    public function getById(int $id): ?User
    {
        $wpUser = get_userdata($id);
        if (!$wpUser) {
            return null;
        }

        $acfFields = get_fields("user_$id");

        return new User(
            $id,
            $wpUser->display_name,
            $wpUser->user_email,
            $this->getEntrepreneur($acfFields),
            user_can($id, 'administrator'),
            user_can($id, 'board_member'),
            $this->getMemberBranches($acfFields),
            $this->getManagedBranches($acfFields)
        );
    }

    private function getEntrepreneur($acfFields): ?Entrepreneur
    {
        $entrepreneurId = get_field_safe('entrepreneur', $acfFields);

        return is_int($entrepreneurId)
            ? $this->entrepreneurRepository->findById($entrepreneurId)
            : null;
    }

    private function getMemberBranches($acfFields): Branches
    {
        $memberBranchIds = get_array_field('member_branches', $acfFields) ?? [];

        return $this->branchRepository->getBranchesByIds(...$memberBranchIds);
    }

    private function getManagedBranches($acfFields): Branches
    {
        $managedBranchIds = get_array_field('branches', $acfFields) ?? [];

        return $this->branchRepository->getBranchesByIds(...$managedBranchIds);
    }

    public function getBranchMembers(int $branchId): Users
    {
        $userIds = get_users([
            'meta_key'     => 'member_branches',
            'meta_value'   => sprintf('"%s"', $branchId),
            'meta_compare' => 'LIKE',
            'number'       => -1,
            'fields'       => 'ids',
        ]);

        return new Users(...array_filter(array_map(function (int $id): ?User {
            return $this->getById($id);
        }, $userIds)));
    }
}
