<?php /** @noinspection PhpUnusedParameterInspection */

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\User;

use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository as AbstractUserRepository;
use MobilitySoft\TBSA\Talk\Domain\User\Users;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;

class LazyUserRepository extends AbstractUserRepository
{
    /**
     * @var AbstractUserRepository
     */
    private $source;

    /**
     * @var LazyLoadingValueHolderFactory
     */
    private $valueHolderFactory;

    public function __construct(
        AbstractUserRepository $source,
        LazyLoadingValueHolderFactory $valueHolderFactory
    ) {
        $this->source             = $source;
        $this->valueHolderFactory = $valueHolderFactory;
    }

    public function getById(int $id): ?User
    {
        /** @var User $lazyUser */
        $lazyUser = $this->valueHolderFactory->createProxy(
            User::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($id) {
                $initializer   = null;
                $wrappedObject = $this->source->getById($id);
            }
        );

        return $lazyUser;
    }

    public function getCurrentUser(): ?User
    {
        /** @var User $lazyUser */
        $lazyUser = $this->valueHolderFactory->createProxy(
            User::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) {
                $initializer   = null;
                $wrappedObject = $this->source->getCurrentUser();
            }
        );

        return $lazyUser;
    }

    public function getBranchMembers(int $branchId): Users
    {
        /** @var Users $lazyUsers */
        $lazyUsers = $this->valueHolderFactory->createProxy(
            Users::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($branchId) {
                $initializer   = null;
                $wrappedObject = $this->source->getBranchMembers($branchId);

                return true;
            }
        );

        return $lazyUsers;
    }
}
