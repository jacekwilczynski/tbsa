<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Submission;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;
use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\Talk\Domain\Event\EventRepository;
use MobilitySoft\TBSA\Talk\Domain\Submission\PaginatedSubmissions;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionQuery;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository as SubmissionRepositoryInterface;
use MobilitySoft\TBSA\Talk\Domain\User\User;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use MobilitySoft\TBSA\Talk\Infrastructure\SpeakersAcf;
use MobilitySoft\TBSA\Talk\Infrastructure\TalkAcf;
use ReflectionClass;
use WP_Post;
use WP_Query;
use const MobilitySoft\TBSA\TALK_SUBMISSION_POST_TYPE_SLUG;

class SubmissionRepository implements SubmissionRepositoryInterface
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * @var SpeakersAcf
     */
    private $speakersAcf;

    /**
     * @var TalkAcf
     */
    private $talkAcf;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(SpeakersAcf $speakerAcf, TalkAcf $talkAcf)
    {
        $this->speakersAcf = $speakerAcf;
        $this->talkAcf = $talkAcf;
    }

    public function inject(EventRepository $eventRepository, UserRepository $userRepository): void
    {
        $this->eventRepository = $eventRepository;
        $this->userRepository = $userRepository;
    }

    public function findSubmissions(SubmissionQuery $query): PaginatedSubmissions
    {
        $args = [
            'post_type'      => TALK_SUBMISSION_POST_TYPE_SLUG,
            'posts_per_page' => $query->getLimit() ?: -1,
            'offset'         => $query->getOffset(),
            'fields'         => 'ids',
        ];

        if (trim($query->getPhrase()) !== '') {
            $args['s'] = $query->getPhrase();
        }

        $submitter = $query->getSubmitter();
        if ($submitter !== null) {
            if ($submitter > 0) {
                $args['author'] = $submitter;
            } else {
                $args['author__not_in'] = [$submitter];
            }
        }

        if ($query->getEvent() !== null) {
            $args['meta_query'][] = [
                'key'   => 'event',
                'value' => $query->getEvent(),
            ];
        }

        if ($query->getBranches() !== null) {
            $branchesQuery = ['relation' => 'OR'];

            foreach ($query->getBranches()->getItems() as $branch) {
                $branchesQuery[] = [
                    'key'   => 'branch',
                    'value' => $branch,
                ];
            }

            $args['meta_query'][] = $branchesQuery;
        }

        if ($query->isOnlyForFutureEvents()) {
            $args['meta_query'][] = [
                'key'     => 'event_datetime',
                'value'   => date(DATE_ATOM),
                'compare' => '>',
            ];
        }

        $totalCount = (int) (new WP_Query($args))->found_posts;
        $ids = get_posts($args);

        if (!$ids) {
            return new PaginatedSubmissions($totalCount);
        }

        return new PaginatedSubmissions(
            $totalCount,
            ...array_filter(array_map(function (int $id): ?Submission {
                return $this->findById($id);
            }, $ids))
        );
    }

    public function findById(int $id): ?Submission
    {
        if (get_post_type($id) !== TALK_SUBMISSION_POST_TYPE_SLUG) {
            return null;
        }

        $acfFields = get_fields($id);
        $wpPost = get_post($id);

        $event = $this->eventRepository->getEventById((int) ($acfFields['event']));
        $speakers = $this->speakersAcf->getSpeakers($acfFields);
        $talk = $this->talkAcf->getTalk($acfFields);
        $submitter = $this->getSubmitter($wpPost);
        $status = isset($acfFields['status']) ? SubmissionStatus::fromString($acfFields['status']) : null;
        $rejectionComment = $status === SubmissionStatus::REJECTED() ? $acfFields['comment'] : '';

        if ($event && $talk) {
            return new Submission($id, $event, $speakers, $talk, $submitter, $status, $rejectionComment);
        }

        return null;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getById(int $id): Submission
    {
        $submission = $this->findById($id);
        if ($submission === null) {
            throw new EntityNotFoundException('Submission', $id);
        }
        return $submission;
    }

    private function getSubmitter(WP_Post $wpPost): User
    {
        return $this->userRepository->getById((int) $wpPost->post_author);
    }

    public function save(Submission...$submissions): void
    {
        foreach ($submissions as $submission) {
            $this->saveOne($submission);
        }
    }

    private function saveOne(Submission $submission): void
    {
        $id = wp_insert_post([
            'ID'          => $submission->getId(),
            'post_type'   => TALK_SUBMISSION_POST_TYPE_SLUG,
            'post_status' => 'publish',
            'post_title'  => $submission->getTalk()->getSubject(),
            'post_author' => $submission->getSubmitter()->getId(),
        ]);

        $speakersAcfFields = $this->speakersAcf->getAcfFields($submission->getSpeakers());
        $talkAcfFields = $this->talkAcf->getAcfFields($submission->getTalk());

        update_field('event', $submission->getEvent()->getId(), $id);
        update_field('event_datetime', $submission->getEvent()->getTimespan()->getStart(), $id);
        update_field('speakers', $speakersAcfFields['speakers'], $id);
        update_field('talk', $talkAcfFields['talk'], $id);
        update_field('status', (string) $submission->getStatus(), $id);

        if ($submission->getEvent()->getBranch()) {
            // Save branch id on the submission to simplify querying submissions by branch
            update_post_meta($id, 'branch', $submission->getEvent()->getBranch()->getId());
        }

        $savedValid = is_int($id) && (bool) $this->findById($id);

        if ($savedValid) {
            wp_update_post(['ID' => $id, 'post_status' => 'publish']);
            $this->forceSetId($submission, $id);
        } else {
            throw new Exception('Error in saving submission.');
        }
    }

    private function forceSetId(Submission $submission, int $id): void
    {
        $class = new ReflectionClass($submission);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($submission, $id);
        $property->setAccessible(false);
    }
}
