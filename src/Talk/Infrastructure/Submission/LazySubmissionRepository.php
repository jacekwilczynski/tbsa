<?php /** @noinspection PhpUnusedParameterInspection */

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Submission;

use MobilitySoft\TBSA\Talk\Domain\Submission\PaginatedSubmissions;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionQuery;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository as SubmissionRepositoryInterface;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;

class LazySubmissionRepository implements SubmissionRepositoryInterface
{
    /**
     * @var SubmissionRepositoryInterface
     */
    private $source;

    /**
     * @var LazyLoadingValueHolderFactory
     */
    private $valueHolderFactory;

    public function __construct(
        SubmissionRepositoryInterface $source,
        LazyLoadingValueHolderFactory $valueHolderFactory
    ) {
        $this->source = $source;
        $this->valueHolderFactory = $valueHolderFactory;
    }

    public function findSubmissions(SubmissionQuery $query): PaginatedSubmissions
    {
        /** @var PaginatedSubmissions $lazySubmissions */
        $lazySubmissions = $this->valueHolderFactory->createProxy(
            PaginatedSubmissions::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($query) {
                $initializer = null;
                $wrappedObject = $this->source->findSubmissions($query);

                return true;
            }
        );

        return $lazySubmissions;
    }

    public function findById(int $id): Submission
    {
        /** @var Submission $lazySubmission */
        $lazySubmission = $this->valueHolderFactory->createProxy(
            Submission::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($id) {
                $initializer = null;
                $wrappedObject = $this->source->findById($id);

                return true;
            }
        );

        return $lazySubmission;
    }

    public function getById(int $id): Submission
    {
        return $this->findById($id);
    }

    public function save(Submission... $submissions): void
    {
        $this->source->save(...$submissions);
    }
}
