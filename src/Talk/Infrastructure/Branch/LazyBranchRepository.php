<?php /** @noinspection PhpUnusedParameterInspection */

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Branch;

use MobilitySoft\TBSA\Talk\Domain\Branch\Branch;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branches;
use MobilitySoft\TBSA\Talk\Domain\Branch\BranchRepository as BranchRepositoryInterface;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;

class LazyBranchRepository implements BranchRepositoryInterface
{
    /**
     * @var BranchRepositoryInterface
     */
    private $source;

    /**
     * @var LazyLoadingValueHolderFactory
     */
    private $valueHolderFactory;

    public function __construct(
        BranchRepositoryInterface $source,
        LazyLoadingValueHolderFactory $valueHolderFactory
    ) {
        $this->source             = $source;
        $this->valueHolderFactory = $valueHolderFactory;
    }

    public function getNonVirtualBranches(): Branches
    {
        /** @var Branches $lazyBranches */
        $lazyBranches = $this->valueHolderFactory->createProxy(
            Branches::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) {
                $initializer   = null;
                $wrappedObject = $this->source->getNonVirtualBranches();

                return true;
            }
        );

        return $lazyBranches;
    }

    public function getBranchById(int $id): ?Branch
    {
        /** @var Branch $lazyBranch */
        $lazyBranch = $this->valueHolderFactory->createProxy(
            Branch::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($id) {
                $initializer   = null;
                $wrappedObject = $this->source->getBranchById($id);

                return true;
            }
        );

        return $lazyBranch;
    }

    /**
     * @return int[]
     */
    public function getNonVirtualBranchIds(): array
    {
        return $this->source->getNonVirtualBranchIds();
    }

    public function getBranchesByIds(int ...$ids): Branches
    {
        /** @var Branches $lazyBranches */
        $lazyBranches = $this->valueHolderFactory->createProxy(
            Branches::class,
            function (&$wrappedObject, $_, $__, $___, &$initializer) use ($ids) {
                $initializer   = null;
                $wrappedObject = $this->source->getBranchesByIds(...$ids);

                return true;
            }
        );

        return $lazyBranches;
    }
}
