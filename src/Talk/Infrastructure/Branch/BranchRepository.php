<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Branch;

use MobilitySoft\TBSA\Common\Infrastructure\BranchRepositoryHelper;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branch;
use MobilitySoft\TBSA\Talk\Domain\Branch\Branches;
use MobilitySoft\TBSA\Talk\Domain\Branch\BranchRepository as BranchRepositoryInterface;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use const MobilitySoft\TBSA\BRANCH_POST_TYPE_SLUG;

class BranchRepository implements BranchRepositoryInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function inject(UserRepository $userRepository): void
    {
        $this->userRepository = $userRepository;
    }

    public function getNonVirtualBranches(): Branches
    {
        $ids = $this->getNonVirtualBranchIds();

        return $this->getBranchesByIds(...$ids);
    }

    /**
     * @return int[]
     */
    public function getNonVirtualBranchIds(): array
    {
        return get_posts([
            'post_type'      => BRANCH_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'fields'         => 'ids',
            'meta_query'     => BranchRepositoryHelper::getNonVirtualBranchesMetaQuery(),
        ]);
    }

    public function getBranchesByIds(int ...$ids): Branches
    {
        $branches = array_filter(array_map(function (int $id): ?Branch {
            return $this->getBranchById($id);
        }, $ids));

        return new Branches(...$branches);
    }

    public function getBranchById(int $id): ?Branch
    {
        if (get_post_type($id) === BRANCH_POST_TYPE_SLUG) {
            $post = get_post($id);

            return new Branch(
                $id,
                $post->post_title,
                get_the_permalink($id),
                get_field('town', $id) ?? ''
            );
        }

        return null;
    }
}
