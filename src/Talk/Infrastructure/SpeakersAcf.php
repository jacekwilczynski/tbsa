<?php

namespace MobilitySoft\TBSA\Talk\Infrastructure;

use MobilitySoft\TBSA\Talk\Domain\Speaker\Business;
use MobilitySoft\TBSA\Talk\Domain\Speaker\Contact;
use MobilitySoft\TBSA\Talk\Domain\Speaker\Speaker;
use MobilitySoft\TBSA\Talk\Domain\Speaker\Speakers;
use MobilitySoft\TBSA\View\Medium\MediumFactory;

class SpeakersAcf
{
    /**
     * @var MediumFactory
     */
    private $mediumFactory;

    public function __construct(MediumFactory $mediumFactory)
    {
        $this->mediumFactory = $mediumFactory;
    }

    public function getSpeakers(array $acfFields): Speakers
    {
        return new Speakers(...array_map(function (array $row): Speaker {
            return $this->getSpeaker($row);
        }, get_array_field('speakers', $acfFields)));
    }

    public function getAcfFields(Speakers $speakers): array
    {
        return [
            'speakers' => array_map(function (Speaker $speaker): array {
                return $this->getRow($speaker);
            }, $speakers->getItems()),
        ];
    }

    private function getSpeaker(array $row): Speaker
    {
        return new Speaker(
            $row['name'],
            new Contact($row['email'], $row['phone']),
            $this->mediumFactory->createMedium($row['photo']),
            $row['description'] ?? '',
            new Business(
                $row['business_name'],
                $row['business_website'] ?? ''
            ),
            $row['save_data'] ?? false,
            $row['entrepreneur'] ?? null
        );
    }

    private function getRow(Speaker $speaker): array
    {
        return [
            'name'             => $speaker->getName(),
            'email'            => $speaker->getContact()->getEmail(),
            'phone'            => $speaker->getContact()->getPhone(),
            'photo'            => $this->mediumFactory->serializeMedium($speaker->getPhoto()),
            'description'      => $speaker->getDescription(),
            'business_name'    => $speaker->getBusiness()->getName(),
            'business_website' => $speaker->getBusiness()->getUrl(),
            'save_data'        => $speaker->shouldBeCopiedToEntrepreneur(),
            'entrepreneur'     => $speaker->getEntrepreneurId(),
        ];
    }
}
