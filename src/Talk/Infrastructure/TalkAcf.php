<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure;

use MobilitySoft\TBSA\Talk\Domain\Talk\Benefits;
use MobilitySoft\TBSA\Talk\Domain\Talk\Industry;
use MobilitySoft\TBSA\Talk\Domain\Talk\Talk;
use WP_Term;

class TalkAcf
{
    public function getTalk(array $acfFields): Talk
    {
        $talkInfo = $acfFields['talk'];

        return new Talk(
            $talkInfo['subject'],
            $this->getIndustry($talkInfo),
            $this->getBenefits($talkInfo),
            $talkInfo['description'] ?? ''
        );
    }

    private function getIndustry(array $talkInfo): ?Industry
    {
        /** @var WP_Term $term */
        $term = $talkInfo['industry'];

        return $term ? new Industry($term->term_id, $term->name) : null;
    }

    private function getBenefits(array $talkInfo): Benefits
    {
        return new Benefits(...array_map(function (array $benefit): string {
            return $benefit['text'];
        }, $talkInfo['benefits']));
    }

    public function getAcfFields(Talk $talk): array
    {
        $acfFields = [
            'talk' => [
                'subject' => $talk->getSubject(),
            ],
        ];

        if ($talk->getIndustry()) {
            $acfFields['talk']['industry'] = $talk->getIndustry()->getId();
        }

        if ($talk->getBenefits()) {
            $acfFields['talk']['benefits'] = array_map(function (string $text) {
                return ['text' => $text];
            }, $talk->getBenefits()->getItems());
        }

        if ($talk->getDescription()) {
            $acfFields['talk']['description'] = $talk->getDescription();
        }

        return $acfFields;
    }
}
