<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Talk;

use MobilitySoft\TBSA\Common\Domain\Exceptions\EntityNotFoundException;
use MobilitySoft\TBSA\Talk\Domain\DetectTemplateVsSubmission;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\Domain\Talk\Talk;
use MobilitySoft\TBSA\Talk\Domain\Talk\TalkRepository as TalkRepositoryInterface;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateRepository;
use MobilitySoft\TBSA\Talk\Domain\TemplateVsSubmission;

class TalkRepository implements TalkRepositoryInterface
{
    /**
     * @var SubmissionRepository
     */
    private $submissionRepository;

    /**
     * @var TemplateRepository
     */
    private $templateRepository;

    /**
     * @var DetectTemplateVsSubmission
     */
    private $detectTemplateVsSubmission;

    public function __construct(
        SubmissionRepository $submissionRepository,
        TemplateRepository $templateRepository,
        DetectTemplateVsSubmission $detectTemplateVsSubmission
    ) {
        $this->submissionRepository = $submissionRepository;
        $this->templateRepository = $templateRepository;
        $this->detectTemplateVsSubmission = $detectTemplateVsSubmission;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getById(int $id): Talk
    {
        $talk = $this->findById($id);
        if ($talk === null) {
            throw new EntityNotFoundException('Talk', $id);
        }
        return $talk;
    }

    private function findById(int $id): ?Talk
    {
        $type = $this->detectTemplateVsSubmission->execute($id);

        if ($type === TemplateVsSubmission::TEMPLATE()) {
            $template = $this->templateRepository->findTemplateById($id);
            if ($template) {
                return $template->getTalk();
            }
        }

        if ($type === TemplateVsSubmission::SUBMISSION()) {
            $submission = $this->submissionRepository->getById($id);
            if ($submission) {
                return $submission->getTalk();
            }
        }

        return null;
    }
}
