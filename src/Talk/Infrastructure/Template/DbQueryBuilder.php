<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure\Template;

use MobilitySoft\TBSA\Talk\Domain\Template\TemplateQuery;
use const MobilitySoft\TBSA\TALK_TEMPLATE_POST_TYPE_SLUG;

class DbQueryBuilder
{
    /**
     * @var TemplateQuery
     */
    private $query;

    /**
     * @var bool
     */
    private $onlyCount = false;

    public function __construct(TemplateQuery $query)
    {
        $this->query = $query;
    }

    public function onlyCount(): DbQueryBuilder
    {
        $this->onlyCount = true;
        return $this;
    }

    public function build(): string
    {
        global $wpdb;

        $vars = [TALK_TEMPLATE_POST_TYPE_SLUG];
        $sql = $this->getSqlStart();
        $sql .= " WHERE (p.post_type = %s AND p.post_status IN ('publish', 'draft')";
        $this->applyUserFilter($sql, $vars);
        $this->applyPhraseFilter($sql, $vars);
        $sql .= ') ORDER BY p.ID DESC';
        $this->applyPaginationSql($sql, $vars);

        return $wpdb->prepare($sql, $vars);
    }

    private function applyUserFilter(string &$sql, array &$vars): void
    {
        if ($this->query->getUserId() !== null) {
            $sql .= " AND m.meta_key = %s AND m.meta_value LIKE '%%\"%s\"%%'";
            $vars[] = 'owners';
            $vars[] = $this->query->getUserId();
        }
    }

    private function applyPhraseFilter(string &$sql, array &$vars): void
    {
        if ($this->query->getPhrase() !== '') {
            $sql .= " AND p.post_title LIKE '%%%s%%";
            $vars[] = $this->query->getPhrase();
        }
    }

    private function applyPaginationSql(string &$sql, array &$vars): void
    {
        if ($this->query->getLimit() !== 0) {
            $sql .= ' LIMIT %d';
            $vars[] = $this->query->getLimit();
        }
        if ($this->query->getOffset() !== 0) {
            $sql .= ' OFFSET %d';
            $vars[] = $this->query->getOffset();
        }
    }

    private function getSqlStart(): string
    {
        global $table_prefix;

        $sql = $this->onlyCount ? 'SELECT COUNT(*)' : 'SELECT DISTINCT p.ID';
        $sql .= " FROM {$table_prefix}posts p";
        if ($this->query->getUserId() !== null) {
            $sql .= " INNER JOIN {$table_prefix}postmeta m ON p.ID = m.post_id";
        }

        return $sql;
    }
}
