<?php

namespace MobilitySoft\TBSA\Talk\Infrastructure\Template;

use MobilitySoft\TBSA\Talk\Domain\Template\Template;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateQuery;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateRepository as TemplateRepositoryInterface;
use MobilitySoft\TBSA\Talk\Domain\Template\Templates;
use MobilitySoft\TBSA\Talk\Domain\User\UserRepository;
use MobilitySoft\TBSA\Talk\Domain\User\Users;
use MobilitySoft\TBSA\Talk\Infrastructure\SpeakersAcf;
use MobilitySoft\TBSA\Talk\Infrastructure\TalkAcf;
use const MobilitySoft\TBSA\TALK_SUBMISSION_POST_TYPE_SLUG;
use const MobilitySoft\TBSA\TALK_TEMPLATE_POST_TYPE_SLUG;

class TemplateRepository implements TemplateRepositoryInterface
{
    /**
     * @var SpeakersAcf
     */
    private $speakersAcf;

    /**
     * @var TalkAcf
     */
    private $talkAcf;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        SpeakersAcf $speakersAcf,
        TalkAcf $talkAcf,
        UserRepository $userRepository
    ) {
        $this->speakersAcf = $speakersAcf;
        $this->talkAcf = $talkAcf;
        $this->userRepository = $userRepository;
    }

    public function findTemplates(TemplateQuery $query): Templates
    {
        $items = array_filter(array_map(function (int $id): ?Template {
            return $this->findTemplateById($id);
        }, $this->findIds($query)));

        if ($query->getOffset() !== 0 || $query->getLimit() !== 0) {
            $totalCount = $this->getCountDisregardingPagination($query);
        } else {
            $totalCount = count($items);
        }

        return new Templates($totalCount, ...$items);
    }

    public function countTemplates(TemplateQuery $query): int
    {
        global $wpdb;
        $dbQuery = (new DbQueryBuilder($query))->onlyCount()->build();
        return (int) $wpdb->get_col($dbQuery)[0];
    }

    public function findTemplateById(int $id): ?Template
    {
        if (!$this->hasSupportedPostType($id)) {
            return null;
        }

        $acfFields = get_fields($id);
        if (!$acfFields) {
            return null;
        }

        return new Template(
            $id,
            $this->speakersAcf->getSpeakers($acfFields),
            $this->talkAcf->getTalk($acfFields),
            $this->getTemplateOwners($acfFields)
        );
    }

    private function getTemplateOwners($acfFields): Users
    {
        return $this->userRepository->getByIds(...get_array_field('owners', $acfFields));
    }

    private function hasSupportedPostType(int $id): bool
    {
        return in_array(
            get_post_type($id),
            [
                TALK_TEMPLATE_POST_TYPE_SLUG,
                TALK_SUBMISSION_POST_TYPE_SLUG,
            ],
            true
        );
    }

    public function getLatestCreatedTemplateId(int $userId): ?int
    {
        $query = new TemplateQuery();
        $query->setUserId($userId);
        $query->setLimit(1);
        return $this->findIds($query)[0] ?? null;
    }

    /**
     * @return int[]
     */
    private function findIds(TemplateQuery $query): array
    {
        global $wpdb;

        return array_map(
            function ($row): int { return (int) $row->ID; },
            $wpdb->get_results((new DbQueryBuilder($query))->build())
        );
    }

    private function getCountDisregardingPagination(TemplateQuery $query): int
    {
        global $wpdb;

        $queryWithoutPagination = clone $query;
        $queryWithoutPagination->setOffset(0);
        $queryWithoutPagination->setLimit(0);
        $dbQuery = (new DbQueryBuilder($queryWithoutPagination))
            ->onlyCount()
            ->build();

        return (int) $wpdb->get_col($dbQuery)[0];
    }
}
