<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure;

use InvalidArgumentException;
use MobilitySoft\TBSA\Talk\Domain\DetectTemplateVsSubmission as DetectTalkVsSubmissionInterface;
use MobilitySoft\TBSA\Talk\Domain\TemplateVsSubmission;
use const MobilitySoft\TBSA\TALK_SUBMISSION_POST_TYPE_SLUG;
use const MobilitySoft\TBSA\TALK_TEMPLATE_POST_TYPE_SLUG;

class DetectTemplateVsSubmission implements DetectTalkVsSubmissionInterface
{
    public function execute(?int $talkId): TemplateVsSubmission
    {
        if ($talkId === null) {
            return TemplateVsSubmission::TEMPLATE();
        }

        $postType = get_post_type($talkId);
        if ($postType === TALK_SUBMISSION_POST_TYPE_SLUG) {
            return TemplateVsSubmission::SUBMISSION();
        }
        if ($postType === TALK_TEMPLATE_POST_TYPE_SLUG) {
            return TemplateVsSubmission::TEMPLATE();
        }

        throw new InvalidArgumentException("The provided id $talkId doesn't belong to a talk.");
    }
}
