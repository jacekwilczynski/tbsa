<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure;

use MobilitySoft\TBSA\Talk\Domain\Entrepreneur\Entrepreneur;
use MobilitySoft\TBSA\Talk\Domain\User\User;

class GetSpeakerAcfFieldsMap
{
    public function fromUser(User $user): array
    {
        $fields = [
            'field_5d57c6a768a46' => $this->decodeHtml($user->getDisplayName()),
            'field_5d57cb6629d16' => $user->getEmail(),
        ];

        $entrepreneur = $user->getEntrepreneur();
        if ($entrepreneur !== null) {
            $fromEntrepreneur = $this->fromEntrepreneur($entrepreneur, $user->getEmail());
            $fields = $this->overwriteWhenNewNotEmpty($fields, $fromEntrepreneur);
        }

        return $fields;
    }

    public function fromEntrepreneur(Entrepreneur $entrepreneur, string $fallbackEmail = ''): array
    {
        $business = $entrepreneur->getBusinesses()->getMain();

        $map = [
            'field_5d96559a6319f' => $entrepreneur->getId(),
            'field_5d57c6a768a46' => $this->decodeHtml($entrepreneur->getName()),
            'field_5d57cb6629d16' => $entrepreneur->getContact()->getEmail() ?: $fallbackEmail,
            'field_5d57cb8a29d17' => $entrepreneur->getContact()->getPhone(),
            'field_5d57c6d468a49' => $entrepreneur->getPhoto() ? $entrepreneur->getPhoto()->getId() : null,
            'field_5d57c6e368a4a' => $this->decodeHtml($entrepreneur->getDescription()),
        ];

        if ($business) {
            $map['field_5d57c6bd68a47'] = $this->decodeHtml($business->getName());
            $map['field_5d57c6c968a48'] = $business->getUrl();
        }

        return $map;
    }

    private function decodeHtml(string $value): string
    {
        $value = html_entity_decode($value);
        $value = mb_ereg_replace('[\s\n]*<br\s?/>[\s\n]*', PHP_EOL, $value);
        return $value;
    }

    private function overwriteWhenNewNotEmpty(array $toOverwrite, array $newValues): array
    {
        foreach ($newValues as $key => $newValue) {
            if (!empty($newValue)) {
                $toOverwrite[$key] = $newValue;
            }
        }
        return $toOverwrite;
    }
}
