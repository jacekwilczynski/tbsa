<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\Infrastructure;

use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Speaker\Speakers;
use MobilitySoft\TBSA\Talk\Domain\Submission\Submission;
use MobilitySoft\TBSA\Talk\Domain\Talk\Benefits;
use MobilitySoft\TBSA\Talk\Domain\Talk\Talk;
use MobilitySoft\TBSA\View\Medium\MediumFactory;

class ApplySubmissionToEvent
{
    /**
     * @var MediumFactory
     */
    private $mediumFactory;

    public function __construct(MediumFactory $mediumFactory)
    {
        $this->mediumFactory = $mediumFactory;
    }

    public function execute(Submission $submission): void
    {
        $this->setSimpleFields($submission->getTalk(), $submission->getEvent());
        $this->addSpeakersToEvent($submission->getSpeakers(), $submission->getEvent());
        $this->addBenefitsToEvent($submission->getTalk()->getBenefits(), $submission->getEvent());
    }

    private function setSimpleFields(Talk $talk, Event $event): void
    {
        $emEvent = em_get_event($event->getId(), 'post_id');
        $emEvent->event_name = $talk->getSubject();
        if ($talk->getDescription()) {
            $emEvent->post_content = $talk->getDescription();
        }
        $emEvent->save();
    }

    private function addSpeakersToEvent(Speakers $speakers, Event $event): void
    {
        foreach ($speakers->getItems() as $speaker) {
            add_row(
                'tbsa_speakers',
                [
                    'type'        => 'custom_data',
                    'description' => $speaker->getDescription(),
                    'data'        => [
                        'name'     => $speaker->getName(),
                        'url'      => $speaker->getEntrepreneurId()
                            ? get_the_permalink($speaker->getEntrepreneurId())
                            : '',
                        'imageUrl' => $this->mediumFactory->serializeMedium($speaker->getPhoto()),
                        'business' => [
                            'name' => $speaker->getBusiness()->getName(),
                            'url'  => $speaker->getBusiness()->getUrl(),
                        ],
                    ],
                ],
                $event->getId()
            );
        }
    }

    private function addBenefitsToEvent(Benefits $benefits, Event $event): void
    {
        foreach ($benefits->getItems() as $benefit) {
            add_row(
                'tbsa_event_benefits',
                ['text' => $benefit],
                $event->getId()
            );
        }
    }
}
