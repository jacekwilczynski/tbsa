<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\View;

class ViewEvent
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $details;

    /**
     * @var string
     */
    private $status;

    /**
     * @var bool
     */
    private $hasIndustryConflict;

    public function __construct(
        int $id,
        string $title,
        string $url,
        array $details,
        string $status,
        bool $hasIndustryConflict
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->url = $url;
        $this->details = $details;
        $this->status = $status;
        $this->hasIndustryConflict = $hasIndustryConflict;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDetails(): array
    {
        return $this->details;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function isHasIndustryConflict(): bool
    {
        return $this->hasIndustryConflict;
    }
}
