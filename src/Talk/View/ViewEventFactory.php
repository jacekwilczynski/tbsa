<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Talk\View;

use MobilitySoft\TBSA\Common\View\Anchor;
use MobilitySoft\TBSA\Talk\Domain\Event\Event;
use MobilitySoft\TBSA\Talk\Domain\Event\Events;

class ViewEventFactory
{
    /**
     * @var callable|null
     */
    private $hasIndustryConflict;

    public function __construct(?callable $hasIndustryConflict)
    {
        $this->hasIndustryConflict = $hasIndustryConflict;
    }

    public function convertMultiple(Events $events): array
    {
        return array_map([$this, 'convertOne'], $events->getItems());
    }

    public function convertOne(Event $event): ViewEvent
    {
        return new ViewEvent(
            $event->getId(),
            $event->getTitle(),
            $event->getUrl(),
            $this->getEventDetails($event),
            (string) $event->getStatus(),
            $this->hasIndustryConflict !== null
                ? call_user_func($this->hasIndustryConflict, $event)
                : false
        );
    }

    private function getEventDetails(Event $event): array
    {
        $details = [
            __('Godzina')   => $event->getTimespan()->getStart()->format('H:i'),
            __('Miejsce')   => $event->getLocation()
                ? $event->getLocation()->getTitle() . ', ' . $event->getLocation()->getAddress()
                : null,
            __('Kategoria') => $event->getCategory() && $event->getBranch()
                ? Anchor::presentable(
                    $event->getBranch()->getUrl() . '#event-category-' . $event->getCategory()->getSlug(),
                    $event->getCategory()->getTitle(),
                    true
                )
                : null,
        ];

        return array_filter($details);
    }
}
