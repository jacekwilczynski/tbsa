<?php

namespace MobilitySoft\TBSA\RestRoute;

class EventCategoryRestRoute
{
    private const namespace = '/tbsa/v2';
    private const resource = 'event-categories';

    public function register(callable $getAll): void
    {
        register_rest_route(self::namespace, '/' . self::resource, [
            'methods'  => 'GET',
            'callback' => $getAll,
            'schema'   => array_merge(
                ['$schema' => 'http://json-schema.org/draft-04/schema#'],
                self::getSingleEventCategorySchema()
            ),
        ]);
    }

    public static function getSingleEventCategorySchema(): array
    {
        return [
            'title'      => 'EventCategory',
            'type'       => 'object',
            'properties' => [
                'id'         => [
                    'description' => 'Main, unique numeric identifier of the category.',
                    'type'        => 'integer',
                ],
                'longTitle'  => [
                    'description' => 'Category name in a longer form (e.g. "Śniadania biznesowe").',
                    'type'        => 'string',
                ],
                'shortTitle' => [
                    'description' => 'Category name in a shorter form (e.g. "Śniadania").',
                    'type'        => 'string',
                ],
                'slug'       => [
                    'description' => 'A meaningful unique identifier of the category.',
                    'type'        => 'string',
                ],
                'iconUrl'    => [
                    'description' => 'URL address to the image of an icon associated with the category.',
                    'type'        => 'string',
                ],
            ],
            'required'   => ['id', 'longTitle', 'shortTitle', 'slug'],
        ];
    }
}
