<?php

namespace MobilitySoft\TBSA\RestRoute;

use MobilitySoft\TBSA\ThemeAssets;

final class EventPresentationRoute
{
    private const namespace = '/tbsa/v1';
    private const resource = 'events';

    /**
     * @var ThemeAssets
     */
    private $themeAssets;

    public function __construct(ThemeAssets $themeAssets)
    {
        $this->themeAssets = $themeAssets;
    }

    public function register(callable $callback): void
    {
        add_action('rest_api_init', function () use ($callback): void {
            register_rest_route(self::namespace, '/' . self::resource, [
                'methods'  => 'GET',
                'callback' => $callback,
                'args'     => [
                    'branch'     => [
                        'type' => 'integer',
                    ],
                    'category'   => [
                        'type' => 'integer',
                    ],
                    'date'       => [
                        'type'              => 'string',
                        'validate_callback' => function (string $value) {
                            return preg_match('/^\d{4}-\d{2}-\d{2}$/', $value) === 1;
                        },
                    ],
                    'render_for' => [
                        'type' => 'string',
                        'enum' => [
                            'map',
                            'calendar',
                            'summary',
                            'details',
                        ],
                    ],
                    'id'         => [
                        'type' => 'integer',
                    ],
                ],
            ]);
        });

        $this->themeAssets->addJsVar('tbsa_event', [
            'ajaxRoute' => self::getRoute(),
        ]);
    }

    private static function getRoute(): string
    {
        return home_url('/wp-json' . self::namespace . '/' . self::resource);
    }
}
