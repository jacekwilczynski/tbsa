<?php

namespace MobilitySoft\TBSA\RestRoute;

use DateTime;

class EventRestRoute
{
    private const namespace = '/tbsa/v2';
    private const resource = 'events';

    public $eventsLimit = 200;
    public $daysLimit = 70;

    public function register(callable $getMany, callable $getOne, callable $getDays, callable $getDaysFromMonth): void
    {
        register_rest_route(self::namespace, '/' . self::resource, [
            [
                'methods'  => 'GET',
                'callback' => $getMany,
                'args'     => [
                    'branch'   => [
                        'description' => 'Id of the branch (pl: towarzystwo - 1 per city) hosting the events.',
                        'type'        => 'integer',
                    ],
                    'category' => [
                        'description' => 'Event category id.',
                        'type'        => 'integer',
                    ],
                    'date'     => [
                        'description'       => 'Single date formatted as yyyy-mm-dd (e.g. 2017-02-04)',
                        'type'              => 'string',
                        'validate_callback' => function (string $value) {
                            return preg_match('/^\d{4}-\d{2}-\d{2}$/', $value) === 1;
                        },
                    ],
                    'limit'    => [
                        'description' => 'How many events should be fetched.',
                        'type'        => 'integer',
                        'minimum'     => 1,
                        'maximum'     => $this->eventsLimit,
                        'default'     => $this->eventsLimit,
                    ],
                ],
            ],
            'schema' => function () {
                return [
                    '$schema'     => 'http://json-schema.org/draft-04/schema#',
                    'description' => 'A list of events.',
                    'type'        => 'array',
                    'items'       => self::getSingleEventSchema(),
                    'maxItems'    => $this->eventsLimit,
                ];
            },
        ]);

        register_rest_route(self::namespace, '/' . self::resource . '/(?P<id>[\d]+)', [
            [
                'methods'  => 'GET',
                'callback' => $getOne,
            ],
            'schema' => function () {
                return array_merge(
                    [
                        '$schema'     => 'http://json-schema.org/draft-04/schema#',
                        'description' => 'Single event.',
                    ],
                    self::getSingleEventSchema()
                );
            },
        ]);

        register_rest_route(self::namespace, '/' . self::resource . '/days', [
            [
                'methods'  => 'GET',
                'callback' => $getDays,
                'args'     => [
                    'branch'   => [
                        'description' => 'Id of the branch (pl: towarzystwo - one per city) hosting the events.',
                        'type'        => 'integer',
                    ],
                    'category' => [
                        'description' => 'Event category id.',
                        'type'        => 'integer',
                    ],
                    'start'    => [
                        'description'       => 'Start date formatted as yyyy-mm-dd (e.g. 2017-02-04). Default: today.',
                        'type'              => 'string',
                        'default'           => (new DateTime())->format('Y-m-d'),
                        'validate_callback' => function (string $value) {
                            return preg_match('/^\d{4}-\d{2}-\d{2}$/', $value) === 1;
                        },
                    ],
                    'end'      => [
                        'description'       => 'End date formatted as yyyy-mm-dd (e.g. 2017-02-04).',
                        'type'              => 'string',
                        'validate_callback' => function (string $value) {
                            return preg_match('/^\d{4}-\d{2}-\d{2}$/', $value) === 1;
                        },
                    ],
                    'limit'    => [
                        'description' => 'How many days should be fetched. If the specified dates range is greater, days will be fetched from the start date.',
                        'type'        => 'integer',
                        'minimum'     => 1,
                        'maximum'     => $this->daysLimit,
                        'default'     => $this->daysLimit,
                    ],
                ],
            ],
            'schema' => function () {
                return [
                    '$schema'     => 'http://json-schema.org/draft-04/schema#',
                    'description' => 'Events grouped by days.',
                    'type'        => 'array',
                    'items'       => [
                        'type'       => 'object',
                        'properties' => [
                            'date'   => self::getDateTimeSchema('What day all the events listed under "events" belong to.'),
                            'events' => [
                                'description' => 'A list of events happening on "date".',
                                'type'        => 'array',
                                'items'       => self::getSingleEventSchema(),
                            ],
                        ],
                        'required'   => ['date', 'events'],
                    ],
                    'maxItems'    => $this->daysLimit,
                ];
            },
        ]);

        register_rest_route(self::namespace, '/' . self::resource . '/(?P<year>[\d]+)/(?P<month>[\d]+)' . '/days', [
            [
                'methods'  => 'GET',
                'callback' => $getDaysFromMonth,
                'args'     => [
                    'branch'   => [
                        'description' => 'Id of the branch (pl: towarzystwo - one per city) hosting the events.',
                        'type'        => 'integer',
                    ],
                    'category' => [
                        'description' => 'Event category id.',
                        'type'        => 'integer',
                    ],
                ],
            ],
            'schema' => function () {
                return [
                    '$schema'     => 'http://json-schema.org/draft-04/schema#',
                    'description' => 'Events from a given month grouped by days.',
                    'type'        => 'array',
                    'items'       => [
                        'type'       => 'object',
                        'properties' => [
                            'date'   => self::getDateTimeSchema('What day all the events listed under "events" belong to.'),
                            'events' => [
                                'description' => 'A list of events happening on "date".',
                                'type'        => 'array',
                                'items'       => self::getSingleEventSchema(),
                            ],
                        ],
                        'required'   => ['date', 'events'],
                    ],
                ];
            },
        ]);
    }

    private static function getSingleEventSchema(): array
    {
        return [
            'title'      => 'Event',
            'type'       => 'object',
            'properties' => [
                'id'          => [
                    'description' => 'Unique numeric identifier of the category.',
                    'type'        => 'integer',
                ],
                'title'       => [
                    'description' => 'The title of the event.',
                    'type'        => 'string',
                ],
                'timespan'    => self::getTimespanSchema(),
                'location'    => self::getLocationSchema(),
                'description' => self::getRichTextSchema('Longer description of the event.'),
                'url'         => [
                    'description' => 'The URL of the web page dedicated to the event.',
                    'type'        => 'string',
                ],
                'price'       => [
                    'description' => 'A textual representation of the event price (or the range of prices, if the event has multiple ticket types).',
                    'type'        => 'string',
                ],
                'category'    => EventCategoryRestRoute::getSingleEventCategorySchema(),
                'speakers'    => self::getSpeakersSchema(),
                'images'      => self::getImagesSchema(),
            ],
            'required'   => ['id', 'title', 'timespan', 'url', 'speakers', 'images'],
        ];
    }

    private static function getTimespanSchema(): array
    {
        return [
            'description' => 'Information about the time of the event.',
            'type'        => 'object',
            'properties'  => [
                'start'      => self::getDateTimeSchema('Time when the event begins.'),
                'end'        => self::getDateTimeSchema('Time when the event ends.'),
                'isWholeDay' => [
                    'type'        => 'boolean',
                    'description' => 'Should the time component of the date/time be discarded.',
                ],
            ],
            'required'    => ['start', 'isWholeDay'],
        ];
    }

    private static function getDateTimeSchema(?string $description = null): array
    {
        $schema = [
            'type'       => 'object',
            'properties' => [
                'text'      => [
                    'description' => 'A textual representation of the date/time.',
                    'type'        => 'string',
                ],
                'timestamp' => [
                    'description' => 'Unix timestamp.',
                    'type'        => 'number',
                ],
            ],
            'required'   => ['text', 'timestamp'],
        ];

        if ($description) {
            $schema['description'] = $description;
        }

        return $schema;
    }

    private static function getLocationSchema(): array
    {
        return [
            'type'       => 'object',
            'properties' => [
                'name'    => [
                    'description' => "The name of the place, e.g. the coffee shop's name.",
                    'type'        => 'string',
                ],
                'town'    => [
                    'description' => 'Text name of the town/city/locality.',
                    'type'        => 'string',
                ],
                'address' => [
                    'description' => 'Full address to wherever the event takes place.',
                    'type'        => 'string',
                ],
            ],
            'required'   => ['name', 'town', 'address'],
        ];
    }

    private static function getRichTextSchema(string $description): array
    {
        return [
            'description' => $description,
            'type'        => 'object',
            'properties'  => [
                'plain' => [
                    'description' => 'Plain text version.',
                    'type'        => 'string',
                ],
                'html'  => [
                    'description' => 'HTML version',
                    'type'        => 'string',
                ],
            ],
            'required'    => ['plain'],
        ];
    }

    private static function getSpeakersSchema(): array
    {
        return [
            'description' => 'A list of people who will be the featured speakers at the event.',
            'type'        => 'array',
            'items'       => [
                'type'       => 'object',
                'properties' => [
                    'name'        => [
                        'description' => "The speaker's full name.",
                        'type'        => 'string',
                    ],
                    'imageUrl'    => [
                        'description' => "The URL of the speakers's photo.",
                        'type'        => 'string',
                    ],
                    'description' => self::getRichTextSchema('A few words about the speaker.'),
                    'business'    => [
                        'description' => "The speaker's firm.",
                        'type'        => 'object',
                        'properties'  => [
                            'name' => [
                                'description' => 'The name of the business.',
                                'type'        => 'string',
                            ],
                            'url'  => [
                                'description' => "The URL of the business's web site.",
                                'type'        => 'string',
                            ],
                        ],
                        'required'    => ['name'],
                    ],
                ],
                'required'   => ['name'],
            ],
        ];
    }

    private static function getImagesSchema(): array
    {
        return [
            'description' => 'Images associated with the event.',
            'type'        => 'object',
            'properties'  => [
                'thumbnail' => self::getImageScema('Thumbnail image.'),
                'cover'     => self::getImageScema("Image displayed at the top of the event's page."),
                'gallery'   => [
                    'description' => 'Gallery of various images, typically photos from similar events.',
                    'type'        => 'array',
                    'items'       => self::getImageScema(),
                ],
            ],
        ];
    }

    private static function getImageScema(?string $description = null): array
    {
        $schema = [
            'type'       => 'object',
            'properties' => [
                'thumbnail' => [
                    'description' => 'The URL of a 150x150 version of the image.',
                    'type'        => 'string',
                ],
                'small'     => [
                    'description' => 'The URL of a max-300px-wide version of the image.',
                    'type'        => 'string',
                ],
                'medium'    => [
                    'description' => 'The URL of a max-768px-wide version of the image.',
                    'type'        => 'string',
                ],
                'large'     => [
                    'description' => 'The URL of a max-1024-wide version of the image',
                    'type'        => 'string',
                ],
                'full'      => [
                    'description' => 'The URL of the full-size version of the image.',
                    'type'        => 'string',
                ],
            ],
        ];

        if ($description) {
            $schema['description'] = $description;
        }

        return $schema;
    }
}
