<?php

namespace MobilitySoft\TBSA\RestRoute;

final class BookingFormRestRoute
{
    private const namespace = '/tbsa/v1';
    private const resource = 'booking-form';

    public function register(callable $callback): void
    {
        add_action('rest_api_init', function () use ($callback): void {
            register_rest_route(self::namespace, '/' . self::resource, [
                'methods'  => 'GET',
                'callback' => $callback,
                'args'     => [
                    'event_id' => [
                        'type' => 'integer',
                    ],
                ],
            ]);
        });
    }
}
