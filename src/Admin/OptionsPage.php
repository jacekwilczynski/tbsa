<?php

namespace MobilitySoft\TBSA\Admin;

final class OptionsPage
{
    public static function configure(): void
    {
        add_action('acf/init', function (): void {
            acf_add_options_page();
            acf_add_options_sub_page('Nagłówek');
            acf_add_options_sub_page('Strona główna');
            acf_add_options_sub_page('O towarzystwach');
            acf_add_options_sub_page('Wyszukiwarka firm');
            acf_add_options_sub_page('O towarzystwie');
            acf_add_options_sub_page('Merkuryusz');
            acf_add_options_sub_page('Stopka');
            acf_add_options_sub_page('Widok po złożeniu zamówienia');
            acf_add_options_sub_page('Kody do wklejenia na stronę');
            acf_add_options_sub_page('Zgłaszanie prelegentów');
            acf_add_options_sub_page('Inne');
        });
    }
}
