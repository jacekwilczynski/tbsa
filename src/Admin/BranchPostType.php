<?php

namespace MobilitySoft\TBSA\Admin;

use MobilitySoft\TBSA\Repository\BranchRepository;

final class BranchPostType
{
    public static function configure(
        string $postType,
        int $position,
        BranchRepository $branchRepository
    ): void {
        add_action('init', static function () use ($postType, $position): void {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Towarzystwa',
                    'singular_name' => 'Towarzystwo',
                    'add_new'       => 'Dodaj nowe',
                    'add_new_item'  => 'Dodaj nowe towarzystwo',
                    'edit_item'     => 'Edytuj towarzystwo',
                    'new_item'      => 'Nowy towarzystwo',
                    'view_item'     => 'Zobacz towarzystwo',
                    'view_items'    => 'Zobacz towarzystwa',
                    'search_items'  => 'Szukaj w towarzystwach',
                    'not_found'     => 'Nie znaleziono towarzystwa',
                    'all_items'     => 'Wszystkie towarzystwa',
                ],
                'public'          => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-location-alt',
                'supports'        => ['title', 'author'],
                'rewrite'         => ['slug' => 'towarzystwa'],
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);
        });

        add_filter(
            "manage_{$postType}_posts_columns",
            static function (array $columns): array {
                unset($columns['date']);
                $columns['town'] = 'Miejscowość';
                $columns['voivodeship'] = 'Województwo';
                return $columns;
            },
            10,
            1
        );

        add_action(
            "manage_{$postType}_posts_custom_column",
            static function (string $column, string $id) use ($branchRepository): void {
                $branch = $branchRepository->getById($id);
                if ($branch === null) {
                    return;
                }
                $location = $branch->getLocation();
                if ($location === null) {
                    return;
                }
                if ($column === 'town') {
                    echo $location->getTown();
                }
                if ($column === 'voivodeship') {
                    echo $location->getVoivodeship();
                }
            },
            10,
            2
        );

        add_filter(
            "manage_edit-{$postType}_sortable_columns",
            static function (array $columns): array {
                $columns['town'] = 'town';
                $columns['voivodeship'] = 'voivodeship';
                return $columns;
            }
        );
    }
}
