<?php

namespace MobilitySoft\TBSA\Admin;

use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use Psr\Container\ContainerInterface;

class TalkSubmissionPostType
{
    private const COLUMN_TOWN = 'town';
    private const COLUMN_EVENT_DATE = 'event_date';
    private const CUSTOM_COLUMNS = [self::COLUMN_TOWN, self::COLUMN_EVENT_DATE];

    public static function configure(
        string $postType,
        int $position,
        ContainerInterface $container
    ): void {
        add_action('init', function () use ($postType, $position) {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Wnioski o prezentacje',
                    'singular_name' => 'Wniosek o prezentację',
                    'add_new'       => 'Dodaj nowy',
                    'add_new_item'  => 'Dodaj nowy wniosek',
                    'edit_item'     => 'Edytuj wniosek',
                    'new_item'      => 'Nowy wniosek',
                    'view_item'     => 'Zobacz wniosek',
                    'view_items'    => 'Zobacz wnioski',
                    'search_items'  => 'Szukaj we wnioskach',
                    'not_found'     => 'Nie znaleziono wniosków',
                    'all_items'     => 'Wszystkie wnioski',
                ],
                'show_ui'         => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-media-interactive',
                'supports'        => ['title', 'author', 'comments'],
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);
        });

        add_filter('wp_insert_post_data', function (array $data) use ($postType): array {
            if ($data['post_type'] === $postType) {
                $data['comment_status'] = 'open';
            }
            return $data;
        });

        self::addCustomColumnsToTableInAdmin($postType, $container);
    }

    private static function addCustomColumnsToTableInAdmin(
        string $postType,
        ContainerInterface $container
    ): void {
        add_filter("manage_{$postType}_posts_columns", function (array $columns): array {
            unset($columns['date']);
            $columns[self::COLUMN_TOWN] = 'Miasto';
            $columns[self::COLUMN_EVENT_DATE] = 'Data wydarzenia';
            return $columns;
        }, 10, 1);

        add_action(
            "manage_{$postType}_posts_custom_column",
            function (string $column, string $id) use ($container): void {
                $isCustomColumn = in_array($column, self::CUSTOM_COLUMNS, true);
                if (!$isCustomColumn) {
                    return;
                }

                $submissionRepository = $container->get(SubmissionRepository::class);
                $submission = $submissionRepository->findById($id);

                if ($submission === null) {
                    echo '-błąd-';
                    return;
                }

                switch ($column) {
                    case self::COLUMN_TOWN:
                        echo $submission->getEvent()->getBranch()->getTown();
                        return;
                    case self::COLUMN_EVENT_DATE:
                        echo $submission->getEvent()->getTimespan()->getStart()->format('Y-m-d');
                        return;
                }
            },
            10,
            2
        );

        add_filter("manage_edit-{$postType}_sortable_columns", function (array $columns) {
            $columns[self::COLUMN_TOWN] = self::COLUMN_TOWN;
            $columns[self::COLUMN_EVENT_DATE] = self::COLUMN_EVENT_DATE;
            return $columns;
        });
    }
}
