<?php

namespace MobilitySoft\TBSA\Admin;

use MobilitySoft\TBSA\Repository\GroupRepository;

final class GroupPostType
{
    public static function configure(
        string $postType,
        int $position,
        GroupRepository $groupRepository
    ): void {
        add_action('init', static function () use ($position): void {
            register_post_type('networking_group', [
                'labels'          => [
                    'name'          => 'Grupy networkingowe',
                    'singular_name' => 'Grupa networkingowa',
                    'add_new'       => 'Dodaj nową',
                    'add_new_item'  => 'Dodaj nową grupę networkingową',
                    'edit_item'     => 'Edytuj grupę networkingową',
                    'new_item'      => 'Nowy grupę networkingową',
                    'view_item'     => 'Zobacz grupę networkingową',
                    'view_items'    => 'Zobacz grupy networkingowe',
                    'search_items'  => 'Szukaj w grupach networkingowych',
                    'not_found'     => 'Nie znaleziono grupy networkingowe',
                    'all_items'     => 'Wszystkie grupy networkingowe',
                ],
                'show_ui'         => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-groups',
                'supports'        => ['title', 'author'],
                'map_meta_cap'    => true,
                'capability_type' => 'networking_group',
            ]);
        });

        add_filter(
            "manage_{$postType}_posts_columns",
            static function (array $columns): array {
                unset($columns['date']);
                $columns['branch'] = 'Towarzystwo';
                $columns['town'] = 'Miasto';
                $columns['voivodeship'] = 'Województwo';
                return $columns;
            },
            10,
            1
        );

        add_action(
            "manage_{$postType}_posts_custom_column",
            static function (string $column, string $id) use ($groupRepository): void {
                $group = $groupRepository->getById($id);
                $branch = $group->getBranch();

                if ($branch === null) {
                    return;
                }
                if ($column === 'branch') {
                    $href = get_edit_post_link($branch->getId());
                    echo '<a href="' . $href . '">' . $branch->getName() . '</a>';
                }

                $location = $branch->getLocation();
                if ($location === null) {
                    return;
                }
                if ($column === 'town') {
                    echo $location->getTown();
                }
                if ($column === 'voivodeship') {
                    echo $location->getVoivodeship();
                }
            },
            10,
            2
        );

        add_filter(
            "manage_edit-{$postType}_sortable_columns",
            static function (array $columns): array {
                $columns['branch'] = 'branch';
                $columns['town'] = 'town';
                $columns['voivodeship'] = 'voivodeship';
                return $columns;
            }
        );
    }
}
