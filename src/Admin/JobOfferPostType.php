<?php

namespace MobilitySoft\TBSA\Admin;

final class JobOfferPostType
{
    public static function configure(string $postType, int $position): void
    {
        add_action('init', function () use ($postType, $position): void {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Oferty pracy',
                    'singular_name' => 'Oferta pracy',
                    'add_new'       => 'Dodaj nową',
                    'add_new_item'  => 'Dodaj nową ofertę pracy',
                    'edit_item'     => 'Edytuj ofertę pracy',
                    'new_item'      => 'Nowy ofertę pracy',
                    'view_item'     => 'Zobacz ofertę pracy',
                    'view_items'    => 'Zobacz oferty pracy',
                    'search_items'  => 'Szukaj w ofertach pracy',
                    'not_found'     => 'Nie znaleziono oferty pracy',
                    'all_items'     => 'Wszystkie oferty pracy',
                ],
                'public'          => true,
                'has_archive'     => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-money',
                'supports'        => ['title', 'thumbnail', 'editor', 'excerpt'],
                'rewrite'         => ['slug' => 'praca'],
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);
        });
    }
}
