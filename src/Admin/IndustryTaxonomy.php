<?php

namespace MobilitySoft\TBSA\Admin;

final class IndustryTaxonomy
{
    /**
     * @param string $slug
     * @param string|string[] $postTypes
     */
    public static function configure(string $slug, $postTypes): void
    {
        add_action('init', function () use ($slug, $postTypes): void {
            register_taxonomy($slug, $postTypes, [
                'labels'       => [
                    'name'                       => 'Branże',
                    'singular_name'              => 'Branża',
                    'all_items'                  => 'Wszystkie branże',
                    'edit_item'                  => 'Edytuj branżę',
                    'view_item'                  => 'Zobacz branżę',
                    'update_item'                => 'Modyfikuj branżę',
                    'add_new_item'               => 'Dodaj nową branżę',
                    'new_item_name'              => 'Nazwa nowej branży',
                    'search_items'               => 'Szukaj w branżach',
                    'popular_items'              => 'Popularne branże',
                    'separate_items_with_commas' => 'Oddzielaj branże przecinkiem',
                    'add_or_remove_items'        => 'Dodaj lub usuń branże',
                    'choose_from_most_used'      => 'Wybierz z najczęściej używanych',
                    'not_found'                  => 'Nie znaleziono branż',
                    'back_to_items'              => 'Powrót do branż',
                ],
                'public'       => false,
                'show_ui'      => true,
                'rewrite'      => [
                    'slug' => 'branze',
                ],
                'capabilities' => [
                    'manage_terms' => 'manage_industries',
                    'edit_terms'   => 'edit_industries',
                    'delete_terms' => 'delete_industries',
                    'assign_terms' => 'assign_industries',
                ],
            ]);
        });
    }
}
