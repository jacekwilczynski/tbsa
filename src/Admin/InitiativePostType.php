<?php

namespace MobilitySoft\TBSA\Admin;

final class InitiativePostType
{
    public static function configure(string $postType, int $position): void
    {
        add_action('init', function () use ($postType, $position): void {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Inicjatywy',
                    'singular_name' => 'Inicjatywa',
                    'add_new'       => 'Dodaj nową',
                    'add_new_item'  => 'Dodaj nową inicjatywę',
                    'edit_item'     => 'Edytuj inicjatywę',
                    'new_item'      => 'Nowy inicjatywę',
                    'view_item'     => 'Zobacz inicjatywę',
                    'view_items'    => 'Zobacz inicjatywy',
                    'search_items'  => 'Szukaj w inicjatywach',
                    'not_found'     => 'Nie znaleziono inicjatyw',
                    'all_items'     => 'Wszystkie inicjatywy',
                ],
                'show_ui'         => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-lightbulb',
                'supports'        => ['title', 'thumbnail'],
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);

            register_taxonomy('initiative_category', 'initiative', [
                'labels'  => [
                    'name'                       => 'Kategorie',
                    'singular_name'              => 'Kategoria',
                    'all_items'                  => 'Wszystkie kategorie',
                    'edit_item'                  => 'Edytuj kategorię',
                    'view_item'                  => 'Zobacz kategorię',
                    'update_item'                => 'Modyfikuj kategorię',
                    'add_new_item'               => 'Dodaj nową kategorię',
                    'new_item_name'              => 'Nazwa nowego kategorie',
                    'search_items'               => 'Szukaj w kategoriach',
                    'popular_items'              => 'Popularne kategorie',
                    'separate_items_with_commas' => 'Oddzielaj kategorie przecinkiem',
                    'add_or_remove_items'        => 'Dodaj lub usuń kategorie',
                    'choose_from_most_used'      => 'Wybierz z najczęściej używanych',
                    'not_found'                  => 'Nie znaleziono kategorii',
                    'back_to_items'              => 'Powrót do kategorii',
                ],
                'show_ui' => true,
            ]);
        });
    }

}
