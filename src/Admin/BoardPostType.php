<?php

namespace MobilitySoft\TBSA\Admin;

final class BoardPostType
{
    public static function configure(string $postType, int $position): void
    {
        add_action('init', function () use ($postType, $position): void {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Zarządy',
                    'singular_name' => 'Zarząd',
                    'add_new'       => 'Dodaj nowy',
                    'add_new_item'  => 'Dodaj nowy zarząd',
                    'edit_item'     => 'Edytuj zarząd',
                    'new_item'      => 'Nowy zarząd',
                    'view_item'     => 'Zobacz zarząd',
                    'view_items'    => 'Zobacz zarządy',
                    'search_items'  => 'Szukaj w zarządach',
                    'not_found'     => 'Nie znaleziono zarządów',
                    'all_items'     => 'Wszystkie zarządy',
                ],
                'show_ui'         => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-awards',
                'supports'        => ['title', 'author'],
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);

            register_taxonomy('range', $postType, [
                'labels'       => [
                    'name'                       => 'Zasięg',
                    'singular_name'              => 'Zasięgi',
                    'all_items'                  => 'Wszystkie zasięgi',
                    'edit_item'                  => 'Edytuj zasięg',
                    'view_item'                  => 'Zobacz zasięg',
                    'update_item'                => 'Modyfikuj zasięg',
                    'add_new_item'               => 'Dodaj nowy zasięg',
                    'new_item_name'              => 'Nazwa nowego zasięgu',
                    'search_items'               => 'Szukaj w branżach',
                    'popular_items'              => 'Popularne zasięgi',
                    'separate_items_with_commas' => 'Oddzielaj zasięgi przecinkiem',
                    'add_or_remove_items'        => 'Dodaj lub usuń zasięgi',
                    'choose_from_most_used'      => 'Wybierz z najczęściej używanych',
                    'not_found'                  => 'Nie znaleziono zasięgów',
                    'back_to_items'              => 'Powrót do zasięgów',
                ],
                'public'       => false,
                'show_ui'      => true,
                'show_in_menu' => false,
                'rewrite'      => ['slug' => 'zasiegi'],
            ]);
        });
    }
}
