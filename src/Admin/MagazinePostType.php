<?php

namespace MobilitySoft\TBSA\Admin;

final class MagazinePostType
{
    public static function configure(string $postType, int $position): void
    {
        add_action('init', function () use ($postType, $position): void {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Merkuryusz',
                    'singular_name' => 'Numer',
                    'add_new'       => 'Dodaj nowy',
                    'add_new_item'  => 'Dodaj nowy numer',
                    'edit_item'     => 'Edytuj numer',
                    'new_item'      => 'Nowy numer',
                    'view_item'     => 'Zobacz numer',
                    'view_items'    => 'Zobacz numery',
                    'search_items'  => 'Szukaj w numerach czasopisma',
                    'not_found'     => 'Nie znaleziono numeru czasopisma',
                    'all_items'     => 'Wszystkie numery',
                ],
                'public'          => false,
                'show_ui'         => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-book',
                'supports'        => false,
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);
        });

        add_action('acf/save_post', function ($id) {
            if (get_post_type() === 'magazine_issue') {
                $number = get_field('number');
                wp_update_post([
                    'ID'         => $id,
                    'post_name'  => $number,
                    'post_title' => $number,
                ]);
                set_post_thumbnail(get_the_ID(), get_field('cover')['ID']);
            }
        });
    }
}
