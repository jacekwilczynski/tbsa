<?php

namespace MobilitySoft\TBSA\Admin;

final class LocationTaxonomy
{
    /**
     * @param string $slug
     * @param string|string[] $postTypes
     */
    public static function configure(string $slug, $postTypes): void
    {
        add_action('init', function () use ($slug, $postTypes): void {
            register_taxonomy($slug, $postTypes, [
                'labels'  => [
                    'name'                       => 'Miasta',
                    'singular_name'              => 'Miasto',
                    'all_items'                  => 'Wszystkie miasta',
                    'edit_item'                  => 'Edytuj miasto',
                    'view_item'                  => 'Zobacz miasto',
                    'update_item'                => 'Modyfikuj miasto',
                    'add_new_item'               => 'Dodaj nową miasto',
                    'new_item_name'              => 'Nazwa nowego miasta',
                    'search_items'               => 'Szukaj w miastach',
                    'popular_items'              => 'Popularne miasta',
                    'separate_items_with_commas' => 'Oddzielaj miasta przecinkiem',
                    'add_or_remove_items'        => 'Dodaj lub usuń miasta',
                    'choose_from_most_used'      => 'Wybierz z najczęściej używanych',
                    'not_found'                  => 'Nie znaleziono miast',
                    'back_to_items'              => 'Powrót do miast',
                ],
                'public'  => false,
                'show_ui' => true,
                'rewrite' => [
                    'slug' => 'miasta'
                ],
                'capabilities' => [
                    'manage_terms' => 'manage_locations',
                    'edit_terms'   => 'edit_locations',
                    'delete_terms' => 'delete_locations',
                    'assign_terms' => 'assign_locations',
                ],
            ]);
        });
    }
}
