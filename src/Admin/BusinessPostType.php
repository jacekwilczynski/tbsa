<?php

namespace MobilitySoft\TBSA\Admin;

final class BusinessPostType
{
    public static function configure(string $postType, int $position): void
    {
        add_action('init', function () use ($postType, $position): void {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Firmy',
                    'singular_name' => 'Firma',
                    'add_new'       => 'Dodaj nową',
                    'add_new_item'  => 'Dodaj nową firmę',
                    'edit_item'     => 'Edytuj firmę',
                    'new_item'      => 'Nowy firmę',
                    'view_item'     => 'Zobacz firmę',
                    'view_items'    => 'Zobacz firmy',
                    'search_items'  => 'Szukaj w firmach',
                    'not_found'     => 'Nie znaleziono firmy',
                    'all_items'     => 'Wszystkie firmy',
                ],
                'show_ui'         => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-building',
                'supports'        => ['title', 'thumbnail', 'author'],
                'rewrite'         => ['slug' => 'firmy'],
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);
        });
    }
}
