<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Admin;

class TalkTemplatePostType
{
    public function configure(string $postType, int $position): void
    {
        add_action('init', function () use ($postType, $position) {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => __('Prezentacje'),
                    'singular_name' => __('Prezentacja'),
                    'add_new'       => __('Dodaj nową'),
                    'add_new_item'  => __('Dodaj nową prezentację'),
                    'edit_item'     => __('Edytuj prezentację'),
                    'new_item'      => __('Nowa prezentacja'),
                    'view_item'     => __('Zobacz prezentację'),
                    'view_items'    => __('Zobacz prezentacje'),
                    'search_items'  => __('Szukaj w prezentacjach'),
                    'not_found'     => __('Nie znaleziono prezentacji'),
                    'all_items'     => __('Wszystkie prezentacje'),
                ],
                'menu_position'   => $position,
                'show_ui'         => true,
                'supports'        => false,
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);

            add_action('acf/save_post', function ($postId) use ($postType) {
                if (get_post_type($postId) === $postType) {
                    $subject = get_field('talk', $postId)['subject'];
                    wp_update_post([
                        'ID'         => $postId,
                        'post_name'  => sanitize_title($subject),
                        'post_title' => $subject,
                    ]);
                }
            });
        });
    }
}
