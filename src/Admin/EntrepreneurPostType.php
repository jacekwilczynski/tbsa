<?php

namespace MobilitySoft\TBSA\Admin;

use WP_Query;

final class EntrepreneurPostType
{
    public static function configure(string $postType, int $position): void
    {
        add_action('init', function () use ($postType, $position): void {
            register_post_type($postType, [
                'labels'          => [
                    'name'          => 'Przedsiębiorcy',
                    'singular_name' => 'Przedsiębiorca',
                    'add_new'       => 'Dodaj nowego',
                    'add_new_item'  => 'Dodaj nowego przedsiębiorcę',
                    'edit_item'     => 'Edytuj przedsiębiorcę',
                    'new_item'      => 'Nowy przedsiębiorca',
                    'view_item'     => 'Zobacz przedsiębiorcę',
                    'view_items'    => 'Zobacz przedsiębiorców',
                    'search_items'  => 'Szukaj w przedsiębiorcach',
                    'not_found'     => 'Nie znaleziono przedsiębiorców',
                    'all_items'     => 'Wszyscy przedsiębiorcy',
                ],
                'public'          => true,
                'menu_position'   => $position,
                'menu_icon'       => 'dashicons-businessman',
                'supports'        => ['title', 'author'],
                'rewrite'         => ['slug' => 'przedsiebiorcy'],
                'map_meta_cap'    => true,
                'capability_type' => $postType,
            ]);
        });

        add_action('acf/save_post', function ($id) use ($postType) {
            if (get_post_type($id) === $postType) {
                set_post_thumbnail($id, get_field('photo'));
            }
        });

        add_filter('acf/pre_render_fields', function ($fields, $post) use ($postType) {
            if (get_post_type($post) === $postType) {
                foreach ($fields as &$field) {
                    if ($field['key'] === '_post_title') {
                        $field['label'] = __('Imię i nazwisko');
                    }

                    if ($field['name'] === 'external_links') {
                        $field['label'] = __('Linki');
                    }
                }
            }

            return $fields;
        }, 10, 2);

        add_action('wp_ajax_migrate_entrepreneur_photos_to_acf', function () use ($postType) {
            if (current_user_can('tech')) {
                $query = new WP_Query([
                    'post_type'      => $postType,
                    'posts_per_page' => -1,
                ]);

                $count = 0;

                while ($query->have_posts()) {
                    $query->the_post();
                    update_field('photo', get_post_thumbnail_id());
                    $count++;
                }

                wp_reset_query();
                echo "Successfully migrated $count entrepreneur photos.";
                wp_die();
            }

            http_response_code(403);
            echo '403 Forbidden';
            wp_die();
        });
    }
}
