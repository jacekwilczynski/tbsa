<?php

namespace MobilitySoft\TBSA\Infrastructure\Booking;

use MobilitySoft\TBSA\Domain\Booking\Booking;
use MobilitySoft\TBSA\Domain\Booking\BookingBuyer;
use MobilitySoft\TBSA\Domain\Booking\BookingRepository;
use Throwable;

class AcfBookingRepository implements BookingRepository
{
    private $fieldSlug = 'tbsa_bookings';

    /**
     * @param Booking $booking
     */
    public function add(Booking $booking): void
    {
        $eventId  = $booking->getEventId();
        $bookings = $this->getByEventId($eventId);

        foreach ($bookings as $index => $existingBooking) {
            if (
                $existingBooking->getOrderId() === $booking->getOrderId() &&
                $existingBooking->getTicketId() === $booking->getTicketId()
            ) {
                $booking->addPlaces($existingBooking->getNumberOfPlaces());
                update_row($this->fieldSlug, $index + 1, $this->serializeBooking($booking), $eventId);

                return;
            }
        }

        add_row($this->fieldSlug, $this->serializeBooking($booking), $eventId);
    }

    /**
     * @param int $eventId
     *
     * @return Booking[]
     */
    public function getByEventId(int $eventId): array
    {
        $field = get_field($this->fieldSlug, $eventId);

        if ( ! is_array($field)) {
            return [];
        }

        return array_filter(array_map(function ($bookingData) use ($eventId) {
            try {
                return $this->deserializeBooking($eventId, $bookingData);
            } catch (Throwable $e) {
                error_log($e);

                return null;
            }
        }, $field), function (?Booking $booking) {
            return $booking;
        });
    }

    /**
     * @param int $eventId
     * @param array $bookingData
     *
     * @return Booking
     */
    private function deserializeBooking(int $eventId, array $bookingData): Booking
    {
        $buyer = new BookingBuyer(
            $bookingData['name'],
            $bookingData['email'],
            $bookingData['phone'],
            $bookingData['user']
        );

        return new Booking(
            $eventId,
            $bookingData['number_of_places'],
            $buyer,
            $bookingData['order'],
            sanitize_title($bookingData['ticket']) ?? ''
        );
    }

    /**
     * @param Booking $booking
     *
     * @return array
     */
    private function serializeBooking(Booking $booking): array
    {
        $buyer = $booking->getBuyer();

        return [
            'number_of_places' => $booking->getNumberOfPlaces(),
            'name'             => $buyer->getName(),
            'email'            => $buyer->getEmail(),
            'phone'            => $buyer->getPhone(),
            'user'             => $buyer->getUserId(),
            'order'            => $booking->getOrderId(),
            'ticket'           => $booking->getTicketId(),
        ];
    }
}
