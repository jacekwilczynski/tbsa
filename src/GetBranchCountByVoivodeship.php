<?php

namespace MobilitySoft\TBSA;

use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Interfaces\StringToIntFunction;

final class GetBranchCountByVoivodeship implements StringToIntFunction
{
    /**
     * @var BranchInterface[]
     */
    private $branches;

    /**
     * @var array
     */
    private $map;

    /**
     * GetBranchCountByVoideship constructor.
     *
     * @param array $branches
     */
    public function __construct(array $branches)
    {
        $this->branches = $branches;
    }

    /**
     * @param string $input
     *
     * @return int
     */
    public function execute(string $input): int
    {
        $this->ensureData();
        return isset($this->map[$input]) ? count($this->map[$input]) : 0;
    }

    private function ensureData(): void
    {
        if (!isset($this->map)) {
            $map = [];

            foreach ($this->branches as $branch) {
                $location = $branch->getLocation();
                if ($location === null) {
                    continue;
                }
                $voivodeship = $location->getVoivodeship();
                if (!array_key_exists($voivodeship, $map)) {
                    $map[$voivodeship] = [];
                }
                $map[$voivodeship][] = $branch;
            }

            $this->map = $map;
        }
    }
}
