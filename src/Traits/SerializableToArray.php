<?php

namespace MobilitySoft\TBSA\Traits;

trait SerializableToArray
{
    public function toArray(): array
    {
        return array_map(function ($item) {
            return self::serializeToArray($item);
        }, get_object_vars($this));
    }

    private static function serializeToArray($value)
    {
        if (is_object($value) && (method_exists($value, 'toArray'))) {
            $value = $value->toArray();
        }

        if (is_iterable($value)) {
            foreach ($value as &$item) {
                $item = self::serializeToArray($item);
            }
        }

        return $value;
    }
}
