<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Entity\Board;
use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedEntrepreneur;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedGroup;
use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;

final class BoardRepository
{
    /**
     * @var string
     */
    private $postType;

    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * BoardRepository constructor.
     *
     * @param string $postType
     */
    public function __construct(string $postType)
    {
        $this->postType = $postType;
    }

    public function inject(EntrepreneurRepository $entrepreneurRepository, GroupRepository $groupRepository): void
    {
        $this->groupRepository        = $groupRepository;
        $this->entrepreneurRepository = $entrepreneurRepository;
    }

    /**
     * @param string $id
     *
     * @return BoardInterface|null
     */
    public function getById(string $id): ?BoardInterface
    {
        return $this->getOne($id);
    }

    /**
     * @param string|null $id
     *
     * @return BoardInterface
     */
    private function getOne(?string $id = null): BoardInterface
    {
        if (get_post_type($id) !== $this->postType) {
            return null;
        }

        $board  = new Board();
        $realId = $id ?? get_the_ID();
        $board->setId($realId);
        $board->setName(get_the_title($id));
        $board->setMembers($this->getMembers($id));
        $board->setGroups($this->getGroups($realId));

        return $board;
    }

    /**
     * @param string|null $id
     *
     * @return EntrepreneurInterface[]
     */
    private function getMembers(?string $id): array
    {
        $fields = get_field('members', $id);

        return array_filter(array_map(function (array $member) {
            $id = isset($member['entrepreneur'][0], $member['entrepreneur'][0]->ID)
                ? $member['entrepreneur'][0]->ID
                : null;
            if ( ! $id) {
                return null;
            }

            $entrepreneur = new LazyLoadedEntrepreneur($this->entrepreneurRepository, $id);

            return new EntrepreneurRelation($member['role'], $entrepreneur);
        }, $fields), function ($member) {
            return $member;
        });
    }

    /**
     * @param string $boardId
     *
     * @return Group[]
     */
    private function getGroups(string $boardId): array
    {
        $ids = $this->groupRepository->getIdsByLocalBoardId($boardId);

        return array_map(function (string $id) {
            return new LazyLoadedGroup($this->groupRepository, $id);
        }, $ids);
    }
}
