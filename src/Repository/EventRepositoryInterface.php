<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Entity\EventInterface;

interface EventRepositoryInterface
{
    /**
     * @param EventQuery $query
     *
     * @return EventInterface[]
     */
    public function query(EventQuery $query): array;

    public function getById(int $id): ?EventInterface;
}
