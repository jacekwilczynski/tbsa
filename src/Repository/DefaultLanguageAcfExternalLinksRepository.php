<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\DTO\Link;

/**
 * Class DefaultLanguageAcfExternalLinksRepository
 * @package MobilitySoft\TBSA\Repository
 */
final class DefaultLanguageAcfExternalLinksRepository implements AcfExternalLinksRepositoryInterface
{
    /**
     * @var AcfExternalLinksRepositoryInterface
     */
    private $externalLinksRepository;

    /**
     * DefaultLanguageExternalLinksRepository constructor.
     *
     * @param AcfExternalLinksRepositoryInterface $externalLinksRepository
     */
    public function __construct(AcfExternalLinksRepositoryInterface $externalLinksRepository)
    {
        $this->externalLinksRepository = $externalLinksRepository;
    }

    /**
     * @param int|string|null $source
     *
     * @return Link[]
     */
    public function getAll($source = null): array
    {
        global $sitepress;
        $defaultLanguage = $sitepress->get_default_language();
        add_filter('acf/settings/current_language', function () use ($defaultLanguage) {
            return $defaultLanguage;
        });
        $result = $this->externalLinksRepository->getAll($source);
        add_filter('acf/settings/current_language', function () {
            return ICL_LANGUAGE_CODE;
        });

        return $result;
    }
}
