<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\Entity\Group;
use MobilitySoft\TBSA\Entity\GroupInterface;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedBoard;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedBranch;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedEntrepreneur;
use WP_Post;

class GroupRepository
{
    /**
     * @var string
     */
    private $postType;

    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var BoardRepository
     */
    private $boardRepository;

    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * GroupRepository constructor.
     *
     * @param string $postType
     */
    public function __construct(string $postType)
    {
        $this->postType = $postType;
    }

    public function inject(
        BoardRepository $boardRepository,
        BranchRepository $branchRepository,
        EntrepreneurRepository $entrepreneurRepository
    ): void {
        $this->boardRepository        = $boardRepository;
        $this->branchRepository       = $branchRepository;
        $this->entrepreneurRepository = $entrepreneurRepository;
    }

    /**
     * @param string $id
     *
     * @return GroupInterface|null
     */
    public function getById(string $id): ?GroupInterface
    {
        return $this->getOne($id);
    }

    /**
     * @param string|null $id
     *
     * @return GroupInterface
     */
    private function getOne(?string $id = null): GroupInterface
    {
        if (get_post_type($id) !== $this->postType) {
            return null;
        }

        $group = new Group();
        $group->setId($id ?? get_the_ID());
        $group->setName(get_the_title($id));
        $group->setBranch($this->getBranch($id));
        $group->setLocalBoard($this->getLocalBoard($id));
        $group->setMembers($this->getMembers($id));

        return $group;
    }

    /**
     * @param string|null $id
     *
     * @return BranchInterface|null
     */
    private function getBranch(?string $id): ?BranchInterface
    {
        $branchId = get_field('branch', $id);

        return $branchId ? new LazyLoadedBranch($this->branchRepository, $branchId) : null;
    }

    /**
     * @param string|null $id
     *
     * @return BoardInterface|null
     */
    private function getLocalBoard(?string $id): ?BoardInterface
    {
        $boardId = get_field('local_board', $id);

        return $boardId ? new LazyLoadedBoard($this->boardRepository, $boardId) : null;
    }

    /**
     * @param string|null $id
     *
     * @return EntrepreneurInterface[]
     */
    private function getMembers(?string $id): array
    {
        $field = get_array_field('entrepreneurs', $id);

        return array_map(function (WP_Post $post) {
            return new LazyLoadedEntrepreneur($this->entrepreneurRepository, $post->ID);
        }, $field);
    }

    /**
     * @param string $branchId
     *
     * @return array
     */
    public function getIdsByBranchId(string $branchId): array
    {
        return get_posts([
            'post_type'  => $this->postType,
            'meta_key'   => 'branch',
            'meta_value' => $branchId,
            'fields'     => 'ids',
        ]);
    }

    /**
     * @param string $entrepreneurId
     *
     * @return GroupInterface[]
     */
    public function getIdsByEntrepreneurId(string $entrepreneurId): array
    {
        return get_posts([
            'post_type'    => $this->postType,
            'meta_key'     => 'entrepreneurs',
            'meta_value'   => '"' . $entrepreneurId . '"',
            'meta_compare' => 'LIKE',
            'fields'       => 'ids',
        ]);
    }

    /**
     * @param string $localBoardId
     *
     * @return string[]
     */
    public function getIdsByLocalBoardId(string $localBoardId): array
    {
        return get_posts([
            'post_type'  => $this->postType,
            'meta_key'   => 'local_board',
            'meta_value' => $localBoardId,
            'fields'     => 'ids',
        ]);
    }
}
