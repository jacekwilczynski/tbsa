<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\DTO\Initiative;
use MobilitySoft\TBSA\DTO\InitiativeCategory;
use WP_Query;
use WP_Term;

final class InitiativeRepository
{
    /**
     * @return InitiativeCategory[]
     */
    public function getCategories(): array
    {
        $terms = get_terms(['taxonomy' => 'initiative_category']);

        return array_map(function (WP_Term $term) {
            $customFields          = get_fields($term);
            $category              = new InitiativeCategory();
            $category->slug        = $term->slug;
            $category->title       = $term->name;
            $category->longTitle   = $customFields['long_title'] ?? $term->name;
            $category->description = $term->description ?? '';

            return $category;
        }, $terms);
    }

    /**
     * @param string $categoryTitle
     * @param array $queryVars
     *
     * @return Initiative[]
     */
    public function getByCategoryTitle(string $categoryTitle = '', array $queryVars = []): array
    {
        $query = new WP_Query(array_merge($queryVars, [
            'post_type' => 'initiative',
            'tax_query' => $categoryTitle
                ? [
                    [
                        'taxonomy' => 'initiative_category',
                        'field'    => 'name',
                        'terms'    => $categoryTitle,
                    ]
                ]
                : null
        ]));

        $initiatives = [];

        while ($query->have_posts()) {
            $query->the_post();
            $initiatives[] = $this->getCurrent();
        }

        wp_reset_query();

        return $initiatives;
    }

    /**
     * @return Initiative
     */
    private function getCurrent(): Initiative
    {
        $initiative   = new Initiative();
        $customFields = get_fields();

        $initiative->title        = get_the_title();
        $initiative->thumbnail    = get_the_post_thumbnail(null, 'large');
        $initiative->description  = $customFields['description'] ?? '';
        $initiative->url          = $customFields['url'] ?? '';
        $initiative->applyUrl     = $customFields['apply_url'] ?? '';
        $initiative->subscribeUrl = $customFields['subscribe_url'] ?? '';

        return $initiative;
    }
}
