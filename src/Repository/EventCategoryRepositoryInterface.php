<?php
/**
 * Created by PhpStorm.
 * User: jw
 * Date: 15.03.19
 * Time: 03:19
 */

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Entity\EventCategory;

interface EventCategoryRepositoryInterface
{
    /**
     * @return EventCategory[]
     */
    public function getAll(): array;

    /**
     * @param string $id
     *
     * @return EventCategory|null
     */
    public function getById(string $id): ?EventCategory;
}
