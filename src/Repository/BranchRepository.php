<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Common\Infrastructure\BranchRepositoryHelper;
use MobilitySoft\TBSA\DTO\Noun;
use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\Branch;
use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\Entity\GroupInterface;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedBoard;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedGroup;
use MobilitySoft\TBSA\ValueObject\BranchLocation;
use WP_Query;

final class BranchRepository
{
    /**
     * @var string
     */
    private $postType;

    /**
     * @var BoardRepository
     */
    private $boardRepository;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    public function __construct(string $postType, EntrepreneurRepository $entrepreneurRepository)
    {
        $this->postType = $postType;
        $this->entrepreneurRepository = $entrepreneurRepository;
    }

    public function inject(BoardRepository $boardRepository, GroupRepository $groupRepository): void
    {
        $this->boardRepository = $boardRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return BranchInterface[]
     */
    public function getByEntrepreneur(EntrepreneurInterface $entrepreneur): array
    {
        $groupsIds = $this->groupRepository->getIdsByEntrepreneurId($entrepreneur->getId());

        $branches = array_map(function (string $groupId) {
            return $this->getByGroupId($groupId);
        }, $groupsIds);

        $branches = array_filter($branches, function ($branch) {
            return $branch;
        });

        return unique_sorted_by_quantity($branches, function (BranchInterface $branch) {
            return $branch->getId();
        });
    }

    public function getByGroupId(string $id): ?BranchInterface
    {
        $group = $this->groupRepository->getById($id);

        return $group ? $group->getBranch() : null;
    }

    public function getById(int $id): ?BranchInterface
    {
        $branches = $this->query(['p' => $id]);

        return $branches ? $branches[0] : null;
    }

    /**
     * @return BranchInterface[]
     */
    public function query(array $args): array
    {
        $query = new WP_Query($args + [
                'post_type'      => $this->postType,
                'posts_per_page' => -1,
            ]);

        return $this->getMany($query);
    }

    /**
     * @return BranchInterface[]
     */
    private function getMany(WP_Query $query): array
    {
        $branches = [];

        while ($query->have_posts()) {
            $query->the_post();
            $branches[] = $this->getOne(get_the_ID());
        }

        wp_reset_query();

        return $branches;
    }

    public function getOne(?string $id = null): ?BranchInterface
    {
        if (get_post_type($id) !== $this->postType) {
            return null;
        }

        $realId = $id ?? get_the_ID();
        $branch = new Branch();
        $branch->setId($realId);
        $branch->setName($this->getName($id));
        $branch->setPhotos($this->getPhotos($id));
        $branch->setRegionalBoard($this->getRegionalBoard($realId));
        $branch->setGroups($this->getGroups($realId));
        $branch->setUrl(get_the_permalink($id));
        $branch->setLocation($this->getLocation($id));
        $branch->setContact($this->getContact($id));

        return $branch;
    }

    private function getName(?string $id): Noun
    {
        $name = new Noun();
        $name->nominative = get_the_title($id);
        $name->genitive = get_field('inflection_genitive', $id);
        $name->locative = get_field('inflection_locative', $id);
        return $name;
    }

    /**
     * @return string[]
     */
    private function getPhotos(?string $id): array
    {
        $field = get_field('photos', $id);

        if (is_array($field)) {
            return array_filter(array_map(
                static function (array $photo) {
                    return $photo['ID'] ?? null;
                },
                $field
            ));
        }

        return [];
    }

    private function getRegionalBoard(string $id): ?BoardInterface
    {
        $boardId = get_field('regional_board', $id);
        return $boardId ? new LazyLoadedBoard($this->boardRepository, $boardId) : null;
    }

    /**
     * @param string $id
     *
     * @return GroupInterface[]
     */
    private function getGroups(string $id): array
    {
        $groupIds = $this->groupRepository->getIdsByBranchId($id);
        return array_map(
            function (string $groupId) {
                return new LazyLoadedGroup($this->groupRepository, $groupId);
            },
            $groupIds
        );
    }

    private function getLocation(?string $id): ?BranchLocation
    {
        $town = get_field('town', $id) ?? '';
        $voivodeship = get_field('voivodeship', $id) ?? '';
        return $town && $voivodeship
            ? new BranchLocation($town, $voivodeship)
            : null;
    }

    private function getContact(?string $id): ?EntrepreneurInterface
    {
        $contactId = get_field('contact', $id);
        return $contactId ? $this->entrepreneurRepository->getById($contactId) : null;
    }

    /**
     * @return BranchInterface[]
     */
    public function getNonVirtualBranches(): array
    {
        return $this->query([
            'meta_query' => BranchRepositoryHelper::getNonVirtualBranchesMetaQuery(),
        ]);
    }
}
