<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\DTO\Link;

/**
 * Interface ExternalLinksRepository
 * @package MobilitySoft\TBSA\Repository
 */
interface AcfExternalLinksRepositoryInterface
{
    /**
     * @param int|string|null $source
     *
     * @return Link[]
     */
    public function getAll($source = null): array;
}
