<?php

namespace MobilitySoft\TBSA\Repository;

use EM_Event;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Infrastructure\Event\FindEmEvents;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetCategoryFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetLocationFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetTimespanFromEmEvent;
use MobilitySoft\TBSA\Common\Infrastructure\Event\SimpleAcfFields;
use MobilitySoft\TBSA\Common\Infrastructure\GetPostExcerpt;
use MobilitySoft\TBSA\Entity\Event;
use MobilitySoft\TBSA\Entity\EventCategory;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedBranch;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedGroup;
use MobilitySoft\TBSA\ValueObject\EventBooking;

class EmEventRepository implements EventRepositoryInterface
{
    /**
     * @var EventCategoryRepositoryInterface
     */
    private $eventCategoryRepository;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var EventInterface[]
     */
    private $eventsById = [];

    /**
     * @var FindEmEvents
     */
    private $findEvents;

    /**
     * @var GetTimespanFromEmEvent
     */
    private $getTimespan;

    /**
     * @var GetCategoryFromEmEvent
     */
    private $getCategoryFromEmEvent;

    /**
     * @var GetLocationFromEmEvent
     */
    private $getLocation;

    /**
     * @var GetPostExcerpt
     */
    private $getExcerpt;

    public function __construct(
        FindEmEvents $findEventIds,
        GetCategoryFromEmEvent $getCategoryFromEmEvent,
        GetLocationFromEmEvent $getEventLocation,
        GetPostExcerpt $getPostExcerpt,
        GetTimespanFromEmEvent $getEventTimespan
    ) {
        $this->findEvents             = $findEventIds;
        $this->getCategoryFromEmEvent = $getCategoryFromEmEvent;
        $this->getExcerpt             = $getPostExcerpt;
        $this->getLocation            = $getEventLocation;
        $this->getTimespan            = $getEventTimespan;
    }

    public function inject(
        BranchRepository $branchRepository,
        EventCategoryRepositoryInterface $eventCategoryRepository,
        GroupRepository $groupRepository
    ): void {
        $this->eventCategoryRepository = $eventCategoryRepository;
        $this->groupRepository         = $groupRepository;
        $this->branchRepository        = $branchRepository;
    }

    /**
     * @param EventQuery $query
     * @param callable $filter
     *
     * @return EventInterface[]
     */
    public function query(EventQuery $query, ?callable $filter = null): array
    {
        $emEvents = $this->findEvents->execute($query);

        $events = array_map([$this, 'fromEmEvent'], $emEvents);

        if (isset($filter)) {
            $events = array_filter($events, $filter);
        }

        return $events;
    }

    /**
     * @param int $id
     *
     * @return EventInterface|null
     */
    public function getById(int $id): ?EventInterface
    {
        if (array_key_exists($id, $this->eventsById)) {
            return $this->eventsById[$id];
        }

        if (get_post_type($id) !== 'event') {
            return null;
        }

        $event                 = $this->fromEmEvent(em_get_event($id, 'post_id'));
        $this->eventsById[$id] = $event;

        return $event;
    }

    /**
     * @param EM_Event $emEvent
     *
     * @return EventInterface
     */
    private function fromEmEvent(EM_Event $emEvent): EventInterface
    {
        $event  = new Event();
        $id     = $emEvent->post_id;
        $fields = SimpleAcfFields::prepare(get_fields($id));

        $event->setId($id);
        $event->setTitle($emEvent->post_title);
        $event->setImage($emEvent->get_image_url());
        $event->setDescription($emEvent->post_content);
        $event->setCategory($this->getCategory($emEvent));
        $event->setLocation($this->getLocation->execute($emEvent));
        $event->setTimespan($this->getTimespan->execute($emEvent));
        $event->setExcerpt($this->getExcerpt->execute($id));
        $event->setContact($fields->getContact());
        $event->setIsExternal($fields->isExternal());
        $event->setExternalUrl($fields->getExternalUrl());
        $event->setBookingsEnabled($fields->hasBookingsEnabled());

        if ($fields->getGroupId()) {
            $event->setGroup(new LazyLoadedGroup($this->groupRepository, $fields->getGroupId()));
        }

        if ($fields->getBranchId()) {
            $event->setBranch(new LazyLoadedBranch($this->branchRepository, $fields->getBranchId()));
        }

        if ($fields->getTickets()) {
            $event->setTickets($fields->getTickets());
        } else {
            $event->setPrice($fields->getPrice());
        }

        return $event;
    }

    private function getCategory(EM_Event $emEvent): ?EventCategory
    {
        $categoryId = $this->getCategoryFromEmEvent->getId($emEvent);

        return $categoryId ? $this->eventCategoryRepository->getById($categoryId) : null;
    }
}
