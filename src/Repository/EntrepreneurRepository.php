<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\DTO\MediaLink;
use MobilitySoft\TBSA\Entity\Entrepreneur;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\Entity\GroupInterface;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedBusiness;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedGroup;
use MobilitySoft\TBSA\ValueObject\BusinessRelation;
use WP_Query;
use WP_Term;
use const MobilitySoft\TBSA\BUSINESS_POST_TYPE_SLUG;

final class EntrepreneurRepository
{
    /**
     * @var string
     */
    private $postType;

    /**
     * @var AcfExternalLinksRepositoryInterface
     */
    private $linksRepository;

    /**
     * @var BusinessRepository
     */
    private $businessRepository;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    public function __construct(string $postType, AcfExternalLinksRepositoryInterface $linksRepository)
    {
        $this->postType = $postType;
        $this->linksRepository = $linksRepository;
    }

    public function inject(BusinessRepository $businessRepository, GroupRepository $groupRepository): void
    {
        $this->groupRepository = $groupRepository;
        $this->businessRepository = $businessRepository;
    }

    /**
     * @return EntrepreneurInterface[]
     */
    public function search(string $phrase): array
    {
        return $this->query(['s' => $phrase]);
    }

    /**
     * @return EntrepreneurInterface[]
     */
    private function query(array $args): array
    {
        $query = new WP_Query(array_merge(
            [
                'post_type'      => $this->postType,
                'posts_per_page' => -1,
            ],
            $args
        ));

        return $this->getMany($query);
    }

    /**
     * @return EntrepreneurInterface[]
     */
    private function getMany(WP_Query $query): array
    {
        $entrepreneurs = [];

        while ($query->have_posts()) {
            $query->the_post();
            $entrepreneurs[] = $this->getOne();
        }

        wp_reset_query();

        return $entrepreneurs;
    }

    private function getOne(?string $id = null): ?EntrepreneurInterface
    {
        if (get_post_type($id) !== $this->postType) {
            return null;
        }

        $entrepreneur = new Entrepreneur();
        $entrepreneur->setId($id ?? get_the_ID());
        $entrepreneur->setName(get_the_title($id));
        $entrepreneur->setPhoto(wp_get_attachment_image_url(get_field('photo', $id), 'medium'));
        $entrepreneur->setDescription(get_field('description', $id));
        $entrepreneur->setLinks($this->linksRepository->getAll($id));
        $entrepreneur->setActivities($this->getActivities($id));
        $entrepreneur->setMedia($this->getMedia($id));
        $entrepreneur->setBusinessRelations($this->getBusinesses($id));
        $entrepreneur->setIsStarred((bool) get_field('is_starred', $id));
        $entrepreneur->setGroups($this->getGroups($entrepreneur->getId()));

        return $entrepreneur;
    }

    private function getActivities(?string $id = null): array
    {
        $field = get_field('activity', $id);

        if (!is_array($field)) {
            return [];
        }

        return array_map(function (array $item) {
            return $item['text'];
        }, $field);
    }

    /**
     * @return MediaLink[]
     */
    private function getMedia(?string $id = null): array
    {
        $media = get_field('media', $id);

        if (!is_array($media)) {
            return [];
        }

        return array_map(function (array $item) {
            $link = new MediaLink();
            $link->title = $item['title'];
            $link->subtitle = $item['subtitle'];
            $link->image = $item['image']['url'];
            $link->url = $item['url'];

            return $link;
        }, $media);
    }

    /**
     * @return BusinessRelation[]
     */
    private function getBusinesses(?string $id = null): array
    {
        $field = get_field('businesses', $id);

        if (!is_array($field)) {
            return [];
        }

        return array_filter(array_map(function (array $item): ?BusinessRelation {
            if (empty($item['business'])) {
                return null;
            }
            $businessId = $item['business'];
            $description = $item['role'];
            if (get_post_type($businessId) !== BUSINESS_POST_TYPE_SLUG) {
                return null;
            }
            $business = new LazyLoadedBusiness($this->businessRepository, $businessId);

            $usesLocationsOverride = isset($item['locations']['source']) && $item['locations']['source'] === 'override';
            if ($usesLocationsOverride) {
                $terms = isset($item['locations']['list']) && is_array($item['locations']['list'])
                    ? $item['locations']['list']
                    : [];
                $locationsOverride = array_map(function (WP_Term $term) {
                    return $term->name;
                }, $terms);
            } else {
                $locationsOverride = null;
            }

            return new BusinessRelation($description, $business, $locationsOverride);
        }, $field), function (?BusinessRelation $businessRelation) {
            return $businessRelation;
        });
    }

    /**
     * @return GroupInterface[]
     */
    private function getGroups(string $id): array
    {
        $groupsIds = $this->groupRepository->getIdsByEntrepreneurId($id);

        return array_map(function (string $id) {
            return new LazyLoadedGroup($this->groupRepository, $id);
        }, $groupsIds);
    }

    /**
     * @return Entrepreneur[]
     */
    public function getByBusinessId(string $businessId): array
    {
        return $this->query([
            'meta_key'     => 'businesses_%_business',
            'meta_value'   => '"' . $businessId . '"',
            'meta_compare' => 'LIKE',
        ]);
    }

    public function getById(int $id): ?EntrepreneurInterface
    {
        return $this->getOne($id);
    }

    public function getFeatured(): ?Entrepreneur
    {
        $seed = date('Ymd');
        $array = $this->query([
            'meta_key'       => 'is_starred',
            'meta_value'     => '1',
            'orderby'        => "RAND($seed)",
            'posts_per_page' => 1,
        ]);

        return $array ? $array[0] : null;
    }
}
