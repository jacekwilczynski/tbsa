<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\DTO\Link;
use MobilitySoft\TBSA\Interfaces\GetLinkIconInterface;

final class AcfExternalLinksRepository implements AcfExternalLinksRepositoryInterface
{
    /**
     * @var GetLinkIconInterface
     */
    private $getLinkContent;

    public function __construct(GetLinkIconInterface $getLinkContent)
    {
        $this->getLinkContent = $getLinkContent;
    }

    /**
     * @param int|string|null $source
     *
     * @return Link[]
     */
    public function getAll($source = null): array
    {
        $field = get_field('external_links', $source);

        if (!is_array($field)) {
            return [];
        }

        return array_map(function (array $item) {
            $link = new Link();
            $type = $item['type']['value'];

            // Type
            $link->type = $type;

            // Href
            $prefix = $this->getHrefPrefix($type);
            $link->href = $prefix . $item['url'];

            // Content
            $contentType = $type === 'social' ? $item['medium']['value'] : $type;
            $link->content = $this->getLinkContent->execute($contentType);

            return $link;
        }, $field);
    }

    private function getHrefPrefix(string $type): string
    {
        switch ($type) {
            case 'email':
                return 'mailto:';
            case 'tel':
                return 'tel:';
            case 'fax':
                return 'fax:';
            default:
                return '';
        }
    }
}
