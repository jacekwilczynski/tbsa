<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Entity\MagazineIssue;
use WP_Query;

class MagazineIssueRepository
{
    /**
     * @return MagazineIssue[]
     */
    public function getAll(): array
    {
        $query = new WP_Query([
            'post_type' => 'magazine_issue',
            'orderby'   => 'meta_value',
            'order'     => 'DESC',
            'meta_key'  => 'number',
            'meta_type' => 'NUMERIC',
        ]);

        $issues = [];
        while ($query->have_posts()) {
            $query->the_post();
            $issues[] = $this->getCurrent();
        }
        wp_reset_query();

        return $issues;
    }

    private function getCurrent(): MagazineIssue
    {
        $number = get_the_title();
        $coverField = get_field('cover');
        $cover = is_array($coverField) && isset($coverField['url']) ? $coverField['url'] : '';
        $about = get_field('about');
        $contents = $this->getContents();
        $link = get_field('link');

        return new MagazineIssue($number, $contents, $cover, $about, $link);
    }

    private function getContents(): array
    {
        $contentsField = get_field('contents');
        if (is_array($contentsField)) {
            return array_map(function (array $item) {
                return $item['text'];
            }, $contentsField);
        }
        return [];
    }
}
