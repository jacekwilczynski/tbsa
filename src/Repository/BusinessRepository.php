<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Entity\Business;
use MobilitySoft\TBSA\Entity\BusinessInterface;
use MobilitySoft\TBSA\LazyLoaded\LazyLoadedEntrepreneur;
use MobilitySoft\TBSA\ValueObject\EntrepreneurRelation;
use WP_Query;

class BusinessRepository
{
    /**
     * @var string
     */
    private $postType;

    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var array
     */
    private $taxonomies;

    /**
     * BusinessRepository constructor.
     *
     * @param string $postType
     * @param array $taxonomies
     */
    public function __construct(string $postType, array $taxonomies)
    {
        $this->postType = $postType;
        $this->taxonomies = $taxonomies;
    }

    public function inject(EntrepreneurRepository $entrepreneurRepository): void
    {
        $this->entrepreneurRepository = $entrepreneurRepository;
    }

    /**
     * @param string|int $id
     *
     * @return BusinessInterface|null
     */
    public function getById($id): ?BusinessInterface
    {
        return $this->getOne($id);
    }

    /**
     * @param string|null $id
     *
     * @return BusinessInterface|null
     */
    private function getOne(?string $id = null): ?BusinessInterface
    {
        if (get_post_type($id) !== $this->postType) {
            return null;
        }

        $currentPost = $id ?? true;

        $business = new Business();
        $business->setId($id ?? get_the_ID());
        $business->setName(get_the_title($id));
        $business->setLogo(get_the_post_thumbnail_url($id, 'medium'));
        $business->setUrl(get_field('url', $id));
        $business->setIndustries(get_term_names($this->taxonomies['industry'], $currentPost));
        $business->setLocations(get_term_names($this->taxonomies['location'], $currentPost));
        $business->setEntrepreneurRelations($this->getEntrepreneurRelations($id));

        return $business;
    }

    /**
     * @param string|null $id
     *
     * @return EntrepreneurRelation[]
     */
    private function getEntrepreneurRelations(?string $id): array
    {
        $field = get_field('entrepreneurs', $id);

        if (!is_array($field)) {
            return [];
        }

        return array_filter(array_map(function (array $item) {
            $entrepreneurId = isset($item['entrepreneur'][0], $item['entrepreneur'][0]->ID)
                ? $item['entrepreneur'][0]->ID
                : null;

            if (!$entrepreneurId) {
                return null;
            }

            $entrepreneur = new LazyLoadedEntrepreneur($this->entrepreneurRepository, $entrepreneurId);

            return new EntrepreneurRelation($item['role'], $entrepreneur);
        }, $field), function ($item) {
            return $item;
        });
    }

    /**
     * @param string $phrase
     *
     * @return BusinessInterface[]
     */
    public function search(string $phrase): array
    {
        return $this->query(['s' => $phrase]);
    }

    /**
     * @param array $args
     *
     * @return BusinessInterface[]
     */
    private function query(array $args): array
    {
        $query = new WP_Query($args + ['post_type' => $this->postType]);

        return $this->getMany($query);
    }

    /**
     * @param WP_Query $query
     *
     * @return BusinessInterface[]
     */
    private function getMany(WP_Query $query): array
    {
        $businesses = [];

        while ($query->have_posts()) {
            $query->the_post();
            $businesses[] = $this->getOne(get_the_ID());
        }

        wp_reset_query();

        return $businesses;
    }

    /**
     * @param string $industry
     * @param string $location
     *
     * @return BusinessInterface[]
     */
    public function getByTerms(string $industry, string $location): array
    {
        $queryArgs = [
            'tax_query' => [
                'relation' => 'AND',
            ],
        ];

        if ($industry) {
            $queryArgs[] = [
                'taxonomy' => $this->taxonomies['industry'],
                'field'    => 'name',
                'terms'    => $industry,
            ];
        }

        if ($location) {
            $queryArgs[] = [
                'taxonomy' => $this->taxonomies['location'],
                'field'    => 'name',
                'terms'    => $location,
            ];
        }

        return $this->query($queryArgs);
    }
}
