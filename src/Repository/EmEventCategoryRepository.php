<?php

namespace MobilitySoft\TBSA\Repository;

use MobilitySoft\TBSA\Entity\EventCategory;
use WP_Term;

final class EmEventCategoryRepository implements EventCategoryRepositoryInterface
{
    /**
     * @var
     */
    private $postType;

    /**
     * EmEventCategoryRepository constructor.
     *
     * @param $postType
     */
    public function __construct($postType)
    {
        $this->postType = $postType;
    }

    /**
     * @return EventCategory[]
     */
    public function getAll(): array
    {
        $terms = get_terms([
            'taxonomy'   => $this->postType,
            'hide_empty' => false,
        ]);

        return array_map(function (WP_Term $term) {
            return $this->getByTerm($term);
        }, $terms);
    }

    /**
     * @param WP_Term $term
     *
     * @return EventCategory
     */
    private function getByTerm(WP_Term $term): EventCategory
    {
        $customFields = get_fields($term);

        $emCategory = em_get_category($term->term_id);
        $category   = new EventCategory();
        $category->setId($term->term_id);
        $category->setSlug($term->slug);
        $category->setTitle($term->name);
        $category->setShortTitle($customFields['short_title'] ?? '');
        $category->setIcon($emCategory->get_image_url());
        $category->setDescription($customFields['description'] ?? '');
        $category->setVideoHeading($customFields['video_heading'] ?? '');
        $category->setVideo($customFields['video'] ?? '');

        return $category;
    }

    /**
     * @param string $id
     *
     * @return EventCategory|null
     */
    public function getById(string $id): ?EventCategory
    {
        return $this->getByTerm(get_term($id));
    }
}
