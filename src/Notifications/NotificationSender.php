<?php

namespace MobilitySoft\TBSA\Notifications;

interface NotificationSender
{
    public function send(?array $data = []): void;
}
