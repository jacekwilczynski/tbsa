<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications;

use MobilitySoft\TBSA\Notifications\Infrastructure\Email\WcEmailNotificationTemplateRepository;
use MobilitySoft\TBSA\Notifications\Infrastructure\SendNotificationVisitor;

class AcfToWcEmailNotificationSenderFactory
{
    /**
     * @var WcEmailNotificationTemplateRepository
     */
    private $templateRepository;

    /**
     * @var SendNotificationVisitor
     */
    private $sendNotificationVisitor;

    public function __construct(
        WcEmailNotificationTemplateRepository $templateRepository,
        SendNotificationVisitor $sendNotificationVisitor
    ) {
        $this->templateRepository = $templateRepository;
        $this->sendNotificationVisitor = $sendNotificationVisitor;
    }

    public function create(string $templateSlug): AcfToWcEmailNotificationSender
    {
        return new AcfToWcEmailNotificationSender(
            $this->templateRepository,
            $this->sendNotificationVisitor,
            $templateSlug
        );
    }
}
