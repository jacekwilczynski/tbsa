<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Infrastructure\Email;

use MobilitySoft\TBSA\Notifications\Domain\Email\EmailNotification;

class SendEmailNotification
{
    public function execute(EmailNotification $email): void
    {
        $headers = [
            'Content-Type: text/html; charset=UTF-8',
        ];

        if ($email->getParties()->getSender()) {
            $headers[] = 'From: ' . $email->getParties()->getSender();
        }

        if ($email->getParties()->getReplyTo()) {
            $headers[] = 'Reply-To: ' . $email->getParties()->getReplyTo();
        }

        wp_mail(
            $email->getParties()->getRecipient(),
            $email->getSubject(),
            $email->getContent(),
            $headers
        );
    }
}
