<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Infrastructure\Email;

use MobilitySoft\TBSA\Notifications\Domain\Email\EmailNotificationTemplate;
use MobilitySoft\TBSA\Notifications\Domain\Email\EmailParties;
use WC_Email;

class CreateWcEmailNotificationTemplate
{
    public static function execute(
        EmailParties $parties,
        string $subject,
        string $heading,
        string $message
    ): EmailNotificationTemplate {
        $wcMailer = WC()->mailer();
        $content  = (new WC_Email)->style_inline($wcMailer->wrap_message($heading, $message));

        return new EmailNotificationTemplate($parties, $subject, $content);
    }
}
