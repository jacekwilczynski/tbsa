<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Infrastructure\Email;

use MobilitySoft\TBSA\Notifications\Domain\Email\EmailAddress;
use MobilitySoft\TBSA\Notifications\Domain\Email\EmailParties;
use MobilitySoft\TBSA\Notifications\Domain\NotificationTemplate;
use MobilitySoft\TBSA\Notifications\Domain\NotificationTemplateRepository;
use MobilitySoft\TBSA\Notifications\Domain\NotificationTemplates;

class WcEmailNotificationTemplateRepository implements NotificationTemplateRepository
{
    public function getBySlug(string $slug): NotificationTemplates
    {
        $settings = get_array_field('talk_bundle__notification_email_settings', 'option');
        $allTemplates = get_array_field('talk_bundle__notification_email_templates', 'option');
        $matchingTemplates = array_filter($allTemplates, function (array $template) use ($slug): bool {
            return $template['triggering_event'] === $slug;
        });

        return new NotificationTemplates(
            ...array_map(function (array $item) use ($settings): NotificationTemplate {
                return $this->createNotificationTemplate($item, $settings);
            }, $matchingTemplates)
        );
    }

    private function createNotificationTemplate(array $notification, array $settings): NotificationTemplate
    {
        return CreateWcEmailNotificationTemplate::execute(
            new EmailParties(
                EmailAddress::fromString($notification['email']),
                EmailAddress::fromString($settings['from'] ?? null),
                EmailAddress::fromString($settings['reply_to'] ?? null)
            ),
            $notification['subject'],
            $notification['heading'],
            $notification['message']
        );
    }
}
