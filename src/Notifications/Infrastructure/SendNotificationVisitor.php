<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Infrastructure;

use MobilitySoft\TBSA\Notifications\Domain\Email\EmailNotification;
use MobilitySoft\TBSA\Notifications\Domain\NotificationVisitor;
use MobilitySoft\TBSA\Notifications\Infrastructure\Email\SendEmailNotification;
use MobilitySoft\TBSA\Notifications\Infrastructure\Email\WcEmail;

class SendNotificationVisitor implements NotificationVisitor
{
    /**
     * @var SendEmailNotification
     */
    private $sendEmailNotification;

    public function __construct(SendEmailNotification $sendEmailNotification)
    {
        $this->sendEmailNotification = $sendEmailNotification;
    }

    public function visitEmailNotification(EmailNotification $notification): void
    {
        $this->sendEmailNotification->execute($notification);
    }
}
