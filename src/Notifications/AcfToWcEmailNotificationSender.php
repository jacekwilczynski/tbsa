<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications;

use MobilitySoft\TBSA\Notifications\Infrastructure\Email\WcEmailNotificationTemplateRepository;
use MobilitySoft\TBSA\Notifications\Infrastructure\SendNotificationVisitor;

class AcfToWcEmailNotificationSender implements NotificationSender
{
    /**
     * @var WcEmailNotificationTemplateRepository
     */
    private $templateRepository;

    /**
     * @var SendNotificationVisitor
     */
    private $sendNotificationVisitor;

    /**
     * @var string
     */
    private $templateSlug;

    public function __construct(
        WcEmailNotificationTemplateRepository $templateRepository,
        SendNotificationVisitor $sendNotificationVisitor,
        string $templateSlug
    ) {
        $this->templateRepository = $templateRepository;
        $this->sendNotificationVisitor = $sendNotificationVisitor;
        $this->templateSlug = $templateSlug;
    }

    public function send(?array $data = []): void
    {
        $templates = $this->templateRepository->getBySlug($this->templateSlug);
        foreach ($templates->getItems() as $template) {
            $notification = $template->fill($data);
            $notification->accept($this->sendNotificationVisitor);
        }
    }
}
