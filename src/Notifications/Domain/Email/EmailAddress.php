<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

class EmailAddress
{
    /**
     * @var string
     */
    private $address;

    private function __construct(string $address)
    {
        $this->address = $address;
    }

    public static function fromString(?string $string): ?self
    {
        return $string ? new self($string) : null;
    }

    public function __toString(): string
    {
        return $this->address;
    }
}
