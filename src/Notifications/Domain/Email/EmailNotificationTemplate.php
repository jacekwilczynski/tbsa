<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

use MobilitySoft\TBSA\Notifications\Domain\Notification;
use MobilitySoft\TBSA\Notifications\Domain\NotificationTemplate;
use MobilitySoft\TBSA\Utils\ReplacePlaceholders;

class EmailNotificationTemplate extends AbstractEmailNotification implements NotificationTemplate
{
    public function fill(array $data): Notification
    {
        $fill = new class($data) implements StringFunction
        {
            /**
             * @var array
             */
            private $data;

            /**
             * @var ReplacePlaceholders
             */
            private $replacePlaceholders;

            public function __construct($data)
            {

                $this->data = $data;
                $this->replacePlaceholders = new ReplacePlaceholders();
            }

            public function execute($template): string
            {
                return $this->replacePlaceholders->execute((string) $template, $this->data);
            }
        };

        return new EmailNotification(
            $this->getParties()->map($fill),
            $fill->execute($this->getSubject()),
            $fill->execute($this->getContent())
        );
    }
}
