<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

interface StringFunction
{
    public function execute(string $input): string;
}
