<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

use MobilitySoft\TBSA\Notifications\Domain\Notification;
use MobilitySoft\TBSA\Notifications\Domain\NotificationVisitor;

class EmailNotification extends AbstractEmailNotification implements Notification
{
    public function accept(NotificationVisitor $visitor): void
    {
        $visitor->visitEmailNotification($this);
    }
}
