<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

class EmailParties
{
    /**
     * @var EmailAddress
     */
    private $recipient;

    /**
     * @var EmailAddress|null
     */
    private $sender;

    /**
     * @var EmailAddress|null
     */
    private $replyTo;

    public function __construct(EmailAddress $recipient, ?EmailAddress $sender, ?EmailAddress $replyTo = null)
    {
        $this->recipient = $recipient;
        $this->sender = $sender;
        $this->replyTo = $replyTo ?? $sender;
    }

    public function getRecipient(): EmailAddress
    {
        return $this->recipient;
    }

    public function getSender(): ?EmailAddress
    {
        return $this->sender;
    }

    public function getReplyTo(): ?EmailAddress
    {
        return $this->replyTo;
    }

    public function map(StringFunction $callback): self
    {
        return new self(
            EmailAddress::fromString($callback->execute((string) $this->recipient)),
            EmailAddress::fromString($callback->execute((string) $this->sender)),
            EmailAddress::fromString($callback->execute((string) $this->replyTo))
        );
    }
}
