<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain\Email;

abstract class AbstractEmailNotification
{
    /**
     * @var EmailParties
     */
    private $parties;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $content;

    public function __construct(EmailParties $parties, string $subject, string $content)
    {
        $this->parties = $parties;
        $this->subject = $subject;
        $this->content = $content;
    }

    public function getParties(): EmailParties
    {
        return $this->parties;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
