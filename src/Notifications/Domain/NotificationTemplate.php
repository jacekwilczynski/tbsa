<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain;

interface NotificationTemplate
{
    public function fill(array $data): Notification;
}
