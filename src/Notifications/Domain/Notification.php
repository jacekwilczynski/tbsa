<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain;

interface Notification
{
    public function accept(NotificationVisitor $visitor): void;
}
