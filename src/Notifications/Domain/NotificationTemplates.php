<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain;

class NotificationTemplates
{
    /**
     * @var NotificationTemplate[]
     */
    private $items;

    public function __construct(NotificationTemplate... $items)
    {
        $this->items = $items;
    }

    /**
     * @return NotificationTemplate[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
