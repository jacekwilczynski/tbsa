<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain;

use MobilitySoft\TBSA\Notifications\Domain\Email\EmailNotification;

interface NotificationVisitor
{
    public function visitEmailNotification(EmailNotification $notification): void;
}
