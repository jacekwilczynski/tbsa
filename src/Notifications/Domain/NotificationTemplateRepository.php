<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Notifications\Domain;

interface NotificationTemplateRepository
{
    public function getBySlug(string $slug): NotificationTemplates;
}
