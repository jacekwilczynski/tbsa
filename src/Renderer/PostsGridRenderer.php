<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;
use WP_Post;

class PostsGridRenderer
{
    private const MORE_BUTTON_TEXT = 'Czytaj więcej';

    /**
     * @var Twig
     */
    private $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function render(?int $limit, int $headingLevel): string
    {
        return do_shortcode($this->twig->render('postsGrid.twig', [
            'buttonText'   => self::MORE_BUTTON_TEXT,
            'headingLevel' => $headingLevel,
            'posts'        => $this->getPostsData($limit),
        ]));
    }

    /**
     * @return array[]
     */
    private function getPostsData(?int $limit): array
    {
        $posts = get_posts([
            'posts_per_page' => $limit ?? -1,
        ]);

        return array_map(function (WP_Post $post) {
            return $this->getPostData($post);
        }, $posts);
    }

    private function getPostData(WP_Post $post): array
    {
        return [
            'permalink' => get_permalink($post),
            'image'     => get_the_post_thumbnail($post),
            'date'      => get_the_date('j F Y', $post),
            'title'     => $post->post_title,
            'excerpt'   => has_excerpt($post) ? $post->post_excerpt : wp_trim_words($post->post_content, 18),
        ];
    }

    /**
     * @param WP_Post[] $posts
     * @param int $headingLevel
     */
    public function renderPosts(array $posts, int $headingLevel): string
    {
        return do_shortcode($this->twig->render('postsGrid.twig', [
            'buttonText'   => self::MORE_BUTTON_TEXT,
            'headingLevel' => $headingLevel,
            'posts'        => array_map(function (WP_Post $post) {
                return $this->getPostData($post);
            }, $posts),
        ]));
    }
}
