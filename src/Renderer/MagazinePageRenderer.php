<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Entity\MagazineIssue;
use MobilitySoft\TBSA\Repository\MagazineIssueRepository;
use Twig\Environment as Twig;

final class MagazinePageRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var MagazineIssueRepository
     */
    private $issueRepository;

    /**
     * @var string
     */
    private $beforeNumber;

    /**
     * @var string
     */
    private $buyUrl;

    public function __construct(Twig $twig, MagazineIssueRepository $issueRepository, string $buyUrl)
    {
        $this->twig = $twig;
        $this->issueRepository = $issueRepository;
        $this->buyUrl = $buyUrl;
    }

    public function render(): string
    {
        $issues = $this->issueRepository->getAll();

        return do_shortcode($this->twig->render('magazinePage.twig', [
            'pageTitle'          => get_field('mag_page_heading', 'option'),
            'topContent'         => get_field('mag_top', 'option'),
            'currentIssue'       => $this->getLatestIssueData($issues[0]),
            'otherIssuesHeading' => get_field('mag_earlier_issues_heading', 'option'),
            'otherIssues'        => $this->getOtherIssuesData(array_slice($issues, 1)),
            'sidebar'            => get_field('mag_sidebar', 'option'),
            'subscribe'          => $this->getSubscribeSection(),
            'buttonsUnderCover'  => [
                [
                    'label' => 'Zapisz się na roczną prenumeratę czasopisma',
                    'href'  => $this->buyUrl,
                ],
                [
                    'label' => 'Zapisz się na bezpłatną prenumeratę online',
                    'href'  => '#subscribe',
                    'class' => 'btn-primary-inverse',
                ],
            ],
        ]));
    }

    private function getLatestIssueData(MagazineIssue $issue): array
    {
        return [
            'cover'           => $issue->getCover(),
            'title'           => $this->getFullTitle($issue->getNumber()),
            'about'           => $issue->getAbout(),
            'contentsHeading' => get_field('mag_contents_heading', 'option'),
            'contents'        => $issue->getContents(),
        ];
    }

    private function getFullTitle(string $number): string
    {
        if (!isset($this->beforeNumber)) {
            $this->beforeNumber = get_field('mag_before_number', 'option');
        }

        return $this->beforeNumber . $number;
    }

    /**
     * @param MagazineIssue[] $issues
     */
    private function getOtherIssuesData(array $issues): array
    {
        return array_map(function (MagazineIssue $issue) {
            return [
                'cover' => $issue->getCover(),
                'title' => $this->getFullTitle($issue->getNumber()),
                'url'   => $issue->getLink(),
            ];
        }, $issues);
    }

    private function getSubscribeSection(): array
    {
        $rotationStep = -15;
        $images = [];
        $posts = get_field('mag_subscribe_images', 'option');

        foreach ($posts as $i => $post) {
            $images[] = [
                'src'      => get_the_post_thumbnail_url($post->ID),
                'alt'      => $this->getFullTitle(get_the_title($post->ID)),
                'rotation' => $i * $rotationStep,
            ];
        }

        return [
            'heading' => get_field('mag_subscribe_heading', 'option'),
            'images'  => $images,
        ];
    }
}
