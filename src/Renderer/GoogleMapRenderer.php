<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;

class GoogleMapRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * GoogleMapRenderer constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function render($atts = []): string
    {
        $address        = $atts['address'] ?? '';
        $encodedAddress = urlencode_deep($address);

        return do_shortcode($this->twig->render('googleMap.twig', [
            'height'     => ! isset($atts['extra_class']) ? ($atts['height'] ?? 300) : null,
            'extraClass' => $atts['extra_class'] ?? '',
            'src'        => 'https://maps.google.com/maps?q=' . $encodedAddress . '&t=&z=16&ie=UTF8&iwloc=&output=embed',
        ]));
    }
}
