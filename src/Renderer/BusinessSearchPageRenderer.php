<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Repository\EntrepreneurRepository;
use MobilitySoft\TBSA\Service\BusinessSearch;
use MobilitySoft\TBSA\Service\CreateDataForPagination;
use MobilitySoft\TBSA\Service\GetBusinessSearchResults;
use MobilitySoft\TBSA\Service\GetBusinessSearchStaticUrl;
use Twig\Environment as Twig;

/**
 * Class BusinessSearchPageRenderer
 * @package MobilitySoft\TBSA\Renderer
 */
final class BusinessSearchPageRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var array
     */
    private $dictionary;

    /**
     * @var string[]
     */
    private $taxonomies;

    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var GetBusinessSearchResults
     */
    private $getBusinessSearchResults;

    /**
     * @var BusinessSearch
     */
    private $businessSearch;

    /**
     * BusinessSearchPageRenderer constructor.
     *
     * @param Twig $twig
     * @param array $dictionary
     * @param array $taxonomies
     * @param EntrepreneurRepository $entrepreneurRepository
     * @param GetBusinessSearchResults $getBusinessSearchResults
     * @param BusinessSearch $businessSearch
     */
    public function __construct(
        Twig $twig,
        array $dictionary,
        array $taxonomies,
        EntrepreneurRepository $entrepreneurRepository,
        GetBusinessSearchResults $getBusinessSearchResults,
        BusinessSearch $businessSearch
    ) {
        $this->twig                     = $twig;
        $this->dictionary               = $dictionary;
        $this->taxonomies               = $taxonomies;
        $this->entrepreneurRepository   = $entrepreneurRepository;
        $this->getBusinessSearchResults = $getBusinessSearchResults;
        $this->businessSearch           = $businessSearch;
    }

    /**
     * @param array|null $atts
     *
     * @return string
     */
    public function render($atts = []): string
    {
        $entrepreneurId = $atts['entrepreneur_id'] ?? null;

        if ($entrepreneurId) {
            $searchResults = [
                'entrepreneurs' => [$this->entrepreneurRepository->getById($entrepreneurId)],
                'totalPages'    => 1,
            ];
        } else {
            $phrase   = $_REQUEST['fraza'] ?? null;
            $industry = $_REQUEST['branza'] ?? null;
            $location = $_REQUEST['miasto'] ?? null;
            $page     = $_REQUEST['strona'] ?? 1;

            $searchResults = $phrase || $industry || $location
                ? $this->businessSearch->execute($phrase, $industry, $location, $page)
                : null;

            if ($searchResults) {
                $createDataForPagination = new CreateDataForPagination(function (int $page) use (
                    $phrase,
                    $industry,
                    $location
                ) {
                    return GetBusinessSearchStaticUrl::execute($phrase, $industry, $location, $page);
                });

                $paginationData = $createDataForPagination->execute($page, $searchResults['totalPages']);
            }
        }


        return do_shortcode($this->twig->render('businessSearchPage.twig', array_merge(
            $this->dictionary,
            [
                'isSingleEntrepreneurPage' => $entrepreneurId,
                'pageTitle'                => 'Wyszukiwarka firm członkowskich',
                'form'                     => [
                    'title'      => 'Znajdź&nbsp;firmę członkowską&nbsp;po',
                    'submitText' => 'Znajdź',
                    'action'     => home_url('/wyszukiwarka-firm'),
                    'fields'     => [
                        [
                            'type'  => 'search',
                            'name'  => 'fraza',
                            'label' => 'Frazie (firma, nazwisko, opis)',
                            'value' => $phrase ?? null,
                        ],
                        [
                            'type'  => 'select',
                            'name'  => 'branza',
                            'label' => 'Branża',
                            'list'  => get_term_names($this->taxonomies['industry']),
                            'value' => $industry ?? null,
                        ],
                        [
                            'type'  => 'select',
                            'name'  => 'miasto',
                            'label' => 'Miasto',
                            'list'  => get_term_names($this->taxonomies['location']),
                            'value' => $location ?? null,
                        ]
                    ]
                ],
                'showingFeatured'          => ! $searchResults,
                'entrepreneurs'            => $this->getBusinessSearchResults->getViewData(
                    $searchResults
                        ? $searchResults['entrepreneurs'] ?? []
                        : [$this->entrepreneurRepository->getFeatured()]
                ),
                'pagination'               => $paginationData ?? null,
                'dictionary'               => $this->dictionary,
            ]))
        );
    }
}
