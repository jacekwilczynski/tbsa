<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;

/**
 * Class WhatWeAreRenderer
 * @package MobilitySoft\TBSA\Renderer
 */
final class WhatWeAreRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * WhatWeAreRenderer constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $field = get_field('what_we_are_items', 'option');

        $items = array_map(function (array $item) {
            return [
                'imgSrc' => $item['image']['url'],
                'text'   => $item['text']
            ];
        }, $field);

        return do_shortcode($this->twig->render('whatWeAre.twig', [
            'logoSrc'      => 'logo.svg',
            'items'        => $items,
            'separatorSrc' => 'plus.svg'
        ]));
    }
}
