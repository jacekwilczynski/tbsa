<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\Event\EventQueryScope;
use MobilitySoft\TBSA\DTO\Link;
use MobilitySoft\TBSA\Entity\BoardInterface;
use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\GetBranchLocalBoards;
use MobilitySoft\TBSA\Repository\BranchRepository;
use MobilitySoft\TBSA\Repository\EventCategoryRepositoryInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\Service\ShitCrypt;
use Twig\Environment as Twig;

final class BranchPageRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var EventCategoryRepositoryInterface
     */
    private $eventCategoryRepository;

    /**
     * @var GetBranchLocalBoards
     */
    private $getBranchLocalBoards;

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    public function __construct(
        Twig $twig,
        BranchRepository $branchRepository,
        EventCategoryRepositoryInterface $eventCategoryRepository,
        GetBranchLocalBoards $getBranchLocalBoards,
        EventRepositoryInterface $eventRepository
    ) {
        $this->twig = $twig;
        $this->branchRepository = $branchRepository;
        $this->eventCategoryRepository = $eventCategoryRepository;
        $this->getBranchLocalBoards = $getBranchLocalBoards;
        $this->eventRepository = $eventRepository;
    }

    public function render(): string
    {
        $branch = $this->branchRepository->getOne();
        $photos = $branch->getPhotos();

        return do_shortcode($this->twig->render('branchPage.twig', [
            'branch'                  => $branch,
            'dictionary'              => [
                'signUpButtonText' => get_field('sign_up_for_next_event_text', 'option'),
                'contactHeading'   => 'Skontaktuj się z nami w razie pytań',
            ],
            'intro'                   => $this->getIntro(),
            'eventCategoryIdTemplate' => 'event-category-{slug}',
            'slides'                  => $photos ? $this->getSlides($photos) : null,
            'eventCategories'         => $this->eventCategoryRepository->getAll(),
            'boards'                  => $this->getBoards($branch),
            'aboutTbsa'               => $this->getAboutTbsa($branch->getId()),
            'contact'                 => $this->getContactData($branch),
            'buyNearestEventUrl'      => $this->getNearestEventUrl(),
        ]));
    }

    private function getIntro(): array
    {
        $field = get_field('branch_page_introduction', 'option');

        return [
            'bigText'                => $field['text_large'],
            'normalText'             => $field['text_normal'],
            'eventCategoriesCaption' => $field['text_above_event_categories'],
        ];
    }

    /**
     * @param int[] $photos
     *
     * @return string[]
     */
    private function getSlides(array $photos): array
    {
        return array_map(function (string $id) {
            $args = [$id, 'full'];

            return [
                'src'    => wp_get_attachment_image_url(...$args),
                'markup' => wp_get_attachment_image(...$args),
            ];
        }, $photos);
    }

    private function getBoards(BranchInterface $branch): array
    {
        $regionalBoard = $branch->getRegionalBoard();
        $localBoards = $this->getBranchLocalBoards->execute($branch);

        if (empty($localBoards)) {
            if ($regionalBoard === null) {
                return [];
            }

            return [
                [
                    'type'    => 'Zarząd',
                    'members' => $regionalBoard->getMembers(),
                ],
            ];
        }

        if ($localBoards instanceof BoardInterface) {
            /** @var BoardInterface $localBoards */
            $localSection = [
                'type'    => 'Zarząd grupy',
                'members' => $localBoards->getMembers(),
            ];

            if ($regionalBoard && $regionalBoard->getId() === $localBoards->getId()) {
                return [
                    [
                        'type'    => 'Zarząd',
                        'members' => $regionalBoard->getMembers(),
                    ],
                ];
            }
        } else {
            $localSection = [
                'type'      => 'Zarządy grup',
                'subboards' => $localBoards,
            ];
        }

        $regionalSection = $regionalBoard ? [
            'type'    => 'Zarząd regionalny',
            'members' => $regionalBoard->getMembers(),
        ] : null;

        return array_filter([$localSection, $regionalSection], function (?array $section) {
            return $section;
        });
    }

    private function getAboutTbsa(int $branchId): array
    {
        $aboutTbsaField = get_field('about_tbsa', 'option');
        $sections = $aboutTbsaField['sections'];
        $galleryField = get_field('branch_page_text_photos', $branchId);

        $photos = array_map(function (array $photoData) {
            return $photoData['ID'];
        }, is_array($galleryField) ? $galleryField : []);

        return [
            'heading'  => $aboutTbsaField['heading'],
            'sections' => array_map(function (array $section, int $index) use ($photos) {
                return [
                    'image' => isset($photos[$index])
                        ? wp_get_attachment_image($photos[$index], 'full')
                        : wp_get_attachment_image($section['image']['ID'], 'full'),
                    'left'  => $section['left'],
                    'right' => $section['right'],
                ];
            }, $sections, array_keys($sections)),
        ];
    }

    private function getContactData(?BranchInterface $branch): ?array
    {
        $entrepreneur = $branch->getContact();

        return $entrepreneur ? [
            'photo' => $entrepreneur->getPhoto(),
            'name'  => $entrepreneur->getName(),
            'links' => array_map(function (Link $link) {
                $needsEncoding = in_array($link->type, ['email', 'tel']);
                $key = random_int(0, 999);

                return [
                    'key'     => $needsEncoding ? $key : null,
                    'href'    => $needsEncoding
                        ? ShitCrypt::encrypt($link->href, $key)
                        : $link->href,
                    'content' => $link->content,
                ];
            }, array_filter($entrepreneur->getLinks(), function (Link $link) {
                return in_array($link->type, ['email', 'tel']);
            })),
        ] : null;
    }

    private function getNearestEventUrl(): ?string
    {
        $query = new EventQuery();
        $query->setBranches(new BranchesInQuery(get_the_ID()));
        $query->setScope(EventQueryScope::FUTURE());
        $query->setLimit(1);
        $query->setExternal(false);

        $events = $this->eventRepository->query($query);

        return $events ? home_url('/wp-json/tbsa/v1/booking-form?event_id=' . $events[0]->getId()) : null;
    }
}
