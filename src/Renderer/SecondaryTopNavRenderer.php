<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\MyAccountPage\AccountManagement;
use MobilitySoft\TBSA\MyAccountPage\MyAccountPage;
use MobilitySoft\TBSA\Repository\AcfExternalLinksRepositoryInterface;
use Twig\Environment as Twig;

class SecondaryTopNavRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var AcfExternalLinksRepositoryInterface
     */
    private $linksRepository;

    public function __construct(
        Twig $twig,
        AcfExternalLinksRepositoryInterface $linksRepository
    ) {
        $this->twig = $twig;
        $this->linksRepository = $linksRepository;
    }

    public function render(): string
    {
        $links = $this->linksRepository->getAll('option');
        $showAccountLinks = MyAccountPage::isEnabled();

        $data = [
            'links'    => $links,
            'switcher' => (new LanguageSwitcherRenderer($this->twig))->render(),
        ];

        $myAccountUrl = wc_get_account_endpoint_url('dashboard');
        if (is_user_logged_in()) {
            if ($showAccountLinks) {
                $data['myAccount'] = ['url' => $myAccountUrl];
            }
            $data['logout'] = ['url' => wp_logout_url(home_url($_SERVER['REQUEST_URI']))];
        } elseif ($showAccountLinks) {
            $data['login'] = ['url' => $myAccountUrl];
        }

        return do_shortcode($this->twig->render(
            'secondaryTopNav.twig',
            array_merge($data, ['modifier' => 'desktop'])
        ));
    }
}
