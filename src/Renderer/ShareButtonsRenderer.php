<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Interfaces\GetLinkIconInterface;
use Twig\Environment as Twig;

/**
 * Class ShareButtonsRenderer
 * @package MobilitySoft\TBSA\Renderer
 */
final class ShareButtonsRenderer implements Renderer
{
    /**
     * @var Twig
     */

    private $twig;

    /**
     * @var GetLinkIconInterface
     */
    private $getLinkIcon;

    /**
     * ShareButtonsRenderer constructor.
     *
     * @param Twig $twig
     * @param GetLinkIconInterface $getLinkIcon
     */
    public function __construct(Twig $twig, GetLinkIconInterface $getLinkIcon)
    {
        $this->twig        = $twig;
        $this->getLinkIcon = $getLinkIcon;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $permalink = urlencode(get_the_permalink());

        return do_shortcode($this->twig->render('shareButtons.twig', [
            'buttons' => [
                [
                    'medium' => 'twitter',
                    'icon'   => $this->getLinkIcon->execute('twitter'),
                    'label'  => 'Tweet',
                    'href'   => "https://twitter.com/share?url=$permalink",
                ],
                [
                    'medium' => 'facebook',
                    'icon'   => $this->getLinkIcon->execute('facebook'),
                    'label'  => 'Udostępnij',
                    'href'   => "https://www.facebook.com/sharer/sharer.php?u=$permalink",
                ]
            ]
        ]));
    }
}
