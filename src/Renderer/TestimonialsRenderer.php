<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Repository\BranchRepository;
use MobilitySoft\TBSA\Repository\EntrepreneurRepository;
use Twig\Environment as Twig;

/**
 * Class TestimonialsRenderer
 * @package MobilitySoft\TBSA\Renderer
 */
final class TestimonialsRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * TestimonialsRenderer constructor.
     *
     * @param Twig $twig
     * @param EntrepreneurRepository $entrepreneurRepository
     * @param BranchRepository $branchRepository
     */
    public function __construct(
        Twig $twig,
        EntrepreneurRepository $entrepreneurRepository,
        BranchRepository $branchRepository
    ) {
        $this->twig                   = $twig;
        $this->entrepreneurRepository = $entrepreneurRepository;
        $this->branchRepository       = $branchRepository;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        $videos = array_map(function (array $item) {
            return [
                'video'  => $item['video'],
                'author' => $this->getAuthorInfo($item['author'][0]->ID),
            ];
        }, get_field('video_testimonials', 'option'));

        $textTestimonials = array_map(function (array $item) {
            return [
                'content' => $item['content'],
                'author'  => $this->getAuthorInfo($item['author'][0]->ID),
            ];
        }, get_field('text_testimonials', 'option'));
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

        return do_shortcode($this->twig->render('testimonials.twig', [
            'videoTestimonials' => $videos,
            'textTestimonials'  => $textTestimonials
        ]));
    }

    /**
     * @param string|null $entrepreneurId
     *
     * @return array|null
     */
    private function getAuthorInfo(?string $entrepreneurId): ?array
    {
        if ( ! $entrepreneurId) {
            return null;
        }
        $entrepreneur      = $this->entrepreneurRepository->getById($entrepreneurId);
        $businessRelations = $entrepreneur->getBusinessRelations();
        $business          = $businessRelations ? $businessRelations[0]->getBusiness() : null;
        $branches          = $this->branchRepository->getByEntrepreneur($entrepreneur);
        $branch            = $branches ? $branches[0] : null;

        return [
            'photo'    => $entrepreneur->getPhoto(),
            'name'     => $entrepreneur->getName(),
            'business' => isset($business) ? [
                'name' => $business->getName(),
                'url'  => $business->getUrl(),
            ] : null,
            'branch'   => $branch ? [
                'name' => $branch->getName(),
                'url'  => $branch->getUrl(),
            ] : null,
        ];
    }
}
