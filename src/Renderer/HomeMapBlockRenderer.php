<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Entity\BranchInterface;
use MobilitySoft\TBSA\Interfaces\StringToIntFunction;
use Twig\Environment as Twig;

final class HomeMapBlockRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var array
     */
    private $getQuantity;

    /**
     * @var BranchInterface[]
     */
    private $branches;

    /**
     * MapRenderer constructor.
     *
     * @param Twig $twig
     * @param StringToIntFunction $getQuantity
     * @param BranchInterface[] $branches
     */
    public function __construct(Twig $twig, StringToIntFunction $getQuantity, array $branches)
    {
        $this->twig        = $twig;
        $this->getQuantity = $getQuantity;
        $this->branches    = $branches;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return do_shortcode($this->twig->render('homeMapBlock.twig', [
            'prompt'   => get_field('home_map_block_prompt', 'option'),
            'branches' => $this->branches,
            'regions'  => $this->getRegions(),
        ]));
    }

    /**
     * @return array
     */
    private function getRegions(): array
    {
        $regions = require(get_theme_file_path('src/map-regions.php'));

        foreach ($regions as $code => &$region) {
            $quantity = $this->getQuantity->execute($code);
            if ($quantity) {
                $region['badge']['text'] = $quantity;
            } else {
                $region['disabled'] = true;
            }
        }

        return $regions;
    }
}
