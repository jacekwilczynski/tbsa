<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;

class SloganButtonsRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * SloganButtonsRenderer constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function render(): string
    {
        return do_shortcode($this->twig->render('sloganButtons.twig', [
            'buttons' => get_field('tbsa_slogan_buttons', 'option'),
        ]));
    }
}
