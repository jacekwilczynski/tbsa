<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;

/**
 * Class LanguageSwitcherRenderer
 * @package MobilitySoft\TBSA\Renderer
 */
final class LanguageSwitcherRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * LanguageSwitcherRenderer constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        if ( ! function_exists('icl_get_languages')) {
            return '';
        }

        /** @noinspection PhpDeprecationInspection */
        $icls = icl_get_languages('skip_missing=0');
        if (count($icls) < 2) {
            return '';
        }

        $languages = [];
        foreach ($icls as $icl) {
            $code      = $icl['language_code'];
            $url       = $icl['url'];
            $isCurrent = $code === ICL_LANGUAGE_CODE;
            $language  = [
                'code' => $code,
                'url'  => $url
            ];
            $method    = $isCurrent ? 'array_unshift' : 'array_push';
            $method($languages, $language);
        }

        return do_shortcode($this->twig->render('languageSwitcher.twig', [
            'languages' => $languages
        ]));
    }
}
