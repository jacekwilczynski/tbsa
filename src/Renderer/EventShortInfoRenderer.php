<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\GetFormattedEventStartTime;
use Twig\Environment as Twig;

final class EventShortInfoRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var GetFormattedEventStartTime
     */
    private $getFormattedEventStartTime;

    /**
     * HomeEventShortRenderer constructor.
     *
     * @param Twig $twig
     * @param GetFormattedEventStartTime $getFormattedEventStartTime
     */
    public function __construct(Twig $twig, GetFormattedEventStartTime $getFormattedEventStartTime)
    {
        $this->twig                       = $twig;
        $this->getFormattedEventStartTime = $getFormattedEventStartTime;
    }

    /**
     * @param EventInterface $event
     *
     * @return string
     */
    public function renderEvent(EventInterface $event): string
    {
        return do_shortcode($this->twig->render('eventShortInfo.twig', [
            'id'   => $event->getId(),
            'icon' => $event->getCategory() ? $event->getCategory()->getIcon() : null,
            'text' => $this->getEventShortText($event),
        ]));
    }

    /**
     * @param EventInterface $event
     *
     * @return string
     */
    private function getEventShortText(EventInterface $event): string
    {
        $startTime = $this->getFormattedEventStartTime->execute($event);
        $result    = $startTime ? $startTime . ' ' : '';

        if ($event->getCategory()) {
            $result .= $event->getCategory()->getShortTitle();
        } else {
            $result .= $event->getTitle();
        }

        $location = $event->getLocation();
        if ($location && $location->getTown()) {
            $result .= ' | ' . $location->getTown();
        }

        return $result;
    }
}
