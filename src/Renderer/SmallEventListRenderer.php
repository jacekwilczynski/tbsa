<?php

namespace MobilitySoft\TBSA\Renderer;

use DateTimeImmutable;
use MobilitySoft\TBSA\Entity\EventInterface;
use Twig\Environment as Twig;

class SmallEventListRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var EventShortInfoRenderer
     */
    private $eventShortInfoRenderer;

    /**
     * SmallEventListRenderer constructor.
     *
     * @param Twig $twig
     * @param EventShortInfoRenderer $eventShortInfoRenderer
     */
    public function __construct(Twig $twig, EventShortInfoRenderer $eventShortInfoRenderer)
    {
        $this->twig = $twig;
        $this->eventShortInfoRenderer = $eventShortInfoRenderer;
    }

    /**
     * @param EventInterface[] $events
     *
     * @return string
     */
    public function renderEvents(array $events): string
    {
        $now = new DateTimeImmutable();
        [$futureEvents, $pastEvents] = array_split($events, function (EventInterface $event) use ($now) {
            return $event->getTimespan()->getStart() > $now;
        });

        $parts = [];

        if ($futureEvents) {
            $parts[] = [
                'heading' => 'Aktualne wydarzenia',
                'events'  => $this->renderList($futureEvents),
            ];
        }

        if ($pastEvents) {
            $parts[] = [
                'heading' => 'Minione wydarzenia',
                'events'  => $this->renderList($pastEvents),
            ];
        }

        return do_shortcode($this->twig->render('smallEventList.twig', ['parts' => $parts]));
    }

    /**
     * @param EventInterface[] $events
     *
     * @return string
     */
    private function renderList(array $events): string
    {
        return implode('', array_map(function (EventInterface $event) {
            return $this->eventShortInfoRenderer->renderEvent($event);
        }, $events));
    }
}
