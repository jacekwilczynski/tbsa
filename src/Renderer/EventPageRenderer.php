<?php

namespace MobilitySoft\TBSA\Renderer;

use DateTimeInterface;
use InvalidArgumentException;
use MobilitySoft\TBSA\Common\Application\Event\GetFormattedEventPrice;
use MobilitySoft\TBSA\Domain\Booking\Booking;
use MobilitySoft\TBSA\Service\PrintableBookings;
use MobilitySoft\TBSA\Service\TranslateMonth;
use MobilitySoft\TBSA\View\EventPage\Domain\Event;
use MobilitySoft\TBSA\View\EventPage\Domain\EventRepository;
use Twig\Environment as Twig;

class EventPageRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * @var PrintableBookings
     */
    private $printableBookings;

    public function __construct(
        Twig $twig,
        EventRepository $eventRepository,
        PrintableBookings $printableBookings
    ) {
        $this->twig = $twig;
        $this->eventRepository = $eventRepository;
        $this->printableBookings = $printableBookings;
    }

    /**
     * @param int|Event $event
     * @param bool $titleAsLink
     */
    public function render($event, bool $titleAsLink = false): string
    {
        [$event, $eventId, $canViewBookings] = $this->parseInputEvent($event);

        $start = $event->getTimespan()->getStart();
        $end = $event->getTimespan()->getEnd();

        $args = [
            'titleAsLink' => $titleAsLink,
            'event'       => $event,
            'start'       => $this->formatTime($start),
            'end'         => $end ? $this->formatTime($end) : null,
        ];

        if ($event->isBookable()) {
            $formattedPrice = $event->getPricing()->accept(new GetFormattedEventPrice(true));
            $args += [
                'formattedPrice' => $formattedPrice,
                'buyText'        => $formattedPrice ? __('Kup bilet') : __('Zapisz się bezpłatnie'),
                'buyUrl'         => home_url('/wp-json/tbsa/v1/booking-form?event_id=' . $event->getId()),
            ];
        }

        if ($canViewBookings) {
            $bookings = $event->getBookings();
            $args['bookings'] = [
                'pdfUrl'         => $this->printableBookings->getPrintUrl($eventId),
                'bookings'       => $bookings,
                'numberOfPeople' => array_reduce($bookings, function (int $count, Booking $booking) {
                    return $count + $booking->getNumberOfPlaces();
                }, 0),
            ];
        }

        return do_shortcode($this->twig->render('eventPage.twig', $args));
    }

    /**
     * @param int|Event $event
     *
     * @return array [Event, int, bool]
     */
    private function parseInputEvent($event): array
    {
        $eventId = $event instanceof Event ? $event->getId() : $event;
        $canViewBookings = current_user_can('edit_event', $eventId);

        if (!$event instanceof Event) {
            if (is_int($eventId)) {
                $event = $canViewBookings
                    ? $this->eventRepository->getEventWithBookings($eventId)
                    : $this->eventRepository->getEvent($eventId);
            } else {
                throw new InvalidArgumentException('$event must be an instance of Event or int.');
            }
        }

        return [$event, $eventId, $canViewBookings];
    }

    private function formatTime(DateTimeInterface $dateTime): string
    {
        return TranslateMonth::execute($dateTime->format('j F Y, H:i'));
    }
}
