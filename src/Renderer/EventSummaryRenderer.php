<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Service\GetPriceString;
use MobilitySoft\TBSA\Service\IsEventBookable;
use MobilitySoft\TBSA\Service\TranslateMonth;
use Twig\Environment as Twig;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

final class EventSummaryRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * EventSummaryRenderer constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param EventInterface $event
     *
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderEvent(EventInterface $event): string
    {
        $isPaid = $event->getTickets() || $event->getPrice();
        $priceString = GetPriceString::execute($event);
        $isBookable = IsEventBookable::execute($event);

        if ($event->isExternal()) {
            $primaryButton = [
                'url'      => $event->getExternalUrl(),
                'text'     => 'Przejdź do strony wydarzenia',
                'external' => true,
            ];
        } else {
            $primaryButton = $isBookable ? [
                'url'  => home_url('/wp-json/tbsa/v1/booking-form?event_id=' . $event->getId()),
                'text' => $isPaid ? 'Kup bilet' : 'Zapisz się bezpłatnie',
            ] : null;
        }

        $secondaryButton = $event->isExternal() ? null : [
            'staticUrl' => get_the_permalink($event->getId()),
            'ajaxUrl'   => home_url('/wp-json/tbsa/v1/events?render_for=details&id=' . $event->getId()),
            'text'      => 'Czytaj więcej',
        ];

        return do_shortcode($this->twig->render('eventSummary.twig', [
            'image'      => get_the_post_thumbnail($event->getId(), 'large'),
            'date'       => TranslateMonth::execute($event->getTimespan()->getStart()->format('d F Y')),
            'address'    => $event->getLocation() ? $event->getLocation()->getAddress() : null,
            'title'      => $event->getTitle(),
            'excerpt'    => get_the_content($event->getId()),
            'price'      => $isPaid && $isBookable ? [
                'label'          => 'Cena',
                'formattedValue' => $priceString,
            ] : null,
            'moreButton' => $secondaryButton,
            'buyButton'  => $primaryButton,
        ]));
    }
}
