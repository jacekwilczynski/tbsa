<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;

/**
 * Class OfferPageRenderer
 * @package MobilitySoft\TBSA\Renderer
 */
final class OfferPageRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * OfferPageRenderer constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return do_shortcode($this->twig->render('offerPage.twig', ['pageTitle' => 'Oferta']));
    }
}
