<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\Event\EventQueryScope;
use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;
use MobilitySoft\TBSA\Entity\EventCategory;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\GetFormattedEventStartTime;
use MobilitySoft\TBSA\Repository\BranchRepository;
use MobilitySoft\TBSA\Repository\EventCategoryRepositoryInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\Service\IsEventBookable;
use Twig\Environment as Twig;

final class EventCategoryButtonsRenderer implements Renderer
{
    private $eventsLimit = 3;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var EventCategoryRepositoryInterface
     */
    private $eventCategoryRepository;

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var GetFormattedEventStartTime
     */
    private $getFormattedEventStartTime;

    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * EventCategoryButtonsRenderer constructor.
     *
     * @param Twig $twig
     * @param EventCategoryRepositoryInterface $eventCategoryRepository
     * @param EventRepositoryInterface $eventRepository
     * @param EventShortInfoRenderer $eventShortInfoRenderer
     * @param GetFormattedEventStartTime $getFormattedEventStartTime
     * @param BranchRepository $branchRepository
     */
    public function __construct(
        Twig $twig,
        EventCategoryRepositoryInterface $eventCategoryRepository,
        EventRepositoryInterface $eventRepository,
        GetFormattedEventStartTime $getFormattedEventStartTime,
        BranchRepository $branchRepository
    ) {
        $this->twig = $twig;
        $this->eventCategoryRepository = $eventCategoryRepository;
        $this->eventRepository = $eventRepository;
        $this->getFormattedEventStartTime = $getFormattedEventStartTime;
        $this->branchRepository = $branchRepository;
    }

    /**
     * @param array $atts
     *
     * @return string
     */
    public function render($atts = []): string
    {
        if (!is_array($atts)) {
            $atts = [];
        }

        return do_shortcode($this->twig->render('eventCategoryButtons.twig', [
            'style'           => $atts['style'] ?? null,
            'extraClass'      => $atts['extra_class'] ?? '',
            'accordion'       => $atts['accordion'] ?? true,
            'horizontal'      => $atts['horizontal'] ?? false,
            'items'           => $this->getItems($atts),
            'branch'          => isset($atts['branch_link']) ? $this->getBranchData($atts['branch']) : null,
            'noEventsMessage' => $atts['no_events_message'] ?? '',
            'badge'           => ['text' => 'Kup bilet'],
        ]));
    }

    /**
     * @param array $atts
     *
     * @return array[]
     */
    private function getItems(array $atts): array
    {
        $allCategories = $this->eventCategoryRepository->getAll();

        $categoriesInBranch = array_map(function (EventCategory $category) use ($atts) {
            $branchId = $atts['branch'] ?? null;
            $eventsData = $this->getEvents($category, $branchId);
            $archiveUrl = get_post_type_archive_link('event') . "?action=search_events&category={$category->getId()}";
            if ($branchId) {
                $archiveUrl .= "&branch=$branchId";
            }

            $plural = get_field('title_plural_genitive', "term_{$category->getId()}") ?? __('wydarzeń');

            return [
                'id'         => $category->getId(),
                'icon'       => $category->getIcon(),
                'label'      => $category->getTitle(),
                'events'     => $eventsData['events'],
                'moreEvents' => $eventsData['haveMore'] ? [
                    'text' => __('Więcej') . " $plural",
                    'url'  => $archiveUrl,
                ] : null,
            ];
        }, $allCategories);

        return array_filter($categoriesInBranch, function (array $data) {
            return count($data['events']);
        });
    }

    /**
     * @param EventCategory $category
     * @param string|null $branchId
     *
     * @return array[]
     */
    private function getEvents(EventCategory $category, ?string $branchId): array
    {
        $query = new EventQuery();

        if (!empty($branchId)) {
            $query->setBranches(new BranchesInQuery($branchId));
        }
        $query->setCategories([$category->getId()]);
        $query->setLimit($this->eventsLimit + 1);
        $query->setScope(EventQueryScope::FUTURE());

        $events = $this->eventRepository->query($query);

        return [
            'events'   => array_slice(array_map(function (EventInterface $event) {
                $date = $event->getTimespan()->getStart()->format('j.m');
                $time = $this->getFormattedEventStartTime->execute($event);
                $locationName = $event->getLocation() ? $event->getLocation()->getTitle() : null;

                return [
                    'isBookable' => IsEventBookable::execute($event),
                    'text'       => $date . ($time ? ", godz. $time" : '') . ($locationName ? " w: $locationName" : ''),
                    'ajaxUrl'    => $event->isExternal() ? '' : home_url('/wp-json/tbsa/v1/events?render_for=details&id=' . $event->getId()),
                    'isExternal' => $event->isExternal(),
                    'staticUrl'  => $event->isExternal() ? $event->getExternalUrl() : get_the_permalink($event->getId()),
                ];
            }, $events), 0, $this->eventsLimit),
            'haveMore' => count($events) > $this->eventsLimit,
        ];
    }

    /**
     * @param string $branchId
     *
     * @return array
     */
    private function getBranchData(string $branchId): array
    {
        $branch = $this->branchRepository->getById($branchId);

        if (!$branch) {
            return null;
        }

        $name = $branch->getName();
        $locative = $name ? $name->locative ?? $name->nominative : null;

        return $locative ? [
            'url'      => $branch->getUrl(),
            'moreText' => 'Więcej o ' . $locative,
        ] : null;
    }
}
