<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Interfaces\GetLinkIconInterface;

final class GetLinkIcon implements GetLinkIconInterface
{
    public function execute(string $type): string
    {
        $iconClass = $this->getIconClass($type);

        return '<span class="' . $iconClass . '"></span>';
    }

    private function getIconClass(string $type): string
    {
        $map = [
            'www'       => 'divi-icon divi-icon--globe-2',
            'email'     => 'divi-icon divi-icon--mail-alt',
            'tel'       => 'divi-icon divi-icon--phone',
            'fax'       => 'fas fa-fax',
            'facebook'  => 'divi-icon divi-icon--facebook',
            'linkedin'  => 'divi-icon divi-icon--linkedin',
            'youtube'   => 'divi-icon divi-icon--youtube',
            'twitter'   => 'divi-icon divi-icon--twitter',
            'instagram' => 'divi-icon divi-icon--instagram',
        ];

        return $map[$type];
    }
}
