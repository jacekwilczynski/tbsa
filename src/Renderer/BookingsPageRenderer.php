<?php

namespace MobilitySoft\TBSA\Renderer;

use DateInterval;
use DateTimeImmutable;
use MobilitySoft\TBSA\Domain\Booking\Booking;
use MobilitySoft\TBSA\Domain\Booking\BookingRepository;
use MobilitySoft\TBSA\Entity\Branch;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Service\GetDays;
use Twig\Environment as Twig;
use WP_User;

class BookingsPageRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var BookingRepository
     */
    private $bookingRepository;

    /**
     * @var GetDays
     */
    private $getDays;

    public function __construct(
        Twig $twig,
        BookingRepository $bookingRepository,
        GetDays $getDays
    ) {
        $this->twig = $twig;
        $this->bookingRepository = $bookingRepository;
        $this->getDays = $getDays;
    }

    /**
     * @param int $year
     * @param int $month
     * @param Branch[] $branches
     * @param int|null $currentBranchId
     * @param bool $showUnavailables
     *
     * @return string
     */
    public function render(
        int $year,
        int $month,
        array $branches,
        ?int $currentBranchId,
        bool $showUnavailables
    ): string {
        $days = $this->getDays(
            $year,
            $month,
            array_map(function (Branch $branch) {
                return $branch->getId();
            }, $branches),
            $currentBranchId,
            $showUnavailables
        );

        $totalNumberOfPeople = 0;
        $totalIncome = 0;
        foreach ($days as $day) {
            foreach ($day['events'] as $event) {
                if (is_numeric($event['numberOfPeople'])) {
                    $totalNumberOfPeople += $event['numberOfPeople'];
                }
                if (is_numeric($event['income'])) {
                    $totalIncome += $event['income'];
                }
            }
        }

        return $this->twig->render('bookingsPage.twig', [
            'year'                => [
                'min'   => 2019,
                'max'   => ((int) date('Y')) + 1,
                'value' => $year,
            ],
            'month'               => [
                'options' => [
                    1  => __('styczeń'),
                    2  => __('luty'),
                    3  => __('marzec'),
                    4  => __('kwiecień'),
                    5  => __('maj'),
                    6  => __('czerwiec'),
                    7  => __('lipiec'),
                    8  => __('sierpień'),
                    9  => __('wrzesień'),
                    10 => __('październik'),
                    11 => __('listopad'),
                    12 => __('grudzień'),
                ],
                'value'   => $month,
            ],
            'branch'              => [
                'options' => [
                                 '' => 'wszystkie',
                                 -1 => 'bez konkretnego towarzystwa',
                             ] + $this->getBranchSelectOptions($branches),
                'value'   => $currentBranchId ?? '',
            ],
            'showUnavailables'    => $showUnavailables,
            'message'             => $days ? null : __('Brak wydarzeń spełniających podane kryteria.'),
            'days'                => $days,
            'totalNumberOfPeople' => $totalNumberOfPeople,
            'totalIncome'         => wc_price($totalIncome),
        ]);
    }

    /**
     * @param int $year
     * @param int $month
     * @param int[] $branchIds
     * @param int|null $branchId
     * @param bool $showUnavailables
     *
     * @return array[]
     */
    private function getDays(int $year, int $month, array $branchIds, ?int $branchId, bool $showUnavailables): array
    {
        $queryArgs = ['branch' => $branchId ?? $branchIds];
        if (!$showUnavailables) {
            $queryArgs['filter'] = [__CLASS__, 'userCanSeeBookings'];
        }

        $date = new DateTimeImmutable("$year-$month-01");
        $days = $this->getDays->execute(
            $queryArgs,
            $date,
            $date->add(new DateInterval('P1M')),
            function (EventInterface $event): array {
                $bookings = $this->bookingRepository->getByEventId($event->getId());
                $branch = $event->getBranch();
                $userHasAccess = self::userCanSeeBookings($event);
                $numberOfPeople = $userHasAccess ? $this->computeNumberOfPeople($bookings) : 'brak dostępu';
                $income = $userHasAccess ? $this->computeIncome($bookings) : 'brak dostępu';

                return [
                    'branch'          => $branch && $branch->getLocation() ? $branch->getLocation()->getTown() : '-',
                    'title'           => $event->getTitle(),
                    'permalink'       => get_the_permalink($event->getId()),
                    'numberOfPeople'  => $numberOfPeople,
                    'income'          => $income,
                    'formattedIncome' => is_numeric($income) ? wc_price($income) : $income,
                ];
            }
        );

        // Filter days that have no events
        return array_filter($days, function (array $day) {
            return $day['events'];
        });
    }

    /**
     * @param EventInterface|int $event
     * @param WP_User|int|null $user
     *
     * @return bool
     */
    public static function userCanSeeBookings($event, $user = null): bool
    {
        if (is_int($user)) {
            $user = get_userdata($user);
        }

        if (empty($user)) {
            $user = get_userdata(get_current_user_id());
        }

        $eventId = $event instanceof EventInterface ? $event->getId() : $event;

        return $user->has_cap('edit_event', $eventId);
    }

    /**
     * @param Booking[] $bookings
     *
     * @return int
     */
    private function computeNumberOfPeople($bookings): int
    {
        return array_sum(array_map(function (Booking $booking) {
            return $booking->getNumberOfPlaces();
        }, $bookings));
    }

    /**
     * @param Booking[] $bookings
     *
     * @return float
     */
    private function computeIncome(array $bookings): float
    {
        $totals = array_filter(array_map(function (Booking $booking) {
            $orderId = $booking->getOrderId();
            if ($orderId === null) {
                return 0;
            }

            $order = wc_get_order($orderId);
            if (!$order) {
                return 0;
            }

            return $order->get_total();
        }, $bookings));

        return array_sum($totals);
    }

    /**
     * @param Branch[] $branches
     *
     * @return array
     */
    private function getBranchSelectOptions(array $branches): array
    {
        $options = [];
        foreach ($branches as $branch) {
            $options[$branch->getId()] = $branch->getName();
        }

        return $options;
    }

    public function renderError(string $message): string
    {
        return $this->twig->render('bookingsPage.twig', ['error' => $message]);
    }
}
