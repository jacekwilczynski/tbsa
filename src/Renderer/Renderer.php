<?php

namespace MobilitySoft\TBSA\Renderer;

interface Renderer
{
    public function render(): string;
}
