<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;

class PostsPageRenderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var PostsGridRenderer
     */
    private $postsGridRenderer;

    /**
     * PostsPageRenderer constructor.
     *
     * @param Twig $twig
     * @param PostsGridRenderer $postsGridRenderer
     */
    public function __construct(Twig $twig, PostsGridRenderer $postsGridRenderer)
    {
        $this->twig              = $twig;
        $this->postsGridRenderer = $postsGridRenderer;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        global $wp_query;
        $posts        = $wp_query->get_posts();
        $postTypeSlug = $wp_query->get('post_type');
        if ( ! $postTypeSlug) {
            $postTypeSlug = 'post';
        }
        $options      = get_field('tbsa_archives', 'option')[$postTypeSlug] ?? [];
        $heading      = $options['heading'] ?? get_post_type_labels(get_post_type_object($postTypeSlug))->name;
        $noneMessage  = $options['none_message'] ?? '';

        return do_shortcode($this->twig->render('postsPage.twig', [
            'heading'    => $heading,
            'content'    => $posts ? $this->postsGridRenderer->renderPosts($posts, 2) : $noneMessage,
            'pagination' => get_the_posts_pagination([
                'prev_text' => '8',
                'next_text' => '9',
            ]),
        ]));
    }
}
