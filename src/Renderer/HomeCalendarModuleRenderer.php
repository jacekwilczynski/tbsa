<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\Event\EventQueryScope;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use Twig\Environment as Twig;

final class HomeCalendarModuleRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * HomeCalendarModuleRenderer constructor.
     *
     * @param Twig $twig
     * @param EventRepositoryInterface $eventRepository
     */
    public function __construct(Twig $twig, EventRepositoryInterface $eventRepository)
    {
        $this->twig            = $twig;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $query = new EventQuery();
        $query->setScope(EventQueryScope::FUTURE());
        $query->setLimit(1);
        $nearestEventArray = $this->eventRepository->query($query);

        if (isset($nearestEventArray[0])) {
            $nearestEvent = $nearestEventArray[0];
            $startTime    = $nearestEvent->getTimespan()->getStart();
            $year         = $startTime->format('Y');
            $month        = $startTime->format('m');
        }

        return do_shortcode($this->twig->render('homeCalendarModule.twig', [
            'year'  => $year ?? null,
            'month' => $month ?? null,
        ]));
    }
}
