<?php

namespace MobilitySoft\TBSA\Renderer;

use Twig\Environment as Twig;

/**
 * Class AffiliatesRenderer
 * @package MobilitySoft\TBSA\Renderer
 */
final class AffiliatesRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * AffiliatesRenderer constructor.
     *
     * @param Twig $twig
     */
    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return do_shortcode($this->twig->render('affiliates.twig', [
            'heading' => 'Partnerzy',
        ]));
    }
}
