<?php

namespace MobilitySoft\TBSA\Renderer;

use MobilitySoft\TBSA\DTO\InitiativeCategory;
use MobilitySoft\TBSA\Repository\InitiativeRepository;
use Twig\Environment as Twig;

final class InitiativesRenderer implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var InitiativeRepository
     */
    private $initiativesRepository;

    public function __construct(Twig $twig, InitiativeRepository $initiativesRepository)
    {
        $this->twig = $twig;
        $this->initiativesRepository = $initiativesRepository;
    }

    public function render(): string
    {
        $categories = $this->initiativesRepository->getCategories();
        $categoryFilters = $this->getFilters($categories);

        return do_shortcode($this->twig->render('initiatives.twig', [
            'heading'       => 'Inicjatywy',
            'goToPageText'  => 'Przejdź na stronę',
            'joinText'      => 'Weź udział',
            'subscribeText' => 'Subskrybuj',
            'filters'       => $categoryFilters,
            'byCategory'    => $this->getByCategory($categories),
        ]));
    }

    /**
     * @param InitiativeCategory[] $categories
     */
    private function getFilters(array $categories): array
    {
        return array_merge(
            array_map(function (InitiativeCategory $category) {
                return [
                    'label' => $category->title,
                    'value' => $category->title,
                ];
            }, $categories)
        );
    }

    /**
     * @param InitiativeCategory[] $categories
     */
    private function getByCategory(array $categories): array
    {
        return array_map(function (InitiativeCategory $category) {
            $initiatives = $this->initiativesRepository->getByCategoryTitle($category->title);

            return [
                'value'       => $category->title,
                'heading'     => $category->longTitle,
                'description' => $category->description,
                'initiatives' => $initiatives,
            ];
        }, $categories);
    }
}
