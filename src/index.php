<?php

namespace MobilitySoft\TBSA;

use MobilitySoft\TBSA\Admin\BoardPostType;
use MobilitySoft\TBSA\Admin\BranchPostType;
use MobilitySoft\TBSA\Admin\BusinessPostType;
use MobilitySoft\TBSA\Admin\EntrepreneurPostType;
use MobilitySoft\TBSA\Admin\GroupPostType;
use MobilitySoft\TBSA\Admin\IndustryTaxonomy;
use MobilitySoft\TBSA\Admin\InitiativePostType;
use MobilitySoft\TBSA\Admin\JobOfferPostType;
use MobilitySoft\TBSA\Admin\LocationTaxonomy;
use MobilitySoft\TBSA\Admin\MagazinePostType;
use MobilitySoft\TBSA\Admin\OptionsPage;
use MobilitySoft\TBSA\Admin\TalkSubmissionPostType;
use MobilitySoft\TBSA\Admin\TalkTemplatePostType;
use MobilitySoft\TBSA\Common\Infrastructure\Event\MigrateEventSpeakersToMulti;
use MobilitySoft\TBSA\EventSearch\EventSearchBundle;
use MobilitySoft\TBSA\FilterHandler\SecondaryTopNavFilterHandler;
use MobilitySoft\TBSA\MyAccountPage\MyAccountPage;
use MobilitySoft\TBSA\OrderBundle\OrderBundle;
use MobilitySoft\TBSA\PageController\BookingsPageController;
use MobilitySoft\TBSA\PageController\EventPageController;
use MobilitySoft\TBSA\Renderer\AffiliatesRenderer;
use MobilitySoft\TBSA\Renderer\BranchPageRenderer;
use MobilitySoft\TBSA\Renderer\BusinessSearchPageRenderer;
use MobilitySoft\TBSA\Renderer\EventCategoryButtonsRenderer;
use MobilitySoft\TBSA\Renderer\EventPageRenderer;
use MobilitySoft\TBSA\Renderer\GoogleMapRenderer;
use MobilitySoft\TBSA\Renderer\HomeCalendarModuleRenderer;
use MobilitySoft\TBSA\Renderer\HomeMapBlockRenderer;
use MobilitySoft\TBSA\Renderer\InitiativesRenderer;
use MobilitySoft\TBSA\Renderer\MagazinePageRenderer;
use MobilitySoft\TBSA\Renderer\OfferPageRenderer;
use MobilitySoft\TBSA\Renderer\PostsGridRenderer;
use MobilitySoft\TBSA\Renderer\PostsPageRenderer;
use MobilitySoft\TBSA\Renderer\ShareButtonsRenderer;
use MobilitySoft\TBSA\Renderer\SloganButtonsRenderer;
use MobilitySoft\TBSA\Renderer\TestimonialsRenderer;
use MobilitySoft\TBSA\Renderer\WhatWeAreRenderer;
use MobilitySoft\TBSA\Repository\BranchRepository;
use MobilitySoft\TBSA\Repository\GroupRepository;
use MobilitySoft\TBSA\RestController\BookingFormController;
use MobilitySoft\TBSA\RestController\EventCategoryRestController;
use MobilitySoft\TBSA\RestController\EventCheckoutController;
use MobilitySoft\TBSA\RestController\EventPresentationController;
use MobilitySoft\TBSA\RestController\EventRestController;
use MobilitySoft\TBSA\RestRoute\BookingFormRestRoute;
use MobilitySoft\TBSA\RestRoute\BookingRestRoute;
use MobilitySoft\TBSA\RestRoute\EventCategoryRestRoute;
use MobilitySoft\TBSA\RestRoute\EventPresentationRoute;
use MobilitySoft\TBSA\RestRoute\EventRestRoute;
use MobilitySoft\TBSA\SalesManago\BookingCount\BuyerMigration;
use MobilitySoft\TBSA\SalesManago\SalesManagoBundle;
use MobilitySoft\TBSA\Service\BoardMemberPermissions;
use MobilitySoft\TBSA\Service\ContainerFactory;
use MobilitySoft\TBSA\Service\EmSearchCustomizer;
use MobilitySoft\TBSA\Service\PrintableBookings;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionQuery;
use MobilitySoft\TBSA\Talk\Domain\Submission\SubmissionRepository;
use MobilitySoft\TBSA\Talk\TalkBundle;
use Twig\Environment as Twig;

require_once __DIR__ . '/config-utils.php';
require_once __DIR__ . '/constants.php';

$container = (new ContainerFactory())->getContainer();

// Services still needed by code not using the DI container
$twig = $container->get(Twig::class);
$branchRepository = $container->get(BranchRepository::class);
$groupRepository = $container->get(GroupRepository::class);

// Configure Wordpress
ConfigUtils\enableQueryingRepeatedRelationshipFields(['businesses']);
OptionsPage::configure();
InitiativePostType::configure(INITIATIVE_POST_TYPE_SLUG, 21);
MagazinePostType::configure(MAGAZINE_POST_TYPE_SLUG, 21);
BoardPostType::configure(BOARD_POST_TYPE_SLUG, 21);
BranchPostType::configure(BRANCH_POST_TYPE_SLUG, 21, $branchRepository);
GroupPostType::configure(NETWORKING_GROUP_POST_TYPE_SLUG, 21, $groupRepository);
EntrepreneurPostType::configure(ENTREPRENEUR_POST_TYPE_SLUG, 21);
BusinessPostType::configure(BUSINESS_POST_TYPE_SLUG, 21);
JobOfferPostType::configure(JOB_OFFER_POST_TYPE_SLUG, 21);
TalkSubmissionPostType::configure(TALK_SUBMISSION_POST_TYPE_SLUG, 21, $container);
$container->get(TalkTemplatePostType::class)->configure(TALK_TEMPLATE_POST_TYPE_SLUG, 21);
IndustryTaxonomy::configure(INDUSTRY_TAXONOMY_SLUG, [
    BUSINESS_POST_TYPE_SLUG,
    EVENT_POST_TYPE_SLUG,
    TALK_TEMPLATE_POST_TYPE_SLUG,
]);
LocationTaxonomy::configure(LOCATION_TAXONOMY_SLUG, [ENTREPRENEUR_POST_TYPE_SLUG, BUSINESS_POST_TYPE_SLUG]);

// Bind to hooks
add_action('wp_enqueue_scripts', function () use ($container) {
    $container->get(ThemeAssets::class)->enqueue();
});

add_action('admin_enqueue_scripts', function () use ($container) {
    $container->get(AdminAssets::class)->enqueue();
});

add_filter('et_html_main_header', function (string $content) use ($container) {
    return $container->get(SecondaryTopNavFilterHandler::class)->filter($content);
});

add_action('get_header', function () {
    if (is_page('o-towarzystwach')) {
        add_filter('the_content', function (string $content) {
            return preg_replace_callback('/%91tbsa_stats post_type=%22([\w_\-]+)%22%93/', function ($matches) {
                if (isset($matches[1])) {
                    return wp_count_posts($matches[1])->publish;
                }
                return '';
            }, $content);
        }, 0, 1);
    }
});

add_shortcode('tbsa_newsletter_form', function ($atts) {
    return sprintf(
        '<div class="form-small-text%1$s">%2$s</div>',
        isset($atts['style']) ? ' form-' . $atts['style'] : ' form-transparent-light',
        do_shortcode('[contact-form-7 id="' . NEWSLETTER_FORM_ID . '"]')
    );
});

add_shortcode('tbsa_magazine_form', function ($atts) {
    return sprintf(
        '<div class="form-small-text%1$s">%2$s</div>',
        isset($atts['style']) ? ' form-' . $atts['style'] : ' form-transparent-light',
        do_shortcode('[contact-form-7 id="' . MAGAZINE_FORM_ID . '"]')
    );
});

add_shortcode('tbsa_contact_form', function ($atts) {
    return sprintf(
        '<div class="form-small-text%1$s">%2$s</div>',
        isset($atts['style']) ? ' form-' . $atts['style'] : ' form-normal',
        do_shortcode('[contact-form-7 id="' . CONTACT_FORM_ID . '"]')
    );
});

add_shortcode('what_we_are', function () use ($container) {
    return $container->get(WhatWeAreRenderer::class)->render();
});

add_shortcode('opinie', function () use ($container) {
    return $container->get(TestimonialsRenderer::class)->render();
});

add_shortcode('blog_kafelki', function (array $atts) use ($container) {
    return $container->get(PostsGridRenderer::class)->render($atts['liczba'] ?? 3, 3);
});

add_shortcode('magazine_page', function () use ($container) {
    return $container->get(MagazinePageRenderer::class)->render();
});

add_shortcode('business_search_page', function ($atts) use ($container) {
    return $container->get(BusinessSearchPageRenderer::class)->render($atts);
});

add_shortcode('offer_page', function () use ($container) {
    return $container->get(OfferPageRenderer::class)->render();
});

add_shortcode('branch_page', function () use ($container) {
    return $container->get(BranchPageRenderer::class)->render();
});

add_action('event_page', function () use ($container) {
    $container->get(EventPageController::class)->execute();
});

add_action('bookings_page', function () use ($container) {
    $container->get(BookingsPageController::class)->execute();
});

add_shortcode('event_page', function ($atts) use ($container) {
    return $container->get(EventPageRenderer::class)->render($atts['event_id'] ?? get_the_ID());
});

add_shortcode('event_categories', function ($atts) use ($container) {
    return $container->get(EventCategoryButtonsRenderer::class)->render($atts);
});

add_shortcode('initiatives', function () use ($container) {
    return $container->get(InitiativesRenderer::class)->render();
});

add_shortcode('merkuryusz_oferta_reklamowa_pdf', function () use ($twig) {
    $field = get_field('mag_ad_offer', 'option');

    return $twig->render('attachmentLink.twig', [
        'icon' => 'pdf-icon.svg',
        'href' => $field['file']['url'],
        'text' => $field['link_text'],
    ]);
});

add_shortcode('share_buttons', function () use ($container) {
    return $container->get(ShareButtonsRenderer::class)->render();
});

add_shortcode('blok_z_mapa', function () use ($container) {
    $branches = $container->get(BranchRepository::class)->getNonVirtualBranches();

    /** @var HomeMapBlockRenderer $renderer */
    $renderer = $container->make(HomeMapBlockRenderer::class, [
        'getQuantity' => $container->make(GetBranchCountByVoivodeship::class, ['branches' => $branches]),
        'branches'    => $branches,
    ]);

    return $renderer->render();
});

add_shortcode('google_map', function ($atts) use ($container) {
    return $container->get(GoogleMapRenderer::class)->render($atts);
});

add_shortcode('blok_z_kalendarzem', function () use ($container) {
    return $container->get(HomeCalendarModuleRenderer::class)->render();
});

add_shortcode('hasla_przyciski', function () use ($container) {
    return $container->get(SloganButtonsRenderer::class)->render();
});

add_shortcode('tbsa_posts_page', function () use ($container) {
    return $container->get(PostsPageRenderer::class)->render();
});

add_action('et_after_post', function () use ($twig) {
    echo do_shortcode($twig->render('postFooter.twig', [
        'legal' => get_post_type() === JOB_OFFER_POST_TYPE_SLUG
            ? get_field('tbsa_legal_employment', 'option')
            : null,
    ]));
});

add_action('et_after_main_content', function () use ($container) {
    echo $container->get(AffiliatesRenderer::class)->render();
});

// Register REST routes & controllers
$container->get(EventPresentationRoute::class)->register(function ($args) use ($container) {
    return $container->get(EventPresentationController::class)->query($args);
});

$container->get(BookingFormRestRoute::class)->register(function ($args) use ($container) {
    return $container->get(BookingFormController::class)->renderForm($args);
});

$container->get(EventCheckoutController::class)->execute();

EmSearchCustomizer::execute();
$container->get(BoardMemberPermissions::class)->configure();

$container->get(PrintableBookings::class)->enable();

$container->get(MyAccountPage::class)->enable();

$container->get(TalkBundle::class)->enable();

$container->get(EventSearchBundle::class)->enable();

$container->get(OrderBundle::class)->enable();

$container->get(SalesManagoBundle::class)->enable();

// Prevent Events Manager from deleting specific events' bookings when modifying a recurring event
add_filter('query', function (string $sql) {
    global $wpdb;
    if (strpos($sql, "DELETE FROM {$wpdb->postmeta} WHERE post_id IN") !== false) {
        return str_replace('WHERE', "WHERE meta_key NOT LIKE '%tbsa_bookings%' AND", $sql);
    }

    return $sql;
}, 10, 1);

add_action('rest_api_init', function () use ($container): void {
    $eventRestRoute = $container->get(EventRestRoute::class);
    $eventRestController = $container->get(EventRestController::class);
    $eventRestRoute->register(
        [$eventRestController, 'query'],
        [$eventRestController, 'getById'],
        [$eventRestController, 'getDays'],
        [$eventRestController, 'getDays']
    );

    $eventCategoryRestRoute = $container->get(EventCategoryRestRoute::class);
    $eventCategoryRestController = $container->get(EventCategoryRestController::class);
    $eventCategoryRestRoute->register([$eventCategoryRestController, 'getAll']);
});

add_action('wp_ajax_migrate_event_speakers_to_multi', function () use ($container): void {
    if (!current_user_can('tech')) {
        http_response_code(403);
        echo '403 Forbidden';
        wp_die();
        return;
    }

    $dryRun = (bool) ($_GET['dry_run'] ?? false);
    $affectedIds = $container
        ->get(MigrateEventSpeakersToMulti::class)
        ->execute((int) ($_GET['offset'] ?? 0), (int) ($_GET['limit'] ?? 0), $dryRun);

    ?>
    <h2>Affected event ids</h2>
    <ul>
        <?php foreach ($affectedIds as $id) : ?>
            <li><?= $id ?></li>
        <?php endforeach ?>
    </ul>
    <?php

    wp_die();
});

add_action('wp_ajax_migrate_talk_submissions', function () use ($container): void {
    if (!current_user_can('tech')) {
        http_response_code(403);
        echo '403 Forbidden';
        wp_die();
        return;
    }

    $submissionRepository = $container->get(SubmissionRepository::class);
    $submissionQuery = new SubmissionQuery();
    $submissionQuery->setOnlyForFutureEvents(false);
    $submissions = $submissionRepository->findSubmissions($submissionQuery);

    $dryRun = (bool) ($_GET['dry_run'] ?? false);
    $affectedIds = [];

    foreach ($submissions->getItems() as $submission) {
        $submissionId = $submission->getId();
        $affectedIds[] = $submissionId;
        if (!$dryRun) {
            update_post_meta(
                $submissionId,
                'event_datetime',
                $submission->getEvent()->getTimespan()->getStart()->format(DATE_ATOM)
            );
        }
    }

    ?>
    <h2>Affected submission ids</h2>
    <ul>
        <?php foreach ($affectedIds as $id) : ?>
            <li><?= $id ?></li>
        <?php endforeach ?>
    </ul>
    <?php

    wp_die();
});

add_action('wp_ajax_migrate_salesmanago_buyers', function () use ($container): void {
    if (!current_user_can('tech')) {
        http_response_code(400);
        wp_die('400 Forbidden');
    }

    $migration = $container->get(BuyerMigration::class);

    try {
        if ($_GET['down'] ?? false) {
            $migration->down();
            wp_die('Buyers table removed.');
        } else {
            $migration->up();
            wp_die('Buyers table added.');
        }
    } catch (\Exception $e) {
        wp_die($e->getMessage());
    }
});
