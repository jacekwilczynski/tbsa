<?php

namespace MobilitySoft\TBSA;

use MobilitySoft\TBSA\Entity\EventInterface;

final class GetFormattedEventStartTime
{
    public function execute(EventInterface $event): string
    {
        return $event->getTimespan()->isWholeDay()
            ? null
            : $event->getTimespan()->getStart()->format('G:i');
    }
}
