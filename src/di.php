<?php

namespace MobilitySoft\TBSA;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use MobilitySoft\TBSA\Domain\Booking\BookingRepository;
use MobilitySoft\TBSA\FilterHandler\SecondaryTopNavFilterHandler;
use MobilitySoft\TBSA\Infrastructure\Booking\AcfBookingRepository;
use MobilitySoft\TBSA\Interfaces\GetLinkIconInterface;
use MobilitySoft\TBSA\MyAccountPage\Domain\User\User;
use MobilitySoft\TBSA\MyAccountPage\Domain\User\UserRepository;
use MobilitySoft\TBSA\MyAccountPage\Infrastructure\User\CachingUserRepository;
use MobilitySoft\TBSA\MyAccountPage\Infrastructure\User\UserRepository as StandardUserRepository;
use MobilitySoft\TBSA\Renderer\BusinessSearchPageRenderer;
use MobilitySoft\TBSA\Renderer\GetLinkIcon;
use MobilitySoft\TBSA\Renderer\MagazinePageRenderer;
use MobilitySoft\TBSA\Renderer\SecondaryTopNavRenderer;
use MobilitySoft\TBSA\Repository\AcfExternalLinksRepository;
use MobilitySoft\TBSA\Repository\AcfExternalLinksRepositoryInterface;
use MobilitySoft\TBSA\Repository\BoardRepository;
use MobilitySoft\TBSA\Repository\BranchRepository;
use MobilitySoft\TBSA\Repository\BusinessRepository;
use MobilitySoft\TBSA\Repository\DefaultLanguageAcfExternalLinksRepository;
use MobilitySoft\TBSA\Repository\EmEventCategoryRepository;
use MobilitySoft\TBSA\Repository\EmEventRepository;
use MobilitySoft\TBSA\Repository\EntrepreneurRepository;
use MobilitySoft\TBSA\Repository\EventCategoryRepositoryInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use MobilitySoft\TBSA\Repository\GroupRepository;
use MobilitySoft\TBSA\RestController\EventCheckoutController;
use MobilitySoft\TBSA\SalesManago\Client\SalesManagoIdentificationData;
use MobilitySoft\TBSA\SalesManago\IdentificationDataProvider\SalesManagoIdentificationDataProvider;
use MobilitySoft\TBSA\Service\TwigEnvironmentFactory;
use MobilitySoft\TBSA\Talk\Application\DomainEventHandling\OnSubmitTalkSuccess;
use MobilitySoft\TBSA\Talk\Domain\DetectTemplateVsSubmission;
use MobilitySoft\TBSA\Talk\Domain\Event\FindEvents;
use MobilitySoft\TBSA\Talk\Domain\FindAvailableEvents;
use MobilitySoft\TBSA\Talk\Domain\FindEventsUserCanApplyFor;
use MobilitySoft\TBSA\Talk\Domain\SubmitTalk\SubmitTalk;
use MobilitySoft\TBSA\Talk\Domain\Talk\TalkRepository;
use MobilitySoft\TBSA\Talk\Domain\Template\TemplateRepository;
use MobilitySoft\TBSA\Talk\Infrastructure\Branch\LazyBranchRepository;
use MobilitySoft\TBSA\Talk\Infrastructure\Event\LazyEventRepository;
use MobilitySoft\TBSA\Talk\Infrastructure\Submission\LazySubmissionRepository;
use MobilitySoft\TBSA\Talk\Infrastructure\User\LazyUserRepository;
use MobilitySoft\TBSA\View\EventPage\Domain\EventCategoryRepository;
use MobilitySoft\TBSA\View\EventPage\Domain\EventRepository;
use Psr\Container\ContainerInterface;
use Twig\Environment as Twig;
use function DI\autowire;
use function DI\factory;
use function DI\get;

return [
    AcfExternalLinksRepositoryInterface::class => factory(function (ContainerInterface $container) {
        $linksRepository = $container->get(AcfExternalLinksRepository::class);
        if (isset($sitepress)) {
            $linksRepository = new DefaultLanguageAcfExternalLinksRepository($linksRepository);
        }

        return $linksRepository;
    }),

    BoardRepository::class =>
        autowire(BoardRepository::class)
            ->constructorParameter('postType', BOARD_POST_TYPE_SLUG)
            ->method('inject',
                get(EntrepreneurRepository::class),
                get(GroupRepository::class)
            ),

    BookingRepository::class => autowire(AcfBookingRepository::class),

    BranchRepository::class =>
        autowire(BranchRepository::class)
            ->constructorParameter('postType', BRANCH_POST_TYPE_SLUG)
            ->method('inject',
                get(BoardRepository::class),
                get(GroupRepository::class)
            ),

    ClientInterface::class => autowire(Client::class),

    EventCheckoutController::class =>
        autowire(EventCheckoutController::class)
            ->constructorParameter('productId', EVENT_PRODUCT_ID),

    EventSearch\Domain\Branch\BranchRepository::class =>
        autowire(EventSearch\Infrastructure\Branch\BranchRepository::class),

    Talk\Domain\Branch\BranchRepository::class =>
        autowire(Talk\Infrastructure\Branch\BranchRepository::class)
            ->method('inject', get(Talk\Infrastructure\User\LazyUserRepository::class))
            ->lazy(),

    BusinessRepository::class =>
        autowire(BusinessRepository::class)
            ->constructorParameter('postType', BUSINESS_POST_TYPE_SLUG)
            ->constructorParameter(
                'taxonomies',
                ['industry' => INDUSTRY_TAXONOMY_SLUG, 'location' => LOCATION_TAXONOMY_SLUG]
            )
            ->method('inject', get(EntrepreneurRepository::class))
            ->lazy(),

    Talk\Domain\Entrepreneur\BusinessRepository::class =>
        autowire(Talk\Infrastructure\Entrepreneur\BusinessRepository::class),

    BusinessSearchPageRenderer::class =>
        autowire(BusinessSearchPageRenderer::class)
            ->constructorParameter(
                'dictionary',
                function (ContainerInterface $container) {
                    return $container->get(SiteDictionary::class)->getNamespace('businessSearch');
                }
            )
            ->constructorParameter(
                'taxonomies',
                ['industry' => INDUSTRY_TAXONOMY_SLUG, 'location' => LOCATION_TAXONOMY_SLUG]
            ),

    DetectTemplateVsSubmission::class => autowire(Talk\Infrastructure\DetectTemplateVsSubmission::class),

    EntrepreneurRepository::class =>
        autowire(EntrepreneurRepository::class)
            ->constructorParameter('postType', ENTREPRENEUR_POST_TYPE_SLUG)
            ->method('inject',
                get(BusinessRepository::class),
                get(GroupRepository::class)
            ),

    Talk\Domain\Entrepreneur\EntrepreneurRepository::class =>
        autowire(Talk\Infrastructure\Entrepreneur\EntrepreneurRepository::class),

    MyAccountPage\Domain\Entrepreneur\EntrepreneurRepository::class =>
        autowire(MyAccountPage\Infrastructure\Entrepreneur\EntrepreneurRepository::class),

    EventCategoryRepository::class => autowire(View\EventPage\Infrastructure\EventCategoryRepository::class),

    EventSearch\Domain\Event\EventCategoryRepository::class =>
        autowire(EventSearch\Infrastructure\Event\EventCategoryRepository::class),

    EventCategoryRepositoryInterface::class =>
        autowire(EmEventCategoryRepository::class)
            ->constructorParameter('postType', EVENT_CATEGORY_POST_TYPE_SLUG),

    Talk\Domain\Event\EventRepository::class =>
        autowire(Talk\Infrastructure\Event\EventRepository::class)
            ->method(
                'inject',
                get(LazySubmissionRepository::class),
                get(LazyUserRepository::class)
            )
            ->lazy(),

    EventSearch\Domain\Event\EventRepository::class =>
        autowire(EventSearch\Infrastructure\Event\EventRepository::class),

    EventRepository::class => autowire(View\EventPage\Infrastructure\EventRepository::class),

    EventRepositoryInterface::class =>
        autowire(EmEventRepository::class)
            ->method('inject',
                get(BranchRepository::class),
                get(EventCategoryRepositoryInterface::class),
                get(GroupRepository::class)
            ),

    FindEvents::class => autowire(FindEventsUserCanApplyFor::class)
        ->constructorParameter('source', get(FindAvailableEvents::class)),

    GetLinkIconInterface::class => autowire(GetLinkIcon::class),

    GroupRepository::class =>
        autowire(GroupRepository::class)
            ->constructorParameter('postType', NETWORKING_GROUP_POST_TYPE_SLUG)
            ->method('inject',
                get(BoardRepository::class),
                get(BranchRepository::class),
                get(EntrepreneurRepository::class)
            )
            ->lazy(),

    MagazinePageRenderer::class =>
        autowire(MagazinePageRenderer::class)
            ->constructorParameter(
                'buyUrl',
                home_url('/zamowienie/?clean-cart=1&add-to-cart=' . MAGAZINE_SUBSCRIPTION_PRODUCT_ID)
            ),

    SalesManagoIdentificationData::class => factory(
        function (ContainerInterface $container): SalesManagoIdentificationData {
            $provider = $container->get(SalesManagoIdentificationDataProvider::class);
            return $provider->getData();
        }
    ),

    SecondaryTopNavFilterHandler::class =>
        autowire(SecondaryTopNavFilterHandler::class)
            ->constructor(get(SecondaryTopNavRenderer::class)),

    SubmitTalk::class =>
        autowire(SubmitTalk::class)
            ->constructorParameter('onSuccess', get(OnSubmitTalkSuccess::class)),

    Talk\Domain\Submission\SubmissionRepository::class =>
        autowire(Talk\Infrastructure\Submission\SubmissionRepository::class)
            ->method(
                'inject',
                get(LazyEventRepository::class),
                get(LazyUserRepository::class)
            )
            ->lazy(),

    TalkRepository::class => autowire(Talk\Infrastructure\Talk\TalkRepository::class),

    TemplateRepository::class => autowire(Talk\Infrastructure\Template\TemplateRepository::class),

    Twig::class => factory(function (ContainerInterface $container): Twig {
        return $container->get(TwigEnvironmentFactory::class)->createTwigEnvironment();
    }),

    User::class => factory(function (ContainerInterface $container) {
        return $container->get(UserRepository::class)->getUser(get_current_user_id());
    }),

    Talk\Domain\User\User::class => factory(function (ContainerInterface $container) {
        return $container->get(Talk\Domain\User\UserRepository::class)->getCurrentUser();
    }),

    UserRepository::class => autowire(CachingUserRepository::class)
        ->constructorParameter('source', get(StandardUserRepository::class)),

    Talk\Domain\User\UserRepository::class =>
        autowire(Talk\Infrastructure\User\UserRepository::class)
            ->method(
                'inject',
                get(LazyBranchRepository::class),
                get(Talk\Domain\Entrepreneur\EntrepreneurRepository::class)
            )
            ->lazy(),

    \wpdb::class => factory(function (): \wpdb {
        global $wpdb;
        return $wpdb;
    }),
];
