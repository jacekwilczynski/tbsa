<?php

namespace MobilitySoft\TBSA\ConfigUtils;

function enableQueryingRepeatedRelationshipFields(array $allowedFieldNames)
{
    add_filter('posts_where', function (string $where) use ($allowedFieldNames) {
        global $wpdb;
        foreach ($allowedFieldNames as $fieldName) {
            if (preg_match("/meta_key = ['\"]?{$fieldName}['\"]?/", $where)) {
                $where = $wpdb->remove_placeholder_escape($where);
                $where = str_replace('meta_key =', 'meta_key LIKE', $where);

                return $where;
            }
        }

        return $where;
    });
}
