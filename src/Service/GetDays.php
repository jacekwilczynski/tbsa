<?php

namespace MobilitySoft\TBSA\Service;

use DateTimeImmutable;
use MobilitySoft\TBSA\Common\Application\Event\CreateEventQueryFromHttpRequestParams;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;

class GetDays
{
    private const PARAMS_TO_PASS = ['branch', 'category', 'filter'];

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var CreateEventQueryFromHttpRequestParams
     */
    private $createEventQueryFromHttpRequestParams;

    public function __construct(
        CreateEventQueryFromHttpRequestParams $createEventQueryFromHttpRequestParams,
        EventRepositoryInterface $eventRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->createEventQueryFromHttpRequestParams = $createEventQueryFromHttpRequestParams;
    }

    public function execute(
        array $params,
        DateTimeImmutable $startDate,
        ?DateTimeImmutable $endDate,
        ?callable $getEventData = null
    ): array {
        $farthestAllowedEndDate = $startDate->modify('+31 days');
        if ($endDate === null || $endDate > $farthestAllowedEndDate) {
            $endDate = $farthestAllowedEndDate;
        }

        $currentDate = $startDate;
        $days = [];
        do {
            $events = $this->getEvents($params, $currentDate);
            $days[] = [
                'date'   => [
                    'text'      => TranslateMonth::execute($currentDate->format('j F Y')),
                    'timestamp' => $currentDate->getTimestamp(),
                ],
                'events' => $getEventData ? array_map(function (EventInterface $event) use ($getEventData) {
                    return $getEventData($event);
                }, $events) : $events,
            ];
            $currentDate = $currentDate->modify('+1 day');
        } while ($currentDate <= $endDate);

        return $days;
    }

    /**
     * @return EventInterface[]
     */
    private function getEvents(array $params, DateTimeImmutable $currentDate): array
    {
        $queryParams = [];
        foreach (self::PARAMS_TO_PASS as $key) {
            if (isset($params[$key])) {
                $queryParams[$key] = $params[$key];
            }
        }
        $eventQuery = $this->createEventQueryFromHttpRequestParams->execute($queryParams);
        $eventQuery->setLimit(10);
        $eventQuery->setDate($currentDate);
        return $this->eventRepository->query($eventQuery);
    }
}
