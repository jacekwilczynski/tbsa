<?php

namespace MobilitySoft\TBSA\Service;

use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\ValueObject\EventTicket;

class GetPriceString
{
    public static function execute(EventInterface $event, bool $emptyStringForZero = false): string
    {
        $tickets = $event->getTickets();

        if ($tickets) {
            if (count($tickets) > 1) {
                $prices = array_map(function (EventTicket $ticket): float {
                    return $ticket->getPrice()->getFloat();
                }, $tickets);

                return wc_format_price_range(min(...$prices), max(...$prices));
            }

            return wc_price($tickets[0]->getPrice()->getFloat());
        }

        return $emptyStringForZero && $event->getPrice() === 0 ? '' : wc_price($event->getPrice());
    }
}
