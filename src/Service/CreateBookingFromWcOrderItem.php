<?php

namespace MobilitySoft\TBSA\Service;

use MobilitySoft\TBSA\Domain\Booking\Booking;
use MobilitySoft\TBSA\Domain\Booking\BookingBuyer;
use Throwable;
use WC_Order_Item;

class CreateBookingFromWcOrderItem
{
    /**
     * @param WC_Order_Item $item
     *
     * @return Booking|null
     */
    public function execute(WC_Order_Item $item): ?Booking
    {
        try {
            $order = $item->get_order();
            $buyer = new BookingBuyer(
                $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
                $order->get_billing_email(),
                $order->get_billing_phone(),
                $order->get_user_id()
            );

            return new Booking(
                $item->get_meta('_tbsa_eventId'),
                $item->get_quantity(),
                $buyer,
                $item->get_order_id(),
                $item->get_meta('_tbsa_ticketId')
            );
        } catch (Throwable $e) {
            error_log($e);

            return null;
        }
    }
}
