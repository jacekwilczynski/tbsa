<?php

namespace MobilitySoft\TBSA\Service;

use MobilitySoft\TBSA\DTO\Link;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\ValueObject\BusinessRelation;

class GetBusinessSearchResults
{
    /**
     * @param EntrepreneurInterface[] $entrepreneurs
     *
     * @return array
     */
    public function getViewData(array $entrepreneurs): array
    {
        return array_map(function (EntrepreneurInterface $entrepreneur) {
            $id = $entrepreneur->getId();

            return [
                'id'          => $id,
                'url'         => get_the_permalink($id),
                'name'        => $entrepreneur->getName(),
                'photo'       => $entrepreneur->getPhoto(),
                'description' => $entrepreneur->getDescription(),
                'activity'    => $entrepreneur->getActivities(),
                'media'       => $entrepreneur->getMedia(),
                'links'       => array_map(function (Link $link) {
                    $needsEncoding = in_array($link->type, ['email', 'tel']);
                    $key           = random_int(0, 999);

                    return [
                        'key'     => $needsEncoding ? $key : null,
                        'href'    => $needsEncoding
                            ? ShitCrypt::encrypt($link->href, $key)
                            : $link->href,
                        'content' => $link->content,
                    ];
                }, $entrepreneur->getLinks()),
                'businesses'  => array_map(function (BusinessRelation $relation) {
                    $business = $relation->getBusiness();

                    return [
                        'role'     => $relation->getDescription(),
                        'business' => [
                            'id'         => $business->getId(),
                            'name'       => $business->getName(),
                            'logo'       => $business->getLogo(),
                            'url'        => $business->getUrl(),
                            'industries' => $business->getIndustries(),
                            'locations'  => $relation->getLocations(),
                        ],
                    ];
                }, $entrepreneur->getBusinessRelations()),
            ];
        }, $entrepreneurs);
    }
}
