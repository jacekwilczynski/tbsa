<?php

namespace MobilitySoft\TBSA\Service;

use MobilitySoft\TBSA\Entity\EventInterface;

class IsEventBookable
{
    public static function execute(EventInterface $event): bool
    {
        if ( ! $event->getTimespan()) {
            return false;
        }

        return (
            ! $event->isExternal() &&
            $event->hasBookingsEnabled() &&
            $event->getTimespan()->getStart()->getTimestamp() > time()
        );
    }
}
