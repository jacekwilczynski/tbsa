<?php

namespace MobilitySoft\TBSA\Service;

class EmSearchCustomizer
{
    public static function execute(): void
    {
        add_filter('em_events_get_default_search', function (array $queryArgs, array $input) {
            $queryArgs['branch'] = false;
            if (isset($input['branch']) && is_numeric($input['branch'])) {
                $queryArgs['branch'] = $input['branch'];
            }

            return $queryArgs;
        }, 1, 2);

        add_filter('em_events_build_sql_conditions', function (array $conditions, array $queryArgs) {
            global $table_prefix, $wpdb;

            if ($queryArgs['branch']) {
                $subQuery             = $wpdb->remove_placeholder_escape(
                    $wpdb->prepare(
                        "SELECT post_id FROM {$table_prefix}postmeta WHERE meta_value LIKE %s AND meta_key='tbsa_event_branches'",
                        '%"' . (int)$queryArgs['branch'] . '"%'
                    )
                );
                $conditions['branch'] = EM_EVENTS_TABLE . ".post_id IN ($subQuery)";
            }

            return $conditions;
        }, 1, 2);
    }
}
