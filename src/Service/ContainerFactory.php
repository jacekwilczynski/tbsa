<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\Service;

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

class ContainerFactory
{
    private const PHP_DI_CACHE_DIR = TBSA_PROJECT_ROOT . '/var/cache/php-di';
    private const PHP_DI_COMPILATION_DIR = self::PHP_DI_CACHE_DIR . '/compiled';
    private const PHP_DI_PROXIES_DIR = self::PHP_DI_CACHE_DIR . '/proxies';

    public function getContainer(): ContainerInterface
    {
        $containerBuilder = new ContainerBuilder();
        if (!$this->isDevModeOn()) {
            $this->ensureDirectoryExists(self::PHP_DI_COMPILATION_DIR);
            $containerBuilder->enableCompilation(self::PHP_DI_COMPILATION_DIR);
            $this->ensureDirectoryExists(self::PHP_DI_PROXIES_DIR);
            $containerBuilder->writeProxiesToFile(true, self::PHP_DI_PROXIES_DIR);
        }
        $containerBuilder->addDefinitions(TBSA_PROJECT_ROOT . '/src/di.php');
        $containerBuilder->useAutowiring(true);
        return $containerBuilder->build();
    }

    private function ensureDirectoryExists(string $path): void
    {
        if (!is_dir($path)) {
            mkdir($path, 0775, true);
        }
        if (!is_dir($path)) {
            throw new \RuntimeException("Directory '$path' was not created");
        }
    }

    private function isDevModeOn(): bool
    {
        return defined('TBSA_DEV_MODE') && TBSA_DEV_MODE;
    }
}
