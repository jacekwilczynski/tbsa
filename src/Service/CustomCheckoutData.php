<?php

namespace MobilitySoft\TBSA\Service;

class CustomCheckoutData
{
    private const FIELDS = [
        'first_name' => FILTER_SANITIZE_STRING,
        'last_name'  => FILTER_SANITIZE_STRING,
        'email'      => FILTER_SANITIZE_EMAIL,
        'phone'      => FILTER_SANITIZE_STRING,
    ];

    public static function load(): array
    {
        $customer = wc()->customer;
        $checkout = wc()->checkout();
        $data     = [];

        foreach (array_keys(self::FIELDS) as $key) {
            $data[$key] = $customer ? $checkout->get_value("billing_$key") : '';
        }

        return filter_var_array($data, self::FIELDS);
    }

    public static function save(array $data): void
    {
        $customer     = wc()->customer;
        if (!$customer) {
            return;
        }

        $customerId   = $customer->get_id();
        $filteredData = filter_var_array($data, self::FIELDS);

        foreach ($filteredData as $key => $value) {
            $method = "set_billing_$key";
            $customer->$method($value);
            if ($customerId) {
                update_post_meta($customerId, "billing_$key", $value);
            }
        }

        $customer->save();
    }
}
