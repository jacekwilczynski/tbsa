<?php

namespace MobilitySoft\TBSA\Service;

use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\ValueObject\EventTicket;

class GetEventTicketById
{
    public static function execute(EventInterface $event, $ticketId): ?EventTicket
    {
        return array_find($event->getTickets(), function (EventTicket $ticket) use ($ticketId) {
            return sanitize_title($ticket->getId()) == sanitize_title($ticketId);
        });
    }
}
