<?php

namespace MobilitySoft\TBSA\Service;

use InvalidArgumentException;
use MobilitySoft\TBSA\Entity\BusinessInterface;
use MobilitySoft\TBSA\Entity\EntrepreneurInterface;
use MobilitySoft\TBSA\Repository\BusinessRepository;
use MobilitySoft\TBSA\Repository\EntrepreneurRepository;
use MobilitySoft\TBSA\ValueObject\BusinessRelation;

class BusinessSearch
{
    /**
     * @var EntrepreneurRepository
     */
    private $entrepreneurRepository;

    /**
     * @var BusinessRepository
     */
    private $businessRepository;

    /**
     * BusinessSearch constructor.
     *
     * @param EntrepreneurRepository $entrepreneurRepository
     * @param BusinessRepository $businessRepository
     */
    public function __construct(
        EntrepreneurRepository $entrepreneurRepository,
        BusinessRepository $businessRepository
    ) {
        $this->entrepreneurRepository = $entrepreneurRepository;
        $this->businessRepository = $businessRepository;
    }

    /**
     * @param string|null $phrase
     * @param string|null $industry
     * @param string|null $location
     *
     * @param int|null $page
     *
     * @return array[]
     */
    public function execute(?string $phrase, ?string $industry, ?string $location, ?int $page = 1): array
    {
        $limit = get_field('tbsa_business_search_results_limit', 'option') ?? 10;
        $offset = ($page - 1) * $limit;

        $entrepreneurs = $this->search($phrase ?? '');

        if ($industry) {
            $entrepreneurs = $this->filterEntrepreneurs($entrepreneurs, 'industries', $industry);
        }

        if ($location) {
            $entrepreneurs = $this->filterEntrepreneurs($entrepreneurs, 'locations', $location);
        }

        usort($entrepreneurs, function (EntrepreneurInterface $a, EntrepreneurInterface $b) {
            $starredDifference = $b->isStarred() <=> $a->isStarred();
            if ($starredDifference) {
                return $starredDifference;
            }
            return $a->getName() <=> $b->getName();
        });

        return [
            'totalPages'    => ceil(count($entrepreneurs) / $limit),
            'entrepreneurs' => array_slice($entrepreneurs, $offset, $limit),
        ];
    }

    /**
     * @param string $phrase
     *
     * @return EntrepreneurInterface[]
     */
    private function search(string $phrase): array
    {
        /** @var EntrepreneurInterface[] $all */
        $all = array_merge(
            $this->entrepreneurRepository->search($phrase),
            ...array_map(function (BusinessInterface $business) {
                return $this->entrepreneurRepository->getByBusinessId($business->getId());
            }, $this->businessRepository->search($phrase))
        );

        /** @var string[] $foundIds */
        $foundIds = [];

        /** @var EntrepreneurInterface[] $unique */
        $unique = [];

        foreach ($all as $entrepreneur) {
            $id = $entrepreneur->getId();
            if (!in_array($id, $foundIds, true)) {
                $foundIds[] = $id;
                $unique[] = $entrepreneur;
            }
        }

        return $unique;
    }

    /**
     * @param EntrepreneurInterface[] $entrepreneurs
     * @param string $field
     * @param $value
     *
     * @return array
     */
    private function filterEntrepreneurs(array $entrepreneurs, string $field, $value): array
    {
        return array_filter(
            $entrepreneurs,
            function (EntrepreneurInterface $entrepreneur) use ($field, $value) {
                $relations = $entrepreneur->getBusinessRelations();
                $nestedValues = array_map(function (BusinessRelation $relation) use ($field): array {
                    if ($field === 'industries') {
                        return $relation->getBusiness()->getIndustries();
                    }
                    if ($field === 'locations') {
                        return $relation->getLocations();
                    }
                    throw new InvalidArgumentException("Cannot filter entrepreneurs by field '$field'.");
                }, $relations);
                $values = count($nestedValues) > 0 ? array_merge(...$nestedValues) : [];

                return in_array($value, $values);
            }
        );
    }

}
