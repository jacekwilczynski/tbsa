<?php

namespace MobilitySoft\TBSA\Service;

use manuelodelain\Twig\Extension\SvgExtension;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class TwigEnvironmentFactory
{
    public function createTwigEnvironment(): Environment
    {
        $devMode = defined('TBSA_DEV_MODE') && TBSA_DEV_MODE;
        $themeRoot = get_stylesheet_directory();
        $loader = new FilesystemLoader($themeRoot . '/templates');
        $environment = new Environment($loader, [
            'cache'      => $devMode ? false : $themeRoot . '/var/cache/twig',
            'debug'      => $devMode,
            'autoescape' => false,
        ]);
        $environment->addExtension(new SvgExtension($themeRoot . '/assets'));
        $environment->addFunction(new TwigFunction('__', '__'));

        return $environment;
    }
}
