<?php

namespace MobilitySoft\TBSA\Service;

class ShitCrypt
{
    public static function encrypt(string $data, int $key): string
    {
        $numbers = [];
        for ($i = 0, $length = strlen($data); $i < $length; $i++) {
            $numbers[] = dechex(ord($data[$i]) + $key + $i);
        }

        return implode('-', $numbers);
    }
}
