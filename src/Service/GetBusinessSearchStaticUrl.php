<?php

namespace MobilitySoft\TBSA\Service;

class GetBusinessSearchStaticUrl
{
    public static function execute(
        ?string $phrase = null,
        ?string $industry = null,
        ?string $location = null,
        ?int $page = null
    ) {
        $url = home_url('/wyszukiwarka-firm/?');

        if ($phrase) {
            $url .= "fraza=$phrase&";
        }

        if ($industry) {
            $url .= "branza=$industry&";
        }

        if ($location) {
            $url .= "miasto=$location&";
        }

        if ($page) {
            $url .= "strona=$page";
        }

        return $url;
    }
}
