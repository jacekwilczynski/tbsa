<?php

namespace MobilitySoft\TBSA\Service;

class TranslateMonth
{
    private const MONTHS = [
        'January'   => 'stycznia',
        'February'  => 'lutego',
        'March'     => 'marca',
        'April'     => 'kwietnia',
        'May'       => 'maja',
        'June'      => 'czerwca',
        'July'      => 'lipca',
        'August'    => 'sierpnia',
        'September' => 'września',
        'October'   => 'października',
        'November'  => 'listopada',
        'December'  => 'grudnia',
    ];

    public static function execute(string $input): string
    {
        foreach (self::MONTHS as $eng => $pl) {
            $input = str_replace($eng, $pl, $input);
        }

        return $input;
    }
}
