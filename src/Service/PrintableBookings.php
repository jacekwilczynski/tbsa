<?php

namespace MobilitySoft\TBSA\Service;

use Dompdf\Dompdf;
use MobilitySoft\TBSA\Domain\Booking\Booking;
use MobilitySoft\TBSA\Domain\Booking\BookingRepository;
use MobilitySoft\TBSA\Entity\EventInterface;
use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use Twig\Environment as Twig;
use WP_REST_Request;
use WP_REST_Response;

class PrintableBookings
{
    /**
     * @var BookingRepository
     */
    private $bookingRepository;

    /**
     * @var array
     */
    private $restRoute = [
        'namespace' => '/tbsa/v1',
        'resource'  => '/bookings-pdf',
    ];

    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    /**
     * @var Twig
     */
    private $twig;

    public function __construct(
        Twig $twig,
        EventRepositoryInterface $eventRepository,
        BookingRepository $bookingRepository
    ) {
        $this->twig = $twig;
        $this->eventRepository = $eventRepository;
        $this->bookingRepository = $bookingRepository;
        $this->restRoute['url'] = home_url('/wp-json' . implode('', $this->restRoute));
    }

    public function enable(): void
    {
        $this->addMetabox();
        $this->registerRestRoute();
    }

    private function addMetabox(): void
    {
        add_action('admin_init', function () {
            add_meta_box(
                'tbsa_event_actions',
                'Działania',
                function () {
                    ?>
                    <div style="text-align: center">
                        <a href="<?= $this->getPrintUrl(get_the_ID()) ?>"
                           class="button button-large button-primary"
                        >Pobierz PDF z rezerwacjami</a>
                    </div>
                    <?php
                },
                'event',
                'side',
                'high'
            );
        });
    }

    public function getPrintUrl(int $eventId): string
    {
        return $this->restRoute['url'] . '?event_id=' . $eventId . '&_wpnonce=' . wp_create_nonce('wp_rest');
    }

    private function registerRestRoute(): void
    {
        add_action('rest_api_init', function () {
            register_rest_route($this->restRoute['namespace'], $this->restRoute['resource'], [
                'methods'  => 'GET',
                'args'     => [
                    'event_id' => [
                        'type'     => 'integer',
                        'required' => true,
                    ],
                ],
                'callback' => function (WP_REST_Request $request) {
                    $eventId = $request->get_param('event_id');
                    if (current_user_can('edit_post', $eventId)) {
                        $event = $this->eventRepository->getById($eventId);
                        if (!$event) {
                            return new WP_REST_Response("Event $eventId does not exist or is invalid", 400);
                        }
                        $bookings = $this->bookingRepository->getByEventId($eventId);
                        $this->streamPdf($event, $bookings);
                        exit();
                    }

                    http_response_code(403);
                    exit();
                },
            ]);
        });
    }

    /**
     * @param EventInterface $event
     * @param Booking[] $bookings
     */
    private function streamPdf(EventInterface $event, array $bookings): void
    {
        $html = $this->twig->render('bookingsPdf.twig', [
            'title'          => $event->getTitle(),
            'date'           => TranslateMonth::execute($event->getTimespan()->getStart()->format('j F Y')),
            'bookings'       => $bookings,
            'imageData'      => file_get_contents(home_url('/wp-content/uploads/logotyp-towarzystw-biznesowych.svg')),
            'numberOfPeople' => array_reduce($bookings, function (int $count, Booking $booking) {
                return $count + $booking->getNumberOfPlaces();
            }, 0),
        ]);

        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');

        $dompdf = new Dompdf(['defaultPaperOrientation' => 'landscape']);
        $dompdf->loadHtml($html);
        $dompdf->render();
        $dompdf->stream('rezerwacje.pdf');
    }
}
