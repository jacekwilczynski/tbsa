<?php

namespace MobilitySoft\TBSA\Service;

/**
 * Class CreateDataForPagination
 * @package MobilitySoft\TBSA\Service
 */
class CreateDataForPagination
{
    /**
     * @var callable
     */
    private $getUrl;

    /**
     * CreateDataForPagination constructor.
     *
     * @param callable $getUrl
     */
    public function __construct(callable $getUrl)
    {
        $this->getUrl = $getUrl;
    }

    /**
     * @param int $currentPage
     * @param int $totalPages
     *
     * @return array
     */
    public function execute(int $currentPage, int $totalPages): array
    {
        $items = [];

        if ($currentPage > 1) {
            $items[] = [
                'content' => 'prev',
                'url'     => call_user_func($this->getUrl, $currentPage - 1)
            ];
        }

        if ($currentPage < $totalPages) {
            $items[] = [
                'content' => 'next',
                'url'     => call_user_func($this->getUrl, $currentPage + 1)
            ];
        }

        return [
            'items' => $items,
        ];
    }
}
