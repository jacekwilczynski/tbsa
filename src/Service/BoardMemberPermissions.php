<?php

namespace MobilitySoft\TBSA\Service;

use MobilitySoft\TBSA\Repository\EventRepositoryInterface;
use WC_Order_Item;
use WP_Query;
use WP_User;

class BoardMemberPermissions
{
    /**
     * @var EventRepositoryInterface
     */
    private $eventRepository;

    public function __construct(EventRepositoryInterface $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function configure(): void
    {
        add_filter('coauthors_edit_author_cap', function () {
            return 'edit_events';
        });

        add_filter('pre_get_posts', static function (WP_Query $query): WP_Query {
            global $pagenow;
            if ($pagenow === 'edit.php' && $query->is_admin && !current_user_can('edit_others_posts')) {
                echo '<style>.page-title-action,.subsubsub{display:none}</style>';
                $query->set('author', get_current_user_id());
            }

            return $query;
        }, 10, 1);

        add_filter('user_has_cap', function ($userCaps, $requestedCaps, $otherArgs, WP_User $user) {
            $isBoardMember = isset($userCaps['board_member']);

            if ($isBoardMember) {
                $tryingToAccessOrder = (bool) array_diff(
                    ['edit_shop_orders', 'edit_others_shop_orders'],
                    $requestedCaps
                );
                if ($tryingToAccessOrder) {
                    $order = wc_get_order(get_the_ID());
                    if ($order) {
                        $items = $order->get_items();
                        /** @var WC_Order_Item[] $items */
                        foreach ($items as $item) {
                            $eventPostId = $item->get_meta('_tbsa_eventId');
                            $eventPost = get_post($eventPostId);
                            if (
                                $eventPost->post_author === $user->ID ||
                                has_term($user->nickname, 'author', $eventPost)
                            ) {
                                $userCaps['edit_others_shop_orders'] = true;
                                break;
                            }
                        }
                    }
                }

                $tryingToAccessEvent = (bool) array_diff(
                    ['edit_events', 'edit_others_events'],
                    $requestedCaps
                );
                $eventId = $otherArgs[2] ?? null;
                if ($tryingToAccessEvent && isset($eventId)) {
                    $event = $this->eventRepository->getById($eventId);
                    if ($event && $event->getBranch()) {
                        $branchId = $event->getBranch()->getId();
                        $userBranches = get_field('branches', $user);
                        if (in_array($branchId, $userBranches)) {
                            $userCaps['edit_others_events'] = true;
                        }
                    }
                }
            }

            return $userCaps;
        }, 10, 4);
    }
}
