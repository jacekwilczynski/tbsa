<?php

namespace MobilitySoft\TBSA\Domain\Booking;

class BookingBuyer
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var int|null
     */
    private $userId;

    /**
     * BookingBuyer constructor.
     *
     * @param string $name
     * @param string $email
     * @param string|null $phone
     * @param int|null $userId
     */
    public function __construct(string $name, string $email, ?string $phone = null, ?int $userId = null)
    {
        if (empty($name)) {
            throw new EmptyNameException('BookingBuyer name must not be empty');
        }

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException("'$email' is not a valid e-mail address");
        }

        $this->name   = $name;
        $this->email  = $email;
        $this->phone  = $phone;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }
}
