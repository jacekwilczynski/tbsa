<?php

namespace MobilitySoft\TBSA\Domain\Booking;

interface BookingRepository
{
    /**
     * @param int $eventId
     *
     * @return Booking[]
     */
    public function getByEventId(int $eventId): array;

    /**
     * @param Booking $booking
     */
    public function add(Booking $booking): void;
}
