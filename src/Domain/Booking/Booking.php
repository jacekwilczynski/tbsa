<?php

namespace MobilitySoft\TBSA\Domain\Booking;

class Booking
{
    /**
     * @var int
     */
    private $eventId;

    /**
     * @var int
     */
    private $numberOfPlaces;

    /**
     * @var int|null
     */
    private $orderId;

    /**
     * @var BookingBuyer
     */
    private $buyer;

    /**
     * @var string|null
     */
    private $ticketId;

    /**
     * Booking constructor.
     *
     * @param int $eventId
     * @param int $numberOfPlaces
     * @param BookingBuyer $buyer
     * @param int|null $orderId
     * @param string|null $ticketId
     *
     * @throws EmptyBookingException
     */
    public function __construct(
        int $eventId,
        int $numberOfPlaces,
        BookingBuyer $buyer,
        ?int $orderId = null,
        ?string $ticketId = null
    ) {
        if ($numberOfPlaces <= 0) {
            throw new EmptyBookingException('Cannot book zero or less places.');
        }

        $this->eventId        = $eventId;
        $this->numberOfPlaces = $numberOfPlaces;
        $this->buyer          = $buyer;
        $this->orderId        = $orderId;
        $this->ticketId       = $ticketId;
    }

    /**
     * @return string|null
     */
    public function getTicketId(): ?string
    {
        return $this->ticketId;
    }

    /**
     * @return BookingBuyer
     */
    public function getBuyer(): BookingBuyer
    {
        return $this->buyer;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }

    /**
     * @return int
     */
    public function getNumberOfPlaces(): int
    {
        return $this->numberOfPlaces;
    }

    /**
     * @return int|null
     */
    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    /**
     * @param int $number
     */
    public function addPlaces(int $number): void
    {
        $this->numberOfPlaces += $number;
    }
}
