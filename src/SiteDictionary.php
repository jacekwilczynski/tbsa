<?php

namespace MobilitySoft\TBSA;

/**
 * Class SiteDictionary
 * @package MobilitySoft\TBSA
 */
final class SiteDictionary
{
    private const data = [
        'businessSearch' => [
            'industry'       => 'Branża',
            'industries'     => 'Branże',
            'activity'       => 'Aktywność',
            'goToBusiness'   => 'Przejdź na stronę',
            'notFound'       => 'Nie znaleziono firm odpowiadających wskazanym kryteriom wyszukiwania',
            'enterSomething' => 'Proszę wypełnić przynajmniej jedno pole',
            'error'          => 'Przepraszamy, wystąpił błąd w działaniu strony.',
        ]
    ];

    /**
     * @param string $namespace
     *
     * @return array
     */
    public function getNamespace(string $namespace): array
    {
        return self::data[$namespace];
    }
}
