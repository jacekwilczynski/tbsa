<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Infrastructure\Event;

use DateTimeImmutable;
use EM_Event;
use EM_Taxonomy_Term;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Common\Infrastructure\Event\FindEmEvents;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetSpeakersFromAcfFields;
use MobilitySoft\TBSA\Common\Infrastructure\Event\GetUrl;
use MobilitySoft\TBSA\Common\Infrastructure\Event\SimpleAcfFields;
use MobilitySoft\TBSA\EventSearch\Domain\Event\Address;
use MobilitySoft\TBSA\EventSearch\Domain\Event\Event;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventCategory;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventLocation;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventRepository as EventRepositoryInterface;
use MobilitySoft\TBSA\EventSearch\Domain\Event\Events;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventTime;
use MobilitySoft\TBSA\EventSearch\Domain\Event\Speakers;
use MobilitySoft\TBSA\Talk\GetMostHopefulEventSubmissionStatus;
use MobilitySoft\TBSA\View\Medium\MediumFactory;
use const MobilitySoft\TBSA\INDUSTRY_TAXONOMY_SLUG;

class EventRepository implements EventRepositoryInterface
{
    /**
     * @var EventCategoryRepository
     */
    private $eventCategoryRepository;

    /**
     * @var FindEmEvents
     */
    private $findEmEvents;

    /**
     * @var MediumFactory
     */
    private $mediumFactory;

    /**
     * @var GetSpeakersFromAcfFields
     */
    private $getSpeakersFromAcfFields;

    /**
     * @var GetMostHopefulEventSubmissionStatus
     */
    private $getMostHopefulEventSubmissionStatus;

    public function __construct(
        EventCategoryRepository $eventCategoryRepository,
        FindEmEvents $findEmEvents,
        MediumFactory $mediumFactory,
        GetSpeakersFromAcfFields $getSpeakersFromAcfFields,
        GetMostHopefulEventSubmissionStatus $getMostHopefulEventSubmissionStatus
    ) {
        $this->eventCategoryRepository = $eventCategoryRepository;
        $this->findEmEvents = $findEmEvents;
        $this->mediumFactory = $mediumFactory;
        $this->getSpeakersFromAcfFields = $getSpeakersFromAcfFields;
        $this->getMostHopefulEventSubmissionStatus = $getMostHopefulEventSubmissionStatus;
    }

    public function findEvents(EventQuery $query): Events
    {
        $emEvents = $this->findEmEvents->execute($query, null);

        return new Events(...array_map(function (EM_Event $emEvent): Event {
            return $this->createEventFromEmEvent($emEvent);
        }, $emEvents));
    }

    protected function createEventFromEmEvent(EM_Event $emEvent): Event
    {
        $id = $emEvent->post_id;
        $acfFields = get_fields($id);
        $parsedAcfFields = SimpleAcfFields::prepare($acfFields);

        return new Event(
            $id,
            $emEvent->post_title,
            $this->getEventTime($emEvent),
            $this->getEventLocation($emEvent),
            $this->getEventCategory($emEvent),
            $this->mediumFactory->createMedium((int) get_post_thumbnail_id($id)),
            (new GetUrl())->execute($id, $parsedAcfFields),
            $parsedAcfFields->isExternal(),
            (new GetEventSales())->execute($id),
            $parsedAcfFields->hasBookingsEnabled(),
            new Industries(...get_term_names(INDUSTRY_TAXONOMY_SLUG, $id)),
            new Speakers(...$this->getSpeakersFromAcfFields->execute($acfFields)),
            $this->getMostHopefulEventSubmissionStatus->execute($id)
        );
    }

    private function getEventTime(EM_Event $emEvent): EventTime
    {
        return new EventTime(
            DateTimeImmutable::createFromMutable($emEvent->start()),
            DateTimeImmutable::createFromMutable($emEvent->end())
        );
    }

    private function getEventLocation(EM_Event $emEvent): ?EventLocation
    {
        $emLocation = $emEvent->get_location();

        if (!$emLocation) {
            return null;
        }

        return new EventLocation(
            $emLocation->location_name,
            $emLocation->location_address,
            $emLocation->location_town
        );
    }

    private function getEventCategory(EM_Event $emEvent): ?EventCategory
    {
        $emCategory = $emEvent->get_categories()->get_first();

        if ($emCategory instanceof EM_Taxonomy_Term) {
            return $this->eventCategoryRepository->createCategoryFromEmTerm($emCategory);
        }

        return null;
    }

    public function countEvents(EventQuery $query): int
    {
        $query = clone $query;
        $query->setLimit(null);
        $query->setOffset(0);

        return count($this->findEmEvents->execute($query));
    }
}
