<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Infrastructure\Event;

use MobilitySoft\TBSA\Domain\Booking\Booking;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventSales;
use MobilitySoft\TBSA\Infrastructure\Booking\AcfBookingRepository;
use MobilitySoft\TBSA\ValueObject\Currency;

class GetEventSales
{
    public function execute(int $eventId): ?EventSales
    {
        if (!$this->currentUserCanSeeBookings($eventId)) {
            return null;
        }

        $bookingRepository = new AcfBookingRepository();
        $bookings = $bookingRepository->getByEventId($eventId);

        return new EventSales(
            $this->computeNumberOfPeople($bookings),
            $this->computeIncome($bookings)
        );
    }

    private function currentUserCanSeeBookings(int $eventId): bool
    {
        return (
            is_user_logged_in() &&
            get_userdata(get_current_user_id())->has_cap('edit_event', $eventId)
        );
    }

    /**
     * @param Booking[] $bookings
     */
    private function computeNumberOfPeople(array $bookings): int
    {
        return array_sum(array_map(function (Booking $booking): int {
            return $booking->getNumberOfPlaces();
        }, $bookings));
    }

    /**
     * @param Booking[] $bookings
     */
    private function computeIncome(array $bookings): Currency
    {
        $totalsByOrderId = [];

        foreach ($bookings as $booking) {
            $orderId = $booking->getOrderId();

            if (array_key_exists($orderId, $totalsByOrderId)) {
                continue;
            }

            $totalsByOrderId[$orderId] = $this->getOrderTotal($orderId);
        }

        return Currency::fromFloat(array_sum($totalsByOrderId));
    }

    private function getOrderTotal(?int $orderId): float
    {
        if ($orderId === null) {
            return 0;
        }

        $order = wc_get_order($orderId);
        if (!$order) {
            return 0;
        }

        return (float) $order->get_total();
    }
}
