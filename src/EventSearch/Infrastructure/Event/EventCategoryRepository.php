<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Infrastructure\Event;

use EM_Taxonomy_Term;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventCategory;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventCategoryRepository as EventCategoryRepositoryInterface;
use MobilitySoft\TBSA\View\Medium\MediumFactory;
use WP_Term;
use const MobilitySoft\TBSA\EVENT_CATEGORY_POST_TYPE_SLUG;

class EventCategoryRepository implements EventCategoryRepositoryInterface
{
    /**
     * @var MediumFactory
     */
    private $mediumFactory;

    public function __construct(MediumFactory $mediumFactory)
    {
        $this->mediumFactory = $mediumFactory;
    }

    /**
     * @return EventCategory[]
     */
    public function getAllCategories(): array
    {
        $terms = get_terms([
            'taxonomy' => EVENT_CATEGORY_POST_TYPE_SLUG,
        ]);

        return array_map(function (WP_Term $term): EventCategory {
            $emTerm = em_get_category($term->term_id);

            return $this->createCategoryFromEmTerm($emTerm);
        }, $terms);
    }

    public function createCategoryFromEmTerm(EM_Taxonomy_Term $emCategory): EventCategory
    {
        $id = $emCategory->term_id;

        return new EventCategory(
            $id,
            get_field('short_title', "term_$id") ?? $emCategory->name,
            $this->mediumFactory->createMedium((int)$emCategory->get_image_id())
        );
    }
}
