<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Infrastructure\Branch;

use MobilitySoft\TBSA\Common\Infrastructure\BranchRepositoryHelper;
use MobilitySoft\TBSA\EventSearch\Domain\Branch\Branch;
use MobilitySoft\TBSA\EventSearch\Domain\Branch\Branches;
use MobilitySoft\TBSA\EventSearch\Domain\Branch\BranchRepository as BranchRepositoryInterface;
use const MobilitySoft\TBSA\BRANCH_POST_TYPE_SLUG;

class BranchRepository implements BranchRepositoryInterface
{
    public function getNonVirtualBranches(): Branches
    {
        $ids = get_posts([
            'post_type'      => BRANCH_POST_TYPE_SLUG,
            'posts_per_page' => -1,
            'fields'         => 'ids',
            'meta_query'     => BranchRepositoryHelper::getNonVirtualBranchesMetaQuery(),
        ]);

        return new Branches(...array_filter(array_map(function (int $id): ?Branch {
            return $this->getBranchById($id);
        }, $ids)));
    }

    private function getBranchById(int $id): ?Branch
    {
        $cityName = get_field('town', $id) ?? get_the_title($id);

        if (!$cityName) {
            return null;
        }

        return new Branch($id, $cityName);
    }
}
