<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\Utils\Pagination\PaginationState;

class FindEvents
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function execute(FindEventsRequest $request): FindEventsResponse
    {
        $query = (new CreateEventQueryFromFindEventsRequest())->execute($request);

        $events = $this->eventRepository->findEvents($query);
        $count  = $this->eventRepository->countEvents($query);

        return new FindEventsResponse(
            $events,
            new PaginationState($request->getOffset(), $request->getLimit(), $count)
        );
    }
}
