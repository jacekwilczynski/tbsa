<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

class FindEventsRequest
{
    /**
     * @var int|null
     */
    private $branch;

    /**
     * @var string|null
     */
    private $minDate;

    /**
     * @var string|null
     */
    private $maxDate;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var RequestedCategory
     */
    private $category;

    private $phrase;

    /**
     * @var string[]
     */
    private $industries;

    public function __construct(
        ?int $branchId,
        ?string $minDate,
        ?string $maxDate,
        int $offset,
        int $limit,
        RequestedCategory $category,
        ?string $phrase,
        array $industries
    ) {
        $this->branch = $branchId;
        $this->minDate = $minDate;
        $this->maxDate = $maxDate;
        $this->offset = $offset;
        $this->limit = $limit;
        $this->category = $category;
        $this->phrase = $phrase;
        $this->industries = $industries;
    }

    public function getMinDate(): ?string
    {
        return $this->minDate;
    }

    public function getMaxDate(): ?string
    {
        return $this->maxDate;
    }

    public function getBranch(): ?int
    {
        return $this->branch;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getCategory(): RequestedCategory
    {
        return $this->category;
    }

    public function getPhrase(): ?string
    {
        return $this->phrase;
    }

    /**
     * @return array
     */
    public function getIndustries(): array
    {
        return $this->industries;
    }
}
