<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\Common\Domain\Submission\SubmissionStatus;
use MobilitySoft\TBSA\View\Medium\Medium;

class Event
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var EventTime
     */
    private $time;

    /**
     * @var EventLocation|null
     */
    private $location;

    /**
     * @var EventCategory|null
     */
    private $category;

    /**
     * @var Medium|null
     */
    private $thumbnail;

    /**
     * @var string
     */
    private $url;

    /**
     * @var bool
     */
    private $external;

    /**
     * @var EventSales|null
     */
    private $sales;

    /**
     * @var bool
     */
    private $bookingsEnabled;

    /**
     * @var Industries
     */
    private $industries;

    /**
     * @var Speakers
     */
    private $speakers;

    /**
     * @var SubmissionStatus|null
     */
    private $talkSubmissionStatus;

    public function __construct(
        int $id,
        string $title,
        EventTime $time,
        ?EventLocation $location,
        ?EventCategory $category,
        ?Medium $thumbnail,
        string $url,
        bool $external,
        ?EventSales $sales,
        bool $bookingsEnabled,
        Industries $industries,
        Speakers $speakers,
        ?SubmissionStatus $talkSubmissionStatus
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->time = $time;
        $this->location = $location;
        $this->category = $category;
        $this->thumbnail = $thumbnail;
        $this->url = $url;
        $this->external = $external;
        $this->sales = $sales;
        $this->bookingsEnabled = $bookingsEnabled;
        $this->industries = $industries;
        $this->speakers = $speakers;
        $this->talkSubmissionStatus = $talkSubmissionStatus;
    }

    public function getLocation(): ?EventLocation
    {
        return $this->location;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCategory(): ?EventCategory
    {
        return $this->category;
    }

    public function getThumbnail(): ?Medium
    {
        return $this->thumbnail;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function isBookable(): bool
    {
        return (
            !$this->isExternal() &&
            $this->bookingsEnabled &&
            $this->getTime()->getStart()->getTimestamp() > time()
        );
    }

    public function isExternal(): bool
    {
        return $this->external;
    }

    public function getTime(): EventTime
    {
        return $this->time;
    }

    public function getSales(): ?EventSales
    {
        return $this->sales;
    }

    public function getIndustries(): Industries
    {
        return $this->industries;
    }

    public function getSpeakers(): Speakers
    {
        return $this->speakers;
    }

    public function getTalkSubmissionStatus(): ?SubmissionStatus
    {
        return $this->talkSubmissionStatus;
    }
}
