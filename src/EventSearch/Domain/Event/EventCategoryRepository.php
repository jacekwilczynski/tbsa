<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

interface EventCategoryRepository
{
    /**
     * @return EventCategory[]
     */
    public function getAllCategories(): array;
}
