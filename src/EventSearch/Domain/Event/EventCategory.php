<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\View\Medium\Medium;

class EventCategory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $shortName;

    /**
     * @var Medium
     */
    private $icon;

    public function __construct(int $id, string $shortName, Medium $icon)
    {
        $this->icon      = $icon;
        $this->id        = $id;
        $this->shortName = $shortName;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }

    public function getIcon(): Medium
    {
        return $this->icon;
    }
}
