<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

class Events
{
    /**
     * @var Event[]
     */
    private $items;

    public function __construct(Event...$items)
    {
        $this->items = $items;
    }

    public function getAggregateSales(): ?EventSales
    {
        $events = $this->getEventsWithSales();

        if (count($events->getItems()) === 0) {
            return null;
        }

        return array_reduce(
            $events->getItems(),
            static function (EventSales $aggregateSales, Event $event): EventSales {
                return $aggregateSales->add($event->getSales());
            },
            EventSales::NONE()
        );
    }

    public function getEventsWithSales(): Events
    {
        return new Events(...array_filter(
            $this->getItems(),
            static function (Event $event): ?EventSales {
                return $event->getSales();
            }
        ));
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
