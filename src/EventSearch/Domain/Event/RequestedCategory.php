<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\Utils\Enum;

class RequestedCategory extends Enum
{
    public static function ANY(): self
    {
        return self::getInstance('ANY');
    }

    public static function withId(int $id): self
    {
        return self::getInstance((string)$id);
    }
}
