<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\Utils\Pagination\PaginationState;

class FindEventsResponse
{
    /**
     * @var Events
     */
    private $events;

    /**
     * @var PaginationState
     */
    private $paginationState;

    public function __construct(Events $events, PaginationState $paginationState)
    {
        $this->events          = $events;
        $this->paginationState = $paginationState;
    }

    public function getEvents(): Events
    {
        return $this->events;
    }

    public function getPaginationState(): PaginationState
    {
        return $this->paginationState;
    }
}
