<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use DateTimeImmutable;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;

class CreateEventQueryFromFindEventsRequest
{
    public function execute(FindEventsRequest $request): EventQuery
    {
        $query = new EventQuery();

        if ($request->getBranch()) {
            $query->setBranches(new BranchesInQuery($request->getBranch()));
        }

        if ($request->getMinDate()) {
            $query->setStartDate(new DateTimeImmutable($request->getMinDate()));
        }

        if ($request->getMaxDate()) {
            $query->setEndDate(new DateTimeImmutable($request->getMaxDate()));
        }

        if ($request->getOffset()) {
            $query->setOffset($request->getOffset());
        }

        if ($request->getLimit()) {
            $query->setLimit($request->getLimit());
        }

        if ($request->getCategory() !== RequestedCategory::ANY()) {
            $query->setCategories([(int)(string)$request->getCategory()]);
        }

        if ($request->getPhrase()) {
            $query->setPhrase($request->getPhrase());
        }

        if ($request->getIndustries()) {
            $query->setIndustries($request->getIndustries());
        }

        return $query;
    }
}
