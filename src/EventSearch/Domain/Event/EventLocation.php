<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

class EventLocation
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $town;

    public function __construct(string $name, string $address, string $town)
    {
        $this->name    = $name;
        $this->address = $address;
        $this->town    = $town;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getTown(): string
    {
        return $this->town;
    }
}
