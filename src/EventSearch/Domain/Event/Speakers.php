<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\Common\Domain\Event\Speaker;

class Speakers
{
    /**
     * @var Speaker[]
     */
    private $items;

    public function __construct(Speaker... $items)
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
