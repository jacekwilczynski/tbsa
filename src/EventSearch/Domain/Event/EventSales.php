<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\ValueObject\Currency;

class EventSales
{
    /**
     * @var int
     */
    private $numberOfPeople;

    /**
     * @var Currency
     */
    private $income;

    public function __construct(int $numberOfPeople, Currency $income)
    {
        $this->numberOfPeople = $numberOfPeople;
        $this->income         = $income;
    }

    public static function NONE(): EventSales
    {
        return new EventSales(0, Currency::fromFloat(0));
    }

    public function add(EventSales $other): EventSales
    {
        return new EventSales(
            $this->getNumberOfPeople() + $other->getNumberOfPeople(),
            $this->getIncome()->add($other->getIncome())
        );
    }

    public function getNumberOfPeople(): int
    {
        return $this->numberOfPeople;
    }

    public function getIncome(): Currency
    {
        return $this->income;
    }
}
