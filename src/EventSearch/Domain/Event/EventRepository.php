<?php

namespace MobilitySoft\TBSA\EventSearch\Domain\Event;

use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;

interface EventRepository
{
    public function findEvents(EventQuery $request): Events;

    public function countEvents(EventQuery $query): int;
}
