<?php

namespace MobilitySoft\TBSA\EventSearch\Domain\Branch;

interface BranchRepository
{
    public function getNonVirtualBranches(): Branches;
}
