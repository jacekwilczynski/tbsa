<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Branch;

class Branch
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cityName;

    public function __construct(int $id, string $cityName)
    {
        $this->id       = $id;
        $this->cityName = $cityName;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCityName(): string
    {
        return $this->cityName;
    }
}
