<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Domain\Branch;

class Branches
{
    /**
     * @var Branch[]
     */
    private $items;

    public function __construct(Branch... $items)
    {
        $this->items = $items;
    }

    /**
     * @return Branch[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
