<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\View;

class EventSearchResults
{
    /**
     * @var EventSearchAggregateData|null
     */
    private $aggregateData;

    /**
     * @var EventSearchGroup[]
     */
    private $groups;

    public function __construct(?EventSearchAggregateData $aggregateData, EventSearchGroup... $groups)
    {
        $this->groups        = $groups;
        $this->aggregateData = $aggregateData;
    }

    public function getAggregateData(): ?EventSearchAggregateData
    {
        return $this->aggregateData;
    }

    /**
     * @return EventSearchGroup[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }
}
