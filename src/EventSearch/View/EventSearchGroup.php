<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\View;

class EventSearchGroup
{
    /**
     * @var string
     */
    private $caption;

    /**
     * @var EventSearchEvent[]
     */
    private $events;

    public function __construct(string $caption, EventSearchEvent... $events)
    {
        $this->caption = $caption;
        $this->events  = $events;
    }

    /**
     * @return string
     */
    public function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @return EventSearchEvent[]
     */
    public function getEvents(): array
    {
        return $this->events;
    }
}
