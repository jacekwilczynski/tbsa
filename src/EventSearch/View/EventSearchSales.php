<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\View;

class EventSearchSales
{
    /**
     * @var int
     */
    private $numberOfPeople;

    /**
     * @var string
     */
    private $income;

    public function __construct(int $numberOfPeople, string $income)
    {
        $this->numberOfPeople = $numberOfPeople;
        $this->income         = $income;
    }

    public function getNumberOfPeople(): int
    {
        return $this->numberOfPeople;
    }

    public function getIncome(): string
    {
        return $this->income;
    }
}
