<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\View;

class EventSearchAggregateData
{
    /**
     * @var int
     */
    private $numberOfEvents;

    /**
     * @var EventSearchSales|null
     */
    private $sales;

    public function __construct(int $numberOfEvents, ?EventSearchSales $sales)
    {
        $this->numberOfEvents = $numberOfEvents;
        $this->sales          = $sales;
    }

    public function getNumberOfEvents(): int
    {
        return $this->numberOfEvents;
    }

    public function getSales(): ?EventSearchSales
    {
        return $this->sales;
    }
}
