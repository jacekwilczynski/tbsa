<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\View;

use MobilitySoft\TBSA\Common\View\Submission\SubmissionStatusForDisplay;

class EventSearchEvent
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $start;

    /**
     * @var string
     */
    private $end;

    /**
     * @var string[]
     */
    private $location;

    /**
     * @var string[]
     */
    private $industries;

    /**
     * @var string[]
     */
    private $speakers;

    /**
     * @var string
     */
    private $thumbnail;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string|null
     */
    private $ajaxUrl;

    /**
     * @var string|null
     */
    private $buyUrl;

    /**
     * @var EventSearchSales|null
     */
    private $sales;

    /**
     * @var SubmissionStatusForDisplay|null
     */
    private $talkSubmissionStatus;

    public function __construct(
        string $title,
        string $start,
        string $end,
        array $location,
        array $industries,
        array $speakers,
        string $thumbnail,
        string $icon,
        string $url,
        ?string $ajaxUrl,
        ?string $buyUrl,
        ?EventSearchSales $sales,
        ?SubmissionStatusForDisplay $talkSubmissionStatus
    ) {
        $this->title = $title;
        $this->start = $start;
        $this->end = $end;
        $this->location = $location;
        $this->industries = $industries;
        $this->speakers = $speakers;
        $this->thumbnail = $thumbnail;
        $this->icon = $icon;
        $this->url = $url;
        $this->ajaxUrl = $ajaxUrl;
        $this->buyUrl = $buyUrl;
        $this->sales = $sales;
        $this->talkSubmissionStatus = $talkSubmissionStatus;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getStart(): string
    {
        return $this->start;
    }

    public function getEnd(): string
    {
        return $this->end;
    }

    /**
     * @return string[]
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    /**
     * @return string[]
     */
    public function getIndustries(): array
    {
        return $this->industries;
    }

    /**
     * @return string[]
     */
    public function getSpeakers(): array
    {
        return $this->speakers;
    }

    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getAjaxUrl(): ?string
    {
        return $this->ajaxUrl;
    }

    public function getBuyUrl(): ?string
    {
        return $this->buyUrl;
    }

    public function getSales(): ?EventSearchSales
    {
        return $this->sales;
    }

    public function getTalkSubmissionStatus(): ?SubmissionStatusForDisplay
    {
        return $this->talkSubmissionStatus;
    }
}
