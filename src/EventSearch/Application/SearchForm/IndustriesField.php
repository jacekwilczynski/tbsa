<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application\SearchForm;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\Select;

class IndustriesField extends Select
{
    /**
     * @var Industries
     */
    private $all;

    /**
     * @var Industries
     */
    private $selected;

    public function __construct(Industries $all, Industries $selected)
    {
        $this->all = $all;
        $this->selected = $selected;
    }

    public function getName(): string
    {
        return 'industries';
    }

    public function getLabel(): string
    {
        return __('Branże');
    }

    /**
     * @return string
     */
    public function getValue(): array
    {
        return $this->selected->getItems();
    }

    public function isMulti(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function getKvPairs(): array
    {
        $items = $this->all->getItems();
        return array_combine($items, $items);
    }

    public function getEmptyLabel(): string
    {
        return sprintf('(%s)', __('wyczyść'));
    }
}
