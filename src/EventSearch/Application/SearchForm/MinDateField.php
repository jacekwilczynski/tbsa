<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application\SearchForm;

use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\DatePicker;

class MinDateField extends DatePicker
{
    public function getName(): string
    {
        return 'min_date';
    }

    public function getLabel(): string
    {
        return __('Od');
    }

    public function isClearable(): bool
    {
        return true;
    }

    public function getCalendarClass(): string
    {
        return 'EventSearch__calendar';
    }
}
