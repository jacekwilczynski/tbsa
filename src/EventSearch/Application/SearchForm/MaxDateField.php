<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application\SearchForm;

use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\DatePicker;

class MaxDateField extends DatePicker
{
    public function getName(): string
    {
        return 'max_date';
    }

    public function getLabel(): string
    {
        return __('Do');
    }

    public function isClearable(): bool
    {
        return true;
    }

    public function getCalendarClass(): string
    {
        return 'EventSearch__calendar';
    }
}
