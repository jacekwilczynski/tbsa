<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application\SearchForm;

use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\Text;

class PhraseField extends Text
{
    public function getName(): string
    {
        return 'phrase';
    }

    public function getLabel(): string
    {
        return __('Fraza');
    }
}
