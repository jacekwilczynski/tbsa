<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application\SearchForm;

use MobilitySoft\TBSA\EventSearch\Domain\Event\EventCategory;
use MobilitySoft\TBSA\EventSearch\Domain\Event\RequestedCategory;
use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\Select;

class CategoryField extends Select
{
    /**
     * @var array
     */
    private $kvPairs;

    /**
     * @var EventCategory[]
     */
    private $categories;

    /**
     * @var RequestedCategory
     */
    private $selectedCategory;

    public function __construct(array $categories, RequestedCategory $selectedCategory)
    {
        $this->categories = $categories;
        $this->selectedCategory = $selectedCategory;
    }

    public function getLabel(): string
    {
        return __('Kategoria');
    }

    public function getKvPairs(): array
    {
        if ($this->kvPairs === null) {
            $this->kvPairs = [];

            foreach ($this->categories as $category) {
                $this->kvPairs[$category->getId()] = $category->getShortName();
            }
        }

        return $this->kvPairs;
    }

    public function getValue(): ?int
    {
        if ($this->selectedCategory === RequestedCategory::ANY()) {
            return null;
        }

        return (int) (string) $this->selectedCategory;
    }

    public function getName(): string
    {
        return 'category';
    }

    public function getPlaceholder(): string
    {
        return __('Wszystkie');
    }
}
