<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application\SearchForm;

use MobilitySoft\TBSA\Common\Domain\Industries;
use MobilitySoft\TBSA\EventSearch\Domain\Branch\BranchRepository;
use MobilitySoft\TBSA\EventSearch\Domain\Event\EventCategoryRepository;
use MobilitySoft\TBSA\EventSearch\Domain\Event\FindEventsRequest;
use MobilitySoft\TBSA\EventSearchCommon\Form\EventSearchForm;
use MobilitySoft\TBSA\EventSearchCommon\Form\EventSearchFormBuilderFactory;
use const MobilitySoft\TBSA\INDUSTRY_TAXONOMY_SLUG;

class SearchFormFactory
{
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var EventCategoryRepository
     */
    private $eventCategoryRepository;

    /**
     * @var EventSearchFormBuilderFactory
     */
    private $eventSearchFormBuilderFactory;

    public function __construct(
        BranchRepository $branchRepository,
        EventCategoryRepository $eventCategoryRepository,
        EventSearchFormBuilderFactory $eventSearchFormBuilderFactory
    ) {
        $this->branchRepository = $branchRepository;
        $this->eventCategoryRepository = $eventCategoryRepository;
        $this->eventSearchFormBuilderFactory = $eventSearchFormBuilderFactory;
    }

    public function createForm(FindEventsRequest $request): EventSearchForm
    {
        $builder = $this->eventSearchFormBuilderFactory->createBuilder();

        $builder
            ->addField(new PhraseField($request->getPhrase()))
            ->addField(new BranchField(
                $this->branchRepository->getNonVirtualBranches(),
                $request->getBranch()
            ))
            ->addField(new IndustriesField(
                new Industries(...get_term_names(INDUSTRY_TAXONOMY_SLUG)),
                new Industries(...$request->getIndustries())
            ))
            ->addField(new CategoryField(
                $this->eventCategoryRepository->getAllCategories(),
                $request->getCategory()
            ))
            ->addField(new MinDateField($request->getMinDate()))
            ->addField(new MaxDateField($request->getMaxDate()))
            ->setSubmitText(__('Szukaj'))
            ->setVisibleLabels(true);

        return $builder->buildForm();
    }
}
