<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application\SearchForm;

use MobilitySoft\TBSA\EventSearch\Domain\Branch\Branches;
use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\Select;

class BranchField extends Select
{
    /**
     * @var Branches
     */
    private $branches;

    /**
     * @var array
     */
    private $kvPairs;

    /**
     * @var int|null
     */
    private $selectedBranchId;

    public function __construct(Branches $branches, ?int $selectedBranchId = null)
    {
        $this->branches         = $branches;
        $this->selectedBranchId = $selectedBranchId;
    }

    public function getValue(): ?int
    {
        return $this->selectedBranchId;
    }

    public function getLabel(): string
    {
        return __('Miasto');
    }

    public function getPlaceholder(): string
    {
        return __('Wszystkie');
    }

    public function getKvPairs(): array
    {
        if ($this->kvPairs === null) {
            $this->kvPairs = [];

            foreach ($this->branches->getItems() as $branch) {
                $this->kvPairs[$branch->getId()] = $branch->getCityName();
            }

            asort($this->kvPairs, SORT_LOCALE_STRING);
        }

        return $this->kvPairs;
    }

    public function getName(): string
    {
        return 'branch';
    }
}
