<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application;

use MobilitySoft\TBSA\Utils\Pagination\GetPageUrl;

class GetEventSearchResultsPageUrl implements GetPageUrl
{
    private const OFFSET_QUERY_PARAM_REGEX = '/[?&]offset=\d+/';
    private const PAGE_QUERY_PARAM_REGEX = '/([?&])page=\d+/';
    private const HAS_QUERY_PARAMS_REGEX = '/\?.+/';

    public function execute(int $page): ?string
    {
        $currentUrl       = home_url($_SERVER['REQUEST_URI']);
        $urlWithoutOffset = preg_replace(self::OFFSET_QUERY_PARAM_REGEX, '', $currentUrl);

        if ($page === 1) {
            return preg_replace(self::PAGE_QUERY_PARAM_REGEX, '', $urlWithoutOffset);
        }

        $param = "page=$page";

        if (preg_match(self::PAGE_QUERY_PARAM_REGEX, $urlWithoutOffset)) {
            return preg_replace(self::PAGE_QUERY_PARAM_REGEX, "\\1$param", $urlWithoutOffset);
        }

        if (preg_match(self::HAS_QUERY_PARAMS_REGEX, $urlWithoutOffset)) {
            return $urlWithoutOffset . "&$param";
        }

        return $urlWithoutOffset . "?$param";
    }
}
