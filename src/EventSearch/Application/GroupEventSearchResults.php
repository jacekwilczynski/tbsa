<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application;

use MobilitySoft\TBSA\EventSearch\View\EventSearchEvent;
use MobilitySoft\TBSA\EventSearch\View\EventSearchGroup;

class GroupEventSearchResults
{
    /**
     * @param EventSearchEvent[] $events
     *
     * @return EventSearchGroup[]
     */
    public function execute(EventSearchEvent... $events): array
    {
        $datesToEventsMap = $this->mapDatesToEvents($events);

        return $this->getGroups($datesToEventsMap);
    }

    /**
     * @param EventSearchEvent[] $events
     *
     * @return array
     */
    private function mapDatesToEvents(array $events): array
    {
        $eventsByDate = [];

        foreach ($events as $event) {
            $date                  = $this->getDate($event->getStart());
            $eventsByDate[$date][] = $event;
        }

        return $eventsByDate;
    }

    private function getDate(string $eventTimeString): string
    {
        $pieces = explode(' ', $eventTimeString);

        return implode(' ', array_slice($pieces, 0, 2));
    }

    /**
     * @param array $eventsByDate
     *
     * @return EventSearchGroup[]
     */
    private function getGroups(array $eventsByDate): array
    {
        $groups = [];

        foreach ($eventsByDate as $date => $events) {
            $groups[] = new EventSearchGroup($date, ...$events);
        }

        return $groups;
    }
}
