<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application;

use MobilitySoft\TBSA\EventSearch\Domain\Event\Events;
use MobilitySoft\TBSA\EventSearch\View\EventSearchAggregateData;
use MobilitySoft\TBSA\EventSearch\View\EventSearchResults;

class GetEventSearchResults
{
    public function execute(Events $events): EventSearchResults
    {
        $singleItems = (new GetEventSearchResultsEvents())->execute($events);
        $groups = (new GroupEventSearchResults())->execute(...$singleItems);
        $aggregateData = $this->getAggregateData($events);

        return new EventSearchResults($aggregateData, ...$groups);
    }

    private function getAggregateData(Events $events): EventSearchAggregateData
    {
        $sales = (new GetEventSearchResultsSales())->execute($events->getAggregateSales());
        return new EventSearchAggregateData(count($events->getEventsWithSales()->getItems()), $sales);
    }
}
