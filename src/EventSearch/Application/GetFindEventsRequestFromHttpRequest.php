<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application;

use DateTimeImmutable;
use MobilitySoft\TBSA\EventSearch\Domain\Event\FindEventsRequest;
use MobilitySoft\TBSA\EventSearch\Domain\Event\RequestedCategory;

class GetFindEventsRequestFromHttpRequest
{
    /**
     * @var int
     */
    private $maxLimit;

    /**
     * @var array
     */
    private $source;

    public function __construct(int $limit, $source = null)
    {
        $this->maxLimit = $limit;
        $this->source = $source ?? $_GET;
    }

    public function execute(): FindEventsRequest
    {
        [$minDate, $maxDate] = $this->correctDates(
            $this->getDate('min'),
            $this->getDate('max')
        );

        return new FindEventsRequest(
            $this->getSelectedBranchId(),
            $minDate,
            $maxDate,
            $this->getOffset(),
            $this->getLimit(),
            $this->getCategory(),
            $this->getPhrase(),
            $this->getIndustries()
        );
    }

    /**
     * @param string|null $minDate
     * @param string|null $maxDate
     *
     * @return string[]
     */
    private function correctDates(?string $minDate, ?string $maxDate): array
    {
        $today = (new DateTimeImmutable())->format('Y-m-d');

        if (!$this->wasPageLoadedViaFormSubmission()) {
            $minDate = $today;
        }

        if ($minDate && $maxDate && $minDate > $maxDate) {
            $minDate = null;
            $maxDate = null;
        }

        return [$minDate, $maxDate];
    }

    private function wasPageLoadedViaFormSubmission(): bool
    {
        return (bool) preg_match('/[?&]min_date=/', $_SERVER['REQUEST_URI']);
    }

    private function getDate(string $which): ?string
    {
        $date = $this->source["{$which}_date"] ?? '';

        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $date)) {
            return $date;
        }

        return null;
    }

    private function getSelectedBranchId(): ?int
    {
        $branchId = $this->source['branch'] ?? null;

        return is_numeric($branchId) ? (int) $branchId : null;
    }

    private function getOffset(): int
    {
        if (isset($this->source['offset'])) {
            return (int) $this->source['offset'];
        }

        $page = (int) ($this->source['page'] ?? 1);

        return ($page - 1) * $this->getLimit();
    }

    private function getLimit(): int
    {
        $requestedLimit = $this->source['limit'] ?? null;

        if (!$requestedLimit || (int) $requestedLimit > $this->maxLimit) {
            return $this->maxLimit;
        }

        return (int) $requestedLimit;
    }

    private function getCategory(): RequestedCategory
    {
        if (empty($this->source['category'])) {
            return RequestedCategory::ANY();
        }

        return RequestedCategory::withId((int) $this->source['category']);
    }

    private function getPhrase(): ?string
    {
        if (empty($this->source['phrase'])) {
            return null;
        }

        return htmlspecialchars(stripslashes($this->source['phrase']));
    }

    /**
     * @return string[]
     */
    private function getIndustries(): array
    {
        if (
            empty($this->source['industries']) ||
            !is_array($this->source['industries'])
        ) {
            return [];
        }

        return array_map(function (string $industry): string {
            return htmlspecialchars($industry);
        }, $this->source['industries']);
    }
}
