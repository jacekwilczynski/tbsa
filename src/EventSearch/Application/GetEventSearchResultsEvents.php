<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application;

use DateTimeInterface;
use MobilitySoft\TBSA\Common\Domain\Event\Speaker;
use MobilitySoft\TBSA\Common\View\Submission\SubmissionStatusForDisplay;
use MobilitySoft\TBSA\EventSearch\Domain\Event\Event;
use MobilitySoft\TBSA\EventSearch\Domain\Event\Events;
use MobilitySoft\TBSA\EventSearch\View\EventSearchEvent;
use MobilitySoft\TBSA\Service\TranslateMonth;

class GetEventSearchResultsEvents
{
    /**
     * @var GetEventSearchResultsSales
     */
    private $getEventSearchResultsSales;

    public function __construct()
    {
        $this->getEventSearchResultsSales = new GetEventSearchResultsSales();
    }

    /**
     * @return EventSearchEvent[]
     */
    public function execute(Events $events): array
    {
        return array_map(function (Event $event) {
            return new EventSearchEvent(
                $event->getTitle(),
                $this->formatDateTime($event->getTime()->getStart()),
                $this->formatDateTime($event->getTime()->getEnd()),
                $this->getLocation($event),
                $event->getIndustries()->getItems(),
                $this->getSpeakers($event),
                $this->getThumbnail($event),
                $this->getIcon($event),
                $event->getUrl(),
                $this->getAjaxUrl($event),
                $this->getBuyUrl($event),
                $this->getEventSearchResultsSales->execute($event->getSales()),
                $this->getSubmissionStatus($event)
            );
        }, $events->getItems());
    }

    private function formatDateTime(DateTimeInterface $dateTime): string
    {
        return TranslateMonth::execute($dateTime->format('j F Y, H:i'));
    }

    private function getLocation(Event $event): array
    {
        $location = $event->getLocation();

        return array_filter([
            $location->getName(),
            $location->getAddress(),
            $location->getTown(),
        ]);
    }

    private function getThumbnail(Event $event): string
    {
        $thumbnail = $event->getThumbnail();
        return $thumbnail ? $thumbnail->getMarkup('large') : '';
    }

    private function getIcon(Event $event): string
    {
        $category = $event->getCategory();

        if ($category) {
            $icon = $category->getIcon();

            if ($icon) {
                return $icon->getMarkup();
            }
        }

        return '';
    }

    private function getAjaxUrl(Event $event): ?string
    {
        if ($event->isExternal()) {
            return null;
        }

        return home_url('/wp-json/tbsa/v1/events?render_for=details&id=' . $event->getId());
    }

    private function getBuyUrl(Event $event): ?string
    {
        if ($event->isBookable()) {
            return home_url('/wp-json/tbsa/v1/booking-form?event_id=' . $event->getId());
        }

        return null;
    }

    /**
     * @return string[]
     */
    private function getSpeakers(Event $event): array
    {
        return array_map(function (Speaker $speaker): string {
            return $speaker->getName();
        }, $event->getSpeakers()->getItems());
    }

    private function getSubmissionStatus(Event $event): ?SubmissionStatusForDisplay
    {
        $status = $event->getTalkSubmissionStatus();
        if ($status) {
            return new SubmissionStatusForDisplay($status);
        }
        return null;
    }
}
