<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application;

use MobilitySoft\TBSA\Common\View\DirectionalPagination;
use MobilitySoft\TBSA\EventSearch\Application\SearchForm\SearchFormFactory;
use MobilitySoft\TBSA\EventSearch\Domain\Event\FindEvents;
use MobilitySoft\TBSA\EventSearch\Domain\Event\FindEventsResponse;
use Twig\Environment as Twig;

class EventSearchRenderer
{
    /**
     * @var FindEvents
     */
    private $findEvents;

    /**
     * @var SearchFormFactory
     */
    private $searchFormFactory;

    /**
     * @var Twig
     */
    private $twig;

    public function __construct(FindEvents $findEvents, SearchFormFactory $searchFormFactory, Twig $twig)
    {
        $this->findEvents        = $findEvents;
        $this->searchFormFactory = $searchFormFactory;
        $this->twig              = $twig;
    }

    public function render(): string
    {
        $request  = (new GetFindEventsRequestFromHttpRequest($this->getMaxLimit()))->execute();
        $form     = $this->searchFormFactory->createForm($request);
        $response = $this->findEvents->execute($request);
        $results  = (new GetEventSearchResults())->execute($response->getEvents());

        return $this->twig->render('eventSearch/page.twig', [
            'form'       => $form->render(),
            'results'    => $results,
            'pagination' => $this->getPagination($response),
            'message'    => $results->getGroups() ? null : __('Nie znaleziono wydarzeń spełniających wybrane kryteria.'),
        ]);
    }

    private function getMaxLimit(): int
    {
        if (current_user_can('administrator')) {
            return 1000;
        }

        if (current_user_can('board_member')) {
            return 100;
        }

        return 20;
    }

    private function getPagination(FindEventsResponse $response): ?DirectionalPagination
    {
        $pagination = new DirectionalPagination(
            new GetEventSearchResultsPageUrl(),
            $response->getPaginationState()
        );

        return $pagination->getNumberOfPages() > 1 ? $pagination : null;
    }
}
