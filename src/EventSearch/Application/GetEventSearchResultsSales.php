<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearch\Application;

use MobilitySoft\TBSA\EventSearch\Domain\Event\EventSales;
use MobilitySoft\TBSA\EventSearch\View\EventSearchSales;

class GetEventSearchResultsSales
{
    public function execute(?EventSales $sales): ?EventSearchSales
    {
        if (!$sales) {
            return null;
        }

        return new EventSearchSales(
            $sales->getNumberOfPeople(),
            wc_price($sales->getIncome()->getFloat())
        );
    }
}
