<?php

namespace MobilitySoft\TBSA\EventSearch;

use MobilitySoft\TBSA\EventSearch\Application\EventSearchRenderer;
use Psr\Container\ContainerInterface;

class EventSearchBundle
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function enable(): void
    {
        add_action('get_header', function () {
            if (is_page(__('wydarzenia'))) {
                // Required only for some global CSS
                acf_form_head();

                add_action('wp_enqueue_scripts', function () {
                    wp_enqueue_style(
                        'event-search',
                        get_theme_file_uri('/build/eventSearch-style.css'),
                        null,
                        defined('TBSA_DEV_MODE') && TBSA_DEV_MODE ? time() : '1.0'
                    );
                });
            }
        });

        add_shortcode('tbsa_event_search', function (): string {
            return $this->container->get(EventSearchRenderer::class)->render();
        });
    }
}
