<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearchCommon\Form;

use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\Field;
use Twig\Environment as Twig;

class EventSearchFormBuilder
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var Field[]
     */
    private $fields;

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $submitText;

    /**
     * @var bool
     */
    private $visibleLabels = false;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function setSubmitText(string $submitText): self
    {
        $this->submitText = $submitText;

        return $this;
    }

    public function addField(Field $field): self
    {
        $this->fields[] = $field;

        return $this;
    }

    public function buildForm(): EventSearchForm
    {
        return new EventSearchForm($this->twig, $this->fields, $this->submitText, $this->action, $this->visibleLabels);
    }

    public function setVisibleLabels(bool $visibleLabels): self
    {
        $this->visibleLabels = $visibleLabels;

        return $this;
    }
}
