<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearchCommon\Form;

use Twig\Environment as Twig;

class EventSearchFormBuilderFactory
{
    /**
     * @var Twig
     */
    private $twig;

    public function __construct(Twig $twig)
    {
        $this->twig = $twig;
    }

    public function createBuilder(): EventSearchFormBuilder
    {
        return new EventSearchFormBuilder($this->twig);
    }
}
