<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearchCommon\Form;

use MobilitySoft\TBSA\Common\View\Renderer;
use MobilitySoft\TBSA\EventSearchCommon\AbstractFields\Field;
use Twig\Environment as Twig;

class EventSearchForm implements Renderer
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var Field[]
     */
    private $fields;

    /**
     * @var string|null
     */
    private $submitText;

    /**
     * @var string|null
     */
    private $action;

    /**
     * @var bool
     */
    private $visibleLabels;

    /**
     * EventSearchForm constructor.
     *
     * @param Twig $twig
     * @param Field[] $fields
     * @param string|null $submitText
     * @param string|null $action
     * @param bool|null $visibleLabels
     */
    public function __construct(
        Twig $twig,
        array $fields,
        ?string $submitText = null,
        ?string $action = null,
        ?bool $visibleLabels = false
    ) {
        $this->twig          = $twig;
        $this->fields        = array_map(function (Field $field) {
            return $field;
        }, $fields);
        $this->submitText    = $submitText;
        $this->action        = $action;
        $this->visibleLabels = $visibleLabels;
    }

    public function render(): string
    {
        return $this->twig->render('eventSearchForm.twig', [
            'form'          => [
                'action'     => $this->action,
                'submitText' => $this->submitText,
                'fields'     => $this->fields,
            ],
            'visibleLabels' => $this->visibleLabels,
        ]);
    }
}
