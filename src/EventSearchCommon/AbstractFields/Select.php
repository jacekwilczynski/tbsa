<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearchCommon\AbstractFields;

abstract class Select implements Field
{
    abstract public function getKvPairs(): array;

    public function getType(): string
    {
        return 'select';
    }

    public function isMulti(): bool
    {
        return false;
    }
}
