<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearchCommon\AbstractFields;

interface Field
{
    public function getType(): string;

    public function getName(): string;

    public function getLabel(): string;

    public function getValue();
}
