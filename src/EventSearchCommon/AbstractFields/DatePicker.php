<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearchCommon\AbstractFields;

abstract class DatePicker implements Field
{
    /**
     * @var string|null
     */
    private $value;

    public function __construct(?string $value = null)
    {
        $this->value = $value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getType(): string
    {
        return 'date';
    }
}
