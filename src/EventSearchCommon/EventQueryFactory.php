<?php

declare(strict_types=1);

namespace MobilitySoft\TBSA\EventSearchCommon;

use DateTimeImmutable;
use MobilitySoft\TBSA\Common\Domain\Event\EventQuery;
use MobilitySoft\TBSA\Common\Domain\BranchesInQuery;

class EventQueryFactory
{
    public function createEventQuery(array $params): EventQuery
    {
        $query = new EventQuery();

        if (isset($params['branches'])) {
            if ($params['branches'] === 'any') {
                $query->setBranches(BranchesInQuery::ANY());
            } else {
                $branchIds = array_map(function ($branchId) {
                    return (int)$branchId;
                }, $params['branches']);
                $query->setBranches(new BranchesInQuery(...$branchIds));
            }
        }

        if (isset($params['date'])) {
            $query->setDate($this->createDate($params['date']));
        } else {
            if (isset($params['start_date'])) {
                $query->setStartDate($this->createDate($params['start_date']));
            }
            if (isset($params['end_date'])) {
                $query->setEndDate($this->createDate($params['end_date']));
            }
        }

        return $query;
    }

    private function createDate(string $input): DateTimeImmutable
    {
        return new DateTimeImmutable(sanitize_title($input));
    }
}
