<?php

namespace MobilitySoft\TBSA;

class AdminAssets
{
    public function enqueue(): void
    {
        wp_enqueue_script(
            'admin-script',
            get_theme_file_uri('/build/admin-script.js'),
            null,
            defined('TBSA_DEV_MODE') && TBSA_DEV_MODE ? time() : '1.1',
            true
        );

        wp_localize_script(
            'admin-script',
            'tbsa_general',
            ['homeUrl' => home_url()]
        );

        wp_enqueue_style(
            'admin-style',
            get_theme_file_uri('/build/admin-style.css'),
            null,
            defined('TBSA_DEV_MODE') && TBSA_DEV_MODE ? time() : '1.0'
        );
    }
}
