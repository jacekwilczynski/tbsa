<?php get_header() ?>
    <div class="my-account-page">
        <div class="my-account-page__container">
            <div class="my-account-panel">
                <?php the_content() ?>
            </div>
        </div>
    </div>
<?php get_footer();
