<?php

define('TBSA_PROJECT_ROOT', __DIR__);

function array_find(array $array, callable $cb, ?bool $returnKey = false)
{
    foreach ($array as $key => $value) {
        if ($cb($value)) {
            return $returnKey ? $key : $value;
        }
    }

    return null;
}

function get_term_names(string $taxonomy, $post = null): array
{
    if (isset($post)) {
        if ($post === true) {
            $post = null;
        }
        $terms = get_the_terms($post, $taxonomy);
    } else {
        $terms = get_terms(['taxonomy' => $taxonomy]);
    }

    if (is_array($terms)) {
        $termNames = array_map(function (WP_Term $term) {
            return $term->name;
        }, $terms);
        usort($termNames, 'strcoll');
        return $termNames;
    }

    return [];
}

function unique_sorted_by_quantity(array $array, ?callable $get_value = null): array
{
    $callbackMode = is_callable($get_value);
    $itemsMap = [];
    if ($callbackMode) {
        foreach ($array as $item) {
            $key = $get_value($item);
            $itemsMap[$key] = $item;
        }
    }

    $quantityMap = [];
    foreach ($array as $item) {
        $key = $callbackMode ? $get_value($item) : $item;
        if (isset($quantityMap[$key])) {
            $quantityMap[$key]++;
        } else {
            $quantityMap[$key] = 1;
        }
    }
    arsort($quantityMap);

    $sorted = [];
    foreach ($quantityMap as $key => $value) {
        $sorted[] = $callbackMode ? $itemsMap[$key] : $key;
    }

    return $sorted;
}

function array_split(array $array, callable $test): array
{
    $truthy = [];
    $falsy = [];

    foreach ($array as $item) {
        if ($test($item)) {
            $truthy[] = $item;
        } else {
            $falsy[] = $item;
        }
    }

    return [$truthy, $falsy];
}

function array_insert(array $array, array $values, int $position): array
{
    if ($position > 1) {
        $before = array_slice($array, 0, $position - 1);
        $after = array_slice($array, $position - 1);
    } elseif ($position === 1) {
        $before = [];
        $after = $array;
    } elseif ($position === -1) {
        $before = $array;
        $after = [];
    } elseif ($position < -1) {
        $before = array_slice($array, 0, $position + 1);
        $after = array_slice($array, $position + 1);
    } else {
        throw new InvalidArgumentException('Position must be non-zero.');
    }

    return array_merge($before, $values, $after);
}

function plain_text(string $html): string
{
    return str_replace("\r", '', strip_tags(html_entity_decode($html)));
}

function render_xml_attributes(array $map): string
{
    return implode(' ', array_map(function ($key, $value): string {
        return sprintf('%s="%s"', $key, $value);
    }, array_keys($map), $map));
}

function get_field_safe($field, $source, $typePrefix = null)
{
    if (is_array($source)) {
        return $source[$field] ?? null;
    }

    if (is_numeric($source) && $typePrefix) {
        $source = "{$typePrefix}_$source";
    }

    return get_field($field, $source);
}

function get_array_field($field, $source, $typePrefix = null)
{
    $value = get_field_safe($field, $source, $typePrefix);
    return is_array($value) ? $value : [];
}

function fallback(...$values)
{
    foreach ($values as $value) {
        if ($value) {
            return $value;
        }
    }

    return null;
}

function is_url(string $url, ?string $currentUrl = null): bool
{
    $currentUrl = $currentUrl ?? home_url($_SERVER['REQUEST_URI']);
    $currentUrl = preg_replace('/\?.*/', '', $currentUrl);
    if ($currentUrl[strlen($currentUrl) - 1] !== '/') {
        $currentUrl .= '/';
    }

    return $currentUrl === $url;
}

require_once __DIR__ . '/src/index.php';
